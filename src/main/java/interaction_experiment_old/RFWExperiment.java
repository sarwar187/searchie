package interaction_experiment_old;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.Oracle;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.WeightUtils;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.State;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;


public class RFWExperiment{
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));


        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int loop = StaticVariables.numRelevanceFeedbacks; // loop is the number of relevance feedback we take
        double evalRFW[][] = new double[loop][StaticVariables.label.length];
        double evalfb[][] = new double[loop][StaticVariables.fblabel.length];
        State state = new State(); //CREATING STATE
        Oracle oracle = new Oracle(defaultCRFSuiteBinary);
        oracle.createOracle(featureDirectory, featureFileName, etype);
        TObjectFloatHashMap<String> optimalMap = oracle.getOptimalMap();
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        printResults.addJSON("pos", String.valueOf(fullPositives.size()));
        printResults.addJSON("neg", String.valueOf(fullPositives.size()));
        System.out.println(fullPositives.size() + "\t" + fullNegatives.size() + "\t" + inputFileName + "\t");
        int numExperiments = 1; //fullPositives.size() - 3;
        //numExperiements = fullPositives.size()/numPositives;  // number of rounds we calculate AP, uAP and F1
        //System.out.println("number of experiments " + numExperiements);
        //System.out.println(numExperiments + "\t");
        List<Pair<Boolean, Double>> rankedPred = new ArrayList<>();

        for (int index = 0; index < numExperiments * numPositives ; index += numPositives) { //iterating over 300 positives, taking three in each round
            //System.out.println("round " + index);
            List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
            for (int i = 0; i < numPositives; i++)
                positives.add(fullPositives.get(index + i));

            List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
            fullConllTest.addAll(fullNegatives); // at first we add all the negatives in the testing data

            for (int i = 0; i < fullPositives.size(); i++) {  // now we take all the positives except the three we had chosen for training
                if (i < index || i >= index + numPositives)
                    fullConllTest.add(fullPositives.get(i));
            }
            //training and test data already prepared!!
            //System.out.println("full positive size\t" + positives.size() + " fullConllTest size\t" + fullConllTest.size() + "fullConllTrain size\t" + fullConllTrain.size() );
            if (fullConllTrain.size() != (positives.size() + fullConllTest.size())) // Just checking if the training and test data sum up to the full training data
                System.out.println("error happened");
            try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                learner.setModel(argp.get("model", "lbfgs"));
                Parameters info = Parameters.create();
                TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                //System.out.println(positives.get(0).get(0).getLabel());
                int count = 0;
                state.setIsOptimal(0);
                for (int i = 0; i < loop; i++) {
                    WeightUtils.FixWeightInteractive fixWeightInteractive = new WeightUtils.FSBPreviousWeightRemoved();
                    TObjectFloatHashMap<String> modifiedWeights = fixWeightInteractive.fixWeight(optimalMap, originalMap, count, oracle, state);
                    LinearTokenClassifier modifiedClassifier = new LinearTokenClassifier(modifiedWeights);
                    //final Map<String, Double> measures = Metrics.evaluateModel(modifiedClassifier, fullConllTest, etype); //The evaluate model function has been changed
                    final Map<String, Double> measures = Metrics.evaluateModel(modifiedClassifier, fullConllTest, positives, etype); //The evaluate model function has been changed

                    for (int j = 0; j < StaticVariables.label.length; j++) {
                        evalRFW[i][j] += measures.get(StaticVariables.label[j]);
                    }
                    count++;// 2;
                    int val1 = measures.get("truth").intValue();
                    if (val1 == 0)
                        rankedPred.add(Pair.of(false, measures.get("score")));
                    else
                        rankedPred.add(Pair.of(true, measures.get("score")));

                    /*final Map<String, Double> m = Metrics.evaluateRankedList(rankedPred); //The evaluate model function has been changed
                    for(int j=0; j<StaticVariables.fblabel.length; j++){
                        evalfb[i][j]+=m.get(StaticVariables.fblabel[j]);
                    }*/
                }
            }
        }
        printResults.printTwoD(0, evalfb, numExperiments, loop);
        //PrintResults.writeJSON(obj, resultDirectory + featureFileName);
    }


}
