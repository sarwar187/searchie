package interaction_experiment_old;

import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.Oracle;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.State;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.WeightUtils;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;




public class AllRFWUExperimentOld {
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));



        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int loop = StaticVariables.numRelevanceFeedbacks; // loop is the number of relevance feedback we take
        Oracle oracle = new Oracle(defaultCRFSuiteBinary);
        oracle.createOracle(featureDirectory, featureFileName, etype);
        TObjectFloatHashMap<String> optimalMap = oracle.getOptimalMap();
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        printResults.addJSON("pos", String.valueOf(fullPositives.size()));
        printResults.addJSON("neg", String.valueOf(fullPositives.size()));
        if(fullPositives.size() <=StaticVariables.minPositivestoTrain)
            return;
        System.out.println(fullPositives.size() + "\t" + fullNegatives.size() + "\t" + inputFileName + "\t");
        int numExperiments = fullPositives.size()/numPositives;  // number of rounds we calculate AP, uAP and F1
        numExperiments = (numExperiments > maximumNumberOfExperiments)?maximumNumberOfExperiments: numExperiments;
        ArrayList<Pair> configuration = new ArrayList<>();
        // pair of optimal and error (1,0) means that we are considering optimal weights and errorActivated
        // we need to send two parameters to state object. One thorugh isOptimal parameter and the other through
        // setError parameter
        configuration.add(Pair.of(1, 0)); //best
        configuration.add(Pair.of(0, 0)); //best
        configuration.add(Pair.of(0, 1)); //better
        configuration.add(Pair.of(0, 1)); //worse

        for(int numConfig=0; numConfig<configuration.size(); numConfig++) {
            double evalfb[][] = new double[loop][StaticVariables.fblabel.length];
            double AP = 0d; double fullAP = 0d;  //added
            int successfulExperiments = 0;

            for (int index = 0; index < numExperiments * numPositives; index += numPositives) { //iterating over 300 positives, taking three in each round
                //System.out.println("round " + index);
                State state = new State(); //CREATING STATE
                state.setIsOptimal((int) configuration.get(numConfig).left);
                StaticVariables.errorActivated = ((int) configuration.get(numConfig).right);
                boolean resultsFound = true;
                List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
                List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
                ArrayList<SearchResult> resultsSeen = new ArrayList<>();
                DataSelector.splitTrainTest(fullPositives, fullNegatives, positives, fullConllTest, index, numPositives);
                //training and test data already prepared!!
                //System.out.println("full positive size\t" + positives.size() + " fullConllTest size\t" + fullConllTest.size() + "fullConllTrain size\t" + fullConllTrain.size() );
                if (fullConllTrain.size() != (positives.size() + fullConllTest.size())) // Just checking if the training and test data sum up to the full training data
                    System.out.println("error happened");

                try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                    CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                    learner.setModel(argp.get("model", "lbfgs"));
                    Parameters info = Parameters.create();
                    TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                    double evallocal[][] = new double[loop][StaticVariables.fblabel.length];
                    Map<String, TokenCount> retrievedTokenMap = new HashMap<>();

                    //printResults.printSimpleTokenList(positives);
                    for (int i = 0; i < loop; i++) {
                        WeightUtils.FSBPreviousWeightRemoved fixWeightInteractive = new WeightUtils.FSBPreviousWeightRemoved();
                        if(StaticVariables.errorActivated==1)
                            state.setNumberofWeightErrors(i);
                        TObjectFloatHashMap<String> modifiedWeights = fixWeightInteractive.fixWeight(optimalMap, originalMap, i , oracle, state);
                        LinearTokenClassifier modifiedClassifier = new LinearTokenClassifier(modifiedWeights);
                        SearchResult searchResult = Metrics.addRelevanceFeedback(modifiedClassifier, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                        if (searchResult == null) {
                            resultsFound = false;
                            System.out.println("broken");
                            break;
                        }
                        final Map<String, Double> m = Metrics.evaluateRankedList(resultsSeen, 0); //The evaluate model function has been changed
                        for (int j = 0; j < StaticVariables.fblabel.length; j++) {
                            evallocal[i][j] = m.get(StaticVariables.fblabel[j]);
                        }
                        if(i==loop-1) fullAP+= Metrics.computeFullAP(modifiedClassifier, fullConllTest,etype, resultsSeen);
                    }
                    AP+= Metrics.evaluateAveragePrecision(resultsSeen, StaticVariables.rankedListSize); //using last index to save AP

                    if (resultsFound) {
                        //printResults.printSearchResultList(resultsSeen);
                        successfulExperiments++;
                        for (int i = 0; i < loop; i++) {
                            for (int j = 0; j < StaticVariables.fblabel.length; j++) {
                                evalfb[i][j] += evallocal[i][j];
                            }
                        }
                    }
                }
            }
            StaticVariables.labelError+=2;
            System.out.println("fAP\t" + new DecimalFormat("#0.000").format(fullAP/successfulExperiments));
            System.out.println("AP\t" + new DecimalFormat("#0.000").format(AP/successfulExperiments));
            printResults.printTwoD(0, evalfb, successfulExperiments, loop);

        }
    }
}
