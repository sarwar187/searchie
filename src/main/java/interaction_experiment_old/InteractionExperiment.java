package interaction_experiment_old;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.Oracle;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.State;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.WeightUtils;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.lemurproject.galago.utility.Parameters;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class InteractionExperiment{

    static String prefix = StaticVariables.prefix;
    public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";

    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));

        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }

        int loop = StaticVariables.numRelevanceFeedbacks; // loop is the number of relevance feedback we take
        State state = new State(); //CREATING STATE
        Oracle oracle = new Oracle(defaultCRFSuiteBinary);
        oracle.createOracle(featureDirectory, featureFileName, etype);
        TObjectFloatHashMap<String> optimalMap = oracle.getOptimalMap();
        int numExperiements = 1;  // number of rounds we calculate AP, uAP and F1
        //numExperiements = fullPositives.size()/numPositives;  // number of rounds we calculate AP, uAP and F1

        // we iterate over loop taking one positive/negative on the topmost position in the ranking
        double eval[][] = new double[loop][StaticVariables.label.length];
        double evalRFW[][] = new double[loop][StaticVariables.label.length];
        double evalRFWOptimal[][] = new double[loop][StaticVariables.label.length];

        System.out.println(fullPositives.size() + "\t" + fullNegatives.size() + "\t" + inputFileName + "\t");
        DecimalFormat df = new DecimalFormat("#0.00");
        JSONObject obj = new JSONObject();
        obj.put("pos", fullPositives.size());
        obj.put("neg", fullNegatives.size());
        //obj.put("file", featureFileName);


        for (int index = 0; index < numExperiements * numPositives ; index += numPositives) { //iterating over 300 positives, taking three in each round
            List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
            List<List<SimpleToken>> permanentPositives = new ArrayList<>(); //positives is the training data
            //we take three initial samples
            for(int i=0; i < numPositives; i++)
                positives.add(fullPositives.get(index + i));

            for(int i=0; i < numPositives; i++)
                permanentPositives.add(fullPositives.get(index + i));

            List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
            fullConllTest.addAll(fullNegatives); // at first we add all the negatives in the testing data
            for(int i = 0; i<fullPositives.size(); i++){  // now we take all the positives except the three we had chosen for training
                if(i<index || i>= index+numPositives)
                        fullConllTest.add(fullPositives.get(i));
            }
            //training and test data already prepared!!
            //System.out.println("full positive size\t" + positives.size() + " fullConllTest size\t" + fullConllTest.size() + "fullConllTrain size\t" + fullConllTrain.size() );
            try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                List<Pair<Boolean, Double>> rankedPred  = new ArrayList<>();
                CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                learner.setModel(argp.get("model", "lbfgs"));
                Parameters info = Parameters.create();
                TObjectFloatHashMap<String> permanentOriginalMap = learner.learnFeatureWeights(permanentPositives, etype, info);
                int count = 0;
                for (int i = 0; i < loop; i++) {
                    learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                    learner.setModel(argp.get("model", "lbfgs"));
                    info = Parameters.create();
                    TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                    //train model with existing data
                    //code for RFW
                    state.setIsOptimal(0);
                    WeightUtils.FixWeightInteractive fixWeightInteractive = new WeightUtils.FSBinInteractive();
                    TObjectFloatHashMap<String> modifiedWeights = fixWeightInteractive.fixWeight(optimalMap, originalMap, count, oracle, state);
                    LinearTokenClassifier modifiedClassifier = new LinearTokenClassifier(modifiedWeights);
                    final Map<String, Double> measuresRFW = Metrics.evaluateModel(modifiedClassifier, fullConllTest, etype); //The evaluate model function has been changed
                    for(int j=0; j<StaticVariables.label.length; j++){
                        evalRFW[i][j]+= (measuresRFW.get(StaticVariables.label[j]).isNaN() == true ? 0: measuresRFW.get(StaticVariables.label[j]));
                    }

                    state.setIsOptimal(1);
                    fixWeightInteractive = new WeightUtils.FSBinInteractive();
                    modifiedWeights = fixWeightInteractive.fixWeight(optimalMap, originalMap, count, oracle, state);
                    modifiedClassifier = new LinearTokenClassifier(modifiedWeights);
                    final Map<String, Double> measuresRFWOptimal = Metrics.evaluateModel(modifiedClassifier, fullConllTest, etype); //The evaluate model function has been changed
                    for(int j=0; j<StaticVariables.label.length; j++){
                        evalRFWOptimal[i][j]+= (measuresRFWOptimal.get(StaticVariables.label[j]).isNaN() == true ? 0: measuresRFWOptimal.get(StaticVariables.label[j]));
                    }

                    //code for RF
                    LinearTokenClassifier tokenClassifier = new LinearTokenClassifier(originalMap);
                    final Map<String, Double> measures = Metrics.evaluateModel(tokenClassifier, fullConllTest, positives, etype); //The evaluate model function has been changed
                    for(int j=0; j<StaticVariables.label.length; j++){
                        eval[i][j]+= (measures.get(StaticVariables.label[j]).isNaN() == true ? 0: measures.get(StaticVariables.label[j]));
                    }

                    if(fullConllTrain.size() != (positives.size() + fullConllTest.size())) // Just checking if the training and test data sum up to the full training data
                        System.out.println("error happened");
                    count++;    // 2;

                }
            }
        }

        /*for(int j=0; j < StaticVariables.label.length; j++) {
            JSONArray list = new JSONArray();
            System.out.print(StaticVariables.label[j] + "\t");
            for (int i = 0; i < loop; i++) {
                if(j>7) System.out.print((int) (evalRFW[i][j] / numExperiements) + "\t");
                else System.out.print(df.format(evalRFW[i][j] / numExperiements) + "\t");
                list.add(df.format(evalRFW[i][j] / numExperiements));
            }
            obj.put(StaticVariables.label[j], list);
            System.out.println();
        }

        for(int j=0; j < StaticVariables.label.length; j++) {
            JSONArray list = new JSONArray();
            System.out.print(StaticVariables.label[j] + "\t");
            for (int i = 0; i < loop; i++) {
                if(j>7) System.out.print((int) (evalRFWOptimal[i][j] / numExperiements) + "\t");
                else System.out.print(df.format(evalRFWOptimal[i][j] / numExperiements) + "\t");
                list.add(df.format(evalRFWOptimal[i][j] / numExperiements));
            }
            obj.put(StaticVariables.label[j], list);
            System.out.println();
        }

        for(int j=0; j < StaticVariables.label.length; j++) {
            JSONArray list = new JSONArray();
            System.out.print(StaticVariables.label[j] + "\t");
            for (int i = 0; i < loop; i++) {
                if(j>7) System.out.print((int) (eval[i][j] / numExperiements) + "\t");
                else System.out.print(df.format(eval[i][j] / numExperiements) + "\t");
                list.add(df.format(eval[i][j] / numExperiements));
            }
            obj.put(StaticVariables.label[j], list);
            System.out.println();
        }*/



    }
}