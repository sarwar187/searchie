package interaction_experiment_old;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class RFExperiment{

    static String prefix = StaticVariables.prefix;
    public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";

    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        String resultDirectory = argp.get("rdir", "result_directory");


        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int loop = 21; // loop is the number of relevance feedback we take
        // we iterate over loop taking one positive/negative on the topmost position in the ranking
        double eval[][] = new double[loop][StaticVariables.label.length];
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        printResults.addJSON("pos", String.valueOf(fullPositives.size()));
        printResults.addJSON("neg", String.valueOf(fullPositives.size()));
        System.out.println(fullPositives.size() + "\t" + fullNegatives.size() + "\t" + inputFileName + "\t");
        int numExperiments = 1;  // number of rounds we calculate AP, uAP and F1
        //numExperiements = fullPositives.size()/3;  // number of rounds we calculate AP, uAP and F1

        for (int index = 0; index < numExperiments * numPositives ; index += numPositives) { //iterating over 300 positives, taking three in each round
            ArrayList<SearchResult> resultsSeen = new ArrayList<>();
            List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
            //we take three initial samples
            for(int i=0; i < numPositives; i++)
                positives.add(fullPositives.get(index + i));

            List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
            fullConllTest.addAll(fullNegatives); // at first we add all the negatives in the testing data
            for(int i = 0; i<fullPositives.size(); i++){  // now we take all the positives except the three we had chosen for training
                if(i<index || i>=index+numPositives)
                    fullConllTest.add(fullPositives.get(i));
            }
            //training and test data already prepared!!
            //System.out.println("full positive size\t" + positives.size() + " fullConllTest size\t" + fullConllTest.size() + "fullConllTrain size\t" + fullConllTrain.size() );
            if(fullConllTrain.size() != (positives.size() + fullConllTest.size())) // Just checking if the training and test data sum up to the full training data
                System.out.println("error happened");
            try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                for (int i = 0; i < loop; i++) {
                    CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                    learner.setModel(argp.get("model", "lbfgs"));
                    Parameters info = Parameters.create();
                    //train model with existing data
                    TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                    //please use the following lines together for unique results
                    LinearTokenClassifier tokenClassifier = new LinearTokenClassifier(originalMap);
                    final Map<String, Double> measures = Metrics.evaluateModel(tokenClassifier, fullConllTest, positives, etype); //The evaluate model function has been changed
                    for(int j=0; j<StaticVariables.label.length; j++){
                        eval[i][j]+= measures.get(StaticVariables.label[j]);
                    }
                }
            }
        }
        //The first parameter is one because we want to print all the values in staticvariables.label
        printResults.printTwoD(1, eval, numExperiments, loop);
    }
}