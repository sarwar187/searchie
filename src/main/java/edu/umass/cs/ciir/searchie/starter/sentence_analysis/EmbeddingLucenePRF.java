package edu.umass.cs.ciir.searchie.starter.sentence_analysis;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EntityGraph;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 7/16/17.
 */
public class EmbeddingLucenePRF {
    static String prefix = StaticVariables.prefix;
    //This file is for loading word embedding from text files
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        PrintResults pr = new PrintResults();

        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int size = fullPositives.size();
        int totalSize = fullConllTrain.size();

        List<List<SimpleToken>> mergedList = new ArrayList<>();
        List<List<SimpleToken>> positives = new ArrayList<>();
        mergedList.addAll(fullPositives);

        for(int i=0;i<totalSize-size;i++){
            mergedList.add(fullNegatives.get(i));
        }
        positives.add(mergedList.get(0));

        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);

        double prior[] = new double[mergedList.size()];
        double vectorLength[] = new double[totalSize];
        double points[][] = new double[totalSize][embeddingSize];

        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        //String expansionApproach = "LUCENE";
        //String expansionApproach = "EMBEDDING";
        //String expansionApproach = "FULL";
        //String expansionApproach = "NONE";
        String expansionApproach = "ALL";
        //String expansionParameter = "TOPK";
        String expansionParameter = "PRF";
        //String expansionParameter = "ISLAND";
        List<Integer> pseudoAndFullPositives = new ArrayList<>();

        int row = 0;
        int countPRF = 0;
        int topk = 3;
        LuceneInMemory lucene = new LuceneInMemory();

        List<Island> islands;
        Set<Integer> sentences = null;

        if(expansionParameter.equals("ISLAND")) {
            islands = EntityGraph.createIslands(mergedList, availableSpans, etype, inputFileName);
            for (Island island : islands) {
                if (island.containsSentence(0)) {
                    sentences = island.getSentences();
                    break;
                }
            }
        }
        //System.out.println(sentences.size() + " sentences found in target island");
        for(List<SimpleToken> tokenList: mergedList) {
            lucene.addTokenList(row, tokenList);
            if(expansionParameter.equals("PRF")) {
                if (expansionApproach.equals("ALL")) {
                    if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, availableSpans) == 1) {
                        prior[row] = 10;
                        countPRF++;
                    }
                    pseudoAndFullPositives.add(row);
                } else {
                    if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, foundSpans) == 1) {
                        prior[row] = 10;
                        countPRF++;
                    }
                }
            }
            if(expansionParameter.equals("ISLAND")){
                if(sentences.contains(row)) {
                    prior[row] = 10;
                    countPRF++;
                }
            }
            //List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList), idf);
            List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList));
            int col = 0;
            for(double d: doubleList){
                points[row][col++] = d;
                vectorLength[row]+= (d*d);
            }
            if(vectorLength[row]-0d < 0.00000001) vectorLength[row]=1;
            row++;
        }

        for(int i=0;i<vectorLength.length;i++){
            vectorLength[i] = Math.sqrt(vectorLength[i]);
            if(vectorLength[i] - 0d < 0.00000001) vectorLength[row]=1;
            //System.out.print(vectorLength[i] + ",");
        }
        //BE CAREFUL ABOUT THE LINE BELOW
        //countPRF=10;

        //context expansion begins here
        int bootstrapSentenceId = 0;
        int [] expandedSentences;
        double[] expandedSentencesWeight;
        if(expansionParameter.equals("TOPK")) {
            expandedSentences = new int[topk];
            expandedSentencesWeight = new double[topk];
        }
        else if(expansionParameter.equals("PRF") || expansionParameter.equals("ALL") || expansionParameter.equals("ISLAND")) {
            expandedSentences = new int[countPRF];
            expandedSentencesWeight = new double[countPRF];
        }
        else {
            expandedSentences = new int[fullPositives.size()];
            expandedSentencesWeight = new double[fullPositives.size()];
        }
        //Map<Integer, Double> hashMap = lucene.search(mergedList.get(bootstrapSentenceId), StaticVariables.sizeOfInitialRetrieval);
        List<Integer> l = new ArrayList<>();
        if(expansionApproach.equals("LUCENE")) {
            l = lucene.search(mergedList.get(bootstrapSentenceId));
            row = 0;
            for (Integer s : l) {
                expandedSentences[row++] = s;
                if(expansionParameter.equals("TOPK")) {
                    if (row == topk)
                        break;
                }
                else if(expansionParameter.equals("PRF")) {
                    if (row == countPRF)
                        break;
                }
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }else if(expansionApproach.equals("EMBEDDING")) {
            row = 0;
            for (SearchResult s : EmbeddingSearch.search(bootstrapSentenceId, points, totalSize, embeddingSize, prior, vectorLength)) {
                expandedSentences[row] = s.getSentenceId();
                expandedSentencesWeight[row] = countPRF - row;
                row++;
                //System.out.println(s.getScore());
                //pr.printSimpleToken(mergedList.get(s.getSentenceId()), "found");
                if(expansionParameter.equals("TOPK")) {
                    if (row == topk)
                        break;
                }
                else if(expansionParameter.equals("PRF") || expansionParameter.equals("ISLAND")) {
                    if (row == countPRF)
                        break;
                }
                /*if (row == countPRF)
                    break;*/
            }
            //int [] expandedSentences = {bootstrapSentenceId};
            //EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, expandedSentencesWeight, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
            //context expansion done. the vector is pasted at location 0 of points array
        }else if(expansionApproach.equals("FULL")) {
            for(int i=0; i<fullPositives.size(); i++){
                expandedSentences[i] = i;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }else if(expansionApproach.equals("ALL")){
            row =0;
            for(Integer i: pseudoAndFullPositives){
                expandedSentences[row] = i;
                row++;
                if(expansionParameter.equals("PRF")) {
                    if (row == countPRF)
                        break;
                }
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }

        setPrior(prior, 0d);
        List<SearchResult> results = EmbeddingSearch.search(bootstrapSentenceId, points, totalSize, embeddingSize, prior, vectorLength);
        int relevantSentences = 0;
        List<Boolean> list = new ArrayList<>();
        for(SearchResult s: results) {
            positives.add(mergedList.get(s.getSentenceId()));
            l.add(s.getSentenceId());
            if (DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype) == 1) {
                list.add(true);
                relevantSentences++;
            }else{
                list.add(false);
            }
        }

        System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + Metrics.computeAP(list, relevantSentences) + "\t" + ((1.0*foundSpans.size())/availableSpans.size())+ "\t" + inputFileName);
        //List<Island> islands = EntityGraph.createIslands(mergedList, availableSpans, etype, inputFileName);
        //System.out.println(EntityGraph.discoveredIslands(l, islands));
    }


    public static void setPrior(double prior[], double val){
        for(int i=0;i<prior.length;i++)
            prior[i] = val;
    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            /*if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
            }*/
            if(!Stopwords.isStemmedStopword(token.lemma)){
                strList.add(token.lemma);
            }
        }
        return strList;
        //return s.trim();
    }



    public static void writeEmbeddingToFile(List<Double> l, BufferedWriter br){
        String str = "";
        for(Double d: l){
            str+= d;
            str+=" ";
        }
        try {
            br.write(str);
            br.write("\n");
        } catch (IOException e) {
            //System.out.println("error in printing embeddings");
            e.printStackTrace();
        }
    }
}
