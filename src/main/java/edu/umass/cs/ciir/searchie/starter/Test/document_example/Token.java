package edu.umass.cs.ciir.searchie.starter.Test.document_example;
import com.google.gson.annotations.SerializedName;

import java.util.HashSet;

public class Token {
    public Token(String features, String label, String lemma, String lemmaId, String tagEnd, String tagMiddle, String tagStart, String token) {
        this.features = features;
        this.label = label;
        this.lemma = lemma;
        this.lemmaId = lemmaId;
        this.tagEnd = tagEnd;
        this.tagMiddle = tagMiddle;
        this.tagStart = tagStart;
        this.token = token;
    }

    private String features;
    private String label;
    private String lemma;
    private String lemmaId;
    @SerializedName("tag-end")
    private String tagEnd;
    @SerializedName("tag-middle")
    private String tagMiddle;
    @SerializedName("tag-start")
    private String tagStart;
    private String token;

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getLemmaId() {
        return lemmaId;
    }

    public void setLemmaId(String lemmaId) {
        this.lemmaId = lemmaId;
    }

    public String getTagEnd() {
        return tagEnd;
    }

    public void setTagEnd(String tagEnd) {
        this.tagEnd = tagEnd;
    }

    public String getTagMiddle() {
        return tagMiddle;
    }

    public void setTagMiddle(String tagMiddle) {
        this.tagMiddle = tagMiddle;
    }

    public String getTagStart() {
        return tagStart;
    }

    public void setTagStart(String tagStart) {
        this.tagStart = tagStart;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}


