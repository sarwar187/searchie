package edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 6/6/17.
 */
public class Utils {


    /*termFrequencyVector(){
        Terms vector = index.getTermVector( docid, field ); // Read the document's document vector.

        // You need to use TermsEnum to iterate each entry of the document vector (in alphabetical order).
        System.out.printf( "%-20s%-10s%-20s\n", "TERM", "FREQ", "POSITIONS" );
        TermsEnum terms = vector.iterator();
        PostingsEnum positions = null;
        BytesRef term;
        while ( ( term = terms.next() ) != null ) {

            String termstr = term.utf8ToString(); // Get the text string of the term.
            long freq = terms.totalTermFreq(); // Get the frequency of the term in the document.

            System.out.printf( "%-20s%-10d", termstr, freq );

            // Lucene's document vector can also provide the position of the terms
            // (in case you stored these information in the index).
            // Here you are getting a PostingsEnum that includes only one document entry, i.e., the current document.
            positions = terms.postings( positions, PostingsEnum.POSITIONS );
            positions.nextDoc(); // you still need to move the cursor
            // now accessing the occurrence position of the terms by iteratively calling nextPosition()
            for ( int i = 0; i < freq; i++ ) {
                System.out.print( ( i > 0 ? "," : "" ) + positions.nextPosition() );
            }
            System.out.println();

        }

    }*/

    public static String getDocno( IndexReader index, String fieldDocno, int docid ) throws IOException {
        // This implementation is just for you to quickly understand how this works.
        // You should consider reuse the fieldset if you need to read docnos for a lot of documents.
        Set<String> fieldset = new HashSet<>();
        fieldset.add( fieldDocno );
        Document d = index.document( docid, fieldset );
        return d.get( fieldDocno );

    }

    void inverse(IndexReader index, Directory dir, String term) throws IOException{

        String field = "contents";
        index = DirectoryReader.open( dir );
        int N = index.numDocs(); // the total number of documents in the index
        int n = index.docFreq( new Term(field, term ) ); // get the document frequency of the term in the "text" field
        double idf = Math.log( ( N + 1 ) / ( n + 1 ) ); // well, we normalize N and n by adding 1 to avoid n = 0
        //System.out.printf( "%-30sN=%-10dn=%-10dIDF=%-8.2f\n", term, N, n, idf );
        long corpusTF = index.totalTermFreq( new Term(field, term ) ); // get the total frequency of the term in the "text" field
        long corpusLength = index.getSumTotalTermFreq(field ); // get the total length of the "text" field
        double pwc = 1.0 * corpusTF / corpusLength;
    }



}
