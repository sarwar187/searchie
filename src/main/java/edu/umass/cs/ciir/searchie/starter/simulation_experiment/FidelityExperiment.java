package edu.umass.cs.ciir.searchie.starter.simulation_experiment;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.io.TemporaryDirectory;
import ciir.jfoley.chai.random.ReservoirSampler;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.Oracle;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.WeightUtils;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FidelityExperiment {
    static String prefix = StaticVariables.prefix;

    public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";

    public static void main(String[] args) throws IOException {

        // galago's Parameters class gives us argument parsing:
        // defaults: java ... BasicExperiment --class=PER --trainingStart=3
        // other: java ... BasicExperiment --class=LOC --trainingStart=20
        Parameters argp = Parameters.parseArgs(args);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "new_dump.out");

        // parameters looks in args for a key or uses the default value:
        int numTraining = argp.get("trainingStart", 5);
        String optimalFile;

        if (etype.equals("PER"))
            optimalFile = "dump_per.out";
        else if (etype.equals("LOC"))
            optimalFile = "dump_loc.out";
        else
            optimalFile = "dump_org.out";

        File testFile = new File(argp.get("input", prefix + "testb.crfsuite"));

        // list of sentences, sentences are list of tokens
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullConllTest = SimpleToken.loadCRFSuiteInputFormat(testFile);
        System.out.println("Testing data loaded: " + fullConllTest.size() + " sentences.");

        // split into positive and negative sentences based on whether they have "etype"
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        System.out.println("full positive size " + fullPositives.size() + " full negative size " + fullNegatives.size());
        Oracle oracle = new Oracle(defaultCRFSuiteBinary);
        oracle.createOracle(featureDirectory, featureFileName, etype);

        //for (int j = 0; j < 1; j++) {
        for(double percent = 0.5; percent < 5.1; percent+=0.5){
            int loop = 100; //loop -> how many times we are evaluating
            //double percent = 0.5; //percent -> what percentage of weights we are modifying
            double apv[] = new double[loop]; //ap[] -> holds AP values for original set of weights
            //double optAP[] = new double[loop];
            WeightUtils.FixWeight[] methods = null;
            WeightUtils.FixWeight originalTruncated = new WeightUtils.OriginalTruncated();
            WeightUtils.FixWeight optimalTruncated = new WeightUtils.OptimalTruncated();

            //WeightUtils.FixWeight fsOptimalCopy = new WeightUtils.FSOptimalCopy();
            //WeightUtils.FixWeight fsEqualBinQuantization = new WeightUtils.FSEqualBinQuantization();
            //WeightUtils.FixWeight fsOptimalTruncated = new WeightUtils.FSOptimalTruncated();

            methods = new WeightUtils.FixWeight[]{originalTruncated, optimalTruncated};
            String[] methodsString = {"org_trunc", "numf", "fs_opt_cp", "numf", "fs_eqbin_quant", "numf", "fs_opt_trunc", "numf"};
            double ap[][] = new double[methods.length][loop];
            int weightsChanged[][] = new int[methods.length][loop];

            // I didn't find negatives to be helpful in my experiments, so only sample from positives:
            // delete all temporary files we create as we go

            for (int i = 0; i < loop; i++) {
                try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                    // CRFSuite learner class (basically, where do we save tmp files?)
                    CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                    // set up a CRFSuite model (lbfgs is typically the best).
                    learner.setModel(argp.get("model", "lbfgs"));
                    // Randomly pick out a few positives:
                    List<List<SimpleToken>> positives = ReservoirSampler.take(numTraining, fullPositives);
                    // Train a model and read it in from CRFSuite:
                    Parameters info = Parameters.create();
                    TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                    //System.out.println(info); // print out any debug information
                    //Build a classifier object from our weights:
                    LinearTokenClassifier tokenClassifier = new LinearTokenClassifier(originalMap);
                    final Map<String, Double> measures = evaluateModel(tokenClassifier, fullConllTest, etype);
                    double beforeAP = measures.get("AP"), beforeuAP = measures.get("uAP"), beforeF1 = (measures.get("F1").isNaN() == true) ? 0 : measures.get("F1");
                    apv[i] = beforeAP;

                    learner.setFlag(1); //set the flag to get certain types of features only from the optimal weights
                    // ** we need some changes in this model of setting flag. Need to change code. put patterns in static variables and create
                    // some nice expressions to find patterns.
                    //TObjectFloatHashMap<String> optimalMap = CRFSuiteLearner.parseCRFSuiteModelDump(new File(StaticVariables.dump_prefix + optimalFile), etype);

                    for(int k = 0; k < methods.length; k++){
                        //System.out.println("inside method " + k);
                        TObjectFloatHashMap<String> modifiedWeights = null;//methods[k].fixWeight(optimalMap, originalMap, percent, oracle);
                        //System.out.println("outside method " + k);
                        LinearTokenClassifier modifiedClassifier = new LinearTokenClassifier(modifiedWeights);
                        Map<String, Double> modified_measures = evaluateModel(modifiedClassifier, fullConllTest, etype);
                        double avPrec = modified_measures.get("AP"), afteruAP = modified_measures.get("uAP"), afterF1 = (modified_measures.get("F1").isNaN() == true) ? 0 : modified_measures.get("F1");
                        ap[k][i] = avPrec;
                        weightsChanged[k][i] = methods[k].getWeightInfo().getNumberOfWeightsChanged();
                    }
                }
            }

            /*TTest ttest = new TTest();
                for(int i=0;i<loop;i++){
                System.out.print(ap[i] + "\t" + apNaive[i] + "\t" + apModified[i] + "\n");
            }*/
            DescriptiveStatistics stats = new DescriptiveStatistics();
            //System.out.print("percent\t" + "original\t");
            //for(int i =0; i<methodsString.length;i++){
            //    System.out.print(methodsString[i] + "\t");
            //}
            //System.out.println();
            for (int i = 0; i < loop; i++) {
                stats.addValue(apv[i]);
            }
            double original = stats.getMean(); stats.clear();
            System.out.printf(percent + "\t");
            System.out.printf("%12.3f\t", original);
            double result[] = new double[methods.length];
            for(int m=0; m < methods.length; m++) {
                for (int n = 0; n < loop; n++) {
                    stats.addValue(ap[m][n]);
                }
                result[m] = stats.getMean();
                System.out.printf("%12.3f\t", result[m]);
                stats.clear();
                for (int n = 0; n < loop; n++) {
                    stats.addValue(weightsChanged[m][n]);
                }
                //System.out.printf("%15d\t", (int)sum/loop);
                System.out.printf("%15d\t", (int) stats.getMean());
                stats.clear();
            }
            System.out.println();
            //System.out.println(result[0] + "\t" + result[1] + "\t" + result + "\t" + (((optimal - original) / original) * 100) + "\t" + (((modified - original) / original) * 100));
        }
        //System.out.println(ttest.pairedT(ap,apNaive));
        //System.out.println(ttest.pairedT(ap,apModified));
        //System.out.println(ttest.pairedT(apNaive,apModified));
    }

    static double logistic(double weight) {
        return 1 / (1 + Math.pow(Math.E, (-1.0) * weight));
    }

    public static Map<String, Double> evaluateModel(LinearTokenClassifier model, List<List<SimpleToken>> testData, String etype) {
        // collect correct, prediction score pairs
        List<Pair<Boolean, Double>> rankedPred = new ArrayList<>();
        Map<String, ScoresForUniqueLemma> bestScoreByLemma = new HashMap<>();

        // collect unique tokens in "ScoresForUniqueLemma" as we score
        // collect non-unique in rankedPred (we don't actually need to sort it).
        int count = 0;
        for (List<SimpleToken> tokens : testData) {

            for (SimpleToken token : tokens) {
                boolean truth = token.truthLabel.equals(etype);
                //System.out.println(token.getFeatures().size());
                double score = model.score(token.getFeatures());
                rankedPred.add(Pair.of(truth, score));
                bestScoreByLemma.computeIfAbsent(token.lemma, ScoresForUniqueLemma::new).score(score, truth);
            }
        }

        // rank the TokenInfo classes by the highest scoring one (create our "unique" ranking).
        List<Pair<Boolean, Double>> uniqueRankedPred = new ArrayList<>();
        for (ScoresForUniqueLemma scoresForUniqueLemma : bestScoreByLemma.values()) {
            uniqueRankedPred.add(Pair.of(scoresForUniqueLemma.fractionTrue() > 0, scoresForUniqueLemma.bestScore()));
        }

        // calculate p,r,f1,acc, etc.
        BinaryClassifierInfo uinfo = new BinaryClassifierInfo();
        uinfo.update(uniqueRankedPred, model.getIntercept());

        BinaryClassifierInfo info = new BinaryClassifierInfo();
        info.update(rankedPred, model.getIntercept());

        Map<String, Double> measures = new HashMap<>();

        // regular-measures
        measures.put("AUC", AUC.compute(rankedPred));
        measures.put("P10", AUC.computePrec(rankedPred, 10));
        measures.put("P100", AUC.computePrec(rankedPred, 100));
        measures.put("P1000", AUC.computePrec(rankedPred, 1000));
        measures.put("AP", AUC.computeAP(rankedPred));
        measures.put("P", (double) info.getPositivePrecision());
        measures.put("R", (double) info.getPositiveRecall());
        measures.put("F1", (double) info.getPositiveF1());
        measures.put("Accuracy", (double) info.getAccuracy());
        measures.put("TP", (double) info.numPredTruePositive);
        measures.put("FP", (double) info.getNumFalsePositives());
        measures.put("TN", (double) info.numPredTrueNegative);
        measures.put("FN", (double) info.getNumFalseNegatives());

        // unique-measures:
        measures.put("uAUC", AUC.compute(uniqueRankedPred));
        measures.put("uP10", AUC.computePrec(uniqueRankedPred, 10));
        measures.put("uP100", AUC.computePrec(uniqueRankedPred, 100));
        measures.put("uP1000", AUC.computePrec(uniqueRankedPred, 1000));
        measures.put("uAP", AUC.computeAP(uniqueRankedPred));
        measures.put("uP", (double) uinfo.getPositivePrecision());
        measures.put("uR", (double) uinfo.getPositiveRecall());
        measures.put("uF1", (double) uinfo.getPositiveF1());
        measures.put("uAccuracy", (double) uinfo.getAccuracy());
        measures.put("uTP", (double) uinfo.numPredTruePositive);
        measures.put("uFP", (double) uinfo.getNumFalsePositives());
        measures.put("uTN", (double) uinfo.numPredTrueNegative);
        measures.put("uFN", (double) uinfo.getNumFalseNegatives());
        return measures;
    }

}
