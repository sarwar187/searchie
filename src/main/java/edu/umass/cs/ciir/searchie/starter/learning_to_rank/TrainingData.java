package edu.umass.cs.ciir.searchie.starter.learning_to_rank;

import java.util.ArrayList;
import java.util.List;

public class TrainingData {
    List<String> entity;

    public List<String> getEntity() {
        return entity;
    }

    public void setEntity(List<String> entity) {
        this.entity = entity;
    }

    public List<String> getContext() {
        return context;
    }

    public void setContext(List<String> context) {
        this.context = context;
    }

    public String getTruthValue() {
        return truthValue;
    }

    public void setTruthValue(String truthValue) {
        this.truthValue = truthValue;
    }

    List<String> context;
    String truthValue;
    String queryId;

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public int getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(int sentenceId) {
        this.sentenceId = sentenceId;
    }

    int  sentenceId;

    TrainingData(){
        entity = new ArrayList<>();
        context = new ArrayList<>();
        truthValue = "0";
    }
}
