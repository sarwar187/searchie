package edu.umass.cs.ciir.searchie.starter.error_experiment;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.Oracle;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.WeightUtils;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.State;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;


public class ErrorExperiment{
    static String prefix = StaticVariables.prefix;
    public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";

    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "new_dump.out");

        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        //Defining parameters of the experiment
        int loop = 10; // loop is the number of relevance feedback we take
        int numberOfWeights = 10; //number of weights changed by the user
        int numExperiements = fullPositives.size() - 3;
        numExperiements = 3;  // number of rounds we calculate AP, uAP and F1
        double eval[][] = new double[4][numberOfWeights+1];
        State state = new State(); //CREATING STATE
        Oracle oracle = new Oracle(defaultCRFSuiteBinary);
        oracle.createOracle(featureDirectory, featureFileName, etype);


        TObjectFloatHashMap<String> optimalMap;
        try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
            CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
            learner.setFlag(1);
            learner.setModel(argp.get("model", "lbfgs"));
            Parameters info = Parameters.create();
            optimalMap = learner.learnFeatureWeights(fullConllTrain, etype, info);
        }
        System.out.println("full positive size " + fullPositives.size() + " full negative size " + fullNegatives.size());
        DecimalFormat df = new DecimalFormat("#0.000");
        for (int index = 0; index < numExperiements * 3 ; index += 3) { //iterating over 300 positives, taking three in each round
            List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
            positives.add(fullPositives.get(index));
            positives.add(fullPositives.get(index + 1));
            positives.add(fullPositives.get(index + 2)); //taking the last positive
            List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
            fullConllTest.addAll(fullNegatives); // at first we add all the negatives in the testing data
            for(int i = 0; i<fullPositives.size(); i++){  // now we take all the positives except the three we had chosen for training
                if(i<index || i>=index+3)
                    fullConllTest.add(fullPositives.get(i));
            }
            System.out.println("full positive size\t" + positives.size() + " fullConllTest size\t" + fullConllTest.size() + "fullConllTrain size\t" + fullConllTrain.size() );
            if(fullConllTrain.size() != (positives.size() + fullConllTest.size())) // Just checking if the training and test data sum up to the full training data
                System.out.println("error happened");
            try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                learner.setModel(argp.get("model", "lbfgs"));
                Parameters info = Parameters.create();
                TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                //System.out.println(positives.get(0).get(0).getLabel());
                for (int i = 0; i < loop + 1; i++) {   //make variable error while changing weights
                    WeightUtils.FixWeightInteractive fixWeightInteractive = new WeightUtils.FSBinInteractive();
                    state.setNumberofWeightErrors(i);
                    TObjectFloatHashMap<String> modifiedWeights = fixWeightInteractive.fixWeight(optimalMap, originalMap, numberOfWeights, oracle, state);
                    LinearTokenClassifier modifiedClassifier = new LinearTokenClassifier(modifiedWeights);
                    final Map<String, Double> measures = evaluateModel(modifiedClassifier, fullConllTest, etype); //The evaluate model function has been changed
                    double beforeAP = measures.get("AP"), beforeuAP = measures.get("uAP"), beforeF1 = (measures.get("F1").isNaN() == true) ? 0 : measures.get("F1");
                    double beforemF1 = (measures.get("mF1").isNaN() == true) ? 0 : measures.get("mF1");
                    eval[0][i]+=beforeAP; eval[1][i]+=beforeuAP; eval[2][i]+=beforeF1; eval[3][i]+=beforemF1;
                }
            }
        }
        for(int i = 0; i<loop+1; i++){System.out.print(df.format(eval[0][i]/numExperiements) + "\t");}
        System.out.println();
        for(int i = 0; i<loop+1; i++){System.out.print(df.format(eval[1][i]/numExperiements) + "\t");}
        System.out.println();
        for(int i = 0; i<loop+1; i++){System.out.print(df.format(eval[2][i]/numExperiements) + "\t");}
        System.out.println();
        for(int i = 0; i<loop+1; i++){System.out.print(df.format(eval[3][i]/numExperiements) + "\t");}
        System.out.println();


    }


    public static Map<String, Double> evaluateModel(LinearTokenClassifier model, List<List<SimpleToken>> testData, String etype) {
        // collect correct, prediction score pairs
        List<Pair<Boolean, Double>> rankedPred = new ArrayList<>();
        Map<String, ScoresForUniqueLemma> bestScoreByLemma = new HashMap<>();

        double true_positive = 0;
        double total_positive = 0;
        double predicted_positive = 0;
        double count_token = 0;
        for (List<SimpleToken> tokens : testData) {
            for (SimpleToken token : tokens) {
                boolean truth = token.truthLabel.equals(etype);
                //System.out.println(token.getFeatures().size());
                double score = model.score(token.getFeatures());
                rankedPred.add(Pair.of(truth, score));
                //System.out.println(truth + "\t" + (score>0));
                if(truth && (score>0)){
                    true_positive++;
                }
                if(score>0)
                    predicted_positive++;
                if(truth)
                    total_positive++;
                count_token++;
                bestScoreByLemma.computeIfAbsent(token.lemma, ScoresForUniqueLemma::new).score(score, truth);
            }
        }

        double precision = true_positive/predicted_positive;
        double recall = true_positive/total_positive;
        double f1 = (2.0 * precision * recall)/(precision+recall);
        // rank the TokenInfo classes by the highest scoring one (create our "unique" ranking).
        List<Pair<Boolean, Double>> uniqueRankedPred = new ArrayList<>();
        for (ScoresForUniqueLemma scoresForUniqueLemma : bestScoreByLemma.values()) {
            uniqueRankedPred.add(Pair.of(scoresForUniqueLemma.fractionTrue() > 0, scoresForUniqueLemma.bestScore()));
        }

        // calculate p,r,f1,acc, etc.
        BinaryClassifierInfo uinfo = new BinaryClassifierInfo();
        uinfo.update(uniqueRankedPred, model.getIntercept());

        BinaryClassifierInfo info = new BinaryClassifierInfo();
        info.update(rankedPred, model.getIntercept());
        //System.out.println(rankedPred.size());
        Map<String, Double> measures = new HashMap<>();

        // regular-measures
        measures.put("AUC", AUC.compute(rankedPred));
        measures.put("P10", AUC.computePrec(rankedPred, 10));
        measures.put("P100", AUC.computePrec(rankedPred, 100));
        measures.put("P1000", AUC.computePrec(rankedPred, 1000));
        measures.put("AP", AUC.computeAP(rankedPred));
        measures.put("P", (double) info.getPositivePrecision());
        measures.put("R", (double) info.getPositiveRecall());
        measures.put("F1", (double) info.getPositiveF1());
        measures.put("mF1", f1);
        //System.out.println(f1 + "\t" + info.getPo);
        System.out.println(info.numPredTrueNegative/ count_token);
        measures.put("Accuracy", (double) info.getAccuracy());
        measures.put("TP", (double) info.numPredTruePositive);
        measures.put("FP", (double) info.getNumFalsePositives());
        measures.put("TN", (double) info.numPredTrueNegative);
        measures.put("FN", (double) info.getNumFalseNegatives());

        // unique-measures:
        measures.put("uAUC", AUC.compute(uniqueRankedPred));
        measures.put("uP10", AUC.computePrec(uniqueRankedPred, 10));
        measures.put("uP100", AUC.computePrec(uniqueRankedPred, 100));
        measures.put("uP1000", AUC.computePrec(uniqueRankedPred, 1000));
        measures.put("uAP", AUC.computeAP(uniqueRankedPred));
        measures.put("uP", (double) uinfo.getPositivePrecision());
        measures.put("uR", (double) uinfo.getPositiveRecall());
        measures.put("uF1", (double) uinfo.getPositiveF1());
        measures.put("uAccuracy", (double) uinfo.getAccuracy());
        measures.put("uTP", (double) uinfo.numPredTruePositive);
        measures.put("uFP", (double) uinfo.getNumFalsePositives());
        measures.put("uTN", (double) uinfo.numPredTrueNegative);
        measures.put("uFN", (double) uinfo.getNumFalseNegatives());
        return measures;
    }

}
