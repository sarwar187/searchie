package edu.umass.cs.ciir.searchie.starter.utils;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.TrainingData;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.lemurproject.galago.core.tools.Search;

import javax.print.attribute.IntegerSyntax;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static edu.umass.cs.ciir.searchie.starter.StaticVariables.printFlag;

/**
 * Created by sarwar on 7/28/17.
 */

public class Evaluation {
    private static final Logger LOGGER = Logger.getLogger( Evaluation.class.getName() );
    String strRecall="";
    String strEvaluationScore="";
    int target = 0;
    public double getMap(){
        return map;
    }
    public int getNullEntities() {
        return nullEntities;
    }

    public int getNumberOfTargetEntities() {
        return nullEntities;
    }

    public void setNullEntities(int nullEntities) {
        this.nullEntities = nullEntities;
    }

    int nullEntities;

    public String getFoundSpansString() {
        return foundSpansString;
    }

    public void setFoundSpansString(String foundSpansString) {
        this.foundSpansString = foundSpansString;
    }

    String foundSpansString="";
    double map = 0;

    public Evaluation(){
        nullEntities = -1;
    }
    public String getStrRecall() {
        return strRecall;
    }

    public void setStrRecall(String strRecall) {
        this.strRecall = strRecall;
    }

    public List<Double> getEvaluationScores() {
        return evaluationScores;
    }

    public String getEvaluationScoresString() {
        NumberFormat formatter = new DecimalFormat("#0.000");
        //System.out.println(formatter.format(4.0));
        String str = "";
        for(Double l: evaluationScores)
            str+= formatter.format(l) + "\t";
        str = str.trim();
        return  str;
    }

    public void setEvaluationScores(List<Double> evaluationScores) {
        this.evaluationScores = evaluationScores;
    }

    List<Double> evaluationScores;




    public String getDataStatistics(List<SearchResult> results, List<List<SimpleToken>> mergedList, Set<List<String>> availableSpans, String etype, String targetEntityType, int bootStrappedSentenceID, List<List<SimpleToken>> fullPositives){
        this.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootStrappedSentenceID);
        List<Double> l = computeEntityDensityOverall(mergedList, etype);
        String output = String.valueOf(mergedList.size()) + "\t";  //average number of sentences per query
        output+=l.get(0) + "\t"; //countEntityToken
        output+=l.get(1) + "\t"; //countEntitySentence
        output+=l.get(2) + "\t"; //entity token density
        output+=l.get(3) + "\t"; //entity sentence density
        output+= String.valueOf(fullPositives.size()) + "\t"; //number of judged sentences
        DescriptiveStatistics ds = new DescriptiveStatistics();
        int countAnnotated = 0;
        for(List<SimpleToken> tokenList:mergedList){
            ds.addValue(tokenList.size());
            for(SimpleToken t: tokenList){
                if(t.truthLabel.equals(etype)){
                    countAnnotated++;
                    break;
                }
            }
        }
        output+=String.valueOf(countAnnotated) + "\t"; //number of annotated sentences
        //output+= String.valueOf(ds.getMean());
        output+=String.valueOf(ds.getMax()); //Maximum Sentence Length
        return output;
    }

    public List<List<TrainingData>> getTrainingData(String inputFileName, List<SearchResult> results, List<List<SimpleToken>> mergedList, Set<List<String>> availableSpans, String etype, String targetEntityType, int bootStrappedSentenceID) {
        Set<List<String>> foundSpans = new HashSet<>();
        List<List<TrainingData>> trainingDataList = new ArrayList<>();
        DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootStrappedSentenceID), etype);
        for (SearchResult s : results) {
            if (s.getSentenceId() == bootStrappedSentenceID)
                continue;
            DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            FeatureExtractor f = new FeatureExtractor((List)mergedList.get(s.getSentenceId()), targetEntityType);
            List<TrainingData> trainingData = f.getTargetEntityListToLowerCaseContextPairs();
            for(TrainingData t: trainingData){
                t.setQueryId(inputFileName);
                t.setSentenceId(s.getSentenceId());
            }
            trainingDataList.add(trainingData);
        }
        return trainingDataList;
    }
    public void evaluateAll(List<SearchResult> results, List<List<SimpleToken>> mergedList, Set<List<String>> availableSpans, String etype, String targetEntityType, int bootStrappedSentenceID) {

        Set<List<String>> foundSpans = new HashSet<>();
        DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootStrappedSentenceID), etype);
        //Adding the first instances in foundspans

        for(List<String> foundSpan: foundSpans){
            for(String str: foundSpan){
                foundSpansString+=str+" ";
            }
            foundSpansString+=",";
        }
        //System.out.println("foundSpansString evaluation");
        //System.out.println(foundSpansString);
        //Foundspans contains the number of entities we have found in the query sentence
        int beginningNumberOfEntities = foundSpans.size();
        //Thats why it is the beginningNumberOfEntities
        int targetNumberOfEntities = availableSpans.size() - beginningNumberOfEntities;
        //if(targetNumberOfEntities == 0)

        //TargetNumberOfEntities is the number of entities that can be found from the dataset
        //PrintResults pr = new PrintResults();
        ArrayList<Boolean> list = new ArrayList<Boolean>();
        Set<List<String>> fullTypeSpans = new HashSet<>();
        //fulltypespans is the same types of entities as the query entity that we can get from a sentence
        int relevantSentences = 0, count = 0;
        int recallArray[] = new int[results.size()];
        //what is the percentage of entities that we can get by retrieving a specific number of sentences
        double extractedEntityPrecisionArray[] = new double[results.size()];
        double extractedEntityRecallArray[] = new double[results.size()];
        //Set<List<String>> foundSpans = new HashSet<>();
        DescriptiveStatistics statsScore = new DescriptiveStatistics();
        int non_zero = 0;
        double sum_score = 0;
        PrintResults pr = new PrintResults();
        for (SearchResult s : results) {
            //LOGGER.log(Level.FINE, "score ", s.getScore());
            //if(s.getScoreEntityContext() > 0.0000000) {
            //    statsScore.addValue(s.getScoreEntityContext());
            //    non_zero++;
            //}
            //System.err.println("score " + s.getScore());

            sum_score+=s.getScore();
            if(s.getSentenceId() == bootStrappedSentenceID)
                continue;
            //pr.printSimpleTokenSimply(mergedList.get(s.getSentenceId()), "result");
            if (DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype) == 1) {
                list.add(true);
                //we got a new relevant sentence!!
                ++relevantSentences;
            } else {
                list.add(false);
                //System.out.println("FALSE");
            }
            //at each point how many unique spans are in my disposal
            FeatureExtractor featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            fullTypeSpans.addAll(featureExtractor.getTargetEntityList());

            recallArray[count] = foundSpans.size() - beginningNumberOfEntities;
            extractedEntityPrecisionArray[count] = Metrics.getEntityPrecision(fullTypeSpans, availableSpans);
            extractedEntityRecallArray[count] = Metrics.getEntityRecall(fullTypeSpans, availableSpans);
            count++;
        }
        //System.err.println(sum_score);
        //System.out.println((statsScore.getMax() - statsScore.getMin())/non_zero);
        //System.out.println("found spans size " + foundSpans.size());
        //foundSpans = DataSelector.getUniqueSpans(positives, etype);
        //List<Double> evaluate = new ArrayList<>();
        //this array will indicate the number of sentences for different recall level
        int recallLevel[] = new int[101];
        evaluationScores = new ArrayList<>();
        evaluationScores.add(recallArray[5]/(1.0*targetNumberOfEntities));
        evaluationScores.add(recallArray[10]/(1.0*targetNumberOfEntities));
        evaluationScores.add(recallArray[20]/(1.0*targetNumberOfEntities));
        //new PrintResults().printSpans(foundSpans, "found spans evaluation function");
        //PROBLEM: WHAT IF I FIND ALL THE ENTITIES IN THE QUERY
        int residualNumberEntities = foundSpans.size()-beginningNumberOfEntities;
        //We started with beginningNumberOfEntities and ended up with foundSpans. residual actually means how many
        //new entities we could find.
        //if there is no relevant document in the corpus
        if(targetNumberOfEntities==0) {
            nullEntities = 1;
            return;
        }
        //if there is relevant document in the corpus and I could not find a single of them
        //if(residualNumberEntities==0) residualNumberEntities++;
//        for(int i=0; i< results.size(); i++){
//            if(targetNumberOfEntities==0)
//                recallArray[i]= 0;  //(int)(((1.0*recallArray[i])/(targetNumberOfEntities)*100));
//            else
//                recallArray[i]= (int)(((1.0 * recallArray[i])/(targetNumberOfEntities)*100));
//        }
        //int temp=recallArray[0];
        recallLevel[0] = 0;
//        int beginInterval = 1;
//        for(int i=0; i< results.size(); i++) {
//            if(recallArray[i]>beginInterval){
//                int endInterval = recallArray[i];
//                for(int j=beginInterval; j<=)
//            }
//
//            /*if((recallArray[i]>0) && (recallArray[i]!=recallArray[i-1])){
//                for(int j=recallArray[i-1]+1; j<=recallArray[i]; j++){
//                    recallLevel[j] = i;
//                }
//            }*/
//        }

        //recallMAP is an (number_of_entities, sentences) map
        Map<Integer, Integer> recallMap = new HashMap<>();
        for(int i=0; i< recallArray.length; i++) {
            if(!recallMap.containsKey(recallArray[i])){
                recallMap.put(recallArray[i], i+1);
            }
        }
        Set<Integer> a = recallMap.keySet();
        List<Integer> lista = new ArrayList<>(a);
        Collections.sort(lista);
        /*for(int i=1; i<101;i++){
            for(Integer r: lista){
                if(i<=r){
                    recallLevel[i] = recallMap.get(r);
                    break;
                }
            }
        }*/
        int beginIndex=1;
        for(Integer r:lista){
            if(r>0){
                int endIndex = r;
                for(int i=beginIndex;i<=endIndex;i++){
                    recallLevel[i] = recallMap.get(r);
                }
                beginIndex = endIndex+1;
            }
        }
        for(int i=beginIndex;i<=100;i++)
            recallLevel[i]=1000;
        //endIndex = 100;
        /*for(int i=1; i<101;i++) {

        }*/

        //String str1 = "";
        //String str2 = "";
        //for(int i=0;i<101;i++)
        //    str1+= recallArray[i] + "\t";
        for(int i=0;i<101;i++)
            strRecall+= recallLevel[i] + ",";

        strRecall=strRecall.trim();
        //str1 = str1.trim();
        //System.out.println(str1);
        //System.out.println(str2);
        //System.out.println("" + availableSpans.size() + "\t" + foundSpans.size() + "\t" + Metrics.computeAP(list, relevantSentences) + "\t" + 1.0 * (double) foundSpans.size() / (double) availableSpans.size() + "\t" + targetEntityType + "\t" + inputFileName + "\t" + mergedList.size());
        //evaluationScores.add(computeEntityDensity(results, mergedList, etype));

        foundSpansString = foundSpansString.trim();
        evaluationScores.add(Metrics.precision(list, 5));
        evaluationScores.add(Metrics.precision(list, 10));
        evaluationScores.add(Metrics.precision(list, 20));
        //evaluationScores.add(Metrics.precision(list, 20));
        map = Metrics.computeAP(list, relevantSentences);
        evaluationScores.add(map);
        evaluationScores.add((1.0 * residualNumberEntities)/targetNumberOfEntities);
        evaluationScores.add((double) residualNumberEntities);
        evaluationScores.add((double) targetNumberOfEntities);
        evaluationScores.add((double) (statsScore.getMax() - statsScore.getMin())/non_zero);

        //evaluationScores.add((double) mergedList.size());
        //evaluate.add((double) );
        //evaluate.add((double) );
        //evaluationScores.add((double) availableSpans.size());
    }


    public void evaluateAllWithoutNovelty(List<SearchResult> results, List<List<SimpleToken>> mergedList, Set<List<String>> availableSpans, String etype, String targetEntityType, int bootStrappedSentenceID) {
        Set<List<String>> foundSpans = new HashSet<>();
        DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootStrappedSentenceID), etype);
        for(List<String> foundSpan: foundSpans){
            for(String str: foundSpan){
                foundSpansString+=str+" ";
            }
            foundSpansString+=",";
        }
        //Foundspans contains the number of entities we have found in the query sentence
        int beginningNumberOfEntities = foundSpans.size();
        //Thats why it is the beginningNumberOfEntities
        int targetNumberOfEntities = availableSpans.size() - beginningNumberOfEntities;
        //TargetNumberOfEntities is the number of entities that can be found from the dataset
        //PrintResults pr = new PrintResults();
        ArrayList<Boolean> list = new ArrayList<Boolean>();
        Set<List<String>> fullTypeSpans = new HashSet<>();
        int relevantSentences = 0, count = 0;
        int recallArray[] = new int[results.size()];
        double extractedEntityPrecisionArray[] = new double[results.size()];
        double extractedEntityRecallArray[] = new double[results.size()];
        //Set<List<String>> foundSpans = new HashSet<>();
        List<List<SimpleToken>> positives = new ArrayList<>();
        for (SearchResult s : results) {
            if (s.getSentenceId() == bootStrappedSentenceID)
                continue;
            if (DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype) == 1) {
                //annotation is completed
                //at first add the new relevant sentence to the positives
                positives.add(mergedList.get(s.getSentenceId()));
                //now extract all the uniquespans ins positives and add them to foundspans
                Set<List<String>> uniqueSpans = DataSelector.getUniqueSpans(positives, etype);
                foundSpans.addAll(uniqueSpans);
                //clear the positive
                positives.clear();
                list.add(true);
                ++relevantSentences;
            }else {
                positives.add(mergedList.get(s.getSentenceId()));
                //now extract all the uniquespans ins positives and add them to foundspans
                Set<List<String>> uniqueSpans = DataSelector.getUniqueSpans(positives, etype);
                if(uniqueSpans.size()>0) {
                    list.add(true);
                    ++relevantSentences;
                }
                else
                    list.add(false);
                positives.clear();
            }

            FeatureExtractor featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            fullTypeSpans.addAll(featureExtractor.getTargetEntityList());

            recallArray[count] = foundSpans.size() - beginningNumberOfEntities;
            extractedEntityPrecisionArray[count] = Metrics.getEntityPrecision(fullTypeSpans, availableSpans);
            extractedEntityRecallArray[count] = Metrics.getEntityRecall(fullTypeSpans, availableSpans);
            count++;
        }
        int recallLevel[] = new int[101];
        evaluationScores = new ArrayList<>();
        evaluationScores.add(recallArray[5]/(1.0*targetNumberOfEntities));
        evaluationScores.add(recallArray[10]/(1.0*targetNumberOfEntities));
        evaluationScores.add(recallArray[20]/(1.0*targetNumberOfEntities));
        int residualNumberEntities = foundSpans.size()-beginningNumberOfEntities;
        if(residualNumberEntities==0) nullEntities = 1;
        if(residualNumberEntities==0) residualNumberEntities++;
        for(int i=0; i< results.size(); i++){
            recallArray[i]= (int)(((1.0*recallArray[i])/(residualNumberEntities)*100));
        }
        //int temp=recallArray[0];
        recallLevel[0] = 0;
        Map<Integer, Integer> recallMap = new HashMap<>();
        for(int i=0; i< results.size(); i++) {
            if(!recallMap.containsKey(recallArray[i])){
                recallMap.put(recallArray[i], i+1);
            }
        }
        Set<Integer> a = recallMap.keySet();
        List<Integer> lista = new ArrayList<>(a);
        Collections.sort(lista);
        int beginIndex=1;
        for(Integer r:lista){
            if(r>0){
                int endIndex = r;
                for(int i=beginIndex;i<=endIndex;i++){
                    recallLevel[i] = recallMap.get(r);
                }
                beginIndex = endIndex+1;
            }
        }
        for(int i=beginIndex;i<=100;i++)
            recallLevel[i]=1000;
        for(int i=0;i<101;i++)
            strRecall+= recallLevel[i] + ",";

        strRecall=strRecall.trim();
        foundSpansString = foundSpansString.trim();
        evaluationScores.add(Metrics.precision(list, 5));
        evaluationScores.add(Metrics.precision(list, 10));
        evaluationScores.add(Metrics.precision(list, 20));
        evaluationScores.add(Metrics.precision(list, 20));
        evaluationScores.add(Metrics.computeAP(list, relevantSentences));
        evaluationScores.add((1.0 * residualNumberEntities)/targetNumberOfEntities);
        evaluationScores.add((double) residualNumberEntities);
        evaluationScores.add((double) targetNumberOfEntities);
    }


    public static List<SearchResult> convertToSearchResults(int topk, List<List<SimpleToken>> mergedList, List<Integer> ranked) {
        double score = topk;
        List<SearchResult> searchResults = new ArrayList<>();
        for (int i = 0; i < topk && i < mergedList.size(); i++) {
            SearchResult s = new SearchResult(ranked.get(i), score);
            searchResults.add(s);
            score--;
        }
        return searchResults;
    }

    public static double computeEntityDensityInSentence(List<SearchResult> searchResults, List<List<SimpleToken>> mergedList, String etype){
        double countEntitySentence=0;
        for(SearchResult s: searchResults){
            for(SimpleToken t: mergedList.get(s.getSentenceId())){
                if(t.truthLabel.equals(etype)){
                    countEntitySentence++;
                    break;
                }
            }
        }
        return countEntitySentence/searchResults.size();
    }

    public static List<Double> computeEntityDensityOverall(List<List<SimpleToken>> mergedList, String etype) {
        double countEntityToken = 0;
        double countEntitySentence = 0;
        for (List<SimpleToken> tokenList : mergedList) {
            int flag = 0;
            for (SimpleToken t : tokenList) {
                if (t.truthLabel.equals(etype)) {
                    countEntityToken++;
                    flag = 1;
                }
            }
            if(flag==1)
                countEntitySentence++;
        }
        List<Double> l = new ArrayList<>();
        l.add(countEntityToken);
        l.add(countEntitySentence);
        l.add(countEntityToken/mergedList.size());
        l.add(countEntitySentence/mergedList.size());
        return l;
    }

    public static double computeEntityDensityInTokens(List<SearchResult> searchResults, List<List<SimpleToken>> mergedList, String etype){
        double countEntityToken=0;
        double countTokens=0;
        for(SearchResult s: searchResults){
            for(SimpleToken t: mergedList.get(s.getSentenceId())){
                if(t.truthLabel.equals(etype)){
                    countEntityToken++;

                    //break;
                }
                countTokens++;
            }
        }
        return countEntityToken/countTokens;
    }
}