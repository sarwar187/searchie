package edu.umass.cs.ciir.searchie.starter.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by sarwar on 2/25/17.
 */
class Point implements Comparable{
    Boolean truth;
    double score;
    Point(Boolean truth, Double score){
        this.truth = truth;
        this.score = score;
    }
    Double getScore(){
        return this.score;
    }

    @Override
    public int compareTo(Object o) {
        Point other = (Point) o;
        if (this == other)
            return 0;
        if (this.getScore() < other.getScore()) return 1;
        else if (this.getScore() == other.getScore()) return 0;
        else return -1;

    }

    public static void main(String args[]){

        Point p1 = new Point(true, 12.0);

        Point p2 = new Point(true, 13.0);
        Point p3 = new Point(true, 11.0);

        ArrayList<Point> a = new ArrayList<Point>();
        a.add(p1);
        a.add(p2);
        a.add(p3);
        Collections.sort(a);
        for(Point p: a){
            System.out.println(p.getScore());
        }
    }

}