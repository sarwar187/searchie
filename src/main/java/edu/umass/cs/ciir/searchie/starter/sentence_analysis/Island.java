package edu.umass.cs.ciir.searchie.starter.sentence_analysis;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by sarwar on 6/11/17.
 */
public class Island {
    private Set<Integer> entities;
    private Set<Integer> sentences;
    private int id;
    public Island(int id){
        entities = new HashSet<>();
        sentences = new HashSet<>();
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void pushEntities(int i){
        entities.add(i);
    }

    public void pushSentences(int i){
        sentences.add(i);
    }


    public boolean containsEntity(int i){
        if(entities.contains(i))
            return true;
        else
            return false;
    }

    public boolean containsSentence(int i){
        if(sentences.contains(i))
            return true;
        else
            return false;
    }


    public Set<Integer> getSentences() {
        return sentences;
    }

    public void printIsland(){
        for(Integer i: entities){
            System.out.print(i + " ");
        }
    }
}


