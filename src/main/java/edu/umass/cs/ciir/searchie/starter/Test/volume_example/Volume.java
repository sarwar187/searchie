package edu.umass.cs.ciir.searchie.starter.Test.volume_example;

import java.util.List;

public class Volume {
    private String status;
    private Boolean managed;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getManaged() {
        return managed;
    }

    public void setManaged(Boolean managed) {
        this.managed = managed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Support getSupport() {
        return support;
    }

    public void setSupport(Support support) {
        this.support = support;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<String> getMapped_wwpns() {
        return mapped_wwpns;
    }

    public void setMapped_wwpns(List<String> mapped_wwpns) {
        this.mapped_wwpns = mapped_wwpns;
    }

    private String name;
    private Support support;
    private String id;
    private int size;
    private List<String> mapped_wwpns;


}
