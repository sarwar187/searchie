package edu.umass.cs.ciir.searchie.starter.feature_engineering;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.simulation_experiment.OptimalModelDump;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import gnu.trove.map.hash.TObjectFloatHashMap;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Oracle {
    public String prefix = StaticVariables.aquaint_prefix;
    public String defaultCRFSuiteBinary;
    public TObjectFloatHashMap<String> quantizedOptimalMap;
    public TObjectFloatHashMap<String> optimalMap;
    Random r;
    public Oracle(String crfsuite) {
        defaultCRFSuiteBinary = crfsuite;
        optimalMap = new TObjectFloatHashMap<>();
        r = new Random();
    }

    /*public int numClusterFeatures(){
        for(String key: quantizedOptimalMap.keySet()){
            if(key.contains("c["))
        }
    }*/

    public void createOracle(String featureDirectory, String featureFile, String etype) throws IOException {
        String optimalFile;
        if (etype.equals("PER"))
            optimalFile = "dump_per.out";
        else if (etype.equals("LOC"))
            optimalFile = "dump_loc.out";
        else if(etype.equals("ORG"))
            optimalFile = "dump_org.out";
        else
            optimalFile = featureFile + ".out";

        try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
            OptimalModelDump learner = new OptimalModelDump(tmpdir, defaultCRFSuiteBinary, new File(optimalFile));
            learner.setModel("lbfgs");
            learner.setFlag(0);
            quantizedOptimalMap = OptimalModelDump.parseCRFSuiteModelDump(new File(featureDirectory + optimalFile), etype);

            ArrayList<QFeatureWeight> optimalWeightList = new ArrayList<>();
            for (String key : quantizedOptimalMap.keySet()) {
                float weight = quantizedOptimalMap.get(key);
                optimalWeightList.add(new QFeatureWeight(key, weight));
                optimalMap.put(key, weight);
            }
            quantizedOptimalMap.clear();
            Collections.sort(optimalWeightList, (o1, o2) -> Double.compare(o1.getWeight(), o2.getWeight()));
            Quantizer q = new Quantizer();
            q.equalBucket(optimalWeightList, StaticVariables.maxLabel);
            for (QFeatureWeight f : optimalWeightList) {
                quantizedOptimalMap.put(f.getFeature(), (float) f.getQuantized());
            }
            optimalWeightList.clear();
        }
    }

    public int getErrorLabel(String feature){
        int label = (int) quantizedOptimalMap.get(feature);
        return implementError(label, r, StaticVariables.labelError);
    }

    int implementError(int label_local, Random r, int labelError) {
        int val = label_local;
        int test = r.nextInt(2);
        if(test==0)
            val = label_local + labelError;
        else
            val = label_local - labelError;
        if (val < 0) val+= (2*labelError);
        if (val > 10) val-= (2*labelError);
        return val;
    }
    public TObjectFloatHashMap getOptimalMap(){
        return optimalMap;
    }

    public int getLabel(String feature) {
        if(quantizedOptimalMap.containsKey(feature))
            return (int) quantizedOptimalMap.get(feature);
        else
            return -1;
    }
}

