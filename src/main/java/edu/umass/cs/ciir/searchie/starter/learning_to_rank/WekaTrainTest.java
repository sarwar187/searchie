package edu.umass.cs.ciir.searchie.starter.learning_to_rank;

import ciir.jfoley.chai.collections.Pair;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import org.lemurproject.galago.utility.Parameters;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.bayes.NaiveBayesUpdateable;
import weka.classifiers.functions.Logistic;
import weka.classifiers.meta.AdaBoostM1;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.supervised.instance.Resample;
import weka.filters.unsupervised.attribute.Remove;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by sarwar on 7/7/17.
 */
public class WekaTrainTest {
    public static void main(String args[]) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        try{
            //Scanner sc = new Scanner(new File("/mnt/nfs/work1/smsarwar/searchie/ltr/train/train.csv")
            String train_file = "/home/sarwar/crfsuite/sydney/weka_test_fold1/train/train.arff";
            //String train_file = "/home/sarwar/crfsuite/sydney/1000/fold5/train/train.csv";
            String test_directory = "/home/sarwar/crfsuite/sydney/weka_test_fold1/test/";

            ConverterUtils.DataSource sourceTrain;
            //sourceTrain = new ConverterUtils.DataSource("/home/sarwar/crfsuite/ltr/train.arff");
            sourceTrain = new ConverterUtils.DataSource(train_file);
            //sourceTrain = new ConverterUtils.DataSource("/mnt/nfs/work1/smsarwar/searchie/ltr/train/train.arff");
            Instances train = sourceTrain.getDataSet();
            String[] options = new String[2];
            options[0] = "-R";                                    // "range"
            options[1] = "1,2";                                     // first attribute
            Remove remove = new Remove();                         // new instance of filter
            remove.setOptions(options);                           // set options
            remove.setInputFormat(train);                          // inform filter about dataset **AFTER** setting options
            Instances newData = Filter.useFilter(train, remove);   // apply filter
            System.out.println("number of attributes " + newData.numAttributes());
            System.out.println(newData.firstInstance());
            if (newData.classIndex() == -1) {
                newData.setClassIndex(newData.numAttributes() - 1);
            }
            //AdaBoostM1 cls = new AdaBoostM1();
            Logistic cls = new Logistic();
            //NaiveBayes naiveBayes = (NaiveBayes) this.classifier;
            //AbstractClassifier cls = (AbstractClassifier) Class.forName("weka.classifiers.functions.LibSVM" ).newInstance();
            //LibSVM cls = new LibSVM();
            //NaiveBayesUpdateable cls = new NaiveBayesUpdateable();
            Resample sampler = new Resample();
            String Fliteroptions="-B 1.0";
            sampler.setOptions(weka.core.Utils.splitOptions(Fliteroptions));
            sampler.setRandomSeed((int)System.currentTimeMillis());
            sampler.setInputFormat(newData);
            newData = Resample.useFilter(newData, sampler);

            Evaluation eval = new Evaluation(newData);
            eval.crossValidateModel(cls, newData, 10, new Random(1));
            System.out.println("CROSS VALIDATION");
            System.out.println(eval.toSummaryString("\nResults\n======\n", false));
            System.out.println("precision");
            System.out.println(eval.precision(0));
            System.out.println("recall");
            System.out.println(eval.recall(0));
            System.out.println("confusion matrix");
            System.out.println(eval.toMatrixString());
            cls.setRidge(1e5);
            cls.buildClassifier(newData);


            //System.out.println("ridge " + cls.getRidge());
            File dir = new File(test_directory);
            File[] directoryListing = dir.listFiles();
            double AP = 0;
            int countFile = 0;
            if (directoryListing != null) {
                for (File child : directoryListing) {
                    if(child.getAbsolutePath().contains("arff")) {
                        ArrayList<Pair<Integer, Double>> sortedList = new ArrayList<>();
                        ConverterUtils.DataSource sourceTest = new ConverterUtils.DataSource(child.getAbsolutePath());
                        Instances initial = sourceTest.getDataSet();
                        Instances test = Filter.useFilter(initial, remove);   // apply filter
                        test.setClassIndex(test.numAttributes() - 1);
                        eval = new Evaluation(newData);
                        eval.evaluateModel(cls, test);
                        System.out.println(child.getAbsolutePath());
                        System.out.println(eval.toSummaryString("\nResults\n======\n", false));
                        System.out.println("precision");
                        System.out.println(eval.precision(0));
                        System.out.println("recall");
                        System.out.println(eval.recall(0));
                        System.out.println("confusion matrix");
                        System.out.println(eval.toMatrixString());
                        int a[] = new int[test.numInstances()];
                        for (int i = 0; i < test.numInstances(); i++) {
                            int sentenceID = (int)initial.instance(i).value(1);
                            double pred = cls.classifyInstance(test.instance(i));
                            String trueClassLabel =
                                    test.instance(i).toString(test.classIndex());
                            double predictionIndex = cls.classifyInstance(test.instance(i));
                            String predictedClassLabel = test.classAttribute().value((int) predictionIndex);
                            double[] predictionDistribution = cls.distributionForInstance(test.instance(i));
                            //if(i<=20)
                                //System.out.println(predictionDistribution[0] + "\t" + predictionDistribution[1] + "\t" + predictedClassLabel + "\t" + trueClassLabel);
                            //sortedList.add(Pair.of(Integer.parseInt(trueClassLabel), predictionDistribution[0]));

                            sortedList.add(Pair.of(Integer.parseInt(trueClassLabel), Double.parseDouble(predictedClassLabel)));
                        }
                        Collections.sort(sortedList, (o1, o2) -> Double.compare(o2.right.doubleValue(), o1.right.doubleValue()));
                        List<Boolean> list = new ArrayList<>();
                        int count = 0;
                        int i = 0;
                        for(Pair p: sortedList){
                            //if(count==20)
                            //    break;
                            //System.out.println(p.getKey() + "\t" + p.getValue());
                            if((int)p.getKey() == 1) {
                                list.add(true);
                                a[i] = 1;
                                count++;
                            }
                            else
                                list.add(false);

                            i++;
                            //printResults.printSimpleToken(mergedList.get((int) p.getKey()), "sentence");
                        }
                        AP+= Metrics.computeAP(list, count);
                        //AP+= Metrics.computeMap(a);
                        // Do something with child
                        countFile++;
                    }

                }
                System.out.println("Mean Average Precision " + AP/countFile);

            }
            /*Resample sampler = new Resample();
            String Fliteroptions="-B 1.0";
            sampler.setOptions(weka.core.Utils.splitOptions(Fliteroptions));
            sampler.setRandomSeed((int)System.currentTimeMillis());
            sampler.setInputFormat(newData);
            newData = Resample.useFilter(newData, sampler);*/
            //Normalize normalize = new Normalize();
            //normalize.toSource(Fliteroptions, newData);
            //Remove remove = new Remove();                         // new instance of filter
            //remove.setOptions(options);                           // set options
            //remove.setInputFormat(train);                          // inform filter about dataset **AFTER** setting options
            //Instances newData = Filter.useFilter(train, remove);   // apply filter

            //rm.setAttributeIndices("2");
            //rm.setAttributeIndices("3");
            //rm.setAttributeIndices("4");
            //rm.setAttributeIndices("5");
            //rm.setAttributeIndices("6");



            //rm.setAttributeIndices("6");
            //rm.setAttributeIndices("5");


            //Remove rm = new Remove();
            //rm.setInputFormat(train);
            //rm.setAttributeIndices("1");
            //FilteredClassifier fc = new FilteredClassifier();
            //cls.setOptions(args);
            //J48 cls = new J48();
            //LibSVM cls = new LibSVM();
            //SMO cls = new SMO();
            //BayesianLogisticRegression cls = new BayesianLogisticRegression();
            //cls.setThreshold(0.52);
            //AdaBoostM1 cls = new AdaBoostM1();
            //NaiveBayes cls = new NaiveBayes();
            //weka.classifiers.meta.Bagging cls = new Bagging();
            //weka.classifiers.functions.IsotonicRegression cls = new IsotonicRegression();
            //j48.setUnpruned(true);        // using an unpruned J48
            // meta-classifier

            //BayesNet cls = new BayesNet();
            //RandomForest cls = new RandomForest();
            //cls.setNumTrees(100);
            //cls.setMaxDepth(3);
            //cls.setNumFeatures(3);

            //fc.setClassifier(cls);
            //fc.setFilter(rm);

            // train and make predictions
            //System.out.println(fc.globalInfo());
            //System.out.println(fc.getFilter());
            //fc.buildClassifier(train);
            //Evaluation eval = new Evaluation(newData);
            //Random rand = new Random(1);  // using seed = 1
            //int folds = 2;
            //eval.crossValidateModel(cls, newData, folds, rand);
            //System.out.println(eval.toSummaryString());
            //System.out.println("precision on buy " + eval.precision(newData.classAttribute().indexOfValue("buy")));

            //System.out.println("recall on buy " + eval.recall(newData.classAttribute().indexOfValue("buy")));
            //System.out.println(eval.confusionMatrix().toString());
            //System.out.println("Precision " + eval.precision(newData.classIndex()-1));
            //System.out.println("Recall " + eval.recall(newData.classIndex()-1));
            //Classfier cls = new weka.classifiers.bayes.NaiveBayes();
            //FilteredClassifier fc = new FilteredClassifier();
            //fc.setFilter(rm);
            //fc.setClassifier(cls);

            //train and make predictions
            //fc.buildClassifier(train);

            // serialize model

            //ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/home/sarwar/crfsuite/ltr/train.model"));
            //ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/mnt/nfs/work1/smsarwar/searchie/ltr/train.model"));
            //oos.writeObject(cls);
            //oos.flush();
            //oos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
