package edu.umass.cs.ciir.searchie.starter.Test.document_example;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class QueryFileParser {

    //    Gson gson = new Gson();
//    JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/Desktop/volume.json"), JsonElement.class);
//    System.out.println(jsonElement);
//    VolumeContainer vc = gson.fromJson(jsonElement.toString(), VolumeContainer.class);
//    System.out.println(vc.vol);
//    System.out.println(vc.volumes.get(0).getName());
//    System.out.println(vc.volumes.get(0).getManaged());
//    "queries" is a top-level list
    //"qid" has the query id we've been using
    //"text" has the question
    public static void main(String args[]) throws FileNotFoundException {
        //Scanner sc = new Scanner(new File("/home/smsarwar/trecqalist.json"));
        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/trecqalist.json"), JsonElement.class);
        //System.out.println(jsonElement.toString());
        QueryContainer queryContainer = gson.fromJson(jsonElement.toString(), QueryContainer.class);
        System.out.println(queryContainer.queries.get(0).qid);
        List<Judgment> judgments = queryContainer.queries.get(0).judgments;
//        List<Judgment> judgements = queryContainer.queries.get(0).judgements;
        System.out.println(judgments.size());
        for(Judgment judgement: judgments){
            if(judgement.correctness==1) {
                System.out.println(judgement.getCandidate());
                System.out.println(judgement.getCorrectness());
            }
        }
        //QueryContainer queryContainer = gson.fromJson(br, QueryContainer.class);
        //System.out.println(queryContainer.getQid() + "\t" + queryContainer.getText());
    }

//    public static String fileToQueryString(String inputFileName, String queryFileLocation) throws IOException {
//        Gson gson = new GsonBuilder()
//                .setLenient()
//                .create();
//
//        GZIPInputStream gzip = new GZIPInputStream(new FileInputStream(queryFileLocation));
//        BufferedReader br = new BufferedReader(new InputStreamReader(gzip));
//        //br.readLine();
//        //JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/trecqalist.json"), JsonElement.class);
//        JsonElement jsonElement = gson.fromJson(br, JsonElement.class);
//        //System.out.println(jsonElement.toString());
//        QueryContainer queryContainer = gson.fromJson(jsonElement.toString(), QueryContainer.class);
//        for(Query q: queryContainer.queries){
//            if(inputFileName.contains(q.qid)) {
//                //System.out.print("Original List Query: ");
//                //System.out.println(q.getText());
//                return q.getText();
//            }
//        }
//        return "";
//    }

    public static String fileToQueryString(String inputFileName, String queryFileLocation) throws IOException {
        queryFileLocation = "/home/smsarwar/trecqa_list_query_lemmatized";
        Scanner sc = new Scanner(new File(queryFileLocation));
        HashMap hashMap = new HashMap<String, String>();
        while(sc.hasNextLine()){
            String str = sc.nextLine();
            //System.out.println(str);
            String strArray[] = str.split("\t");
            hashMap.put(strArray[0], strArray[1]);
        }
        return (String) hashMap.get(inputFileName);
    }

    public static Set<List<String>> fileToJudgments(String inputFileName, String queryFileLocation) throws IOException {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        GZIPInputStream gzip = new GZIPInputStream(new FileInputStream(queryFileLocation));
        BufferedReader br = new BufferedReader(new InputStreamReader(gzip));
        //br.readLine();
        //JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/trecqalist.json"), JsonElement.class);
        JsonElement jsonElement = gson.fromJson(br, JsonElement.class);
        //System.out.println(jsonElement.toString());
        HashSet<List<String>> hashSet = new HashSet<>();
        QueryContainer queryContainer = gson.fromJson(jsonElement.toString(), QueryContainer.class);

        for(Query q: queryContainer.queries){
            if(inputFileName.contains(q.qid)) {
                //System.out.print("Original List Query: ");
                //System.out.println(q.getText());
                List<Judgment> judgments = q.judgments;
                for(String answer: q.getAnswers()){
                    List<String> stringList = new ArrayList<>();
                    StringTokenizer st = new StringTokenizer(answer," ");
                    while(st.hasMoreTokens()){
                        stringList.add(st.nextToken());
                    }
                    hashSet.add(stringList);
                    //System.out.println(judgement.getCorrectness());
                }
            }
        }
        //System.out.println(hashSet.size());
        return hashSet;
    }
}
