package edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class LuceneInMemory {
    RAMDirectory idx;
    IndexWriter writer;

    public LuceneInMemory() {
        try {
            this.idx = new RAMDirectory();
            StandardAnalyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig((Analyzer)analyzer);
            iwc.setSimilarity((Similarity)new BM25Similarity(1.2f, 0.75f));
            this.writer = new IndexWriter((Directory)this.idx, iwc);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void addTokenList(Integer id, List<SimpleToken> tokenList) {
        String indexString = "";
        for (SimpleToken token : tokenList) {
            indexString = indexString + token.lemma;
            indexString = indexString + " ";
        }
        indexString = indexString.trim();
        try {
            this.writer.addDocument((Iterable)LuceneInMemory.createDocument(id, indexString));
            this.writer.commit();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    //SHAHRZAD
    public void addAbstract(Integer id, String abstractString) {
        abstractString = abstractString.trim();
        try {
            this.writer.addDocument((Iterable)LuceneInMemory.createDocument(id, abstractString));
            this.writer.commit();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void close() {
        this.idx.close();
    }

    //SHAHRZAD
    public List<Integer> search(String queryString, int topk) {
        ArrayList<Integer> l = new ArrayList<Integer>();
        String text = queryString.trim();
        try {
            DirectoryReader indexReader = DirectoryReader.open((Directory)this.idx);
            IndexSearcher indexSearcher = new IndexSearcher((IndexReader)indexReader);
            indexSearcher.setSimilarity((Similarity)new BM25Similarity(1.2f, 0.75f));
            QueryParser queryParser = new QueryParser("contents", (Analyzer)new StandardAnalyzer());
            Query query = queryParser.parse(QueryParser.escape((String)text));
            TopDocs topDocs = indexSearcher.search(query, topk);
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document document = indexSearcher.doc(scoreDoc.doc);
                l.add(Integer.parseInt(document.get("id")));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return l;
    }


    public List<Integer> search(List<SimpleToken> q) {
        String text = "";
        ArrayList<Integer> l = new ArrayList<Integer>();
        for (SimpleToken token : q) {
            text = text + token.lemma;
            text = text + " ";
        }
        text = text.trim();
        try {
            DirectoryReader indexReader = DirectoryReader.open((Directory)this.idx);
            IndexSearcher indexSearcher = new IndexSearcher((IndexReader)indexReader);
            indexSearcher.setSimilarity((Similarity)new BM25Similarity(1.2f, 0.75f));
            QueryParser queryParser = new QueryParser("contents", (Analyzer)new StandardAnalyzer());
            Query query = queryParser.parse(QueryParser.escape((String)text));
            TopDocs topDocs = indexSearcher.search(query, StaticVariables.sizeOfInitialRetrieval);
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document document = indexSearcher.doc(scoreDoc.doc);
                l.add(Integer.parseInt(document.get("id")));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return l;
    }

    public List<SearchResult> search(List<SimpleToken> q, String test) {
        String text = "";
        ArrayList<SearchResult> l = new ArrayList<SearchResult>();
        for (SimpleToken token : q) {
            text = text + token.lemma;
            text = text + " ";
        }
        text = text.trim();
        try {
            DirectoryReader indexReader = DirectoryReader.open((Directory)this.idx);
            IndexSearcher indexSearcher = new IndexSearcher((IndexReader)indexReader);
            indexSearcher.setSimilarity((Similarity)new BM25Similarity(1.2f, 0.75f));
            QueryParser queryParser = new QueryParser("contents", (Analyzer)new StandardAnalyzer());
            Query query = queryParser.parse(QueryParser.escape((String)text));
            TopDocs topDocs = indexSearcher.search(query, StaticVariables.sizeOfInitialRetrieval);
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document document = indexSearcher.doc(scoreDoc.doc);
                l.add(new SearchResult(Integer.parseInt(document.get("id")), (double) scoreDoc.score));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return l;
    }


    public List<Integer> termBoostedSearchWithoutCarret(List<SimpleToken> q) {
        String text = "";
        ArrayList<Integer> l = new ArrayList<Integer>();
        HashMap<String, Integer> hashMap = new HashMap<>();

        for (SimpleToken token : q){
            if(hashMap.containsKey(token.lemma)){
                hashMap.put(token.lemma, hashMap.get(token.lemma) + 1);
            }
            else
                hashMap.put(token.lemma, 1);
        }

        //At first I create a boolean query builder
        BooleanQuery.Builder booleanQuery = new BooleanQuery.Builder();
        for (SimpleToken token : q) {
            Query query = new TermQuery(new Term("contents", token.lemma));
            BoostQuery boostQuery = new BoostQuery(query, 1f);
            booleanQuery.add(boostQuery, BooleanClause.Occur.SHOULD);
        }

        try {
            DirectoryReader indexReader = DirectoryReader.open((Directory)this.idx);
            IndexSearcher indexSearcher = new IndexSearcher((IndexReader)indexReader);
            indexSearcher.setSimilarity((Similarity)new BM25Similarity(1.2f, 0.75f));
            QueryParser queryParser = new QueryParser("contents", (Analyzer)new StandardAnalyzer());
            Query query = queryParser.parse(QueryParser.escape((String)text));
            TopDocs topDocs = indexSearcher.search(booleanQuery.build(), StaticVariables.sizeOfInitialRetrieval);
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document document = indexSearcher.doc(scoreDoc.doc);
                l.add(Integer.parseInt(document.get("id")));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return l;
    }

    public Map<Integer, Double> search(List<SimpleToken> q, int retrievalSize) {
        String text = "";
        HashMap<Integer, Double> rankedPred = new HashMap<Integer, Double>();
        for (SimpleToken token : q) {
            text = text + token.lemma;
            text = text + " ";
        }
        text = text.trim();
        try {
            DirectoryReader indexReader = DirectoryReader.open((Directory)this.idx);
            IndexSearcher indexSearcher = new IndexSearcher((IndexReader)indexReader);
            indexSearcher.setSimilarity((Similarity)new BM25Similarity(1.2f, 0.75f));
            QueryParser queryParser = new QueryParser("contents", (Analyzer)new StandardAnalyzer());
            Query query = queryParser.parse(QueryParser.escape((String)text));
            TopDocs topDocs = indexSearcher.search(query, retrievalSize);
            double maxScore = 0.0;
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document document = indexSearcher.doc(scoreDoc.doc);
                double score = scoreDoc.score;
                if (score > maxScore) {
                    maxScore = score;
                }
                rankedPred.put(Integer.parseInt(document.get("id")), new Double(score));
            }
            rankedPred.put(-1, maxScore);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return rankedPred;
    }

    public Map<Integer, Double> normalizedSearchScore(List<SimpleToken> q, int retrievalSize) {
        String text = "";
        HashMap<Integer, Double> rankedPred = new HashMap<Integer, Double>();
        for (SimpleToken token : q) {
            text = text + token.lemma;
            text = text + " ";
        }
        text = text.trim();
        try {
            DirectoryReader indexReader = DirectoryReader.open((Directory)this.idx);
            IndexSearcher indexSearcher = new IndexSearcher((IndexReader)indexReader);
            indexSearcher.setSimilarity((Similarity)new BM25Similarity(1.2f, 0.75f));
            QueryParser queryParser = new QueryParser("contents", (Analyzer)new StandardAnalyzer());
            Query query = queryParser.parse(QueryParser.escape((String)text));
            TopDocs topDocs = indexSearcher.search(query, retrievalSize);
            double maxScore = 0.0;
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document document = indexSearcher.doc(scoreDoc.doc);
                double score = scoreDoc.score;
                if (score > maxScore) {
                    maxScore = score;
                }
                rankedPred.put(Integer.parseInt(document.get("id")), new Double(score));
            }
            rankedPred.put(-1, maxScore);
            for (Integer i : rankedPred.keySet()) {
                rankedPred.put(i, rankedPred.get(i) / maxScore);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return rankedPred;
    }

    private static Document createDocument(Integer id, String content) {
        Document doc = new Document();
        FieldType fieldTypeText = new FieldType();
        fieldTypeText.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        fieldTypeText.setStoreTermVectors(true);
        fieldTypeText.setStoreTermVectorPositions(true);
        fieldTypeText.setTokenized(true);
        fieldTypeText.setStored(true);
        fieldTypeText.freeze();
        doc.add((IndexableField)new Field("contents", content, fieldTypeText));
        doc.add((IndexableField)new Field("id", String.valueOf(id), fieldTypeText));
        return doc;
    }
}
