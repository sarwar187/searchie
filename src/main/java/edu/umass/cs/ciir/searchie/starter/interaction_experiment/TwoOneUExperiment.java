package edu.umass.cs.ciir.searchie.starter.interaction_experiment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.DocumentContainer;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.QueryFileParser;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Sentence;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Token;
import edu.umass.cs.ciir.searchie.starter.chiir2018.BatchHandler;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.Evaluation;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.lemurproject.galago.core.eval.stat.Stat;
import org.lemurproject.galago.core.tools.Search;
import org.lemurproject.galago.utility.Parameters;

import java.io.*;
import java.net.InetAddress;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import static edu.umass.cs.ciir.searchie.starter.StaticVariables.printFlag;

public class TwoOneUExperiment {
    private static final Logger logger = Logger.getLogger(TwoOneUExperiment.class.getName());
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args) throws IOException {
        logger.setLevel(Level.FINE);
        StaticVariables.threshold = 0.1;
        StaticVariables.expansionRM3 = 1;
        int embeddingSize = StaticVariables.embedding_size;
        LuceneInMemory lucene = new LuceneInMemory();
        PrintResults pr = new PrintResults();

        //Parameter handling
        Parameters argp = Parameters.parseArgs(args);
        String etype = argp.get("class", "LIST");
        String inputDirectory = argp.get("dir", "/home/smsarwar/");
        String inputFileName = argp.get("file", "trecqalist_99.1_1000.jsonl.gz");
        String hostName = InetAddress.getLocalHost().getHostName();
        HashMap<String, Integer> bootstrapHashMap = BatchHandler.loadBootstrapSentence();
        int bootstrapSentenceId = bootstrapHashMap.get(inputFileName);
        //System.out.println("inputFileName");
        String list_query = QueryFileParser.fileToQueryString(inputFileName, "/home/smsarwar/trecqalist.json.gz");
        HashMap<String, List<String>> hashMap = BatchHandler.loadBatch(0);


        double previousScore = 0;
        Map<Integer, String> senteceToDocumentMapping = new HashMap<>();

        //Data loading
        List<List<SimpleToken>> fullConllTrain = new ArrayList<>();
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        IDF idf = new IDF();
        int row = 0;
        GZIPInputStream gzip = new GZIPInputStream(new FileInputStream(inputDirectory + inputFileName));
        BufferedReader br = new BufferedReader(new InputStreamReader(gzip));

        while(true){
            String jsonString = br.readLine();
            if(jsonString == null)
                break;
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
            DocumentContainer documentContainer = gson.fromJson(jsonElement.toString(), DocumentContainer.class);
            if (documentContainer.score == previousScore)
                continue;
            previousScore = documentContainer.score;
            for (Sentence sentence : documentContainer.sentences) {
                List<SimpleToken> sList = new ArrayList<>();
                for (Token token : sentence.tokens) {
                    sList.add(SimpleToken.convertTokenToSimpleToken(token));
                }
                fullConllTrain.add(sList);
                senteceToDocumentMapping.put(row++, documentContainer.doc);
            }
        }

        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }

        ArrayList<List<SimpleToken>> mergedList = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> positives = new ArrayList<List<SimpleToken>>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);

        String output="";
        positives.add(mergedList.get(bootstrapSentenceId));
        SentenceEmbedding sm = new SentenceEmbedding();
        if(hostName.equals("brooloo"))
            sm.setEnvironment(hostName);
        sm.loadWordEmbedding(idf);
        FeatureExtractor f = new FeatureExtractor((List)mergedList.get(bootstrapSentenceId));
        String targetEntityType = f.getTargetEntityType();
        Evaluation eval = new Evaluation();
        //String expansionApproach = StaticVariables.expansionApproachFull;
        //String expansionParameter = StaticVariables.expansionParameterPRF;
        //String expansionApproach = StaticVariables.expansionApproachNone;
        //String expansionParameter = StaticVariables.expansionParameterNone;
        String expansionApproach = StaticVariables.expansionApproachEmbedding;
        //String expansionParameter = StaticVariables.expansionParametertopk;
        String expansionParameter = StaticVariables.expansionParameterPRF;

        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        int topk = StaticVariables.topk;
        EmbeddingSearch embeddingSearch = new EmbeddingSearch(mergedList, fullPositives, embeddingSize, etype, inputFileName, lucene, sm);

        double configuration [][] = {{0.4, 0.4, 0.2}};

        String newFileName = inputFileName.substring(0, inputFileName.length() - 8) + "test.result";
        BufferedWriter br1 = new BufferedWriter(new FileWriter(new File("/mnt/nfs/work1/smsarwar/searchie/ltr_sbatch/fold5/test/" + newFileName)));

        for(int i = 0; i<configuration.length; i++) {
            Set<List<String>> foundSpans = new HashSet<>();
            DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootstrapSentenceId), etype);
            double alpha = configuration[i][0]; //uniform
            double beta = configuration[i][1]; //entity, automatically derive gamma
            HashMap<String, Double> queryModelMap = BatchHandler.getQueryModelMap(list_query, bootstrapSentenceId, mergedList, hashMap, alpha, beta);
            //System.out.println(Arrays.stream(queryModel).sum());
            //embeddingSearch.setQueryModel(queryModel);
            embeddingSearch.setQueryModelMap(queryModelMap);
            //This function is not doing any query expansion now
            List<SearchResult> expandedSentencesQuery = embeddingSearch.queryExpansion(bootstrapSentenceId, expansionApproach, expansionParameter, availableSpans, targetEntityType, topk);
            //alpha for entity query model, beta for context query model, gamma for uniform query model
            List<SearchResult> results = embeddingSearch.search(bootstrapSentenceId);
            //embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, "none");
            br1.write(inputFileName);
            br1.newLine();
            for (int p = 0; p < results.size(); p++) {
                br1.write(String.valueOf(results.get(p).getSentenceId()) + "\t" + String.valueOf(results.get(p).getScore()) + "\t" + mergedList.get(results.get(p).getSentenceId()).size());
                //br1.write(String.valueOf(results.get(p).getSentenceId()));
                br1.newLine();
            }
            br1.close();


            embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, "none");
            //embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, StaticVariables.luceneScore);
            //embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, "none");

//            for (int p = 0; p < results.size(); p++) {
//                br1.write(String.valueOf(results.get(p).getSentenceId()) + "\t" + String.valueOf(results.get(p).getScore()));
//                br1.newLine();
//            }

            results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
//            String queryString = QueryFileParser.fileToQueryString(inputFileName, "/home/smsarwar/trecqalist.json.gz");
//            String[]  queryStringArray = queryString.split(" ");
//            ArrayList<String> queryStringList = new ArrayList<>();
//            for(String str: queryStringArray) queryStringList.add(str);
//            List<Double> listQueryEmbedding = sm.getSentenceEmbedding(queryStringList);
//
//            double listQueryEmbeddingPoint[][] = new double[1][StaticVariables.embedding_size];
//            for(int index = 0; index < StaticVariables.embedding_size; index++){
//                listQueryEmbeddingPoint[0][index] = listQueryEmbedding.get(i).doubleValue();
//            }

            logger.info("###############################################################################");
            logger.info("List Query");
            logger.info(QueryFileParser.fileToQueryString(inputFileName, "/home/smsarwar/trecqalist.json.gz"));
            logger.info("Query Sentence");
            logger.info(pr.printSimpleTokenSimply(mergedList.get(bootstrapSentenceId), ""));
            logger.info("Search Results");


            for (int p = 0; p < 10; p++) {
                logger.info(pr.printSimpleTokenSimply(mergedList.get(results.get(p).getSentenceId()), ""));
            }
            //results.sort((o1, o2) -> o2.getScoreEntityContext().compareTo(o1.getScoreEntityContext()));
            eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
            if (eval.getNumberOfTargetEntities() != 1) {
                output += eval.getEvaluationScoresString() + "\t"; // + eval.getFoundSpansString();


                output += "\t" + inputFileName + "\t" + targetEntityType + "\t" + expandedSentencesQuery.size() + "\t";
                output += QueryFileParser.fileToQueryString(inputFileName, "/home/smsarwar/trecqalist.json.gz");
                //System.out.println(inputFileName);
                //System.out.println(printAll(inputFileName, bootstrapSentenceId, embeddingSearch.getQueryRepresentation(bootstrapSentenceId)));
                System.out.println(output + "\t" + sm.getNumberOfMissingTokens());
                logger.fine(output);
            }
            logger.info("************************************************************************************");
        }
    }

    static String printAll(String inputFileName, int bootStrapSentenceId, double[] rmQueryEmbedding){
        String output = "";
        output+=inputFileName+"#"+String.valueOf(bootStrapSentenceId) + "\t";
        for(Double d: rmQueryEmbedding){
            output+=String.valueOf(d) + " ";
        }
        output=output.trim();
        return output;
    }
}

