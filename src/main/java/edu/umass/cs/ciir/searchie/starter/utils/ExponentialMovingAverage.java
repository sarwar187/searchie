package edu.umass.cs.ciir.searchie.starter.utils;

import java.util.Collections;

class ExponentialMovingAverage {



    private double alpha;

    private Double oldValue;
    public ExponentialMovingAverage(double alpha) {
        this.alpha = alpha;
    }

    public double average(double value) {
        if (oldValue == null) {
            oldValue = value;
            return value;
        }
        double newValue = oldValue + alpha * (value - oldValue);
        oldValue = newValue;
        return newValue;
    }

    public static void main(String args[]){
        ExponentialMovingAverage em = new ExponentialMovingAverage(0.9);
        //em.oldValue = 2.0;
        double init = 1.0;
        double a[] = {0.995, 0.993, 0.992, 0.981, 0.980};
        //Collections.reverse(ArrayList<>);
        for(int i=0; i<5; i++) {
            System.out.println(Math.exp(a[i]));
            init = em.average(Math.exp(a[i]));
            System.out.println(init);
        }
    }
}