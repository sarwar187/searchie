package edu.umass.cs.ciir.searchie.starter;

import gnu.trove.map.hash.TObjectFloatHashMap;

import java.security.PublicKey;

/**
 * Created by sarwar on 4/29/17.
 */
public class TokenCount {
    public double getRetrieved() {
        return retrieved;
    }

    public void setRetrieved(double retrieved) {
        this.retrieved = retrieved;
    }

    double retrieved;

    public double getSuccesful() {
        return succesful;
    }

    public void setSuccesful(double succesful) {
        this.succesful = succesful;
    }

    double succesful;

    public TokenCount(double retrieved, double succesful){
        this.retrieved = retrieved;
        this.succesful = succesful;
    }

    public void incrementSuccessfulByOne(){
        this.succesful++;
    }

    public void incrementRetrievedByOne(){
        this.retrieved++;
    }
}
