package edu.umass.cs.ciir.searchie.starter.utils;

import java.util.HashMap;

public class QueryModel {
    public static void normalizeQueryModel(HashMap<String, Double> queryModel, double divisor){
        for(String key: queryModel.keySet()){
            queryModel.put(key, queryModel.get(key)/divisor);
        }
    }

    public static void mergeQueryModel(HashMap<String, Double> finalQueryModel, HashMap<String, Double> tempQueryModel, double weight){
        for(String key: tempQueryModel.keySet()){
            if(finalQueryModel.containsKey(key)){
                finalQueryModel.put(key, finalQueryModel.get(key) + weight * tempQueryModel.get(key));
            }else{
                finalQueryModel.put(key, weight * tempQueryModel.get(key));
            }

        }
    }
}
