package edu.umass.cs.ciir.searchie.starter.interaction_experiment_new;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;


//Span Based AddPRF
//

public class SimpleCRF {
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "1"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int loop = StaticVariables.numRelevanceFeedbacks; // loop is the number of relevance feedback we take
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        /*if(fullPositives.size() <=StaticVariables.minPositivestoTrain)
            return;
        if(fullNegatives.size()>=StaticVariables.maxNumSentence)
            return;*/
        //System.out.println(fullPositives.size() + "\t" + fullNegatives.size() + "\t" + inputFileName + "\t");
        int numExperiments = fullPositives.size()/numPositives;  // number of rounds we calculate AP, uAP and F1
        double testAP=0d, testuAP=0d;
        numExperiments = (numExperiments > maximumNumberOfExperiments)?maximumNumberOfExperiments: numExperiments;
        ArrayList<Integer> configuration = new ArrayList<>();
        // pair of optimal and error (1,0) means that we are considering optimal weights and errorActivated
        // we need to send two parameters to state object. One thorugh isOptimal parameter and the other through
        // setError parameter
        configuration.add(1); //worse

        for(int numConfig=0; numConfig<configuration.size(); numConfig++) {
            SearchResult searchResult = null;
            for (int index = 0; index < numExperiments * numPositives; index += numPositives) { //iterating over 300 positives, taking three in each round
                List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
                List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
                ArrayList<SearchResult> resultsSeen = new ArrayList<>();
                DataSelector.splitTrainTest(fullPositives, fullNegatives, positives, fullConllTest, index, numPositives);
                fullConllTest = fullConllTrain;
                Set<String> availableTokens = DataSelector.getUniqueTokens(fullPositives, etype);
                Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
                //System.out.println("available tokens size " + availableTokens.size());
                Set<String> foundTokens = DataSelector.getUniqueTokens(positives, etype);

                try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                    CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                    learner.setModel(argp.get("model", "lbfgs"));
                    Parameters info = Parameters.create();
                    learner.learnFeatureWeights(fullPositives, etype, info);
                    LinearTokenClassifier rf;
                    //PRF.addPRF(fullConllTrain, positives, etype);
                    Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
                    int currentFoundSpanSize = foundSpans.size();
                    Set<Integer> alreadyAdded = new HashSet<>();
                    //PRF.addPRFWithSpan(fullConllTrain, foundSpans , positives, etype, alreadyAdded);
                    TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                    Map<String, TokenCount> retrievedTokenMap = new HashMap<>();
                    int unsuccessful = 0;
                    int i =0;
                    for ( ; i < loop; i++) {
                        rf = new LinearTokenClassifier(originalMap);
                        searchResult = Metrics.addRelevanceFeedback(rf, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                        originalMap = learner.learnFeatureWeights(positives, etype, info);
                        int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, resultsSeen.get(i).getSimpleTokens(), etype);
                        //changeWeight = true;
                        if (successful != 1) {
                            unsuccessful++;
                        }
                        if (unsuccessful > 2) {
                            StaticVariables.isUnique = StaticVariables.isUnique ^ true;
                            unsuccessful = 0;
                        }


                        /*for(List<SimpleToken> t: fullPositives){
                            if(t.equals(resultsSeen.get(i).getSimpleTokens())){
                                successful=1;
                            }
                        }*/

                        if (searchResult == null) {
                            System.out.println("broken");
                            break;
                        }
                        if(foundSpans.size()==availableSpans.size()) {
                            //System.out.println(i+ "\t" + inputFileName + "\t" + fullConllTest.size());
                            break;
                        }

                    }
                    System.out.println(i + "\t" +  availableSpans.size() + "\t" + foundSpans.size() + "\t" + inputFileName);
                }
            }
        }
    }

    public static Set<String> setMinus(Set<String> availableTokens, Set<String> foundTokens){
        Set<String> tempSet = new HashSet<>();
        tempSet.addAll(availableTokens);
        tempSet.removeAll(foundTokens);
        return tempSet;
    }


    public static int compareMaps(TObjectFloatHashMap<String> a, TObjectFloatHashMap<String> b){
        int flag = 0;
        for(String key: a.keySet()){
            if(b.containsKey(key)){
                if(a.get(key)!= b.get(key)) {
                    System.out.println("a values " + a.get(key));
                    System.out.println("b values " + b.get(key));

                    flag = 1;
                    //return flag;
                }
            }
        }
        return flag;
    }

    public static int foundInGroundTruth(Set<String> extrapolated, Set<String> groundTruthFeatures){
        int hit = 0;
        System.out.println("extrapolated tokens");
        if(extrapolated == null)
            return 0;
        for(String s: extrapolated){
            if(groundTruthFeatures.contains(s)){
                System.out.println(s);
                hit++;
            }
        }
        return hit;
    }
}
