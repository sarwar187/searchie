package edu.umass.cs.ciir.searchie.starter.interaction_experiment;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.Island;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EntityGraph;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 8/4/17.
 */
public class LTRTrainingUnique {
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args) throws IOException {
        double[] expandedSentencesWeight;
        int[] expandedSentences;
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", "/home/sarwar/crfsuite/");
        String inputFileName = argp.get("file", "train_list.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list_tr.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int[] occurrence = new int[100];
        PrintResults pr = new PrintResults();
        LuceneInMemory lucene = new LuceneInMemory();
        ArrayList<List<SimpleToken>> fullPositives = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> fullNegatives = new ArrayList<List<SimpleToken>>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);

            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }

        int bootstrapSentenceId = 0;
        ArrayList<List<SimpleToken>> mergedList = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> positives = new ArrayList<List<SimpleToken>>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);
        String output="";
        positives.add(mergedList.get(bootstrapSentenceId));
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        FeatureExtractor f = new FeatureExtractor((List)mergedList.get(bootstrapSentenceId));
        String targetEntityType = f.getTargetEntityType();
        //System.out.println(targetEntityType);
        //FeatureExtractor queryFeatureExtractor = new FeatureExtractor((List)mergedList.get(0));
        //String targetEntityType = queryFeatureExtractor.getTargetEntityType();
        //Set<List<String>> foundSpansCaseSensitive = DataSelector.getUniqueSpansCaseSensitive(positives, etype);
        //List<Double> targetEntityEmbedding = EmbeddingSearch.getEntityEmbedding(foundSpansCaseSensitive, sm);


        //Evaluation eval = new Evaluation();
        String expansionApproach = StaticVariables.expansionApproachEmbedding;
        String expansionParameter = StaticVariables.expansionParameterPRF;
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        int topk = 0;
        EmbeddingSearch embeddingSearch = new EmbeddingSearch(mergedList, fullPositives, embeddingSize, etype, inputFileName, lucene, sm);
        embeddingSearch.queryExpansion(bootstrapSentenceId,expansionApproach,expansionParameter,availableSpans, targetEntityType, topk);
        //Query Expansion Ends
        //Seaching with New Query Representation
        //System.out.println(sm.entityEmbeddingContextExpansion.size());
        List<SearchResult> results = embeddingSearch.search(bootstrapSentenceId);
        //List<SearchResult>
        //eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
        //if(eval.getNullEntities() == 1) return;
        //output+=eval.getEvaluationScoresString();
        //output+="\t";
        embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, "none");
        //embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, StaticVariables.luceneScore);
        embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, "none");
        //embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, StaticVariables.entityContextScore);
        //eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
        //output+=eval.getEvaluationScoresString();
        //System.out.println(output);
        BufferedWriter br= new BufferedWriter(new FileWriter(new File(featureDirectory + featureFileName)));
        NumberFormat formatter = new DecimalFormat("#0.000");
        Set<List<String>> foundSpans = new HashSet<>();
        Set<List<String>> querySpans = new HashSet<>();
        DataSelector.addNewSpansWithGroundTruth(querySpans, availableSpans, mergedList.get(bootstrapSentenceId), etype);
        DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootstrapSentenceId), etype);
        for (SearchResult s : results) {
            //positives.add(mergedList.get(s.getSentenceId()));
            //int successful = DataSelector.addNewSpansWithGroundTruth(constantSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            //foundSpans.addAll(constantSpans);
            //constantSpans.clear();
            //constantSpans = DataSelector.getUniqueSpans(positives, etype);
            String featureString = inputFileName + "," + s.getSentenceId() + ",";
            FeatureExtractor fe = new FeatureExtractor(mergedList.get(s.getSentenceId()));
            featureString += fe.posNERFeatures();
            //int containsQueryEntity = DataSelector.containsQueryEntity(mergedList.get(s.getSentenceId()), foundSpans);
            //featureString+= String.valueOf(containsQueryEntity) + ",";
            //List<SimpleToken> t = mergedList.get(i).get();
            if(successful==1) {
                //if(hashMap.containsKey(i)) {
                //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 1);
                featureString += (formatter.format(s.getScore().doubleValue()) +  "," + formatter.format(s.getLuceneScore().doubleValue()) + "," + formatter.format(s.getScoreEntity().doubleValue()) + "," + formatter.format(s.getScoreEntityContext().doubleValue()) + "," + mergedList.get(s.getSentenceId()).size() + "," + 1);
                //System.out.println(featureString);
                br.write(featureString);
                br.newLine();
                //}
            }else{
                //if(hashMap.containsKey(i)) {
                //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 0);
                featureString += (formatter.format(s.getScore().doubleValue()) + "," + formatter.format(s.getLuceneScore().doubleValue()) + "," + formatter.format(s.getScoreEntity().doubleValue()) + "," + formatter.format(s.getScoreEntityContext().doubleValue()) +  "," + mergedList.get(s.getSentenceId()).size() +"," + 0);
                //System.out.println(featureString);
                br.write(featureString);
                br.newLine();
                //}
            }
            //PrintResults pr = new PrintResults();
            //pr.printSimpleToken(mergedList.get(i), "input");

        }
        //System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + (1.0 * foundSpans.size()/availableSpans.size()));
        br.close();
    }

}
