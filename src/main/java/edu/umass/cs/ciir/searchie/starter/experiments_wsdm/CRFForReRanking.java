package edu.umass.cs.ciir.searchie.starter.experiments_wsdm;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.CRFSuiteLearner;
import edu.umass.cs.ciir.searchie.starter.LinearTokenClassifier;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.Evaluation;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 8/10/17.
 */
public class CRFForReRanking {
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args) throws IOException {
        double[] expandedSentencesWeight;
        int[] expandedSentences;
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", "/home/sarwar/crfsuite/");
        String inputFileName = argp.get("file", "train_list.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int[] occurrence = new int[100];
        PrintResults pr = new PrintResults();
        LuceneInMemory lucene = new LuceneInMemory();
        ArrayList<List<SimpleToken>> fullPositives = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> fullNegatives = new ArrayList<List<SimpleToken>>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);

            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }

        int bootstrapSentenceId = 0;
        ArrayList<List<SimpleToken>> mergedList = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> positives = new ArrayList<List<SimpleToken>>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);
        String output = "";
        positives.add(mergedList.get(bootstrapSentenceId));
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        FeatureExtractor f = new FeatureExtractor((List) mergedList.get(bootstrapSentenceId));
        String targetEntityType = f.getTargetEntityType();
        //System.out.println(targetEntityType);
        //FeatureExtractor queryFeatureExtractor = new FeatureExtractor((List)mergedList.get(0));
        //String targetEntityType = queryFeatureExtractor.getTargetEntityType();
        //Set<List<String>> foundSpansCaseSensitive = DataSelector.getUniqueSpansCaseSensitive(positives, etype);
        //List<Double> targetEntityEmbedding = EmbeddingSearch.getEntityEmbedding(foundSpansCaseSensitive, sm);
        Evaluation eval = new Evaluation();
        String expansionApproach = StaticVariables.expansionApproachEmbedding;
        String expansionParameter = StaticVariables.expansionParameterPRF;
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        int topk = 5;
        EmbeddingSearch embeddingSearch = new EmbeddingSearch(mergedList, fullPositives, embeddingSize, etype, inputFileName, lucene, sm);
        embeddingSearch.queryExpansion(bootstrapSentenceId, expansionApproach, expansionParameter, availableSpans, targetEntityType, topk);
        //Query Expansion Ends
        //Seaching with New Query Representation
        //System.out.println(sm.entityEmbeddingContextExpansion.size());
        List<SearchResult> results = embeddingSearch.search(bootstrapSentenceId);
        eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
        if (eval.getNullEntities() == 1) return;
        output += eval.getEvaluationScoresString();
        output += "\t";

        try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
            CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
            learner.setModel(argp.get("model", "lbfgs"));
            Parameters info = Parameters.create();
            TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
            LinearTokenClassifier rf = new LinearTokenClassifier(originalMap);

            for (SearchResult s : results) {
                double sumScore = Double.NEGATIVE_INFINITY;
                for (SimpleToken token : mergedList.get(s.getSentenceId())) {
                    sumScore += rf.score(token.getFeatures());
                }
                s.setCRFScore(sumScore);
            }

            embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, StaticVariables.crfScore);
            //embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, StaticVariables.entityContextScore);
            eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
            output += eval.getEvaluationScoresString();
            System.out.println(output);
        }
    }

}
