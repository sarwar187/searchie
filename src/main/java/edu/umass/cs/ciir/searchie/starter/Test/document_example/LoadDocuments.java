package edu.umass.cs.ciir.searchie.starter.Test.document_example;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

public class LoadDocuments {
    public static void main(String[] args) throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter(new File("/home/smsarwar/restricted_token_list.txt")));
        Scanner sc = new Scanner(new File("/home/smsarwar/trecqalist_72.6_1000.jsonl"));
        double previousScore=0;
        int documentsSeen = 0;
        while(sc.hasNextLine()) {
            String jsonString = sc.nextLine();
            Gson gson = new Gson();
            //JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/Desktop/volume.json"), JsonElement.class);
            JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
            //System.out.println(jsonElement);
            DocumentContainer documentContainer = gson.fromJson(jsonElement.toString(), DocumentContainer.class);
            System.out.println(documentContainer.doc + "\t" + documentContainer.qid + "\t" + documentContainer.score);
            if(documentContainer.score == previousScore)
                continue;
            previousScore = documentContainer.score;
            int i = 0;
            int flag = 0;
            for(Sentence sentence: documentContainer.sentences){
                String output = "'";
                for(Token token: sentence.tokens) {
                    output+= token.getToken() + " ";
                }
                output = output.trim();
                output+="'";
                System.out.println(output);
                //for(Token token: sentence.tokens){
                //System.out.println(token.getToken());
                if(flag==0) {
                    Scanner input = new Scanner(System.in);
                    int response = input.nextInt();
                    if (response == 1) {
                        br.write("spam, " + output);
                        br.newLine();
                    }

                    if (response == 0) {
                        br.write("ham, " + output);
                        br.newLine();
                        flag = 1;
                    }
                }else{
                    br.write("ham, " + output);
                    br.newLine();
                }
                //output += token.getToken() + " ";
                //}
                //output = output.trim();
                //if(i==1)
                //    System.out.println(output);
                i++;
                if(i==2)
                    break;
            }
            documentsSeen++;
            if(documentsSeen==10)
                break;
            //System.out.println(documentContainer.sentences.get(0).tokens.get(1).getToken());
            //System.out.println(vc.vol);
            //System.out.println(vc.volumes.get(0).getName());
            //System.out.println(vc.volumes.get(0).getManaged());
        }
        br.close();

        //for (Task task : fromJson) {
        //    System.out.println(task);
        //}
    }
    /*public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("/home/smsarwar/trecqalist_72.6_1000.jsonl"));
        int count = 0;
        while(sc.hasNextLine()){
            String check = sc.nextLine();
            if(count==0)
                System.out.println(check);
            count++;
        }
        System.out.println(count);
    }*/
    /*I know that someone can kiss better than you*/
}
