package edu.umass.cs.ciir.searchie.starter.interaction_experiment;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by sarwar on 7/18/17.
 */
public class LTREvaluateFeedback {
    public static void main(String[] args) throws Exception {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        List<List<SimpleToken>> pseudoPositives = new ArrayList<>();

        //Set<List<String>> foundSpans = new HashSet<>();
        IDF idf = new IDF();

        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        //lucene.close();
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        List<List<SimpleToken>> positives = new ArrayList<>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);
        positives.add(mergedList.get(0));
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);

        Scanner sc = new Scanner(new File(featureDirectory + featureFileName));
        //Scanner sc = new Scanner(new File("/mnt/nfs/work1/smsarwar/searchie/ltr/test/" + inputFileName +".arff.result"));
        //Scanner sc = new Scanner(new File("/home/sarwar/crfsuite/ltr/test/" + "trecqalist_90.1_1000.crfsuite"+".arff.result"));

        List<Integer> ranked = new ArrayList<>();
        while(sc.hasNextInt()){
            //System.out.println(sc.next());
            ranked.add(sc.nextInt());
        }
        //System.out.println("ranked size " + ranked.size());
        PrintResults pr = new PrintResults();
        //pr.printSimpleToken(mergedList.get(0), "Query");
        /*for(int i=0; i<5; i++){
            pr.printSimpleToken(mergedList.get(ranked.get(i)), "Result " + i);
            //int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(.getSentenceId()), etype);
        }*/
        int topk = 1000;
        List<Integer> expandedSentencesList = new ArrayList<>();
        for(int i=0; i<topk && i<mergedList.size(); i++){
            expandedSentencesList.add(ranked.get(i));
        }
        double prior[] = new double[mergedList.size()];
        double vectorLength[] = new double[mergedList.size()];
        double points[][] = new double[mergedList.size()][embeddingSize];
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        int row = 0, bootstrapSentenceId = 0;
        int expandedSentences[] = new int[10];
        int index = 0;
        for(int i: expandedSentencesList){
            expandedSentences[index] = i;
            index++;
            if(index==10)
                break;
        }

        for(List<SimpleToken> tokenList: mergedList) {
            //List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList), idf);
            List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList));
            int col = 0;
            for(double d: doubleList){
                points[row][col++] = d;
                vectorLength[row]+= (d*d);
            }
            if(vectorLength[row]-0d < 0.00000001) vectorLength[row]=1;
            row++;
        }
        EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
        vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        List<SearchResult> results = EmbeddingSearch.search(bootstrapSentenceId, points, mergedList.size(), embeddingSize, prior, vectorLength);
        int relevantSentences = 0;
        List<Boolean> list = new ArrayList<>();
        for(SearchResult s: results) {
            positives.add(mergedList.get(s.getSentenceId()));
            if (DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype) == 1) {
                list.add(true);
                relevantSentences++;
            }else{
                list.add(false);
            }
        }
        //Integer expandedSentences[] = (Integer) expandedSentencesList.toArray();
        //System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + inputFileName + "\t" + computeMap(a, countSuccessful) + "\t" + Metrics.precision(a, 5) + "\t" + Metrics.precision(a, 10));
        //recall + recall@5 + recall@10 + MAP + p@5 + p@10
        System.out.println((1.0 * foundSpans.size()/availableSpans.size()) + inputFileName);

    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            /*if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
            }*/
            if(!Stopwords.isStemmedStopword(token.lemma)){
                strList.add(token.lemma);
            }
        }
        return strList;
        //return s.trim();
    }

    static double computeMap(int[] a, int size){
        double countRelevant=0;
        double AP = 0;
        for(int i=0 ;i<a.length; i++){
            if(a[i]==1) {
                countRelevant++;
                AP+= countRelevant / (i+1);
            }
        }
        if(countRelevant == 0)
            return 0;
        //if(AP>10)
        //  return 1;
        //else
        return AP/ size;
    }

}
