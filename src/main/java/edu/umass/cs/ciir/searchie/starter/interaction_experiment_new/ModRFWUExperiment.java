package edu.umass.cs.ciir.searchie.starter.interaction_experiment_new;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.*;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;


//Span Based AddPRF
//

public class ModRFWUExperiment{
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "1"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int loop = StaticVariables.numRelevanceFeedbacks; // loop is the number of relevance feedback we take
        Oracle oracle = new Oracle(defaultCRFSuiteBinary);
        oracle.createOracle(featureDirectory, featureFileName, etype);
        TObjectFloatHashMap<String> optimalMap = oracle.getOptimalMap();
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        if(fullPositives.size() <=StaticVariables.minPositivestoTrain)
            return;
        if(fullNegatives.size()>=StaticVariables.maxNumSentence)
            return;
        //System.out.println(fullPositives.size() + "\t" + fullNegatives.size() + "\t" + inputFileName + "\t");
        int numExperiments = fullPositives.size()/numPositives;  // number of rounds we calculate AP, uAP and F1
        double testAP=0d, testuAP=0d;
        numExperiments = (numExperiments > maximumNumberOfExperiments)?maximumNumberOfExperiments: numExperiments;
        ArrayList<Integer> configuration = new ArrayList<>();
        // pair of optimal and error (1,0) means that we are considering optimal weights and errorActivated
        // we need to send two parameters to state object. One thorugh isOptimal parameter and the other through
        // setError parameter
        configuration.add(1); //worse

        for(int numConfig=0; numConfig<configuration.size(); numConfig++) {
            double evalfb[][] = new double[loop][StaticVariables.fblabel.length];
            double AP = 0d; double fullAP = 0d;  //added
            SearchResult searchResult;
            for (int index = 0; index < numExperiments * numPositives; index += numPositives) { //iterating over 300 positives, taking three in each round
                //System.out.println("round " + index);
                State state = new State(); //CREATING STATE
                state.setIsOptimal(0);
                StaticVariables.errorActivated = 0;
                Set<String> exploredTokens = null;
                boolean resultsFound = true;
                List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
                List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
                ArrayList<SearchResult> resultsSeen = new ArrayList<>();
                DataSelector.splitTrainTest(fullPositives, fullNegatives, positives, fullConllTest, index, numPositives);
                Set<String> availableTokens = DataSelector.getUniqueTokens(fullPositives, etype);
                Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
                //System.out.println("available tokens size " + availableTokens.size());
                Set<String> foundTokens = DataSelector.getUniqueTokens(positives, etype);
                //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
                //WRITE SOON
                //alternative of previous statement
                //Set<String> foundTokens = DataSelector.addNewTokens(.getUniqueTokens(positives, etype);

                //Set<String> exploredTokens = new HashSet<>();
                //DataSelector.extrapolateTokens(foundTokens, exploredTokens); //construct exploredtokens from tokens found so far
                state.setAvailableTokens(setMinus(availableTokens, foundTokens));

                //if (fullConllTrain.size() != (positives.size() + fullConllTest.size())) // Just checking if the training and test data sum up to the full training data
                //    System.out.println("error happened");

                try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                    CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                    learner.setModel(argp.get("model", "lbfgs"));
                    Parameters info = Parameters.create();
                    learner.learnFeatureWeights(fullPositives, etype, info);
                    Set<String> groundTruthFeatures = FeatureSelector.getFeatureSet(learner.getListFeatureMap());
                    LinearTokenClassifier rf;
                    //PRF.addPRF(fullConllTrain, positives, etype);
                    Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
                    int currentFoundSpanSize = foundSpans.size();
                    //Set<Integer> alreadyAdded = new HashSet<>();
                    //PRF.addPRFWithSpan(fullConllTrain, foundSpans , positives, etype, alreadyAdded);
                    TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                    Map<String, TokenCount> retrievedTokenMap = new HashMap<>();
                    //PRF.addRankedPRFWithSpan(fullConllTrain, foundSpans , positives, etype, new LinearTokenClassifier(originalMap));
                    //System.out.println("positives size "  + positives.size());
                    //printResults.printSimpleTokenList(positives, "printing positives...");
                    //starting the loop with i=0. It means we are not taking any weight relevance feedback in the beginning
                    boolean changeWeight = false;
                    //boolean changeWeight = true;
                    for (int i = 0; i < loop; i++) {
                        //printResults.printSimpleTokenList(positives, "printing positives...");
//                        if(i%10==0 && i>1){
//                            changeWeight = changeWeight ^ true;
//                        }
                        if(i == 20)
                            StaticVariables.isUnique = true;
//                        if(i>=20)
//                            changeWeight = changeWeight ^ true;
//                        //}
                        //printResults.printSimpleTokenList(positives, "printing positives...");

                        if(changeWeight==true){
                            //StaticVariables.isUnique = false;
                            //System.out.println("RF");
                            rf = new LinearTokenClassifier(originalMap);
                            //rf = new LinearTokenClassifier(optimalMap);
                            searchResult = Metrics.addRelevanceFeedback(rf, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                            //searchResult = Metrics.addRelevanceFeedbackSentence(rf, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique);
                            //DataSelector.addNewTokensWithGroundTruth(foundTokens, availableTokens, resultsSeen.get(i).getSimpleTokens(), etype);
                            //DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, resultsSeen.get(i).getSimpleTokens(), etype);
                            //DataSelector.addNewTokens(foundTokens, resultsSeen.get(i).getSimpleTokens(), etype);
                            //DataSelector.addPRF(fullConllTrain, positives, etype);
                            //DataSelector.modifyData(positives, foundTokens, etype);
                            originalMap = learner.learnFeatureWeights(positives, etype, info);
                            //changeWeight = true;
                        }
                        else {
                            //StaticVariables.isUnique = true;
                            //System.out.println("RFW");
                            Set<String> baseTokens = FeatureSelector.getFeatureSet(learner.getListFeatureMap()); //THIS returns the context word tokens only
                            //Set<String> baseTokens = FeatureSelector.getRawFeatureSet(learner.getListFeatureMap()); //THIS returns the context word tokens only
                            //DataSelector.removeGroundTruthFromBase(baseTokens, availableTokens);
                            //System.out.println("number of features " + (baseTokens.size()));
                            //Set<String> extrapolatedTokens = DataSelector.extrapolateTokens(baseTokens); //construct exploredtokens from tokens found so far
                            //System.out.println("base tokens");
                            //for(String s:baseTokens)
                            //    System.out.println(s);
                            state.setAvailableTokens(setMinus(availableTokens, foundTokens));
                            //state.setExploredTokens(exploredTokens);
                            //System.out.println("explored tokens size " + exploredTokens.size());
                            //baseTokens.addAll(extrapolatedTokens);
                            //DataSelector.removeGroundTruthFromBase(baseTokens, availableTokens);
                            exploredTokens = baseTokens;
                            state.setExploredTokens(exploredTokens); //Setting context word tokens for feature exploration
                            //System.out.println("number of features " + (baseTokens.size()));
                            WeightUtils.FSBPreviousWeightRemoved fixWeightInteractive = new WeightUtils.FSBPreviousWeightRemoved();
                            //WeightUtils.FSBPreviousWeightRemovedExplored fixWeightInteractive = new WeightUtils.FSBPreviousWeightRemovedExplored();
                            if (StaticVariables.errorActivated == 1)
                                state.setNumberofWeightErrors(-1);
                            TObjectFloatHashMap<String> modifiedWeights = fixWeightInteractive.fixWeight(optimalMap, originalMap, 1, oracle, state);

                            //TObjectFloatHashMap<String> modifiedWeights = fixWeightInteractive.fixWeight(optimalMap, originalMap, 1, oracle, state);
                            rf = new LinearTokenClassifier(modifiedWeights);
                            //rf = new LinearTokenClassifier(optimalMap);


                            /*System.out.println("optimal map size " + optimalMap.size());
                            for(String key: optimalMap.keySet()){
                                //System.out.println("opt key " + key + " value " + optimalMap.get(key));
                                originalMap.put(key, optimalMap.get(key));
                                //originalMap.adjustOrPutValue(key, optimalMap.get(key), optimalMap.get(key));
                            }
                            rf = new LinearTokenClassifier(originalMap);*/
                            searchResult = Metrics.addRelevanceFeedback(rf, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                            //originalMap = learner.learnFeatureWeights(positives, etype, info);
                            //searchResult = Metrics.addRelevanceFeedbackSentence(rf, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique);
                            //DataSelector.addNewTokensWithGroundTruth(foundTokens, availableTokens, resultsSeen.get(i).getSimpleTokens(), etype);
                            //DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, resultsSeen.get(i).getSimpleTokens(), etype);
                            //DataSelector.addNewTokens(foundTokens, resultsSeen.get(i).getSimpleTokens(), etype);
                            //if(searchResult.isTrue())
                            //  DataSelector.addPRF(fullConllTrain, positives, etype);
                            //  int check = compareMaps(originalMap, modifiedWeights);
                            //  check++;
                            //originalMap = learner.learnFeatureWeights(positives, etype, info);
                            //changeWeight = false;
                        }
                        //System.out.println("loop " + i);
                        //Modify data with co-located positives
                        //DataSelector.modifyDataWithAllTokensLastResult(positives.get(positives.size()-1), availableTokens, etype);
                        DataSelector.addNewTokensWithGroundTruth(foundTokens, availableTokens, resultsSeen.get(i).getSimpleTokens(), etype);
                        //System.out.println("before");
                        int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, resultsSeen.get(i).getSimpleTokens(), etype);
                        //This always creates a new tokenlist and appends it with positive. So, nothing is changed in fullConllTrain
                        //PRF.addPRFWithSpan(fullConllTrain, foundSpans , positives, etype, alreadyAdded);
                        //printResults.printSimpleToken(resultsSeen.get(i).getSimpleTokens());
                        //System.out.println("max scored token " + resultsSeen.get(i).getMaxScoredToken());
                        if(successful==1)
                            retrievedTokenMap.get(resultsSeen.get(i).getMaxScoredToken()).incrementSuccessfulByOne();
                        /*List<List<SimpleToken>> temp = new ArrayList<>();
                        temp.add(resultsSeen.get(i).getSimpleTokens());
                        for (List<String> s: DataSelector.getUniqueSpans(temp, etype)){
                            foundSpans.add(s);
                        }*/
                        //System.out.println("after");
                        //printResults.printSimpleToken(resultsSeen.get(i).getSimpleTokens());

                        //if(foundSpans.size()>currentFoundSpanSize){
                        //
                        //}
                        //DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, positives.get(positives.size()-1), etype);
                        /*if(foundSpans.size()==availableSpans.size()) {
                            System.out.println("number of interactions" + i);
                            break;
                        }*/

                        //DataSelector.modifyDataWithAllTokensLastResult(resultsSeen.get(i).getSimpleTokens(), availableTokens, etype);

                        if (searchResult == null) {
                            resultsFound = false;
                            System.out.println(i+ "\t" + state.getUsedFeatures().size() + "\t" + inputFileName + "\t" + fullConllTest.size());
                            //System.out.println("broken");
                            break;
                        }
                        final Map<String, Double> m = Metrics.evaluateRankedList(resultsSeen, 0); //The evaluate model function has been changed
                        //System.out.println("Let's see what we get");

                        //System.out.println();
                        //Find new tokens given the tokens already founded and then extrapolate based on word embedding
                        //Set<String> newTokens = DataSelector.addNewTokens(foundTokens, resultsSeen.get(i).getSimpleTokens(), etype);
                        //DataSelector.extrapolateTokens(newTokens, exploredTokens);

                        if(i==loop-1){
                            fullAP+= Metrics.computeFullAP(rf, fullConllTest, etype, resultsSeen);
                            Map<String, Double> result = Metrics.evaluateModel(rf, fullConllTrain, etype);
                            testAP = result.get("AP");
                            testuAP = result.get("uAP");
                            fullAP+= Metrics.computeFullAP(rf, fullConllTest,etype, resultsSeen);
                        }
                        //System.out.println(foundSpans.size() + "\t" + availableSpans.size());
                        /*if((foundSpans.size()*1.0d)/availableSpans.size()>= 0.9) {
                            System.out.println(i + "\t" + state.getUsedFeatures().size() + "\t" + inputFileName + "\t" + fullConllTest.size());
                            break;
                        }*/
                    }
                    //System.out.println("features corrected");
                    /*for(String s: state.getUsedFeatures()){
                        System.out.println(s);
                    }*/
                    //System.out.println("ground truth feature size " + groundTruthFeatures.size());
                    //System.out.println("recovered " + foundInGroundTruth(exploredTokens, groundTruthFeatures));

                    //System.out.println("percentage of groundtruth " + /1.0 * groundTruthFeatures.size());

                    /*System.out.println("available spans");
                    for(List<String> tokenList: availableSpans){
                        for(String token: tokenList)
                            System.out.println(token);
                        System.out.println();
                    }
                    //foundSpans = DataSelector.getUniqueSpans(, etype);

                    System.out.println("found spans");
                    for(List<String> tokenList: foundSpans){
                        for(String token: tokenList)
                            System.out.println(token);
                        System.out.println();
                    }*/
                    //AP+= Metrics.evaluateAveragePrecision(resultsSeen, StaticVariables.rankedListSize); //using last index to save AP
                    /*for(String s: state.getUsedFeatures()){
                        System.out.println("feature token " + s);
                    }*/
                    System.out.println(testuAP + "\t" + testAP + "\t" + availableSpans.size() + "\t" + foundSpans.size() + "\t" + state.getUsedFeatures().size() + "\t" + inputFileName);
                    //System.out.println(testuAP + "\t" + testAP + "\t" + availableTokens.size() + "\t" + foundTokens.size() + "\t" + state.getUsedFeatures().size() + "\t" + inputFileName);
                }
            }
        }
    }

    public static Set<String> setMinus(Set<String> availableTokens, Set<String> foundTokens){
        Set<String> tempSet = new HashSet<>();
        tempSet.addAll(availableTokens);
        tempSet.removeAll(foundTokens);
        return tempSet;
    }


    public static int compareMaps(TObjectFloatHashMap<String> a, TObjectFloatHashMap<String> b){
        int flag = 0;
        for(String key: a.keySet()){
            if(b.containsKey(key)){
                if(a.get(key)!= b.get(key)) {
                    System.out.println("a values " + a.get(key));
                    System.out.println("b values " + b.get(key));

                    flag = 1;
                    //return flag;
                }
            }
        }
        return flag;
    }

    public static int foundInGroundTruth(Set<String> extrapolated, Set<String> groundTruthFeatures){
        int hit = 0;
        System.out.println("extrapolated tokens");
        if(extrapolated == null)
            return 0;
        for(String s: extrapolated){
            if(groundTruthFeatures.contains(s)){
                System.out.println(s);
                hit++;
            }
        }
        return hit;
    }
}
