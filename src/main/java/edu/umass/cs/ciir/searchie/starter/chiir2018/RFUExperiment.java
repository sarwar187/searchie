package edu.umass.cs.ciir.searchie.starter.chiir2018;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.DocumentContainer;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.QueryFileParser;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Sentence;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Token;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.Evaluation;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.zip.GZIPInputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.DocumentContainer;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.QueryFileParser;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Sentence;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Token;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.Evaluation;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class RFUExperiment {
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args) throws IOException {
        double[] expandedSentencesWeight;
        int[] expandedSentences;
//        Parameters argp = Parameters.parseArgs(args);
//        //int numTraining = argp.get("trainingStart", 3);
//        String etype = argp.get("class", "LIST");
//        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
//        String inputDirectory = argp.get("dir", "/home/smsarwar/crfsuite/");
//        String inputFileName = argp.get("file", "train_list.crfsuite");
//        File trainFile = new File(inputDirectory + inputFileName);
//        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
//        String featureFileName = argp.get("ffile", "dump_list.out");
//        String resultDirectory = argp.get("rdir", "result_directory");
        int embeddingSize = 200;
        int[] occurrence = new int[100];
        PrintResults pr = new PrintResults();
        LuceneInMemory lucene = new LuceneInMemory();

        //Loading Data
//        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
//        ArrayList<List<SimpleToken>> fullPositives = new ArrayList<List<SimpleToken>>();
//        ArrayList<List<SimpleToken>> fullNegatives = new ArrayList<List<SimpleToken>>();
//        IDF idf = new IDF();
//        for (List<SimpleToken> sent : fullConllTrain) {
//            idf.addSentence(sent);
//
//            boolean isPositive = false;
//            for (SimpleToken token : sent) {
//                if (!token.getLabel().equals(etype)) continue;
//                isPositive = true;
//                break;
//            }
//            if (isPositive) {
//                fullPositives.add(sent);
//                continue;
//            }
//            fullNegatives.add(sent);
//        }
        //Data loading ends


        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String inputDirectory = argp.get("dir", "/home/smsarwar/");
        String inputFileName = argp.get("file", "trecqalist_94.4_1000.jsonl.gz");
        int bootstrapSentenceId = 0;
        HashMap<String, List<String>> hashMap = BatchHandler.loadBatch(bootstrapSentenceId);

        if (!hashMap.containsKey(inputFileName))
            return;

        double previousScore = 0;
        Map<Integer, String> senteceToDocumentMapping = new HashMap<>();

        //Data loading
        List<List<SimpleToken>> fullConllTrain = new ArrayList<>();
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        IDF idf = new IDF();
        int row = 0;


        GZIPInputStream gzip = new GZIPInputStream(new FileInputStream(inputDirectory + inputFileName));
        BufferedReader br = new BufferedReader(new InputStreamReader(gzip));

        while (true) {
            //while (sc.hasNextLine()) {
            //String jsonString = sc.nextLine();
            String jsonString = br.readLine();
            if (jsonString == null)
                break;
            //Gson gson = new Gson();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            //JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/Desktop/volume.json"), JsonElement.class);
            JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
            DocumentContainer documentContainer = gson.fromJson(jsonElement.toString(), DocumentContainer.class);
            //System.out.println(documentContainer.doc + "\t" + documentContainer.qid + "\t" + documentContainer.score);
            if (documentContainer.score == previousScore)
                continue;
            previousScore = documentContainer.score;
            int i = 0;
            for (Sentence sentence : documentContainer.sentences) {
                String output = "";
                List<SimpleToken> sList = new ArrayList<>();
                for (Token token : sentence.tokens) {
                    sList.add(SimpleToken.convertTokenToSimpleToken(token));
                    //if(!token.getLemma().equals(token.getToken()))
                    //System.out.println(token.getLemma() + "\t" + token.getToken());
                }
                fullConllTrain.add(sList);
                senteceToDocumentMapping.put(row++, documentContainer.doc);
            }
            //for (Task task : fromJson) {
            //    System.out.println(task);
            //}
        }
        //Data loading ends
        //System.out.println("size " + fullConllTrain.size());
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }
        //for(List<SimpleToken> test: fullPositives)
        //    pr.printSimpleToken(test, "test");

        //Data Loading ends
        /*if(!inputFileName.equals("trecqalist_114.4_1000.crfsuite")){
            return;
        }else{
            for(int k =0; k<20; k++){
                pr.printSimpleToken(fullConllTrain.get(k), "sentence");
            }
        }*/


        ArrayList<List<SimpleToken>> mergedList = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> positives = new ArrayList<List<SimpleToken>>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);

        //if(expansionApproach.equals(StaticVariables.expansionApproachFullContextToken)) {

        //pr.printSimpleTokenSimply(mergedList.get(bootstrapSentenceId), "modified query");
        //System.out.println();
        //pr.printSimpleToken(mergedList.get(bootstrapSentenceId), "modified query lemmatized");
        //contains the tokens from the query
        /*for (List<SimpleToken> judgedTokenList : fullPositives) {
            List<SimpleToken> tempTokenList = EmbeddingSearch.simpleTokenToTokenListWithoutEntityToken(judgedTokenList, etype);
            //List<SimpleToken> tempTokenList = EmbeddingSearch.simpleTokenToTokenListWithoutEntityToken(judgedTokenList, etype);
            for (SimpleToken t : tempTokenList) {
                queryTokenList.add(t);
            }
        }*/
        //}
        String output = "";
        positives.add(mergedList.get(bootstrapSentenceId));
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        FeatureExtractor f = new FeatureExtractor((List) mergedList.get(bootstrapSentenceId));
        String targetEntityType = f.getTargetEntityType();
        //System.out.println(targetEntityType);
        //pr.printSimpleTokenList(fullPositives, "none");
        //pr.printSimpleTokenSimply(mergedList.get(bootstrapSentenceId), "none");
        //System.out.println();
//        EmbeddingSearch.getParagraph(mergedList.get(bootstrapSentenceId), fullConllTrain, 2, positives);
//        for(List<SimpleToken> lst: positives)
//            pr.printSimpleTokenSimply(lst, "none");
//        System.out.println();
        //System.out.println(targetEntityType);
        //FeatureExtractor queryFeatureExtractor = new FeatureExtractor((List)mergedList.get(0));
        //String targetEntityType = queryFeatureExtractor.getTargetEntityType();
        //Set<List<String>> foundSpansCaseSensitive = DataSelector.getUniqueSpansCaseSensitive(positives, etype);
        //List<Double> targetEntityEmbedding = EmbeddingSearch.getEntityEmbedding(foundSpansCaseSensitive, sm);
        Evaluation eval = new Evaluation();
        //String expansionApproach = StaticVariables.expansionApproachFull;
        //String expansionParameter = StaticVariables.expansionParameterPRF;
        String expansionApproach = StaticVariables.expansionApproachNone;
        String expansionParameter = StaticVariables.expansionParameterNone;
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        int topk = 0;
        EmbeddingSearch embeddingSearch = new EmbeddingSearch(mergedList, fullPositives, embeddingSize, etype, inputFileName, lucene, sm);
        embeddingSearch.queryExpansion(bootstrapSentenceId, expansionApproach, expansionParameter, availableSpans, targetEntityType, topk);
        //Query Expansion Ends
        //Seaching with New Query Representation
        //System.out.println(sm.entityEmbeddingContextExpansion.size());
        List<SearchResult> results = embeddingSearch.search(bootstrapSentenceId);

        //List<List<SimpleToken>> possibleQueries = BatchHandler.getPossibleQueries(bootstrapSentenceId, mergedList);
        //List<List<SimpleToken>> possibleQueries = BatchHandler.getContextuallyExpandedQueries(inputFileName, bootstrapSentenceId, mergedList, hashMap);
        int topk_list[] = {5};

        eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
        //output += targetEntityType + "\t";
        output += eval.getEvaluationScoresString() + "\t"; // + eval.getFoundSpansString();
        output += "\t" + inputFileName + "\t";
        output += QueryFileParser.fileToQueryString(inputFileName, "/home/smsarwar/trecqalist.json.gz");
        //System.out.println();
        //output+=eval.getStrRecall();
        System.out.println(output);
        embeddingSearch.reloadQueryRepresentation(bootstrapSentenceId);

    }
}
