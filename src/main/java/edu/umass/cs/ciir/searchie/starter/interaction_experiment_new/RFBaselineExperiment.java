package edu.umass.cs.ciir.searchie.starter.interaction_experiment_new;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RFBaselineExperiment{

    static String prefix = StaticVariables.prefix;
    public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";

    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        String resultDirectory = argp.get("rdir", "result_directory");
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));


        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int loop = StaticVariables.numRelevanceFeedbacks; // loop is the number of relevance feedback we take
        // we iterate over loop taking one positive/negative on the topmost position in the ranking
        double eval[][] = new double[loop][StaticVariables.label.length];
        double evalfb[][] = new double[loop][StaticVariables.fblabel.length];
        double AP = 0d; double fullAP = 0d;  //added
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        printResults.addJSON("pos", String.valueOf(fullPositives.size()));
        printResults.addJSON("neg", String.valueOf(fullPositives.size()));
        if(fullPositives.size() <=StaticVariables.minPositivestoTrain)
            return;
        System.out.println(fullPositives.size() + "\t" + fullNegatives.size() + "\t" + inputFileName + "\t");
        int successfulExperiments = 0;
        int numExperiments = fullPositives.size()/numPositives;  // number of rounds we calculate AP, uAP and F1
        numExperiments = (numExperiments > maximumNumberOfExperiments)?maximumNumberOfExperiments: numExperiments;
        //System.out.println(numExperiments);
        for (int index = 0; index < numExperiments * numPositives ; index += numPositives) { //iterating over 300 positives, taking three in each round
            //If it is not possible to find 20 unique entities in this loop resultfound will be false
            boolean resultsFound = true;
            //resultsSeen would store the tokens we have seen so far
            ArrayList<SearchResult> resultsSeen = new ArrayList<>();
            List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
            List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
            DataSelector.splitTrainTest(fullPositives, fullNegatives, positives, fullConllTest, index, numPositives);
            //training and test data already prepared!!
            //System.out.println("full positive size\t" + positives.size() + " fullConllTest size\t" + fullConllTest.size() + "fullConllTrain size\t" + fullConllTrain.size() );
            //evallocal would store the values produced in the loop below. If we can get loop number of entity mentions, we would add it with evalfb
            double evallocal[][] = new double[loop][StaticVariables.fblabel.length];
            try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                learner.setModel(argp.get("model", "lbfgs"));
                Parameters info = Parameters.create();
                TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                LinearTokenClassifier tokenClassifier = new LinearTokenClassifier(originalMap);
                Map<String, TokenCount> retrievedTokenMap = new HashMap<>();
                for (int i = 0; i < loop; i++) {
                    //please use the following lines together for unique results
                    SearchResult searchResult = Metrics.addRelevanceFeedback(tokenClassifier, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                    if(fullConllTrain.size() != (positives.size() + fullConllTest.size())) // Just checking if the training and test data sum up to the full training data
                        System.out.println("error happened");
                    //if at any iteration, we do not find anything unique for the user, we discard that sample, overall
                    if(searchResult==null) {
                        resultsFound = false;
                        System.out.println("broken");
                        break;
                    }
                    //In the following function second parameter 0 indicates the cutoff for producing the rankedlist
                    final Map<String, Double> m = Metrics.evaluateRankedList(resultsSeen, 0); //The evaluate model function has been changed
                    for(int j=0; j<StaticVariables.fblabel.length; j++){
                        evallocal[i][j] = m.get(StaticVariables.fblabel[j]);
                    }
                    if(i==loop-1)fullAP+= Metrics.computeFullAP(tokenClassifier, fullConllTest,etype, resultsSeen);
                    //originalMap = learner.learnFeatureWeights(positives, etype, info);
                    //tokenClassifier = new LinearTokenClassifier(originalMap);
                }
            }
            AP+= Metrics.evaluateAveragePrecision(resultsSeen, StaticVariables.rankedListSize); //using last index to save AP

            if(resultsFound) {
                //printResults.printSearchResultList(resultsSeen);
                successfulExperiments++;
                for (int i = 0; i < loop; i++) {
                    for (int j = 0; j < StaticVariables.fblabel.length; j++) {
                        evalfb[i][j] += evallocal[i][j];
                    }
                }
            }

        }
        System.out.println("fAP\t" + new DecimalFormat("#0.000").format(fullAP/successfulExperiments));
        System.out.println("AP\t" + new DecimalFormat("#0.000").format(AP/successfulExperiments));
        printResults.printTwoD(0, evalfb, successfulExperiments, loop);

    }
}