package edu.umass.cs.ciir.searchie.starter.Test.document_example;

public class Judgment {
    public String getCandidate() {
        return candidate;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    String candidate;

    public Integer getCorrectness() {
        return correctness;
    }

    public void setCorrectness(Integer correctness) {
        this.correctness = correctness;
    }

    Integer correctness;
}
