package edu.umass.cs.ciir.searchie.starter.feature_engineering;

/**
 * Created by sarwar on 12/10/16.
 */
public class FeatureWeight {
    public FeatureWeight(String feature, double weight) {
        this.feature = feature;
        this.weight = weight;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    String feature;
    Double weight;



}




class QFeatureWeight extends FeatureWeight {

    int quantized;

    public QFeatureWeight(String feature, double weight) {
        super(feature, weight);
        this.quantized = -1;
    }

    public int getQuantized() {
        return quantized;
    }

    public void setQuantized(int quantized) {
        this.quantized = quantized;
    }
}