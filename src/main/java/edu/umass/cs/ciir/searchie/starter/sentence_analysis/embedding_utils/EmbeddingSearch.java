/*
 * Decompiled with CFR 0_122.
 */
package edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.interaction_experiment.TwoOneUExperiment;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.Island;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.QueryModel;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import javafx.util.Pair;
import org.apache.commons.lang3.ArrayUtils;
import org.lemurproject.galago.core.eval.stat.Stat;
import weka.clusterers.EM;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static edu.umass.cs.ciir.searchie.starter.StaticVariables.printFlag;

public class EmbeddingSearch {
    private static final Logger logger = Logger.getLogger(EmbeddingSearch.class.getName());

    double points[][];
    double fullPositivePoints[][];
    double vectorLength[];
    double prior[];
    double vectorLengthFullPositives[];
    int containsFoundEntity;
    List<List<SimpleToken>> mergedList;
    List<List<SimpleToken>> fullPositives;
    String etype;
    String inputFileName;
    int embeddingSize;
    LuceneInMemory lucene;
    SentenceEmbedding sm;
    List<Integer> prf;
    List<Double> initialQueryRepresentation;
    List<Double> initialQueryEntityRepresentation;
    HashMap<String, Double> finalQueryModel;
    HashMap<String, Double> entityQueryModel;
    double [] queryModel;
    double initialQueryVectorLength=1;

    public HashMap<String, Double> getQueryModelMap() {
        return queryModelMap;
    }

    public HashMap<String, Double> getFinalQueryModelMap() {
        return finalQueryModel;
    }

    public void setQueryModelMap(HashMap<String, Double> queryModelMap) {
        this.queryModelMap = queryModelMap;
    }

    HashMap<String, Double> queryModelMap;


    public double[] getQueryModel() {
        return queryModel;
    }

    public void setEntityQueryModel(HashMap<String, Double> entityQueryModel){
        this.entityQueryModel = entityQueryModel;
    }
    public void setQueryModel(double[] queryModel) {
        this.queryModel = queryModel;
    }

    EmbeddingSearch(){

    }

    public EmbeddingSearch(List<List<SimpleToken>> mergedList, List<List<SimpleToken>> fullPositives, int embeddingSize, String etype, String inputFileName, LuceneInMemory lucene, SentenceEmbedding sm){
        //logger.setLevel(Level.SEVERE);
        this.mergedList = mergedList;
        this.etype = etype;
        this.embeddingSize = embeddingSize;
        this.inputFileName = inputFileName;
        this.lucene = lucene;
        this.sm = sm;
        this.fullPositives = fullPositives;
        //this will keep embedding of the judged positive sentences. without the entity token.
        this.prf = new ArrayList<>();
        initialQueryRepresentation = new ArrayList<>();
        prior = new double[mergedList.size()];
        vectorLength = new double[mergedList.size()];
        vectorLengthFullPositives = new double[fullPositives.size()];
        points = new double[mergedList.size()][embeddingSize];
        fullPositivePoints = new double[fullPositives.size()][embeddingSize];
        queryModel = new double[mergedList.size()];
    }


    public EmbeddingSearch(List<List<SimpleToken>> mergedList, List<List<SimpleToken>> fullPositives, int embeddingSize, String etype, String inputFileName){
        //logger.setLevel(Level.SEVERE);
        this.mergedList = mergedList;
        this.etype = etype;
        this.embeddingSize = embeddingSize;
        this.inputFileName = inputFileName;
        this.lucene = lucene;
        this.sm = sm;
        this.fullPositives = fullPositives;
        prior = new double[mergedList.size()];
        vectorLength = new double[mergedList.size()];
        points = new double[mergedList.size()][embeddingSize];
        this.prf = new ArrayList<>();
        initialQueryRepresentation = new ArrayList<>();
        queryModel = new double[mergedList.size()];
    }

    public void initiateLuceneIndex(LuceneInMemory lucene){
        this.lucene = lucene;
    }

    public List<Double> getQueryRepresentation(){
        List<Double> targetEntityContextEmbedding = new ArrayList<>();
        for (int i = 0; i < embeddingSize; ++i) {
            targetEntityContextEmbedding.add(points[0][i]);
        }
        return targetEntityContextEmbedding;
    }

    public double[] getQueryRepresentation(int bootstrapsentenceID){
        double[] queryRepresentation = new double[embeddingSize];
        for (int i = 0; i < embeddingSize; ++i) {
            queryRepresentation[i] = points[bootstrapsentenceID][i];
        }
        return queryRepresentation;
    }


    public static List<SearchResult> search(int querySentenceId, double[][] points, int totalSize, int embeddingSize, double[] prior, double[] vectorLength) {
        PriorityQueue<SearchResult> pq = new PriorityQueue<SearchResult>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        for (int i = 0; i < totalSize; ++i) {
            double score = 0.0;
            for (int j = 0; j < embeddingSize; ++j) {
                score += points[querySentenceId][j] * points[i][j];
            }
            score /= vectorLength[querySentenceId];
            score /= vectorLength[i];
            //score += prior[i];
            if (pq.size() < StaticVariables.sizeOfInitialRetrieval) {
                pq.add(new SearchResult(i, score));
                continue;
            }
            SearchResult s = pq.peek();
            if (s.getScore() >= score) continue;
            pq.poll();
            pq.add(new SearchResult(i, score));
        }
        ArrayList<SearchResult> results = new ArrayList<SearchResult>(pq.size());
        results.addAll(pq);
        results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        return results;
    }

    public static List<SearchResult> search(int querySentenceId, double[][] points, int totalSize, int embeddingSize, double[] prior, double[] vectorLength, int sizeOfRetireval) {
        PriorityQueue<SearchResult> pq = new PriorityQueue<SearchResult>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        for (int i = 0; i < totalSize; ++i) {
            double score = 0.0;
            for (int j = 0; j < embeddingSize; ++j) {
                score += points[querySentenceId][j] * points[i][j];
            }
            score /= vectorLength[querySentenceId];
            score /= vectorLength[i];
            score += prior[i];
            if (pq.size() < sizeOfRetireval) {
                pq.add(new SearchResult(i, score));
                continue;
            }
            SearchResult s = pq.peek();
            if (s.getScore() >= score) continue;
            pq.poll();
            pq.add(new SearchResult(i, score));
        }
        ArrayList<SearchResult> results = new ArrayList<SearchResult>(pq.size());
        results.addAll(pq);
        results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        return results;
    }



    public List<SearchResult> search(double[] queryEmbedding, int embeddingSize) {
        PriorityQueue<SearchResult> pq = new PriorityQueue<SearchResult>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        for (int i = 0; i < mergedList.size(); ++i) {
            double score = 0.0;
            for (int j = 0; j < embeddingSize; ++j) {
                score += queryEmbedding[j] * points[i][j];
            }
            score /= EmbeddingSearch.getEucledian(queryEmbedding, embeddingSize);
            score /= vectorLength[i];
            if (pq.size() < StaticVariables.sizeOfInitialRetrieval) {
                pq.add(new SearchResult(i, score));
                continue;
            }
            SearchResult s = pq.peek();
            if (s.getScore() >= score) continue;
            pq.poll();
            pq.add(new SearchResult(i, score));
        }
        ArrayList<SearchResult> results = new ArrayList<SearchResult>(pq.size());
        results.addAll(pq);
        results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        return results;
    }

    public List<SearchResult> reScoreEntityQueryEmbedding(double[] queryEmbedding, int embeddingSize, List<SearchResult> results) {
        for (int i = 0; i < results.size(); ++i) {
            double score = 0.0;
            for (int j = 0; j < embeddingSize; ++j) {
                score += queryEmbedding[j] * points[results.get(i).getSentenceId()][j];
            }
            score /= EmbeddingSearch.getEucledian(queryEmbedding, embeddingSize);
            score /= vectorLength[i];
            results.get(i).setScore(results.get(i).getScore() + score);
        }
        return results;
    }



    public List<SearchResult> search(int querySentenceId) {
        PriorityQueue<SearchResult> pq = new PriorityQueue<SearchResult>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        for (int i = mergedList.size()-1; i >= 0; i--) {
            double score = 0.0;
//            for (int j = 0; j < embeddingSize; ++j) {
//                score += points[querySentenceId][j] * points[i][j];
//            }
//            score /= vectorLength[querySentenceId];
//            score /= vectorLength[i];
//            score += prior[i];
            score = cosineSimilarity(querySentenceId, i, points);
            //score+=prior[i];
            if (pq.size() < StaticVariables.sizeOfInitialRetrieval) {
                pq.add(new SearchResult(i, score));
                continue;
            }
            SearchResult s = pq.peek();
            if (s.getScore() >= score) continue;
            pq.poll();
            pq.add(new SearchResult(i, score));
        }
        ArrayList<SearchResult> results = new ArrayList<SearchResult>(pq.size());
        results.addAll(pq);
        results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        return results;
    }

    //if I give query sentence and a set of expanded sentences, it will return the ranked list of expanded sentences
    //using the points array
    public static List<SearchResult> rankPRFSentences(int querySentenceId, int[] expandedSentences, double points[][], int embeddingSize, double[] vectorLength) {
        PriorityQueue<SearchResult> pq = new PriorityQueue<SearchResult>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));

        for (int i = 0; i < expandedSentences.length; i++) {
            double score = 0.0;
            for (int j = 0; j < embeddingSize; ++j) {
                score += points[querySentenceId][j] * points[expandedSentences[i]][j];
            }
            score /= vectorLength[querySentenceId];
            score /= vectorLength[expandedSentences[i]];
            //score += prior[i];
            if (pq.size() <= StaticVariables.topk) { //replace with staticvariables.topk
                pq.add(new SearchResult(expandedSentences[i], score));
                continue;
            }
            SearchResult s = pq.peek();
            if (s.getScore() >= score) continue;
            pq.poll();
            pq.add(new SearchResult(expandedSentences[i], score));
        }

        ArrayList<SearchResult> results = new ArrayList<SearchResult>(pq.size());
        results.addAll(pq);
        results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        return results;
    }

    public static List<SearchResult> rankPRFSentences(List<Double> queryEmbedding, int[] expandedSentences, double points[][], int embeddingSize, double[] vectorLength) {
        PriorityQueue<SearchResult> pq = new PriorityQueue<SearchResult>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        double[] queryNorm = new double[embeddingSize];
        for(int i=0; i< embeddingSize; i++){
            queryNorm[i] = queryEmbedding.get(i);
        }


        for (int i = 0; i < expandedSentences.length; i++) {
            double score = 0.0;
            for (int j = 0; j < embeddingSize; j++) {
                score += queryEmbedding.get(j) * points[expandedSentences[i]][j];
            }
            score /= getEucledian(queryNorm, embeddingSize);
            score /= vectorLength[expandedSentences[i]];
            //score += prior[i];
            if (pq.size() <= StaticVariables.topk) { //replace with staticvariables.topk
                pq.add(new SearchResult(expandedSentences[i], score));
                continue;
            }
            SearchResult s = pq.peek();
            if (s.getScore() >= score) continue;
            pq.poll();
            pq.add(new SearchResult(expandedSentences[i], score));
        }

        ArrayList<SearchResult> results = new ArrayList<SearchResult>(pq.size());
        results.addAll(pq);
        results.sort((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        return results;
    }


    public static double getEucledian(double[] vector, double embeddingSize) {
        double length = 0.0;
        for (int j = 0; j < embeddingSize; ++j) {
            length += vector[j] * vector[j];
        }
        double vectorLength = Math.sqrt(length);
        if(vectorLength - 0.000000001 == 0)
            return 1;
        else
            return vectorLength;
    }


    /*public static void queryExpansion(double[][] points, int embeddingSize, int querySentenceId, double[] vectorLength, int[] expansionSentences) {
        EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expansionSentences, embeddingSize);
        vectorLength[querySentenceId] = EmbeddingSearch.getVectorLength(points, querySentenceId, embeddingSize);
    }*/

    public static double[] getAggregatedRepresentationCopiedtoZero(double[][] points, int[] indices, int embeddingSize) {
        double[] representation = new double[embeddingSize];
        for (int i = 0; i < indices.length; ++i) {
            for (int j = 0; j < embeddingSize; ++j) {
                if (indices[i] < 0) {
                    double[] arrd = representation;
                    int n = j;
                    arrd[n] = arrd[n] - points[Math.abs(indices[i])][j];
                    continue;
                }
                double[] arrd = representation;
                int n = j;
                arrd[n] = arrd[n] + points[Math.abs(indices[i])][j];
            }
        }
        int j = 0;
        while (j < embeddingSize) {
            double[] arrd = representation;
            int n = j++;
            arrd[n] = arrd[n] / (double)indices.length;
        }
        for (j = 0; j < embeddingSize; ++j) {
            points[0][j] = representation[j];
        }
        return representation;
    }

    public static double[] getAggregatedRepresentationCopiedtoIndex(double[][] points, int[] indices, int embeddingSize, int bootStrappedSentenceId) {
        double[] representation = new double[embeddingSize];
        for (int i = 0; i < indices.length; ++i) {
            for (int j = 0; j < embeddingSize; ++j) {
                if (indices[i] < 0) {
                    double[] arrd = representation;
                    int n = j;
                    arrd[n] = arrd[n] - points[Math.abs(indices[i])][j];
                    continue;
                }
                double[] arrd = representation;
                int n = j;
                arrd[n] = arrd[n] + points[Math.abs(indices[i])][j];
            }
        }
        int j = 0;
        while (j < embeddingSize) {
            double[] arrd = representation;
            int n = j++;
            arrd[n] = arrd[n] / (double)indices.length;
        }
        for (j = 0; j < embeddingSize; ++j) {
            points[bootStrappedSentenceId][j] = representation[j];
        }
        return representation;
    }



    public static double[] getAggregatedRepresentationCopiedtoIndex(double[][] points, int embeddingSize, int bootStrappedSentenceId, double[] prior, int countPRF) {
        int[] indices = new int[countPRF];
        int row=0;
        for(int i=0;i<prior.length;i++) {
            if (prior[i] > 0)
                indices[row++] = i;
        }
        double[] representation = new double[embeddingSize];
        for (int i = 0; i < indices.length; ++i) {
            for (int j = 0; j < embeddingSize; ++j) {
                if (indices[i] < 0) {
                    double[] arrd = representation;
                    int n = j;
                    arrd[n] = arrd[n] - points[Math.abs(indices[i])][j];
                    continue;
                }
                double[] arrd = representation;
                int n = j;
                arrd[n] = arrd[n] + points[Math.abs(indices[i])][j];
            }
        }
        int j = 0;
        while (j < embeddingSize) {
            double[] arrd = representation;
            int n = j++;
            arrd[n] = arrd[n] / (double)indices.length;
        }
        for (j = 0; j < embeddingSize; ++j) {
            points[bootStrappedSentenceId][j] = representation[j];
        }
        return representation;
    }

    public static double[] getAggregatedRepresentationCopiedtoZero(double[][] points, int[] indices, double[] indicesWeight, int embeddingSize) {
        double[] representation = new double[embeddingSize];
        double indicesWeightSum = 0.0;
        for (int i = 0; i < indices.length; ++i) {
            for (int j = 0; j < embeddingSize; ++j) {
                if (indices[i] < 0) {
                    double[] arrd = representation;
                    int n = j;
                    arrd[n] = arrd[n] - indicesWeight[i] * points[Math.abs(indices[i])][j];
                    continue;
                }
                double[] arrd = representation;
                int n = j;
                arrd[n] = arrd[n] + indicesWeight[i] * points[Math.abs(indices[i])][j];
            }
            indicesWeightSum += indicesWeight[i];
        }
        int j = 0;
        while (j < embeddingSize) {
            double[] arrd = representation;
            int n = j++;
            arrd[n] = arrd[n] / indicesWeightSum;
        }
        for (j = 0; j < embeddingSize; ++j) {
            points[0][j] = representation[j];
        }
        return representation;
    }

    public static double[] getAggregatedRepresentationCopiedtoIndex(double[][] points, int[] indices, double[] indicesWeight, int embeddingSize, int bootStrappedSentenceId) {
        double[] representation = new double[embeddingSize];
        double indicesWeightSum = 0.0;
        for (int i = 0; i < indices.length; ++i) {
            for (int j = 0; j < embeddingSize; ++j) {
                if (indices[i] < 0) {
                    representation[j] = representation[j] - indicesWeight[i] * points[Math.abs(indices[i])][j];
                    continue;
                }
                representation[j] = representation[j] + indicesWeight[i] * points[Math.abs(indices[i])][j];
            }
            indicesWeightSum += indicesWeight[i];
        }
        if(indicesWeightSum==0) {
            indicesWeightSum = 1.0;
            System.out.println("look at getAggregatedRepresentationCopiedtoIndex function in Embeddingsearch expansion sentence weight sums to zero");
        }
        for (int j = 0; j < embeddingSize; j++) {
            representation[j]/=indicesWeightSum;
            points[bootStrappedSentenceId][j] = representation[j];
        }
        return representation;
    }

    public void getAggregatedRepresentationCopiedatIndex(int[] indices, int embeddingSize, int bootStrappedSentenceId) {
        double[] representation = new double[embeddingSize];
        double[] indicesWeight = new double[indices.length];
        for(int i=0;i<indicesWeight.length;i++)
            indicesWeight[i] = 1d;
        double indicesWeightSum = 0.0;
        for (int i = 0; i < indices.length; ++i) {
            for (int j = 0; j < embeddingSize; ++j) {
                if (indices[i] < 0) {
                    double[] arrd = representation;
                    int n = j;
                    arrd[n] = arrd[n] - indicesWeight[i] * points[Math.abs(indices[i])][j];
                    continue;
                }
                double[] arrd = representation;
                int n = j;
                arrd[n] = arrd[n] + indicesWeight[i] * points[Math.abs(indices[i])][j];
            }
            indicesWeightSum += indicesWeight[i];
        }
        int j = 0;
        while (j < embeddingSize) {
            double[] arrd = representation;
            int n = j++;
            arrd[n] = arrd[n] / indicesWeightSum;
        }
        for (j = 0; j < embeddingSize; ++j) {
            points[bootStrappedSentenceId][j] = representation[j];
        }
    }




    public static double getVectorLength(double[][] points, int index, int embeddingSize) {
        double length = 0.0;
        for (int j = 0; j < embeddingSize; ++j) {
            length += points[index][j] * points[index][j];
        }
        return Math.sqrt(length);
    }

    public static double cosineSimilarity(List<Double> vectorA, List<Double> vectorB) {
        double dotProduct = 0.0;
        double normA = 0.0;
        double normB = 0.0;
        for (int i = 0; i < vectorA.size(); ++i) {
            dotProduct += vectorA.get(i) * vectorB.get(i);
            normA += Math.pow(vectorA.get(i), 2.0);
            normB += Math.pow(vectorB.get(i), 2.0);
        }
        if (normA - 0.0 < 1.0E-8) {
            normA = 1.0;
        }
        if (normB - 0.0 < 1.0E-8) {
            normB = 1.0;
        }

        return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
    }

    public static double cosineSimilarity(int vecA, int vecB, double [][] points) {
        List<Double> vectorA = new ArrayList<>();
        List<Double> vectorB = new ArrayList<>();

        for (int i = 0; i < StaticVariables.embedding_size; i++) {
            vectorA.add(points[vecA][i]);
            vectorB.add(points[vecB][i]);
        }

        double dotProduct = 0.0;
        double normA = 0.0;
        double normB = 0.0;
        for (int i = 0; i < vectorA.size(); ++i) {
            dotProduct += vectorA.get(i) * vectorB.get(i);
            //dotProduct += vectorB.get(i) * 1;
            normA += Math.pow(vectorA.get(i), 2.0);
            normB += Math.pow(vectorB.get(i), 2.0);
        }
        if (normA - 0.0 < 1.0E-8) {
            normA = 1.0;
        }
        if (normB - 0.0 < 1.0E-8) {
            normB = 1.0;
        }
        //return dotProduct;
        //return dotProduct/Math.sqrt(normB);
        return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
    }


    public static List<Double> getEntityEmbedding(Set<List<String>> foundSpans, SentenceEmbedding sm) {
        List embedding = new ArrayList<Double>();
        int count = 0;
        for (List<String> s : foundSpans) {
            if (count == 0) {
                for (Double d : sm.getSentenceEmbedding(s)) {
                    embedding.add(d);
                }
            } else {
                List<Double> temp_embedding = sm.getSentenceEmbedding(s);
                embedding = SentenceEmbedding.addEmbeddings(embedding, temp_embedding);
            }
            ++count;
        }
        for (int i = 0; i < embedding.size(); ++i) {
            double val = (double) embedding.get(i);
            embedding.set(i, val / (double)count);
        }
        return embedding;
    }

    public static List<Double> computeScore(Set<List<String>> typeSpans, Set<List<String>> fullTypeSpans, List<List<Double>> targetEntityTokenEmbedding, List<List<Double>> targetEntityContextEmbedding, SentenceEmbedding sm) {
        double maxEntityTokenScore = 0.0;
        double maxEntityContextScore = 0.0;
        double maxQueryScore = 0.0;
        for (List<String> typeSpan : typeSpans) {
            double temp;
            if (fullTypeSpans.contains(typeSpan)) continue;
            for (List<Double> d : targetEntityTokenEmbedding) {
                temp = EmbeddingSearch.cosineSimilarity(d, sm.getSentenceEmbedding(typeSpan));
                if (temp <= maxEntityTokenScore) continue;
                maxEntityTokenScore = temp;
            }
            for (List<Double> d : targetEntityContextEmbedding) {
                //This line would be uncommented for searching without KNN
                temp = EmbeddingSearch.cosineSimilarity(d, sm.getEntityEmbedding(typeSpan));
                //The line below is for KNN search
                //temp = sm.getEntityScore(typeSpan);

                if (temp <= maxEntityContextScore) continue;
                maxEntityContextScore = temp;
            }

            //temp = EmbeddingSearch.cosineSimilarity(d, sm.getEntityEmbedding(typeSpan));
        }
        ArrayList<Double> score = new ArrayList<>();
        score.add(maxEntityContextScore);
        score.add(maxEntityTokenScore);
        return score;
    }


    public Set<List<String>> getTopkEntities(Set<List<String>> typeSpans, double[] foundSpansEmbedding){

        Set<List<String>> filteredTypeSpans = new HashSet<>();
        ArrayList<SearchResult> results = new ArrayList<>(typeSpans.size());

        for(List<String> typeSpan: typeSpans) {
            //The line below takes context into expansion
            SearchResult sr = new SearchResult(typeSpan, EmbeddingSearch.cosineSimilarity(initialQueryRepresentation, sm.getEntityEmbedding(typeSpan)));
            //The line below does not take context into expansion
            //SearchResult sr = new SearchResult(typeSpan, EmbeddingSearch.cosineSimilarity(foundSpansEmbeddingList, sm.getEntityEmbedding(typeSpan)));
            results.add(sr);
        }
        results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        int median = StaticVariables.topkEntities;
        //int median = results.size()/4;
        for(int i=0;i<median && i<results.size();i++){
            filteredTypeSpans.add(results.get(i).getTypeSpan());
        }
        //filteredTypeSpans.add(results.get(median).getTypeSpan());
        return filteredTypeSpans;
    }

    public static List<Double> computeScoreUpdated(Set<List<String>> typeSpans, Set<List<String>> fullTypeSpans, List<List<Double>> targetEntityTokenEmbedding, List<List<Double>> targetEntityContextEmbedding, SentenceEmbedding sm, List<Double> initialQueryRepresentation, Double initialQueryVectorLength) {
        double maxEntityTokenScore = 0.0;
        double maxEntityContextScore = 0.0;
        double maxQueryScore = 0.0;
        for (List<String> typeSpan : typeSpans) {
            double temp;
            if (fullTypeSpans.contains(typeSpan)) continue;
            for (List<Double> d : targetEntityTokenEmbedding) {
                temp = EmbeddingSearch.cosineSimilarity(d, sm.getSentenceEmbedding(typeSpan));
                if (temp <= maxEntityTokenScore) continue;
                maxEntityTokenScore = temp;
            }
            for (List<Double> d : targetEntityContextEmbedding) {
                //This line would be uncommented for searching without KNN
                temp = EmbeddingSearch.cosineSimilarity(d, sm.getEntityEmbedding(typeSpan));
                //The line below is for KNN search
                //temp = sm.getEntityScore(typeSpan);

                if (temp <= maxEntityContextScore) continue;
                maxEntityContextScore = temp;
            }

            temp = EmbeddingSearch.cosineSimilarity(initialQueryRepresentation, sm.getEntityEmbedding(typeSpan));
            if(maxQueryScore > temp)
                maxQueryScore = temp;
        }
        maxQueryScore = EmbeddingSearch.cosineSimilarity(initialQueryRepresentation, sm.getEntityEmbeddingWithSentences(typeSpans));
        ArrayList<Double> score = new ArrayList<>();
        score.add(maxQueryScore);
        score.add(maxEntityTokenScore);
        return score;
    }


    public static List<Double> computeScoreOriginal(Set<List<String>> typeSpans, Set<List<String>> fullTypeSpans, List<List<Double>> targetEntityTokenEmbedding, List<List<Double>> targetEntityContextEmbedding, SentenceEmbedding sm) {
        double maxEntityTokenScore = 0.0;
        double maxEntityContextScore = 0.0;
        for (List<String> typeSpan : typeSpans) {
            double temp;
            //if (fullTypeSpans.contains(typeSpan)) continue;
            for (List<Double> d : targetEntityTokenEmbedding) {
                temp = EmbeddingSearch.cosineSimilarity(d, sm.getSentenceEmbedding(typeSpan));
                if (temp <= maxEntityTokenScore) continue;
                maxEntityTokenScore = temp;
            }
            for (List<Double> d : targetEntityContextEmbedding) {
                temp = EmbeddingSearch.cosineSimilarity(d, sm.getEntityEmbedding(typeSpan));
                if (temp <= maxEntityContextScore) continue;
                maxEntityContextScore = temp;
            }
        }
        ArrayList<Double> score = new ArrayList<>();
        score.add(maxEntityContextScore);
        score.add(maxEntityTokenScore);
        return score;
    }

    public static List<List<Double>> targetEntityTokenEmbedingScore(SentenceEmbedding sm, Set<List<String>> foundSpans) {
        ArrayList<List<Double>> targetEntityTokenEmbedding = new ArrayList<List<Double>>();
        for (List<String> stringList : foundSpans) {
            targetEntityTokenEmbedding.add(sm.getSentenceEmbedding(stringList));
        }
        return targetEntityTokenEmbedding;
    }

    public static List<List<Double>> targetEntityContextEmbedingScore(SentenceEmbedding sm, Set<List<String>> foundSpans) {
        ArrayList<List<Double>> targetEntityContextEmbedding = new ArrayList<>();
        for (List<String> stringList : foundSpans) {
            targetEntityContextEmbedding.add(sm.getEntityEmbedding(stringList));
        }
        return targetEntityContextEmbedding;
    }

    public static List<Double> targetEntityContextEmbedingAverageScore(SentenceEmbedding sm, Set<List<String>> foundSpans) {
        int embeddingSize = 0;
        ArrayList<List<Double>> targetEntityContextEmbedding = new ArrayList<>();
        for (List<String> stringList : foundSpans) {
            List<Double> weight = new ArrayList<>();
            for(int i=0;i<stringList.size();i++){
                weight.add(1.0);
            }
            targetEntityContextEmbedding.add(sm.getSentenceEmbedding(stringList, weight));
        }
        embeddingSize = targetEntityContextEmbedding.get(0).size();
        double average[] = new double[embeddingSize];

        for(List<Double> doubleList: targetEntityContextEmbedding){
            for(int i=0;i < embeddingSize;i++){
                average[i]+= doubleList.get(i);
            }
        }
        for(int i=0; i<embeddingSize; i++){
            average[i]/= (double) foundSpans.size();
        }
        ArrayList<Double> a = new ArrayList<>();
        for(int i=0; i<embeddingSize;i++){
            a.add(average[i]);
        }
        return a;
    }


//    public static double[] getRepresentation(List<String> str, SentenceEmbedding sm){
//
//    }

    public double[] combineEntityRepresentation(double[] expandedEmbedding, double[] queryEmbedding, int bootstrappedSentenceId, int embeddingSize){

        double[] finalEmbedding = new double[embeddingSize];
        for(int i=0; i<embeddingSize; i++) {
            //finalEmbedding[i]+= (StaticVariables.sentenceExpansionWeight * points[bootstrappedSentenceId][i] + StaticVariables.queryEntityExpansionWeight * queryEmbedding[i] + StaticVariables.otherEntityExpansionWeight * expandedEmbedding[i]);
            finalEmbedding[i]+= (StaticVariables.sentenceExpansionWeight * points[bootstrappedSentenceId][i] + StaticVariables.queryEntityExpansionWeight * queryEmbedding[i] + StaticVariables.otherEntityExpansionWeight * expandedEmbedding[i]);
            //finalEmbedding[i]+= (StaticVariables.sentenceExpansionWeight * points[bootstrappedSentenceId][i] + StaticVariables.queryEntityExpansionWeight * queryEmbedding[i]);

            //finalEmbedding[i]+= (StaticVariables.sentenceExpansionWeight * points[bootstrappedSentenceId][i] + StaticVariables.queryEntityExpansionWeight * queryEmbedding[i]);
            //+ StaticVariables.otherEntityExpansionWeight * expandedEmbedding[i]);
        }
        return finalEmbedding;
    }

    public double[] computeEntitySetRepresentation(Set<List<String>> typeSpans, int embeddingSize) {
        double[] finalEmbedding = new double[embeddingSize];
        int count = 0;
        for(List<String> span: typeSpans) {
            List<Double> weight = new ArrayList<>();
            for(int i=0;i<span.size();i++){
                weight.add(1.0);
            }
            List<Double> embedding = sm.getSentenceEmbedding(span, weight);
            int index = 0;
            for(Double d: embedding){
                finalEmbedding[index]+=d;
                //finalEmbedding[index]+= initialQueryRepresentation.get(index);
                index++;
            }
            count++;
        }
        if(count==0) count++;
        for(int i=0;i<finalEmbedding.length; i++){
            finalEmbedding[i]/=count;
            //finalEmbedding[i]/=1;
            //finalEmbedding[i] = 0.00001;
        }

        return finalEmbedding;
    }

    public List<Double> computeSentenceSetRepresentation(List<List<String>> sentences, List<List<Double>> weights, int embeddingSize) {
        double[] finalEmbedding = new double[embeddingSize];
        List<Double> finalEmbeddingList = new ArrayList<>();
        int count = 0;
        for(List<String> span: sentences) {
            List<Double> embedding = sm.getSentenceEmbedding(span, weights.get(count));
            int index = 0;
            for(Double d: embedding){
                finalEmbedding[index]+=d;
                //finalEmbedding[index]+= initialQueryRepresentation.get(index);
                index++;
            }
            count++;
        }
        if(count==0) count++;
        for(int i=0;i<finalEmbedding.length; i++){
            finalEmbedding[i]/=count;
        }
        for(int i=0;i<finalEmbedding.length; i++) {
            finalEmbeddingList.add(finalEmbedding[i]);
        }
        return finalEmbeddingList;
    }

    //considering entity distance are inclusive
    public static double distanceToEntity(Integer wordPosition, Integer entityBeginIndex, Integer entityEndIndex, Integer sentenceLength){
        wordPosition++; entityBeginIndex++; entityEndIndex++;

        if (wordPosition >= entityBeginIndex && wordPosition <= entityEndIndex)
            return 1.0/Math.abs(entityBeginIndex - entityEndIndex);
        else if (wordPosition < entityBeginIndex){
            return Math.abs(wordPosition - entityBeginIndex)/(1.0 * entityBeginIndex);
        }else{
            double val = Math.abs(sentenceLength - entityEndIndex);
            return 1.0 - (Math.abs(wordPosition - entityEndIndex)/(1.0 * val));
        }
    }


    public List<SearchResult> queryExpansion(int bootStrappedSentenceID, String expansionApproach, String expansionParameter, Set<List<String>> availableSpans, String targetEntityType, int topk) {
        Set<List<String>> foundSpans = new HashSet<>();
        List<SearchResult> searchResults = new ArrayList<>();
        DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootStrappedSentenceID), etype);
        //added new spans from availableSpans found in query with the foundSpans
        int row = 0;
        int countPRF = 0;
        //if(expansionParameter.equals(StaticVariables.expansionParameterPRF) && topk > 0)
        //    countPRF = topk;
        //If we use top-k=0 and expansionParameter as PRF we will take countPRF as 0 and countPRF would be determined from the search results that is computed with prior
        //embedding search is with PRF and Lucene search is with top-k. row is an index variable
        //List<Integer> pseudoAndFullPositives = new ArrayList<>();


        //tokens from judged sentences are being added to queryTokenList. Actually, we are forming a large sentence.
        int fullPositiveRow=0;
        List<Island> islands;
        Set<Integer> sentences = null;
        if (expansionApproach.equals(StaticVariables.expansionApproachIsland)) {
            islands = EntityGraph.createIslands(mergedList, availableSpans, etype, inputFileName);
            for (Island island : islands) {
                if (island.containsSentence(bootStrappedSentenceID)) {
                    sentences = island.getSentences();
                    break;
                }
            }
        }
        //Retrieved sentences from island. island is defined in the paper.
        //System.out.println(sentences.size() + " sentences found in target island");
        FeatureExtractor featureExtractor = null;
        Set<List<String>> typeSpans = null;
        for (List<SimpleToken> tokenList : mergedList) {
            featureExtractor = new FeatureExtractor(mergedList.get(row), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityList();
            //typeSpans are essentially the output of the NER tagger. They are extracted with featureExtractor
            for (List<String> strList : foundSpans) {
                if (DataSelector.containsFoundEntity(tokenList, strList) != 1) continue;
                sm.updateContextExpansionMap(strList, row);
            }
            //we are also doing entity embedding in this loop. We will have to use user provided entities for computing entity contextual similarity.
            for (List<String> strList : typeSpans) {
                sm.updateContextExpansionMap(strList, row);
            }
            lucene.addTokenList(row, tokenList);
            //we also added sentences to lucene object that has been created in memory
            if (expansionParameter.equals(StaticVariables.expansionParameterPRF)) {
                if (expansionApproach.equals(StaticVariables.expansionApproachAll)) {
                    if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, availableSpans) == 1) {
                        prior[row] = 10.0;
                        ++countPRF;
                    }
                    //pseudoAndFullPositives.add(row);
                }else if (expansionApproach.equals(StaticVariables.expansionApproachEmbedding)) {
                    if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, foundSpans) == 1) {
                        prior[row] = 10.0;
                        ++countPRF;
                    }
                }
                else if (expansionApproach.equals(StaticVariables.expansionApproachIsland)) {
                    if(sentences.contains(row)) {
                        prior[row] = 10.0;
                        ++countPRF;
                    }
                }
                else if (expansionApproach.equals(StaticVariables.expansionApproachFull)) {
                    if(fullPositives.contains(tokenList)) {
                        prior[row] = 10.0;
                        ++countPRF;
                    }
                }
            }
            //If there exists an expansionParamemter we try to work out certain things like adding prior and increment countPRF.
            //We increase countPRF if there exists a sentence with foundEntity. Then we set a limit on countPRF if there exists a top-k. top-k imposes a limit on countPRF.
            List<Double> doubleList = sm.getSentenceEmbedding(EmbeddingSearch.simpleTokenToList(tokenList));
            int col = 0;
            Iterator iterator = doubleList.iterator();
            while (iterator.hasNext()) {
                double d = (Double)iterator.next();
                points[row][col++] = d;
                vectorLength[row]+= d*d;
                if(row == bootStrappedSentenceID)
                    initialQueryRepresentation.add(d);
            }
            //done with computing the embedding and populating the points array
            if(fullPositives.contains(tokenList)) {
                List<Double> doubleListJudgedWithoutEntityToken = sm.getSentenceEmbedding(EmbeddingSearch.simpleTokenToListWithoutEntityToken(tokenList, etype));
                iterator = doubleListJudgedWithoutEntityToken.iterator();
                col = 0;
                while (iterator.hasNext()) {
                    double d = (Double) iterator.next();
                    fullPositivePoints[fullPositiveRow][col++] = d;
                    vectorLengthFullPositives[fullPositiveRow]+=(d*d);
                }
                if (vectorLengthFullPositives[fullPositiveRow] - 0.0 < 1.0E-8) {
                    vectorLengthFullPositives[fullPositiveRow] = 1.0;
                }
                //computing the vector length by computing the square root.
                fullPositiveRow++;
            }

            if (vectorLength[row] - 0.0 < 1.0E-8) {
                vectorLength[row] = 1.0;
            }
            //we can not allow vector length to be zero, it will result in a division error.
//            if(row==bootStrappedSentenceID){
//
//            }
            ++row;
        }
        //sm.constructEntityEmbedding(points);
        //In the previous loop we worked on creating contextual embedding for typeSpans and foundSpans. That is we kept track of sentences where they appear. Now with the previous line
        //we compute their embedding and store them in a map entityEmbeddingContextExpansion in sentenceEmbedding. This map contains a lowercase string and a list of double denoting the embedding vector.

        //sm.constructEntityEmbedding(mergedList, sm);
        //this indicates an alternative which is token based not sentence based.


        for (int i = 0; i < vectorLength.length; i++) {
            vectorLength[i] = Math.sqrt(vectorLength[i]);
            if (vectorLength[i] - 0d < 0.00000001) vectorLength[row] = 1;
            if(row==bootStrappedSentenceID)
                initialQueryVectorLength = vectorLength[row];
            //System.out.print(vectorLength[i] + ",");
        }
        //computing the vector length by computing the square root.

        //context expansion begins here
        int[] expandedSentences;
        double[] expandedSentencesWeight;
        if (expansionParameter.equals(StaticVariables.expansionParametertopk)) {
            expandedSentences = new int[topk];
            expandedSentencesWeight = new double[topk];
        }else if (expansionParameter.equals(StaticVariables.expansionParameterPRF)){
            //else if (expansionParameter.equals(StaticVariables.expansionParameterPRF)) {
            //If expansionParameter is PRF, I have prepared countPRF for all possible approaches like Full, All, Island, Embedding
            expandedSentences = new int[countPRF];
            expandedSentencesWeight = new double[countPRF];
        }else{
            expandedSentences = new int[countPRF];
            expandedSentencesWeight = new double[countPRF];
        }

        /*else {
            //System.out.println("none");
            //System.out.println(fullPositives.size());
            expandedSentences = new int[fullPositives.size()];
            expandedSentencesWeight = new double[fullPositives.size()];
        }*/
        //Map<Integer, Double> hashMap = lucene.search(mergedList.get(bootstrapSentenceId), StaticVariables.sizeOfInitialRetrieval);
        List<Integer> l;
        if (expansionApproach.equals(StaticVariables.expansionApproachLucene)) {
            //only top-k is relevant for lucene search
            l = lucene.search(mergedList.get(bootStrappedSentenceID));
            row = 0;
            for (Integer s : l) {
                expandedSentences[row++] = s;
                if (row == topk)
                    break;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, expandedSentences, embeddingSize, bootStrappedSentenceID);
            vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
        } else if (expansionApproach.equals(StaticVariables.expansionApproachEmbedding) || expansionApproach.equals(StaticVariables.expansionApproachFull)) {
            row = 0; //works as an index to expanded sentences
            searchResults = EmbeddingSearch.search(bootStrappedSentenceID, points, mergedList.size(), embeddingSize, prior, vectorLength, countPRF);
            for (SearchResult s : searchResults) {
                expandedSentences[row] = s.getSentenceId();
                //+new PrintResults().printSimpleTokenSimply(mergedList.get(s.getSentenceId()), "sentences query expansion");
                //prf.add(s.getSentenceId());
                //System.out.println(s.getScore());
                //PrintResults pr = new PrintResults();
                //pr.printSimpleToken(mergedList.get(s.getSentenceId()), "found");
                if (expansionParameter.equals(StaticVariables.expansionParametertopk)) {
                    if (row == topk)
                        break;
                    if(s.getSentenceId()==bootStrappedSentenceID){
                        expandedSentencesWeight[row] = topk;
                    }else {
                        //How do we determine weights of expanded sentences
                        expandedSentencesWeight[row] = topk - row;
                    }
                } else if (expansionParameter.equals(StaticVariables.expansionParameterPRF)) {
                    if (row == countPRF)
                        break;
                    //Question is how we can find the weight of an expansion sentence??
                    //create a submatrix
                    //weighting scheme 0-> uniform; 1->linear; 2->score_based
                    if(s.getSentenceId()==bootStrappedSentenceID){
                        if(StaticVariables.weightingScheme==0)
                            expandedSentencesWeight[row] = 1;
                        else if(StaticVariables.weightingScheme==1)
                            expandedSentencesWeight[row] = countPRF;
                        else if(StaticVariables.weightingScheme==2)
                            expandedSentencesWeight[row] = s.getScore();
                        else
                            expandedSentencesWeight[row] = Math.exp(s.getScore());
                    }else {
                        if(StaticVariables.weightingScheme==0)
                            expandedSentencesWeight[row] = 1;
                        else if(StaticVariables.weightingScheme==1)
                            expandedSentencesWeight[row] = countPRF - row;
                        else if(StaticVariables.weightingScheme==2)
                            expandedSentencesWeight[row] = s.getScore();
                        else
                            expandedSentencesWeight[row] = Math.exp(s.getScore());
                    }
                }
                row++;
            }

            //FOR RM3
            if(StaticVariables.expansionRM3==1) {
                PrintResults pr = new PrintResults();
                ArrayList<String> queryBeforeRetrieval = new ArrayList<>();
                ArrayList<Double> queryBeforeRetrievalWeights = new ArrayList<>();
                for(String key: queryModelMap.keySet()) {
                    queryBeforeRetrieval.add(key);
                    queryBeforeRetrievalWeights.add(queryModelMap.get(key));
                }
                List<Double> originalQuery = sm.getSentenceEmbedding(queryBeforeRetrieval, queryBeforeRetrievalWeights);
                List<Double> scores = new ArrayList<>();
                HashMap<String, Double> termHashMap = new HashMap<String, Double>();
                int indexExpandedSentencesWeight = 0;
                for (int i : expandedSentences) {
                    //pr.printSimpleTokenSimply(mergedList.get(i), "");
                    List<Double> tempPRF = new ArrayList<>();
                    for (int j = 0; j < embeddingSize; j++) {
                        tempPRF.add(points[i][j]);
                    }
                    //scores.add(0.2 * expandedSentencesWeight[indexExpandedSentencesWeight] + 0.8 * Math.exp(EmbeddingSearch.cosineSimilarity(originalQuery, tempPRF)));
                    double score = EmbeddingSearch.cosineSimilarity(originalQuery, tempPRF);
                    //scores.add(Math.exp(score) + Math.exp(expandedSentencesWeight[indexExpandedSentencesWeight]));
                    if(StaticVariables.RMweighted == 0)
                        scores.add(1.0);
                    else
                        scores.add(score);
                    //scores.add(Math.exp(score));
                    //logger.info("expansion sentence: " + indexExpandedSentencesWeight + " interaction weight: " + expandedSentencesWeight[indexExpandedSentencesWeight] + " score: " + score);
                    //scores.add(Math.exp(0.5 * expandedSentencesWeight[indexExpandedSentencesWeight] + 0.5 * EmbeddingSearch.cosineSimilarity(originalQuery, tempPRF)));
                    //scores.add(1.0);
                    indexExpandedSentencesWeight++;
                }

                int index = 0;
                ArrayList<String> expandedTermsAll = new ArrayList<>();
                ArrayList<Double> weightsAll = new ArrayList<>();

                for (int i : expandedSentences) {
                    //getting the expansion sentences one by one
                    List<SimpleToken> prfSentence = mergedList.get(i);
                    List<SimpleToken> filteredTokenList = new ArrayList<>();
                    //At first clearing the tokenlist by removing stopwords
//                    double totalCount = 0;
//                    HashMap<String, Double> wordProbabilityDictionary = new HashMap();
//                    for(SimpleToken token: prfSentence) {
//                        double count = 0;
//                        if (!Stopwords.isStopword(token.lemma)) {
//                            count+=1;
//                            totalCount+=1;
//                            filteredTokenList.add(token);
//                            int flag = 0;
//                            for (List<String> list : availableSpans) {
//                                for (String str : list) {
//                                    if (token.equals(str)) {
//                                        flag = 1;
//                                        break;
//                                    }
//                                }
//                            }
//                            if (flag == 1) {
//                                filteredTokenList.add(token);
//                                count+=5;
//                                totalCount+=5;
//                            }
//                            if (wordProbabilityDictionary.containsKey(token.lemma)){
//                                wordProbabilityDictionary.put(token.lemma, wordProbabilityDictionary.get(token.lemma) + count);
//                            }else{
//                                wordProbabilityDictionary.put(token.lemma, count);
//                            }
//                        }
//                    }
                    //QueryModel.normalizeQueryModel(wordProbabilityDictionary, totalCount);
                    //getting the score
                    Pair<Integer, Integer> pair = null;
                    for(List<String> temp: foundSpans){
                        //SentenceChunker
                        pair = DataSelector.listEntityPosition(prfSentence, temp);
                        if(pair.getKey() != 0 && pair.getValue() != 0){
                            break;
                        }
                    }

                    double score = scores.get(index);
                    //now token level score computation

                    //pushing the tokens in the available spans to check it prf with similar types of entities work on not

                    int tokenIndex = 0;
                    for (SimpleToken st : filteredTokenList) {
                        expandedTermsAll.add(st.getLemma());
                        weightsAll.add(1.0 / filteredTokenList.size());
                        //double wordProbability = distanceToEntity(tokenIndex, pair.getKey(), pair.getValue(), prfSentence.size());
                        double wordProbability = 1.0 / filteredTokenList.size();
                        if (termHashMap.containsKey(st.getLemma())) {
                                termHashMap.put(st.getLemma(), (wordProbability * score) + termHashMap.get(st.getLemma()));
                                //termHashMap.put(st.getLemma(), (wordProbabilityDictionary.get(st.lemma) * score) + termHashMap.get(st.getLemma()));
                                //termHashMap.put(st.getLemma(), (wordProbabilityDictionary.get(st.lemma)) + termHashMap.get(st.getLemma()));
                                //termHashMap.put(st.getLemma(), 1.0);
                        } else {
                                termHashMap.put(st.getLemma(), (wordProbability * score));
                                //termHashMap.put(st.getLemma(), 1.0);
                        }
                        tokenIndex++;
                    }
                    index++;
                }

                PriorityQueue<SearchResult> pq = new PriorityQueue<SearchResult>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));

                for (String key: termHashMap.keySet()) {
                    if(Stopwords.isStopword(key))continue;
                    double score = termHashMap.get(key);
                    if (pq.size() < Integer.MAX_VALUE) {
                        pq.add(new SearchResult(key, score));
                        continue;
                    }
                    SearchResult s = pq.peek();
                    if (s.getScore() >= score) continue;
                    pq.poll();
                    pq.add(new SearchResult(key, score));
                }

                termHashMap.clear();
                double sumscore = 0;
                while(pq.size()>0){
                   SearchResult sr = pq.poll();
                   termHashMap.put(sr.getSentenceIdString(), sr.getScore());
                   sumscore += sr.getScore();
                }

                QueryModel.normalizeQueryModel(termHashMap, sumscore);
                this.finalQueryModel = new HashMap<>();
                //QueryModel.mergeQueryModel(finalQueryModel, queryModelMap, 0.8);
                QueryModel.mergeQueryModel(finalQueryModel, termHashMap, 1);

                ArrayList<Pair<String, Double>> termList = new ArrayList<>();
                for (String key : finalQueryModel.keySet()) {
                    double value = finalQueryModel.get(key);
                    termList.add(new Pair<>(key, value));
                }

                Collections.sort(termList, (o1,o2) -> o2.getValue().compareTo(o1.getValue()));

                logger.info(inputFileName);
                    //pr.printSimpleTokenSimply(mergedList.get(bootStrappedSentenceID), "Query");
                logger.info("Epansion Sentences");
                for(int p=0; p <10 && p<expandedSentences.length; p++){
                    logger.info(pr.printSimpleTokenSimply(mergedList.get(expandedSentences[p]),"" ));
                }

                ArrayList<String> expandedTerms = new ArrayList<>();
                ArrayList<Double> weights = new ArrayList<>();

                //remember to change this line. It creates the finalquery model with 20 appended keywords
                finalQueryModel = new HashMap<>();

                //for(int p=0; p < termList.size(); p++){
                for(int p=0; p < 30 && p < termList.size(); p++){
                    Pair<String, Double> item = termList.get(p);
                    expandedTerms.add(item.getKey());
                    //weights.add(1.0);
                    weights.add(item.getValue());
                    finalQueryModel.put(item.getKey(), 1.0);
                }
                //The line below is for expansion with all keywords with equal weight

                //originalQuery = sm.getSentenceEmbedding(expandedTermsAll, weightsAll);
                originalQuery = sm.getSentenceEmbedding(expandedTerms, weights);
                updateQueryRepresentation(bootStrappedSentenceID, originalQuery);
                //updateQueryRepresentation(bootStrappedSentenceID, computeSentenceSetRepresentation(expandedSentenceList, weightList, embeddingSize));
                //EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, expandedSentences, scores.stream().mapToDouble(Double::doubleValue).toArray(), embeddingSize, bootStrappedSentenceID);
                //vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
            }
            else {
//                if(flag==0) {
                    EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, expandedSentences, expandedSentencesWeight, embeddingSize, bootStrappedSentenceID);
                    vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
//                }else{
//                    EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, newExpandedSentences, newExpandedSentencesWeight, embeddingSize, bootStrappedSentenceID);
//                    vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
//                }
           }
        //context expansion done. the vector is pasted at location 0 of points array
        }else if (expansionApproach.equals(StaticVariables.expansionApproachNone)){
            setPrior(prior, 0.0);
            return searchResults;
        }else{
            EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, embeddingSize, bootStrappedSentenceID, prior, countPRF);
            vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
        }

        /*else if (expansionApproach.equals(StaticVariables.expansionApproachFull)) {
            for (int i = 0; i < fullPositives.size(); i++) {
                expandedSentences[i] = i;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, expandedSentences, embeddingSize, bootStrappedSentenceID);
            vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
        } else if (expansionApproach.equals(StaticVariables.expansionApproachAll)) {
            row = 0;
            for (Integer i : pseudoAndFullPositives) {
                expandedSentences[row++] = i;
                if (row == countPRF)
                    break;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, expandedSentences, embeddingSize, bootStrappedSentenceID);
            //EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
        }else if (expansionApproach.equals(StaticVariables.expansionApproachIsland)) {
            row = 0;
            for (Integer i : pseudoAndFullPositives) {
                expandedSentences[row++] = i;
                if (row == countPRF)
                    break;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, expandedSentences, embeddingSize, bootStrappedSentenceID);
            //EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
        }*/
        setPrior(prior, 0.0);
        return searchResults;
    }



    public static void normalizeQueryModel(double[] model){
        double sum = Arrays.stream(model).sum();
        for(int i=0;i<model.length;i++){
            model[i]/=sum;
        }
    }

    //this function combines two query models assuming they are normalized
    public static void combineQueryModels(double[] model1, double[] model2, double alpha){

    }

    public static void setPrior(double prior[], double val){
        for(int i=0;i<prior.length;i++)
            prior[i] = val;
    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            /*if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
            }*/
            if(!Stopwords.isStemmedStopword(token.lemma)){
                strList.add(token.lemma);
            }
            //strList.add(token.lemma);
        }
        return strList;
        //return s.trim();
    }

    public static boolean isAllCapitalized(String str){
        if (str.toUpperCase().equals(str)) {
            return true;
        }
        else
            return false;
    }

    public static ArrayList<String> simpleTokenToListWithoutEntityToken(List<SimpleToken> tokenList, String etype){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            /*if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
            }*/
            if((!Stopwords.isStemmedStopword(token.lemma)) && (!token.truthLabel.equals(etype))){
                strList.add(token.lemma);
            }
        }
        return strList;
        //return s.trim();
    }

    public static ArrayList<SimpleToken> simpleTokenToTokenListWithoutEntityToken(List<SimpleToken> tokenList, String etype){
        ArrayList<SimpleToken> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            /*if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
            }*/
            if((!Stopwords.isStemmedStopword(token.lemma)) && (!token.truthLabel.equals(etype))){
                //if((!Stopwords.isStemmedStopword(token.lemma))){// && (!token.truthLabel.equals(etype))){
                strList.add(token);
            }
        }
        return strList;
        //return s.trim();
    }

    public List<Integer> reRankingFilter(List<Integer> ranked, List<List<SimpleToken>> mergedList, int topk, String targetEntityType) {
        List<Integer> irrelevant = new ArrayList<>();
        List<Integer> relevant = new ArrayList<>();
        int spanSize = 0;
        Set<List<String>> fullTypeSpans = new HashSet<>();
        for (int i = 0; i < topk && i < mergedList.size(); i++) {
            FeatureExtractor featureExtractor = new FeatureExtractor(mergedList.get(ranked.get(i)), targetEntityType);
            Set<List<String>> typeSpans = featureExtractor.getTargetEntityListToLowerCase();
            fullTypeSpans.addAll(typeSpans);
            if (fullTypeSpans.size() - spanSize == 0) {
                irrelevant.add(ranked.get(i));
            } else {
                //pr.printSpans(typeSpans, "type spans");
                spanSize = fullTypeSpans.size();
                //System.out.println(s.getScore() + "\t" + score + "\t" + contextExpansionScore);
                relevant.add(ranked.get(i));
            }
        }
        relevant.addAll(irrelevant);
        return relevant;
    }


    public void reRankingFilterSearchResultsEval(int bootstrapSentenceId, List<SearchResult> results, List<List<SimpleToken>> mergedList, String targetEntityType, String sortBy) {
        //List<Integer> irrelevant = new ArrayList<>();
        //List<Integer> relevant = new ArrayList<>();
        Set<List<String>> fullTypeSpans = new HashSet<>();
        FeatureExtractor featureExtractor = new FeatureExtractor(mergedList.get(bootstrapSentenceId), targetEntityType);
        Set<List<String>> typeSpans = featureExtractor.getTargetEntityListToLowerCase();
        fullTypeSpans.addAll(typeSpans);
        int spanSize = fullTypeSpans.size();

        //System.out.println(luceneScore.size());
        for (SearchResult s: results) {
            featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityListToLowerCase();
            fullTypeSpans.addAll(typeSpans);
            //setting original scores
            //original scores set
            if (fullTypeSpans.size() - spanSize == 0) {
                s.setScore(0d);
            } else {
                spanSize = fullTypeSpans.size();
                //System.out.println(s.getScore() + "\t" + score + "\t" + contextExpansionScore);
                //relevant.add(s.getSentenceId());
            }
        }

//        for(SearchResult s: results){
//            System.out.println(s.getLuceneScore());
//        }

        if(sortBy.equals("none"))
            return;
        else {
            if(sortBy.equals(StaticVariables.embeddingscore))
                results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
            if(sortBy.equals(StaticVariables.luceneScore))
                results.sort((o1, o2) -> o2.getLuceneScore().compareTo(o1.getLuceneScore()));
        }
        //relevant.addAll(irrelevant);
        //return relevant;
    }

    public void reRankingFilterWithCRF(int bootstrapSentenceId, List<SearchResult> results, List<List<SimpleToken>> mergedList, String targetEntityType, String sortBy){

    }

    public Set<List<String>> getTypeSpans(List<List<SimpleToken>> fullPositives, String targetEntityType){
        Set<List<String>> fullTypeSpans = new HashSet<>();

        for(int i=0; i<fullPositives.size(); i++){
            FeatureExtractor featureExtractor = new FeatureExtractor(fullPositives.get(i), targetEntityType);
            Set<List<String>> typeSpans = featureExtractor.getTargetEntityListToLowerCase();
            fullTypeSpans.addAll(typeSpans);
        }
        return fullTypeSpans;
    }

    public void reRankingFilterSearchResults(int bootstrapSentenceId, List<SearchResult> results, List<List<SimpleToken>> mergedList, String targetEntityType, String sortBy) {
        //List<Integer> irrelevant = new ArrayList<>();
        //List<Integer> relevant = new ArrayList<>();

        Set<List<String>> fullTypeSpans = new HashSet<>();
        FeatureExtractor featureExtractor = new FeatureExtractor(mergedList.get(bootstrapSentenceId), targetEntityType);
        Set<List<String>> typeSpans = featureExtractor.getTargetEntityListToLowerCase();
        fullTypeSpans.addAll(typeSpans);
        int spanSize = fullTypeSpans.size();

        Map<Integer, Double> luceneScore = lucene.normalizedSearchScore(mergedList.get(bootstrapSentenceId), StaticVariables.sizeOfInitialRetrieval);
        //System.out.println(luceneScore.size());
        for (SearchResult s: results) {
            featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityListToLowerCase();
            fullTypeSpans.addAll(typeSpans);
            //setting original scores
            s.setOriginalScore(s.getScore());
            if (luceneScore.containsKey(s.getSentenceId())) {
                s.setOriginalLuceneScore(luceneScore.get(s.getSentenceId()));
            }else{
                s.setOriginalLuceneScore(0d);
            }
            //original scores set
            if (fullTypeSpans.size() - spanSize == 0) {
                s.setScore(0d);
                s.setLuceneScore(0d);
                s.setCRFScore(0d);
            } else {
                spanSize = fullTypeSpans.size();
                double lscore=0d;
                if (luceneScore.containsKey(s.getSentenceId())) {
                    lscore = luceneScore.get(s.getSentenceId());
                }
                s.setLuceneScore(lscore);
                //System.out.println(s.getScore() + "\t" + score + "\t" + contextExpansionScore);
                //relevant.add(s.getSentenceId());
            }
        }

//        for(SearchResult s: results){
//            System.out.println(s.getLuceneScore());
//        }

        if(sortBy.equals("none"))
            return;
        else {
            if(sortBy.equals(StaticVariables.embeddingscore))
                results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
            if(sortBy.equals(StaticVariables.luceneScore))
                results.sort((o1, o2) -> o2.getLuceneScore().compareTo(o1.getLuceneScore()));
            if(sortBy.equals(StaticVariables.crfScore))
                results.sort((o1, o2) -> o2.getCRFScore().compareTo(o1.getCRFScore()));
        }
        //relevant.addAll(irrelevant);
        //return relevant;
    }

    public void modReRankingFilterSearchResults(int bootstrapSentenceId, List<SearchResult> results, List<List<SimpleToken>> mergedList, String targetEntityType, Set<List<String>> availableSpans, String sortBy) {
        Set<List<String>> foundSpans = new HashSet<>();
        DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootstrapSentenceId), etype);
        //Get query entities in found spans.
        HashSet<List<String>> fullTypeSpans = new HashSet<List<String>>();
        FeatureExtractor entityFeatureExtractor = new FeatureExtractor(mergedList.get(bootstrapSentenceId), targetEntityType);
        Set<List<String>> typeSpans = entityFeatureExtractor.getTargetEntityList();
        //typeSpans = getTopkEntities(typeSpans, queryExpansion(foundSpans, embeddingSize));

        //Get all the promising entities.
        fullTypeSpans.addAll(typeSpans);
        int spanSize = fullTypeSpans.size();

        List<List<Double>> targetEntityTokenEmbeddingList = EmbeddingSearch.targetEntityTokenEmbedingScore(sm, foundSpans);
        List<List<Double>> targetEntityContextEmbeddingList = EmbeddingSearch.targetEntityContextEmbedingScore(sm, foundSpans);
        for (SearchResult s : results) {
            FeatureExtractor featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityList();
            //List<Double> sentenceScore = EmbeddingSearch.computeScore(typeSpans, fullTypeSpans, targetEntityTokenEmbeddingList, targetEntityContextEmbeddingList, sm);
            List<Double> sentenceScore = EmbeddingSearch.computeScoreUpdated(typeSpans, fullTypeSpans, targetEntityTokenEmbeddingList, targetEntityContextEmbeddingList, sm, initialQueryRepresentation, initialQueryVectorLength);

            //List<Double> sentenceOriginalScore = EmbeddingSearch.computeScoreOriginal(typeSpans, fullTypeSpans, targetEntityTokenEmbeddingList, targetEntityContextEmbeddingList, sm);
            //s.setOriginalScoreEntityContext(sentenceOriginalScore.get(0));
            //s.setOriginalScoreEntity(sentenceOriginalScore.get(1));

            fullTypeSpans.addAll(typeSpans);
            if (fullTypeSpans.size() - spanSize == 0) {
                s.setScore(0d);
                s.setScoreEntity(0d);
                s.setScoreEntityContext(0d);
                //continue;
            }else {
                spanSize = fullTypeSpans.size();
                //s.setScore(s.getScore());
                //s.setScore(s.getScore() + sentenceScore.get(1));
                s.setScoreEntityContext(sentenceScore.get(0));
                s.setScoreEntity(sentenceScore.get(1));
                //s.setScore(s.getScore());
            }
        }
        //if(sortBy.equals(StaticVariables.luceneScore) || sortBy.equals(StaticVariables.embeddingscore))
        //    results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        if(sortBy.equals("none"))
            return;
        else {
            if (sortBy.equals(StaticVariables.entityTokenScore))
                results.sort((o1, o2) -> o2.getScoreEntity().compareTo(o1.getScoreEntity()));
            if (sortBy.equals(StaticVariables.entityContextScore))
                results.sort((o1, o2) -> o2.getScoreEntityContext().compareTo(o1.getScoreEntityContext()));
        }
    }

    //send expanded entities of target entity type
    public void modReRanking(int bootstrapSentenceId, List<SearchResult> results, List<List<SimpleToken>> mergedList, String targetEntityType, double[] queryEmbedding, String sortBy) {
        //Set<List<String>> foundSpans = new HashSet<>();
        //DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootstrapSentenceId), etype);
        //Get query entities in found spans.
        List<List<Double>> targetEntityTokenEmbeddingList = new ArrayList<>();
        List<List<Double>> targetEntityContextEmbeddingList = new ArrayList<>();
        List<Double> qEmbedding = new ArrayList<>();
        for(int i=0; i<queryEmbedding.length;i++){
            qEmbedding.add(queryEmbedding[i]);
        }
        targetEntityTokenEmbeddingList.add(qEmbedding);
        targetEntityContextEmbeddingList.add(qEmbedding);

        HashSet<List<String>> fullTypeSpans = new HashSet<List<String>>();
        FeatureExtractor entityFeatureExtractor = new FeatureExtractor(mergedList.get(bootstrapSentenceId), targetEntityType);
        Set<List<String>> typeSpans = entityFeatureExtractor.getTargetEntityList();
        //typeSpans = getTopkEntities(typeSpans, queryExpansion(foundSpans, embeddingSize));
        //Get all the promising entities.
        fullTypeSpans.addAll(typeSpans);
        int spanSize = fullTypeSpans.size();

        for (SearchResult s : results) {
            FeatureExtractor featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityList();
            List<Double> sentenceScore = EmbeddingSearch.computeScore(typeSpans, fullTypeSpans, targetEntityTokenEmbeddingList, targetEntityContextEmbeddingList, sm);
            fullTypeSpans.addAll(typeSpans);
            if (fullTypeSpans.size() - spanSize == 0) {
                s.setScore(0d);
                s.setScoreEntity(0d);
                s.setScoreEntityContext(0d);
            }else {
                spanSize = fullTypeSpans.size();
                s.setScoreEntityContext(sentenceScore.get(0));
                s.setScoreEntity(sentenceScore.get(1));
            }
        }
        if(sortBy.equals("none"))
            return;
        else {
            if (sortBy.equals(StaticVariables.entityTokenScore))
                results.sort((o1, o2) -> o2.getScoreEntity().compareTo(o1.getScoreEntity()));
            if (sortBy.equals(StaticVariables.entityContextScore))
                results.sort((o1, o2) -> o2.getScoreEntityContext().compareTo(o1.getScoreEntityContext()));
        }
    }


    public void expandWithLucene(List<SimpleToken> userQuery, int topk, int bootStrappedSentenceID){
        //only top-k is relevant for lucene search
        //It comes here after being expanded
        List<Integer> l = lucene.search(userQuery);
        //List<Integer> l = lucene.termBoostedSearchWithoutCarret(userQuery);
        int row = 0;
        int[] expandedSentences = new int[Math.min(topk, l.size())];

        for (Integer s : l) {
            expandedSentences[row++] = s;
            if (row == Math.min(topk, l.size()))
                break;
        }
        //System.out.println(expandedSentences.length);
        EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, expandedSentences, embeddingSize, bootStrappedSentenceID);
        vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
    }



    public void expandWithLuceneByBoosting(List<SimpleToken> userQuery, int topk, int bootStrappedSentenceID){
        //only top-k is relevant for lucene search
        List<Integer> l = lucene.search(userQuery);
        int row = 0;
        int[] expandedSentences = new int[Math.min(topk, l.size())];

        for (Integer s : l) {
            expandedSentences[row++] = s;
            if (row == Math.min(topk, l.size()))
                break;
        }
        //System.out.println(expandedSentences.length);
        EmbeddingSearch.getAggregatedRepresentationCopiedtoIndex(points, expandedSentences, embeddingSize, bootStrappedSentenceID);
        vectorLength[bootStrappedSentenceID] = EmbeddingSearch.getVectorLength(points, bootStrappedSentenceID, embeddingSize);
    }



    public void updateQueryRepresentation(int bootstrapSentenceID, List<SimpleToken> queryTokens, SentenceEmbedding sm){
        List<Double> representation = sm.getSentenceEmbedding(EmbeddingSearch.simpleTokenToList(queryTokens));
        for (int j = 0; j < representation.size(); ++j) {
            points[bootstrapSentenceID][j] = representation.get(j);
        }
        vectorLength[bootstrapSentenceID] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceID, embeddingSize);
    }

    public void updateQueryRepresentation(int bootstrapSentenceID, List<Double> embedding){
        for (int j = 0; j < embedding.size(); ++j) {
            points[bootstrapSentenceID][j] = embedding.get(j);
        }
        vectorLength[bootstrapSentenceID] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceID, embeddingSize);
    }

    //reloads the query representation that is computed initially. It is helpful when multiple rounds of search is performed
    public void reloadQueryRepresentation(int bootstrapSentenceID){
        for (int j = 0; j < this.initialQueryRepresentation.size(); ++j) {
            points[bootstrapSentenceID][j] = initialQueryRepresentation.get(j);
        }
        vectorLength[bootstrapSentenceID] = initialQueryVectorLength;
    }


    //Takes an arraylist positive and fills in the paragraph so that we can get surrounding sentences for query sentence
    public static void getParagraph(List<SimpleToken> querySentence, List<List<SimpleToken>> fullConllTrain, int windowSize, List<List<SimpleToken>> positives){
        List<List<SimpleToken>> paragraph = new ArrayList<>();
        int index = fullConllTrain.indexOf(querySentence);
        int begin_index = index - windowSize;
        if(begin_index<0) begin_index = 0;
        int end_index = index + windowSize;
        if(end_index >= fullConllTrain.size()) end_index = fullConllTrain.size()-1;
        for(int i = begin_index; i<=end_index; i++)
            paragraph.add(fullConllTrain.get(i));
        positives.addAll(paragraph);
        //return paragraph;
    }

}
