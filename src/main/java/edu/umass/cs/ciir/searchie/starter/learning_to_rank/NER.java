package edu.umass.cs.ciir.searchie.starter.learning_to_rank;

/**
 * Created by sarwar on 6/24/17.
 */
public enum NER {
    OTHER("O"),
    ORGANIZATION("ORGANIZATION"),
    NUMBER("NUMBER"),
    DOLLAR("-$-"),
    LOCATION("LOCATION"),
    TIME("TIME"),
    MISC("MISC"),
    PERSON("PERSON"),
    ORDINAL("ORDINAL"),
    DURATION("DURATION"),
    PERCENT("PERCENT"),
    MONEY("MONEY");

    private final String tag;

    private NER(String tag) {
        this.tag = tag;
    }

    /**
     * Returns the encoding for this part-of-speech.
     *
     * @return A string representing a Penn Treebank encoding for an English
     * part-of-speech.
     */
    public String toString() {
        return getTag();
    }

    protected String getTag() {
        return this.tag;
    }

    public static NER get(String value) {
        for (NER v : values()) {
            if (value.equals(v.getTag())) {
                return v;
            }
        }

        throw new IllegalArgumentException("Unknown NER tag: '" + value + "'.");
    }
}
