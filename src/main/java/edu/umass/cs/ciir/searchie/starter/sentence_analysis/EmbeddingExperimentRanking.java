package edu.umass.cs.ciir.searchie.starter.sentence_analysis;

import com.google.common.primitives.Doubles;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Created by sarwar on 5/20/17.
 */
public class EmbeddingExperimentRanking {
    static String prefix = StaticVariables.prefix;

    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");

        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);

        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        List<List<SimpleToken>> pseudoPositives = new ArrayList<>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        if (fullPositives.size() <= StaticVariables.minPositivestoTrain)
            return;
        if (fullNegatives.size() >= StaticVariables.maxNumSentence)
            return;
        Random random = new Random(System.currentTimeMillis());
        int size = fullPositives.size();
        int totalSize = size * 2;
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        mergedList.addAll(fullPositives);

        for(int i=0;i<totalSize-size;i++){
            mergedList.add(fullNegatives.get(i));
        }
        System.out.println("merged list size " + mergedList.size());
        double vectorLength[] = new double[totalSize];
        double points[][] = new double[totalSize][embeddingSize];

        //using word2vec program in C
        /*BufferedWriter br = new BufferedWriter(new FileWriter(new File(featureDirectory + "//embedding//" + inputFileName)));
        int row = 0;
        for(List<SimpleToken> tokenList: mergedList){
            ArrayList<String> strList = simpleTokenToString(tokenList, idf);
            List<Double> doubleList  = DataSelector.sentenceEmbedding(strList.get(0), strList.get(1));
            writeEmbeddingToFile(doubleList, br);
            int col = 0;
            for(double d: doubleList){
                points[row][col] = d;
                col++;
                vectorLength[row]+= (d*d);

            }
            row++;
        }
        br.close();*/
        //using embedding dump
        //loadSentenceEmbeddingFromFile(new File(featureDirectory + "//embedding//" + inputFileName), points, vectorLength);
        //using the binary dumped in text file
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        int row = 0;
        for(List<SimpleToken> tokenList: mergedList){
            System.out.println("done");
            List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList), idf);
            //writeEmbeddingToFile(doubleList, br);
            int col = 0;
            for(double d: doubleList){
                points[row][col] = d;
                col++;
                vectorLength[row]+= (d*d);

            }
            row++;
        }

        for(int i=0;i<vectorLength.length;i++){
            vectorLength[i] = Math.sqrt(vectorLength[i]);
        }
        System.out.println("sentence embedding done ");
        PriorityQueue<SearchResult> pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        //pq = new PriorityQueue<>((o1, o2) -> o1.getValue().compareTo(o2.getValue()));
        int bootstrapSentenceId = 0;
        for(int i=0;i<totalSize;i++){
            double score = 0;
            for(int j=0; j<embeddingSize; j++){
                score += (points[bootstrapSentenceId][j] * points[i][j]);
            }
            score/=vectorLength[bootstrapSentenceId];
            score/=vectorLength[i];
            if(pq.size() < size) {
                pq.add(new SearchResult(i, score));
            }else{
                SearchResult s = pq.peek();
                if(s.getScore() < score) {
                    pq.poll();
                    pq.add(new SearchResult(i, score));
                }
            }
            //System.out.println(i + "\t" + score);
        }

        List<SearchResult> results = new ArrayList<>(pq.size());
        results.addAll(pq);
        //Collections.sort(results, (o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        double success = 0;
        for(SearchResult s: results){
            System.out.println(s.getSentenceId() +  "\t" + s.getScore());
            if(s.getSentenceId()>=0 && s.getSentenceId()<size){
                success++;
            }
        }
        System.out.println(size + "\t" + success/size + "\t" + inputFileName);
//        int[] centroidIndex = new Random().ints(0, (size)-1).distinct().limit(2).toArray();
//        int k = 2;
//        double centroids[][] = new double[k][embeddingSize];
//        centroidIndex[0] = random.nextInt(size);
//        centroidIndex[1] = random.nextInt(size) + size;
//
//        centroids[0] = Arrays.copyOf(points[centroidIndex[0]], embeddingSize);
//        centroids[1] = Arrays.copyOf(points[centroidIndex[1]], embeddingSize);
//
////        int n = 1000; // the number of data to cluster
////        int k = 10; // the number of cluster
////        double[][] points = new double[n][2];
////        // lets create random points between 0 and 100
////        for (int i = 0; i < n; i++) {
////            points[i][0] = Math.abs(random.nextInt() % 100);
////            points[i][1] = Math.abs(random.nextInt() % 100);
////        }
////        double[][] centroids = new double[k][2];
////        // lets create random centroids between 0 and 100 (in the same space as our points)
////        for (int i = 0; i < k; i++) {
////            centroids[i][0] = Math.abs(random.nextInt() % 100);
////            centroids[i][1] = Math.abs(random.nextInt() % 100);
////        }
//        EKmeans eKmeans = new EKmeans(centroids, points);
//        eKmeans.setIteration(100);
//        //eKmeans.setDistanceFunction(EKmeans.MANHATTAN_DISTANCE_FUNCTION);
//        eKmeans.setDistanceFunction(EKmeans.EUCLIDEAN_DISTANCE_FUNCTION);
//        //eKmeans.setListener(this);
//        eKmeans.run();
//        int[] assignments = eKmeans.getAssignments();
//        // here we just print the assignement to the console.
//        int labelZero = 0;
//        int labelOne = 0;
//        for (int i = 0; i < size/2; i++) {
//            //System.out.println(MessageFormat.format("point {0} is assigned to cluster {1}", i, assignments[i]));
//            if(assignments[i]==0)
//                labelZero++;
//            else
//                labelOne++;
//        }
//        System.out.println("fullpositives zero:" + labelZero + " one:" + labelOne);
//        labelZero = 0;
//        labelOne = 0;
//        for (int i = size/2; i < size; i++) {
//            //System.out.println(MessageFormat.format("point {0} is assigned to cluster {1}", i, assignments[i]));
//            if(assignments[i]==0)
//                labelZero++;
//            else
//                labelOne++;
//
//        }
//        System.out.println("fullnegatives zero:" + labelZero + " one:" + labelOne);
    }

    public static String simpleTokenToString(List<SimpleToken> tokenList){
        String s = "";
        for(SimpleToken token: tokenList){
            if(token.truthLabel.equals("LIST")){
                System.out.println(token.lemma);
                continue;
            }
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            if(token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if(token.lemma.length()>2) {
                s += token.lemma;
                s += " ";
            }
        }
        return s.trim();
    }
    public static ArrayList<String> simpleTokenToString(List<SimpleToken> tokenList, IDF idf){
        ArrayList<String> strList = new ArrayList<>();
        String tokenString = "";
        String idfString = "";
        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            if(token.truthLabel.equals("LIST")){
                System.out.println(token.lemma);
                continue;
            }
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
                tokenString += token.lemma;
                tokenString += " ";
                idfString += idf.getIDF(token.lemma);
                idfString += " ";
            }

        }
        strList.add(tokenString);
        strList.add(idfString);
        return strList;
        //return s.trim();
    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            if(token.truthLabel.equals("LIST")){
                //System.out.println(token.lemma);
                continue;
            }
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
                strList.add(token.lemma);
            }
        }
        return strList;
        //return s.trim();
    }

    /*public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            if(token.truthLabel.equals("LIST")){
                System.out.println(token.lemma);
                continue;
            }
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
                strList.add(token.lemma);
            }
        }
        return strList;
        //return s.trim();
    }*/


    public static void writeEmbeddingToFile(List<Double> l, BufferedWriter br){
        String str = "";
        for(Double d: l){
            str+= d;
            str+="\t";
        }
        try {
            br.write(str);
            br.write("\n");
        } catch (IOException e) {
            System.out.println("error in printing embeddings");
            e.printStackTrace();
        }
    }

    public static void loadSentenceEmbeddingFromFile(File f, double[][] points, double[] vectorLength){
        try {
            //System.out.println(f);
            Scanner sc = new Scanner(f);
            int row = 0;
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                //System.out.println(line);
                StringTokenizer st = new StringTokenizer(line, "\t");
                int col = 0;
                int flag = 0;
                while(st.hasMoreTokens()){
                    String temp = st.nextToken();
                    Double d;
                    if(Doubles.tryParse(temp)!=null) {
                        d = Double.parseDouble(temp);
                    }else{
                        System.out.println("error parsing " + temp);
                        d = 0.001d;
                    }
                    points[row][col] = d;
                    vectorLength[row]+= (d*d);
                    col++;
                }
//                if(flag==1) {
//                    System.out.println("error parsing");
//                    System.out.println(line);
//                }
                row++;
            }
            System.out.println("number of rows scanned " + row);
        } catch (FileNotFoundException e) {
            System.out.println("input embedding file not found");
            e.printStackTrace();
        }

    }
}
