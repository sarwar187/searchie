package edu.umass.cs.ciir.searchie.starter.interaction_experiment_new;

import edu.umass.cs.ciir.searchie.starter.feature_engineering.Oracle;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.WeightInfo;
import gnu.trove.map.hash.TObjectFloatHashMap;

/**
 * Created by sarwar on 3/16/17.
 */
public class Experiment {

    public interface FixWeight {
        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, double percent, Oracle oracle);
        public WeightInfo getWeightInfo();
    }
}
