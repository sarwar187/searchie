package edu.umass.cs.ciir.searchie.starter.Test.volume_example;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import edu.umass.cs.ciir.searchie.starter.Test.volume_example.VolumeContainer;

public class LoadVolume {
    public static void main(String[] args) throws FileNotFoundException {
        Gson gson = new Gson();
        JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/Desktop/volume.json"), JsonElement.class);
        System.out.println(jsonElement);
        VolumeContainer vc = gson.fromJson(jsonElement.toString(), VolumeContainer.class);
        System.out.println(vc.vol);
        System.out.println(vc.volumes.get(0).getName());
        System.out.println(vc.volumes.get(0).getManaged());

        //for (Task task : fromJson) {
        //    System.out.println(task);
        //}
    }
}
