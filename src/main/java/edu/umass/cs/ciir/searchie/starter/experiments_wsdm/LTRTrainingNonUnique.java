package edu.umass.cs.ciir.searchie.starter.experiments_wsdm;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 8/11/17.
 */
public class LTRTrainingNonUnique {
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args) throws IOException {
        double[] expandedSentencesWeight;
        int[] expandedSentences;
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", "/home/sarwar/crfsuite/");
        String inputFileName = argp.get("file", "train_list.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int[] occurrence = new int[100];
        PrintResults pr = new PrintResults();
        LuceneInMemory lucene = new LuceneInMemory();
        ArrayList<List<SimpleToken>> fullPositives = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> fullNegatives = new ArrayList<List<SimpleToken>>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);

            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }

        int bootstrapSentenceId = 0;
        ArrayList<List<SimpleToken>> mergedList = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> positives = new ArrayList<List<SimpleToken>>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);
        String output="";
        positives.add(mergedList.get(bootstrapSentenceId));
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        FeatureExtractor f = new FeatureExtractor((List)mergedList.get(bootstrapSentenceId));
        String targetEntityType = f.getTargetEntityType();
        //System.out.println(targetEntityType);
        //FeatureExtractor queryFeatureExtractor = new FeatureExtractor((List)mergedList.get(0));
        //String targetEntityType = queryFeatureExtractor.getTargetEntityType();
        //Set<List<String>> foundSpansCaseSensitive = DataSelector.getUniqueSpansCaseSensitive(positives, etype);
        //List<Double> targetEntityEmbedding = EmbeddingSearch.getEntityEmbedding(foundSpansCaseSensitive, sm);


        //Evaluation eval = new Evaluation();
        String expansionApproach = StaticVariables.expansionApproachEmbedding;
        String expansionParameter = StaticVariables.expansionParameterPRF;
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        int topk = 0;
        EmbeddingSearch embeddingSearch = new EmbeddingSearch(mergedList, fullPositives, embeddingSize, etype, inputFileName, lucene, sm);
        embeddingSearch.queryExpansion(bootstrapSentenceId,expansionApproach,expansionParameter,availableSpans, targetEntityType, topk);
        //Query Expansion Ends
        //Seaching with New Query Representation
        //System.out.println(sm.entityEmbeddingContextExpansion.size());
        List<SearchResult> results = embeddingSearch.search(bootstrapSentenceId);
        //List<SearchResult>
        //eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
        //if(eval.getNullEntities() == 1) return;
        //output+=eval.getEvaluationScoresString();
        //output+="\t";
        embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, "none");
        //embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, StaticVariables.luceneScore);
        embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, "none");
        //embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, StaticVariables.entityContextScore);
        //eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
        //output+=eval.getEvaluationScoresString();
        //System.out.println(output);
        BufferedWriter br= new BufferedWriter(new FileWriter(new File(featureDirectory + featureFileName)));
        NumberFormat formatter = new DecimalFormat("#0.000");
        Set<List<String>> foundSpans = new HashSet<>();
        Set<List<String>> querySpans = new HashSet<>();
        DataSelector.addNewSpansWithGroundTruth(querySpans, availableSpans, mergedList.get(bootstrapSentenceId), etype);
        DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootstrapSentenceId), etype);
        DescriptiveStatistics statsScore = new DescriptiveStatistics();
        DescriptiveStatistics statsLuceneScore = new DescriptiveStatistics();
        DescriptiveStatistics statsEntityScore = new DescriptiveStatistics();
        DescriptiveStatistics statsEntityContextScore = new DescriptiveStatistics();
        for(SearchResult s: results){
            //s.setScore((0.4 * s.getScore()) + (0.2 * s.getLuceneScore()) + (0.1 * s.getScoreEntity()) + (0.3 * s.getScoreEntityContext()));
            //s.setScore((0.4 * s.getScore()) + (0.2 * s.getLuceneScore()) + (0.1 * s.getScoreEntity()) + (0.3 * s.getScoreEntityContext()));
            statsScore.addValue(s.getScore());
            statsLuceneScore.addValue(s.getLuceneScore());
            statsEntityScore.addValue(s.getScoreEntity());
            statsEntityContextScore.addValue(s.getScoreEntityContext());
        }

        Set<List<String>> fullTypeSpans = new HashSet<>();
        FeatureExtractor featureExtractor = new FeatureExtractor(mergedList.get(bootstrapSentenceId), targetEntityType);
        Set<List<String>> typeSpans = featureExtractor.getTargetEntityListToLowerCase();
        fullTypeSpans.addAll(typeSpans);
        int spanSize = fullTypeSpans.size();

        for (SearchResult s : results) {
            //positives.add(mergedList.get(s.getSentenceId()));
            //int successful = DataSelector.addNewSpansWithGroundTruth(constantSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            foundSpans.clear();
            foundSpans.addAll(querySpans);
            //foundSpans.addAll(constantSpans);
            //constantSpans.clear();
            //constantSpans = DataSelector.getUniqueSpans(positives, etype);
            String featureString = inputFileName + "," + s.getSentenceId() + ",";
            FeatureExtractor fe = new FeatureExtractor(mergedList.get(s.getSentenceId()));
            featureString += fe.posNERFeatures();
            fe = new FeatureExtractor(mergedList.get(bootstrapSentenceId));
            featureString += fe.posNERFeatures();
            featureString += String.valueOf(statsScore.getStandardDeviation()) + ",";
            featureString += String.valueOf(statsLuceneScore.getStandardDeviation()) + ",";
            featureString += String.valueOf(statsEntityScore.getStandardDeviation()) + ",";
            featureString += String.valueOf(statsEntityContextScore.getStandardDeviation()) + ",";
            int containsQueryEntity = DataSelector.containsQueryEntity(mergedList.get(s.getSentenceId()), querySpans);
            featureString+= String.valueOf(containsQueryEntity) + ",";


            featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityListToLowerCase();
            fullTypeSpans.addAll(typeSpans);
            if (fullTypeSpans.size() - spanSize == 0) {
                featureString+= "0,";
            }else{
                featureString+= "1,";
            }

            //List<SimpleToken> t = mergedList.get(i).get();
            if (successful == 1) {
                featureString += (formatter.format(s.getScore()) + "," + formatter.format(s.getLuceneScore()) + "," + formatter.format(s.getScoreEntity()) + "," + formatter.format(s.getScoreEntityContext()) + "," );
                featureString += (formatter.format(s.getOriginalScore()) + "," + formatter.format(s.getOriginalLuceneScore()) + "," + formatter.format(s.getOriginalScoreEntity()) + "," + formatter.format(s.getOriginalScoreEntityContext()) + "," + mergedList.get(s.getSentenceId()).size() + "," + 1);
            } else{
                featureString += (formatter.format(s.getScore()) + "," + formatter.format(s.getLuceneScore()) + "," + formatter.format(s.getScoreEntity()) + "," + formatter.format(s.getScoreEntityContext()) + "," );
                featureString += (formatter.format(s.getOriginalScore()) + "," + formatter.format(s.getOriginalLuceneScore()) + "," + formatter.format(s.getOriginalScoreEntity()) + "," + formatter.format(s.getOriginalScoreEntityContext()) + "," + mergedList.get(s.getSentenceId()).size() + "," + 0);
            }
            //if(hashMap.containsKey(i)) {
            //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 1);
            //featureString += (formatter.format(s.getScore()) +  "," + formatter.format(s.getLuceneScore()) + "," + formatter.format(s.getScoreEntity()) + "," + formatter.format(s.getScoreEntityContext()) + "," + mergedList.get(s.getSentenceId()).size() + "," + relevanceJudgment);

            //System.out.println(featureString);
            br.write(featureString);
            br.newLine();
            //}
            //}else{
            //if(hashMap.containsKey(i)) {
            //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 0);
            //    featureString += (formatter.format(s.getScore()) + "," + formatter.format(s.getLuceneScore()) + "," + formatter.format(s.getScoreEntity()) + "," + formatter.format(s.getScoreEntityContext()) +  "," + mergedList.get(s.getSentenceId()).size() +"," + 0);
            //System.out.println(featureString);
            //    br.write(featureString);
            //    br.newLine();
            //}
            //}
            //PrintResults pr = new PrintResults();
            //pr.printSimpleToken(mergedList.get(i), "input");

        }
        //System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + (1.0 * foundSpans.size()/availableSpans.size()));
        br.close();
    }


}
