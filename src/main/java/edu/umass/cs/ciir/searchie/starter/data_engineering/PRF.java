package edu.umass.cs.ciir.searchie.starter.data_engineering;

import edu.umass.cs.ciir.searchie.starter.LinearTokenClassifier;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import static edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector.compareStringList;
import static edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector.getUniqueTokens;

/**
 * Created by sarwar on 4/19/17.
 */
public class PRF {
    public static void annotatePRF(Set<List<String>> groundTruths, List<SimpleToken> resultSeen, String eType) {
        for (List<String> tokenList : groundTruths) {
            int length = tokenList.size();
            for (int i = 0; i < resultSeen.size() - length + 1; i++) {
                List<String> temp = new ArrayList<>();
                for (int j = i; j < length; j++) {
                    temp.add(resultSeen.get(j).lemma.toLowerCase().trim());
                }
                if (DataSelector.compareStringList(temp, tokenList) == 1) {
                    for (int j = i; j < length; j++) {
                        resultSeen.get(j).truthLabel = eType;
                    }
                }
            }
        }
    }

    public static void addPRF(List<List<SimpleToken>> fullConllTrain, List<List<SimpleToken>> positives, String etype) {
        Set<String> foundTokens = DataSelector.getUniqueTokens(positives, etype);
        for (List<SimpleToken> sent : fullConllTrain) {
            ArrayList<SimpleToken> temp = new ArrayList<>();
            int flag = 0;
            for (SimpleToken token : sent) {
                SimpleToken tempToken = new SimpleToken(token);
                if (foundTokens.contains(token.lemma)) {
                    //System.out.println(token.lemma);
                    tempToken.truthLabel = etype;
                    flag = 1;
                } else {
                    tempToken.truthLabel = "O";
                }
                temp.add(tempToken);
            }
            if (flag == 1)
                positives.add(temp);
            //train.add(temp);
        }
    }

    //given a string, s and a set of strings, G find the location of any string in G, that is in s.
    //given a string, s and a set of strings, G find the location of any string in G, that is in s.
    public static void addPRFWithSpan(List<List<SimpleToken>> fullConllTrain, Set<List<String>> availableSpans, List<List<SimpleToken>> positives, String etype, Set<Integer> alreadyAdded) {
        int count=0;
        Integer index = 0;
        for (List<SimpleToken> train : fullConllTrain) {
            //make a clone of train.
            ArrayList<SimpleToken> temp = new ArrayList<>();
            for (SimpleToken s: train) {
                SimpleToken tempToken = new SimpleToken(s);
                temp.add(tempToken);
            }
            //flag=0 means we could  not find any matching span
            int flag = 0;
            //Now look for the span. Iterate throguh the available spans. If a suitable span is found annotate it as entity.
            //Otherwise annotate that span as other
            for (List<String> availableSpan : availableSpans) {
                int length = availableSpan.size();
                for (int i = 0; i < temp.size() - length + 1; i++) {
                    List<String> temp1 = new ArrayList<>();
                    for (int j = i; j < i+ length; j++) {
                        temp1.add(temp.get(j).lemma.toLowerCase().trim());
                    }
                    if (compareStringList(temp1, availableSpan) == 1) {
                        //System.out.println(availableSpan.get(0));
                        flag = 1;
                        for (int j = i; j < i+length; j++) {
                            temp.get(j).truthLabel = "PLIST";
                        }
                    }
                }
            }
            if(flag==1) {
                for(SimpleToken token: temp) {
                    if(token.truthLabel.equals("PLIST")) {
                        token.truthLabel = etype;
                        //System.out.println(token.lemma);
                    }
                    else
                        token.truthLabel="O";
                }
                if(!alreadyAdded.contains(index)) {
                    positives.add(temp);
                    alreadyAdded.add(index);
                    count++;
                }
            }
            index++;
        }
        //System.out.println("conll train size " + fullConllTrain.size());
        //System.out.println("PRF Done");
        //System.out.println(count);
    }

    public static void addRankedPRFWithSpan(List<List<SimpleToken>> fullConllTrain, Set<List<String>> availableSpans, List<List<SimpleToken>> positives, String etype, LinearTokenClassifier rf) {
        int count=0;
        PriorityQueue<SearchResult> pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        List<List<SimpleToken>> pseudoPositives = new ArrayList<>();
        for (List<SimpleToken> train : fullConllTrain) {
            double score = 0d;

            //make a clone of train.
            ArrayList<SimpleToken> temp = new ArrayList<>();
            for (SimpleToken s: train) {
                SimpleToken tempToken = new SimpleToken(s);
                temp.add(tempToken);
            }
            //flag=0 means we could  not find any matching span
            int flag = 0;
            //Now look for the span. Iterate throguh the available spans. If a suitable span is found annotate it as entity.
            //Otherwise annotate that span as other
            for (List<String> availableSpan : availableSpans) {
                int length = availableSpan.size();
                for (int i = 0; i < temp.size() - length + 1; i++) {
                    List<String> temp1 = new ArrayList<>();
                    for (int j = i; j < i + length; j++) {
                        temp1.add(temp.get(j).lemma.toLowerCase().trim());
                    }
                    if (compareStringList(temp1, availableSpan) == 1) {
                        //System.out.println(availableSpan.get(0));
                        flag = 1;
                        for (int j = i; j < i + length; j++) {
                            temp.get(j).truthLabel = "PLIST";
                        }
                    }
                }
            }
            if(flag==1) {
                for(SimpleToken token: temp) {
                    if(token.truthLabel.equals("PLIST")) {
                        token.truthLabel = etype;
                        //System.out.println(token.lemma);
                    }
                    else
                        token.truthLabel="O";
                    score += rf.score(token.getFeatures());
                }
                pseudoPositives.add(temp);
                count++;
                SearchResult searchResult = new SearchResult(count, score, temp);
                if (pq.size() < StaticVariables.numberOfPRFSentences) {
                    pq.add(searchResult);
                } else {
                    SearchResult s = pq.peek();
                    if (s.getScore() < searchResult.getScore()) {
                        pq.poll();
                        pq.add(searchResult);
                    }
                }
            }

            /*for(List<SimpleToken> tokenList: pseudoPositives){

            }*/
        }
        while(pq.peek()!=null){
            positives.add(pq.peek().getSimpleTokens());
            pq.poll();
        }
        //System.out.println("PRF Done");
        //System.out.println(count);
    }
}
