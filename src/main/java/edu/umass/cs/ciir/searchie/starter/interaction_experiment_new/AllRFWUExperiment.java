package edu.umass.cs.ciir.searchie.starter.interaction_experiment_new;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class AllRFWUExperiment {
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));

        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);



        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");

        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);

        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }

        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        printResults.addJSON("pos", String.valueOf(fullPositives.size()));
        printResults.addJSON("neg", String.valueOf(fullPositives.size()));
        printResults.printSpans(availableSpans, "available spans");
        System.out.println(availableSpans.size());

        for(int i=0; i<fullConllTrain.size(); i++){
            Set<List<String>> str = DataSelector.annotateUser(availableSpans, fullConllTrain.get(i), etype);
            if(str.size()>0) {
                printResults.printSimpleToken(fullConllTrain.get(i), "sentence");
                printResults.printSpans(str, "found spans");
            }
            for(int j=0; j<str.size(); j++)
                occurrence[j]++;
        }

        for(int i=0;i<20;i++)
            System.out.print(occurrence[i] + "\t");
        System.out.print(fullConllTrain.size() + "\t" + inputFileName);
        System.out.println();
    }
}
