package edu.umass.cs.ciir.searchie.starter.experiments_wsdm;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 8/8/17.
 */
public class LiuDataDump {
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args)
            throws IOException
    {
        Parameters argp = Parameters.parseArgs(args);

        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int[] occurrence = new int[100];
        NumberFormat formatter = new DecimalFormat("#0000");

        PrintResults pr = new PrintResults();
        List<List<SimpleToken>> fullPositives = new ArrayList();
        List<List<SimpleToken>> fullNegatives = new ArrayList();

        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain)
        {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype))
                {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        List<List<SimpleToken>> mergedList = new ArrayList();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);
        double[] prior = new double[((List)mergedList).size()];
        double[] vectorLength = new double[((List)mergedList).size()];
        double[][] points = new double[((List)mergedList).size()][embeddingSize];
        FeatureExtractor fe = new FeatureExtractor((List)((List)mergedList).get(0));
        String[] queryNerFeatures = fe.getNerFeatures();
        String directory = "/mnt/nfs/work1/smsarwar/searchie/text/";
        BufferedWriter br = new BufferedWriter(new FileWriter(new File(directory + inputFileName + ".dat")));

        List<List<SimpleToken>> positives = new ArrayList();
        positives.add(mergedList.get(0));
        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        int row = 0;
        int col;
        int countPRF = 0;
        for (List<SimpleToken> tokenList : mergedList)
        {
            if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, foundSpans) == 1)
            {
                prior[row] = 10.0D;
                countPRF++;
            }
            List<Double> doubleList = sm.getSentenceEmbedding(simpleTokenToList(tokenList));
            col = 0;
            for (Iterator localIterator4 = doubleList.iterator(); localIterator4.hasNext();)
            {
                double d = ((Double)localIterator4.next()).doubleValue();
                points[row][(col++)] = d;
                vectorLength[row] += d * d;
            }
            if (vectorLength[row] - 0.0D < 1.0E-8D) {
                vectorLength[row] = 1.0D;
            }
            row++;
        }

        for (int i = 0; i < vectorLength.length; i++)
        {
            vectorLength[i] = Math.sqrt(vectorLength[i]);
            if (vectorLength[i] - 0.0D < 1.0E-8D) {
                vectorLength[row] = 1.0D;
            }
        }
        int bootstrapSentenceId = 0;
        int[] expandedSentences = new int[countPRF];
        double[] expandedSentencesWeight = new double[countPRF];
        row = 0;
        for (SearchResult s : EmbeddingSearch.search(bootstrapSentenceId, points, ((List)mergedList).size(), embeddingSize, prior, vectorLength))
        {
            expandedSentences[row] = s.getSentenceId();
            expandedSentencesWeight[row] = (countPRF - row);
            row++;
        }
        EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, expandedSentencesWeight, embeddingSize);
        vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);

        double rankingPoint = 1.0D;double averagePrecision = 0.0D;
        setPrior(prior, 0.0D);
        List<SearchResult> results = EmbeddingSearch.search(bootstrapSentenceId, points, ((List)mergedList).size(), embeddingSize, prior, vectorLength);
        int relevantSentences = 0;
        List<Boolean> list = new ArrayList();

        String qid = inputFileName + "_0";

        int count = 0;
        for (SearchResult s : results)
        {
            List<SimpleToken> l = mergedList.get(s.getSentenceId());

            String docID = inputFileName + "_" + s.getSentenceId();
            String qrel;
            if (DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, l, etype) == 1) {
                qrel = "1";
            } else {
                qrel = "0";
            }
            fe = new FeatureExtractor(l);
            String[] nerFeatures = fe.getNerFeatures();
            String processedDocSentence = simpleTokenToStringProcessed(l, nerFeatures);
            String rawDocSentence = simpleTokenToStringRaw(l);
            String finalString = docID + "\t" + qid + "\t" + qrel + "\t" + processedDocSentence + "\t" + rawDocSentence;

            br.write(finalString);
            br.newLine();
        }
        br.close();
        String processedQuerySentence = simpleTokenToStringProcessed(mergedList.get(0), queryNerFeatures);
        String rawQuerySentence = simpleTokenToStringRaw(mergedList.get(0));
        String queryEmbedding = "";
        for (int i = 0; i < embeddingSize; i++) {
            queryEmbedding = queryEmbedding + points[bootstrapSentenceId][i] + " ";
        }
        queryEmbedding = queryEmbedding.trim();
        String queryString = qid + "\t" + processedQuerySentence + "\t" + rawQuerySentence + "\t" + queryEmbedding;
        System.out.println(queryString);
    }

    public static String simpleTokenToStringProcessed(List<SimpleToken> tokenList, String[] nerFeatures)
    {
        String str = "";
        for (int i = 0; i < tokenList.size(); i++)
        {
            SimpleToken token = tokenList.get(i);

            str = str + token.lemma + "==" + token.truthLabel + "==" + nerFeatures[i] + " ";
        }
        str = str.trim();
        return str;
    }

    public static String simpleTokenToStringRaw(List<SimpleToken> tokenList)
    {
        String str = "";
        for (int i = 0; i < tokenList.size(); i++)
        {
            SimpleToken token = tokenList.get(i);

            str = str + token.lemma + " ";
        }
        str = str.trim();
        return str;
    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList)
    {
        ArrayList<String> strList = new ArrayList();
        for (SimpleToken token : tokenList) {
            if (!Stopwords.isStemmedStopword(token.lemma)) {
                strList.add(token.lemma);
            }
        }
        return strList;
    }

    public static void setPrior(double[] prior, double val)
    {
        for (int i = 0; i < prior.length; i++) {
            prior[i] = val;
        }
    }

}
