package edu.umass.cs.ciir.searchie.starter.utils;

import ciir.jfoley.chai.classifier.AUC;
import ciir.jfoley.chai.classifier.BinaryClassifierInfo;
import ciir.jfoley.chai.classifier.RankingMeasures;
import ciir.jfoley.chai.collections.Pair;
import ciir.jfoley.chai.collections.util.ListFns;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.data_engineering.RelevanceFeedbackWithCRF;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.WeightUtils;

import java.util.*;

/**
 * Created by sarwar on 2/3/17.
 */
public class Metrics {
    List<Pair<Boolean, Double>> rankedPred;
    PriorityQueue<SearchResult> pq;
    public Set<String> foundTokens;
    public List<List<SimpleToken>> fullPositives;
    /*public Metrics(List<Pair<Boolean, Double>> rankedPred){
        this.rankedPred = rankedPred;
        pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
    }*/



    public static Map<String, Double> evaluateModel(LinearTokenClassifier model, List<List<SimpleToken>> testData, List<List<SimpleToken>> trainData, String etype) {
        // collect correct, prediction score pairs
        List<Pair<Boolean, Double>> rankedPred = new ArrayList<>();
        Map<String, ScoresForUniqueLemma> bestScoreByLemma = new HashMap<>();
        // collect unique tokens in "ScoresForUniqueLemma" as we score
        // collect non-unique in rankedPred (we don't actually need to sort it).
        //MODIFICATION SHEIKH START
        PriorityQueue<SearchResult> pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        //Collections.sort(results, (o1, o2) -> o2.getScore().compareTo(o1.getScore()));*/
        int top = StaticVariables.top;  // you can change this parameter. Currently it is set to 1.
        // we are only considering the top item for relevance feedback.
        //int toRemove[] = new int[top];
        for (int i = 0; i < testData.size(); i++){ //List<SimpleToken> tokens : testData) {
            //we iterate over all the tokens in testData. We use the index i to remove the top scored sentence
            // from the the testData
            List<SimpleToken> tokens = testData.get(i);
            double maxScore = 0; //maxScore will contain the highest scoring token in a sentence, eventually
            boolean truthSentence = false;
            //In this way we find highest scoring sentence and it is the one with the HIGHEST SCORING TOKEN
            for (SimpleToken token : tokens) {
                boolean truth = token.truthLabel.equals(etype);
                double score = model.score(token.getFeatures());
                rankedPred.add(Pair.of(truth, score));
                if(score > maxScore) {
                    maxScore = score;
                    truthSentence = truth;
                }
                bestScoreByLemma.computeIfAbsent(token.lemma, ScoresForUniqueLemma::new).score(score, truth);
            }
            // Now we have the score of a sentence in maxScore
            // We create a searchResult with the index of the sentence in test data, its score and all its tokens
            SearchResult searchResult = new SearchResult(i, maxScore, tokens);
            searchResult.setTrue(truthSentence);
            // If it is the highest scoring sentence, it will be stored in the Priority Queue after seeing all the test data
            if(pq.size() < top) {
                pq.add(searchResult);
            }else{
                SearchResult temp = pq.peek();
                if(temp.getScore() < searchResult.getScore()){
                    pq.poll();
                    pq.add(searchResult);
                }
            }
        }
        // MODIFICATION SHEIKH END (CONTINUE TO THE END PLEASE)
        // rank the TokenInfo classes by the highest scoring one (create our "unique" ranking).
        List<Pair<Boolean, Double>> uniqueRankedPred = new ArrayList<>();
        for (ScoresForUniqueLemma scoresForUniqueLemma : bestScoreByLemma.values()) {
            uniqueRankedPred.add(Pair.of(scoresForUniqueLemma.fractionTrue() > 0, scoresForUniqueLemma.bestScore()));
        }
        // calculate p,r,f1,acc, etc.
        BinaryClassifierInfo uinfo = new BinaryClassifierInfo();
        uinfo.update(uniqueRankedPred, model.getIntercept());
        BinaryClassifierInfo info = new BinaryClassifierInfo();
        info.update(rankedPred, model.getIntercept());
        //System.out.println(rankedPred.size());
        Map<String, Double> measures = new HashMap<>();
        // regular-measures
        measures.put("AUC", AUC.compute(rankedPred));
        measures.put("P10", AUC.computePrec(rankedPred, 10));
        measures.put("P100", AUC.computePrec(rankedPred, 100));
        measures.put("P1000", AUC.computePrec(rankedPred, 1000));
        measures.put("AP", AUC.computeAP(rankedPred));
        measures.put("P", (double) info.getPositivePrecision());
        measures.put("R", (double) info.getPositiveRecall());
        measures.put("F1", (double) info.getPositiveF1());
        measures.put("Accuracy", (double) info.getAccuracy());
        measures.put("TP", (double) info.numPredTruePositive);
        measures.put("FP", (double) info.getNumFalsePositives());
        measures.put("TN", (double) info.numPredTrueNegative);
        measures.put("FN", (double) info.getNumFalseNegatives());
        //measures.put("POS". (double) info.n
        // unique-measures:
        measures.put("uAUC", AUC.compute(uniqueRankedPred));
        measures.put("uP10", AUC.computePrec(uniqueRankedPred, 10));
        measures.put("uP100", AUC.computePrec(uniqueRankedPred, 100));
        measures.put("uP1000", AUC.computePrec(uniqueRankedPred, 1000));
        measures.put("uAP", AUC.computeAP(uniqueRankedPred));
        measures.put("uP", (double) uinfo.getPositivePrecision());
        measures.put("uR", (double) uinfo.getPositiveRecall());
        measures.put("uF1", (double) uinfo.getPositiveF1());
        measures.put("uAccuracy", (double) uinfo.getAccuracy());
        measures.put("uTP", (double) uinfo.numPredTruePositive);
        measures.put("uFP", (double) uinfo.getNumFalsePositives());
        measures.put("uTN", (double) uinfo.numPredTrueNegative);
        measures.put("uFN", (double) uinfo.getNumFalseNegatives());

        //MODIFICATION SHEIKH START
        // now we have the higest scoring sentence and we have to remove it from the Priority Queue and shift it
        // to the training data. We wanted to have it in the testData until the evaluation measures are not computed
        SearchResult temp = null;
        while(!pq.isEmpty()){
            temp = pq.peek();
            pq.poll();
            temp.getSimpleTokens();
            testData.remove(temp.getSentenceId());
            trainData.add(temp.getSimpleTokens());
            //temp.isTrue()
        }
        //MODIFICATION SHEIKH END
        measures.put("truth", temp.isTrue()? 1.0: 0.0);
        measures.put("score", temp.getScore());

        return measures;
    }

    public static Map<String, Double> evaluateModel(LinearTokenClassifier model, List<List<SimpleToken>> testData, String etype) {
        // collect correct, prediction score pairs
        List<Pair<Boolean, Double>> rankedPred = new ArrayList<>();
        Map<String, ScoresForUniqueLemma> bestScoreByLemma = new HashMap<>();
        for (List<SimpleToken> tokens : testData) {
            for (SimpleToken token : tokens) {
                boolean truth = token.truthLabel.equals(etype);
                //System.out.println(token.getFeatures().size());
                double score = model.score(token.getFeatures());
                rankedPred.add(Pair.of(truth, score));
                bestScoreByLemma.computeIfAbsent(token.lemma, ScoresForUniqueLemma::new).score(score, truth);
            }
        }

        // rank the TokenInfo classes by the highest scoring one (create our "unique" ranking).
        List<Pair<Boolean, Double>> uniqueRankedPred = new ArrayList<>();
        for (ScoresForUniqueLemma scoresForUniqueLemma : bestScoreByLemma.values()) {
            uniqueRankedPred.add(Pair.of(scoresForUniqueLemma.fractionTrue() > 0, scoresForUniqueLemma.bestScore()));
        }

        // calculate p,r,f1,acc, etc.
        BinaryClassifierInfo uinfo = new BinaryClassifierInfo();
        uinfo.update(uniqueRankedPred, model.getIntercept());

        BinaryClassifierInfo info = new BinaryClassifierInfo();
        info.update(rankedPred, model.getIntercept());
        //System.out.println(rankedPred.size());
        Map<String, Double> measures = new HashMap<>();

        // regular-measures
        measures.put("AUC", AUC.compute(rankedPred));
        measures.put("P10", AUC.computePrec(rankedPred, 10));
        measures.put("P100", AUC.computePrec(rankedPred, 100));
        measures.put("P1000", AUC.computePrec(rankedPred, 1000));
        measures.put("AP", AUC.computeAP(rankedPred));
        measures.put("P", (double) info.getPositivePrecision());
        measures.put("R", (double) info.getPositiveRecall());
        measures.put("F1", (double) info.getPositiveF1());
        measures.put("Accuracy", (double) info.getAccuracy());
        measures.put("TP", (double) info.numPredTruePositive);
        measures.put("FP", (double) info.getNumFalsePositives());
        measures.put("TN", (double) info.numPredTrueNegative);
        measures.put("FN", (double) info.getNumFalseNegatives());

        // unique-measures:
        measures.put("uAUC", AUC.compute(uniqueRankedPred));
        measures.put("uP10", AUC.computePrec(uniqueRankedPred, 10));
        measures.put("uP100", AUC.computePrec(uniqueRankedPred, 100));
        measures.put("uP1000", AUC.computePrec(uniqueRankedPred, 1000));
        measures.put("uAP", AUC.computeAP(uniqueRankedPred));
        measures.put("uP", (double) uinfo.getPositivePrecision());
        measures.put("uR", (double) uinfo.getPositiveRecall());
        measures.put("uF1", (double) uinfo.getPositiveF1());
        measures.put("uAccuracy", (double) uinfo.getAccuracy());
        measures.put("uTP", (double) uinfo.numPredTruePositive);
        measures.put("uFP", (double) uinfo.getNumFalsePositives());
        measures.put("uTN", (double) uinfo.numPredTrueNegative);
        measures.put("uFN", (double) uinfo.getNumFalseNegatives());


        return measures;
    }

    public static Map<String, Double> evaluateRankedList(ArrayList<SearchResult> resultSeen, double intercept) {
        double countTrue=0d;
        for(SearchResult searchResult: resultSeen){
            if(searchResult.isTrue()){
                countTrue++;
            }
        }
        Map<String, Double> measures = new HashMap<>();
        measures.put("P", countTrue/resultSeen.size());
        return measures;
    }

    public static double evaluateAveragePrecision(ArrayList<SearchResult> resultSeen, int numRelevant) {
        ArrayList<Boolean> a = new ArrayList<>();
        for(SearchResult searchResult: resultSeen){
            a.add(searchResult.isTrue());
        }
        return computeAP(a, numRelevant);
    }



    public static SearchResult addRelevanceFeedback(LinearTokenClassifier model, List<List<SimpleToken>> testData, List<List<SimpleToken>> trainData, String etype, ArrayList<SearchResult> resultsSeen, boolean isUnique, Map<String, TokenCount> retrievedTokenMap) {
        // collect correct, prediction score pairs
        //System.out.println("eneterd relevance feedback");
        List<Pair<Boolean, Double>> rankedPred = new ArrayList<>();
        Map<String, ScoresForUniqueLemma> bestScoreByLemma = new HashMap<>();
        // collect unique tokens in "ScoresForUniqueLemma" as we score
        // collect non-unique in rankedPred (we don't actually need to sort it).
        //MODIFICATION SHEIKH START
        PriorityQueue<SearchResult> pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        //Collections.sort(results, (o1, o2) -> o2.getScore().compareTo(o1.getScore()));*/
        int top = StaticVariables.top;  // you can change this parameter. Currently it is set to 1.
        // we are only considering the top item for relevance feedback.
        //int toRemove[] = new int[top];
        int numberOfNonZeroTokens = 0;
        for (int i = 0; i < testData.size(); i++) { //List<SimpleToken> tokens : testData) {
            //we iterate over all the tokens in testData. We use the index i to remove the top scored sentence
            //from the the testData
            List<SimpleToken> tokens = testData.get(i);
            double maxScore = Double.NEGATIVE_INFINITY; //maxScore will contain the highest scoring token in a sentence, eventually
            //double sumScore = 0;

            String maxScoredToken = "";
            boolean truthSentence = false;
            //In this way we find highest scoring sentence and it is the one with the HIGHEST SCORING TOKEN
            double scoreArray[] = new double[tokens.size()];
            int j=0;
            for (SimpleToken token : tokens) {
                boolean truth = token.truthLabel.equals(etype);
                double score = model.score(token.getFeatures());
                //sumScore+=score;
                scoreArray[j++] = score;
                rankedPred.add(Pair.of(truth, score));
                if (score > maxScore) {
                    maxScore = score;
                    truthSentence = truth;
                    maxScoredToken = token.lemma;
                }
                bestScoreByLemma.computeIfAbsent(token.lemma, ScoresForUniqueLemma::new).score(score, truth);
            }

            /*if(!retrievedTokenMap.containsKey(maxScoredToken)){
                retrievedTokenMap.put(maxScoredToken, new TokenCount(1d,1d));
            }*/

            //TokenCount t = retrievedTokenMap.get(maxScoredToken);

            /*if(t.getSuccesful()/t.getRetrieved()< 0.50)
                continue;
            */

            if(isUnique == true) {
                if (isSelectedToken(resultsSeen, maxScoredToken)) {
                    continue;
                }
            }
            // Now we have the score of a sentence in maxScore
            // We create a searchResult with the index of the sentence in test data, its score and all its tokens
            // searchResult will contain the id of maximum scored sentence, its score, all the tokens of the sentence, and also the maximum scored token
            //SARWAR EDIT Number of non zero tokens
            //int maxScoreOnNonZeroRun = maxRunNonZero(runOfNonZero);
            //int maxScoreOnNonZeroRun = numNonZeroSpans(runOfNonZero);
            //SearchResult searchResult = new SearchResult(i, maxScoreOnNonZeroRun, tokens);
            //SearchResult searchResult = new SearchResult(i, nonZero, tokens);
            //SearchResult searchResult = new SearchResult(i, sumScore, tokens);
            //RelevanceFeedbackWithCRF.ScoreMax scoreSentence = new RelevanceFeedbackWithCRF.ScoreMax();
            RelevanceFeedbackWithCRF.ScoreMax scoreSentence = new RelevanceFeedbackWithCRF.ScoreMax();
            double score = scoreSentence.scoreSentence(scoreArray);
            SearchResult searchResult = new SearchResult(i, score, tokens);
            //SearchResult searchResult = new SearchResult(i, maxScore, tokens);
            searchResult.setTrue(truthSentence);
            searchResult.setMaxScoredToken(maxScoredToken);
            // If it is the highest scoring sentence, it will be stored in the Priority Queue after seeing all the test data
            if (pq.size() < top) {
                pq.add(searchResult);
            } else {
                SearchResult temp = pq.peek();
                if (temp.getScore() < searchResult.getScore()) {
                    pq.poll();
                    pq.add(searchResult);
                }
            }
        }
        SearchResult temp = null;
        while (!pq.isEmpty()) {
            temp = pq.peek();
            pq.poll();
            temp.getSimpleTokens();
            testData.remove(temp.getSentenceId());
            /*for(SimpleToken simpleToken: temp.getSimpleTokens()){

            }*/
            trainData.add(temp.getSimpleTokens());
            //temp.isTrue()
        }
        //adding the ground truth of value of the maximum scored token, its score computed by our algorithm, and the token itself
        //measures.put("truth", temp.isTrue()? 1.0: 0.0);
        //measures.put("score", temp.getScore());
        //model.addToken(temp.getMaxScoredToken());

        if(temp!=null) {
            resultsSeen.add(temp);
            //TokenCount t = retrievedTokenMap.get(temp.getMaxScoredToken());
            //System.out.println("probability " + t.getSuccesful()/t.getRetrieved() + " " + temp.getMaxScoredToken());
            //t.incrementRetrievedByOne();
        }
        //System.out.println("score " + temp.getScore());
        //System.out.println("exited relevance feedback");

        return temp;
    }


    public static boolean containsFullPositives(List<SimpleToken> testItem, List<List<SimpleToken>> fullPositives){
        boolean flag = false;
        for(List<SimpleToken> t: fullPositives){
            if(t == testItem){
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static int maxRunNonZero(int values[]){
        int maxLength = 0;
        int tempLength = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] == 1) {
                maxLength = Math.max(maxLength, ++tempLength);
            } else {
                tempLength = 0;
            }
        }
        return maxLength;
    }

    public static int numNonZeroSpans(double values[]){
        int maxLength = 0;
        int tempLength = 0;
        int count = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > 0) {
                tempLength+=values[i];
                maxLength = Math.max(maxLength, tempLength);
            } else {
                //if(tempLength>=2)
                //    count++;
                tempLength = 0;
            }
        }
        return maxLength;
    }

    public static SearchResult addRelevanceFeedbackSentence(LinearTokenClassifier model, List<List<SimpleToken>> testData, List<List<SimpleToken>> trainData, String etype, ArrayList<SearchResult> resultsSeen, boolean isUnique) {
        // collect correct, prediction score pairs
        //System.out.println("eneterd relevance feedback");
        List<Pair<Boolean, Double>> rankedPred = new ArrayList<>();
        Map<String, ScoresForUniqueLemma> bestScoreByLemma = new HashMap<>();
        // collect unique tokens in "ScoresForUniqueLemma" as we score
        // collect non-unique in rankedPred (we don't actually need to sort it).
        //MODIFICATION SHEIKH START
        PriorityQueue<SearchResult> pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        //Collections.sort(results, (o1, o2) -> o2.getScore().compareTo(o1.getScore()));*/
        int top = StaticVariables.top;  // you can change this parameter. Currently it is set to 1.
        // we are only considering the top item for relevance feedback.
        //int toRemove[] = new int[top];
        int numberOfNonZeroTokens = 0;
        for (int i = 0; i < testData.size(); i++) { //List<SimpleToken> tokens : testData) {
            //we iterate over all the tokens in testData. We use the index i to remove the top scored sentence
            //from the the testData
            List<SimpleToken> tokens = testData.get(i);
            double maxScore = 0; //maxScore will contain the highest scoring token in a sentence, eventually
            double sumScore = 0;
            String maxScoredToken = "";
            boolean truthSentence = false;
            //In this way we find highest scoring sentence and it is the one with the HIGHEST SCORING TOKEN
            double nonZero=0;
            for (SimpleToken token : tokens) {
                boolean truth = token.truthLabel.equals(etype);
                double score = model.score(token.getFeatures());
                rankedPred.add(Pair.of(truth, score));
                if (score > maxScore) {
                    maxScore = score;
                    sumScore+=score;
                    truthSentence = truth;
                    maxScoredToken = token.lemma;
                }
                if(score>0)
                    nonZero++;
                bestScoreByLemma.computeIfAbsent(token.lemma, ScoresForUniqueLemma::new).score(score, truth);
            }

            if(isUnique == true) {
                if (isSelectedToken(resultsSeen, maxScoredToken)) {
                    continue;
                }
            }
            // Now we have the score of a sentence in maxScore
            // We create a searchResult with the index of the sentence in test data, its score and all its tokens
            // searchResult will contain the id of maximum scored sentence, its score, all the tokens of the sentence, and also the maximum scored token
            //SARWAR EDIT Number of non zero tokens
            //SearchResult searchResult = new SearchResult(i, nonZero, tokens);
            SearchResult searchResult = new SearchResult(i, sumScore, tokens);
            searchResult.setTrue(truthSentence);
            searchResult.setMaxScoredToken(maxScoredToken);
            // If it is the highest scoring sentence, it will be stored in the Priority Queue after seeing all the test data
            if (pq.size() < top) {
                pq.add(searchResult);
            } else {
                SearchResult temp = pq.peek();
                if (temp.getScore() < searchResult.getScore()) {
                    pq.poll();
                    pq.add(searchResult);
                }
            }
        }
        SearchResult temp = null;
        while (!pq.isEmpty()) {
            temp = pq.peek();
            pq.poll();
            temp.getSimpleTokens();
            testData.remove(temp.getSentenceId());
            /*for(SimpleToken simpleToken: temp.getSimpleTokens()){

            }*/
            trainData.add(temp.getSimpleTokens());
            //temp.isTrue()
        }
        //adding the ground truth of value of the maximum scored token, its score computed by our algorithm, and the token itself
        //measures.put("truth", temp.isTrue()? 1.0: 0.0);
        //measures.put("score", temp.getScore());
        //model.addToken(temp.getMaxScoredToken());
        resultsSeen.add(temp);
        //System.out.println("exited relevance feedback");

        return temp;
    }

    public static SearchResult seeTopResult(LinearTokenClassifier model, List<List<SimpleToken>> testData, List<List<SimpleToken>> trainData, String etype, ArrayList<SearchResult> resultsSeen, boolean isUnique) {
        PriorityQueue<SearchResult> pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        int top = StaticVariables.top;  // you can change this parameter. Currently it is set to 1.
        for (int i = 0; i < testData.size(); i++) { //List<SimpleToken> tokens : testData) {
            List<SimpleToken> tokens = testData.get(i);
            double maxScore = Double.NEGATIVE_INFINITY; //maxScore will contain the highest scoring token in a sentence, eventually
            String maxScoredToken = "";
            boolean truthSentence = false;
            double nonZero=0;
            //In this way we find highest scoring sentence and it is the one with the HIGHEST SCORING TOKEN
            for (SimpleToken token : tokens) {
                boolean truth = token.truthLabel.equals(etype);
                double score = model.score(token.getFeatures());
                if (score > maxScore) {
                    maxScore = score;
                    truthSentence = truth;
                    maxScoredToken = token.lemma;
                }
                if(score>0)
                    nonZero++;
            }
            if(isUnique == true) {
                if (isSelectedToken(resultsSeen, maxScoredToken)) {
                    continue;
                }
            }
            //SARWAR EDIT Number of non zero tokens
            //SearchResult searchResult = new SearchResult(i, nonZero, tokens);
            SearchResult searchResult = new SearchResult(i, maxScore, tokens);
            searchResult.setTrue(truthSentence);
            searchResult.setMaxScoredToken(maxScoredToken);
            if (pq.size() < top) {
                pq.add(searchResult);
            } else {
                SearchResult temp = pq.peek();
                if (temp.getScore() < searchResult.getScore()) {
                    pq.poll();
                    pq.add(searchResult);
                }
            }
        }
        SearchResult temp = null;
        while (!pq.isEmpty()) {
            temp = pq.peek();
            pq.poll();
        }
        resultsSeen.add(temp);
        return temp;
    }

    static boolean isSelectedToken(ArrayList<SearchResult> resultsSeen, String lemma){
        boolean flag = false;
        for (SearchResult searchResult: resultsSeen){
            if(lemma.equals(searchResult.getMaxScoredToken())){
                flag = true;
                return flag;
            }
        }
        return flag;
    }

    public static double computeFullAP(LinearTokenClassifier model, List<List<SimpleToken>> testData, String etype, ArrayList<SearchResult> resultsSeen) {
        List<Pair<Boolean, Double>> rankedPred = new ArrayList<>();
        double threshold=10000000;
        for(SearchResult searchResult: resultsSeen){
            rankedPred.add(Pair.of(searchResult.isTrue(), threshold));
            threshold--;
        }
        for (int i = 0; i < testData.size(); i++) { //List<SimpleToken> tokens : testData) {
            List<SimpleToken> tokens = testData.get(i);
            //In this way we find highest scoring sentence and it is the one with the HIGHEST SCORING TOKEN
            for (SimpleToken token : tokens) {
                boolean truth = token.truthLabel.equals(etype);
                double score = model.score(token.getFeatures());
                rankedPred.add(Pair.of(truth, score));
            }
        }
        return AUC.computeAP(rankedPred);
    }


    public static double computeAP(List<Boolean> data, int numRelevant) {
        // if there are no relevant documents,
        // the average is artificially defined as zero, to mimic trec_eval
        // Really, the output is NaN, or the query should be ignored [point of debate]
        if(numRelevant == 0) return 0;

        double sumPrecision = 0;
        int recallPointCount = 0;

        for (int i = 0; i < data.size(); i++) {
            boolean truePositive = data.get(i);
            if(truePositive) { // at recall point, calculate the precision and add it to our mean
                double rank = i + 1;
                recallPointCount++;
                sumPrecision += recallPointCount / rank;
            }
        }

        // note we use the passed-in numRelevant instead of recallPointCount so that we allow for not-retrieved true-positives in our denominator
        return sumPrecision / (double) numRelevant;
    }

    public static double computeMap(int[] a){
        double countRelevant=0;
        double AP = 0;
        for(int i=0; i<a.length; i++){
            if(a[i]==1) {
                countRelevant++;
                AP+= countRelevant / (double)(i+1);
            }
        }
        //        if(AP>10)
        //            return 1;
        //        else
        if(countRelevant==0)
            return 0;
        return AP/countRelevant;
    }


    public static double precision(int[] a, int at){
        double precision = 0;
        double countRelevant = 0;
        double countTotalRelevant = 0;
        for(int i=0;i<a.length;i++){
            if(a[i]==1) {
                if (i < at) {
                    countRelevant++;
                } else {
                    countTotalRelevant++;
                }
            }
        }

        if(countRelevant==countTotalRelevant)
            return 1;
        else{
            return countRelevant/at;
        }
    }

    public static double precision(List<Boolean> a, int at){
        double countRelevant = 0;
        double countTotalRelevant = 0;
        for(int i=0;i<a.size();i++){
            if(a.get(i)==true) {
                if (i < at) {
                    countRelevant++;
                } else {
                    countTotalRelevant++;
                }
            }
        }

        //if(countRelevant==countTotalRelevant)
        //    return 1;
        //else{
        return countRelevant/at;
        //}
    }

    /*public static double recall(List<Boolean> a, int at, ){
        double precision = 0;
        double countRelevant = 0;
        double countTotalRelevant = 0;
        for(int i=0;i<a.size();i++){
            if(a.get(i)==true) {
                if (i < at) {
                    countRelevant++;
                } else {
                    countTotalRelevant++;
                }
            }
        }

        if(countRelevant==countTotalRelevant)
            return 1;
        else{
            return countRelevant/at;
        }
    }*/


    public static double getEntityPrecision(Set<List<String>> typeSpans, Set<List<String>> availableSpans) {
        double count = 0;
        for(List<String> stringList: typeSpans){
            List<String> a = new ArrayList<>();
            for(String s: stringList){
                a.add(s.toLowerCase());
            }
            if(availableSpans.contains(a)){
                count++;
            }
        }
        return count/typeSpans.size();
    }

    public static double getEntityRecall(Set<List<String>> typeSpans, Set<List<String>> availableSpans){
        double count = 0;
        for(List<String> stringList: typeSpans){
            List<String> a = new ArrayList<>();
            for(String s: stringList){
                a.add(s.toLowerCase());
            }
            if(availableSpans.contains(a)){
                count++;
            }
        }
        return count/availableSpans.size();
    }

    /*public double computeuAP(int at){
        SearchResult searchResult = new SearchResult();
        // If it is the highest scoring sentence, it will be stored in the Priority Queue after seeing all the test data
        if(pq.size() < at) {
            pq.add(searchResult);
        }else{
            SearchResult temp = pq.peek();
            if(temp.getScore() < searchResult.getScore()){
                pq.poll();
                pq.add(searchResult);
            }
        }
    }*/

}
