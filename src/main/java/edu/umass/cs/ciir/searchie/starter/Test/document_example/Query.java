package edu.umass.cs.ciir.searchie.starter.Test.document_example;

import java.util.List;

public class Query {
    public String getQid() {
        return qid;
    }
    public void setQid(String qid) {
        this.qid = qid;
    }

    //"queries" is a top-level list
    //"qid" has the query id we've been using
    // "text" has the question
    public String qid;
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    String text;
    //public JudgmentContainer judgementContainer;
    List<Judgment> judgments;
    List<String> answers;
    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

}