package edu.umass.cs.ciir.searchie.starter.feature_engineering;


import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import gnu.trove.map.hash.TObjectFloatHashMap;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**

/**
 * Created by sarwar on 12/10/16.
 */
public class FeatureSelector {
    TObjectFloatHashMap<String> originalMap, optimalMap;
    State state;
    public FeatureSelector(){

    }

    public FeatureSelector(TObjectFloatHashMap<String> originalMap, TObjectFloatHashMap<String> optimalMap){
        this.originalMap = originalMap; //originalMap contains all the weights after training
        this.optimalMap = optimalMap; //optimalMap only contains a specific pattern of weights
    }

    void setState(State state){
        this.state = state;
    }

    public ArrayList<FeatureWeight> filterByPattern(){

        ArrayList<FeatureWeight> originalPatternFiltered = new ArrayList<>();
        for (String key : optimalMap.keySet()) {
            if (originalMap.containsKey(key)) {
                //System.out.println("feature added from optimal " + key);
                originalPatternFiltered.add(new FeatureWeight(key, originalMap.get(key)));
            } //REMOVE THE ELSE
            /*else {
                //System.out.println("feature added from optimal " + key);
                originalPatternFiltered.add(new FeatureWeight(key, optimalMap.get(key)));
            }*/
        }
        Collections.sort(originalPatternFiltered, (o1, o2) -> Double.compare(o1.getWeight(), o2.getWeight()));
        return originalPatternFiltered;
    }

    public static ArrayList<FeatureWeight> mapToList(TObjectFloatHashMap<String> map) {
        ArrayList<FeatureWeight> list = new ArrayList<>();
        for (String key : map.keySet()) {
            list.add(new FeatureWeight(key,map.get(key)));
        }
        Collections.sort(list, (o1, o2) -> Double.compare(o1.getWeight(), o2.getWeight()));
        return list;
    }

    public ArrayList<FeatureWeight> filterBySign(int sign, ArrayList<FeatureWeight> a){
        ArrayList<FeatureWeight> filtered = new ArrayList<>();
        if(sign>0){
            for(FeatureWeight featureWeight: filtered)
                if(featureWeight.getWeight()>0)
                    filtered.add(featureWeight);
        }
        if(sign<0){
            for(FeatureWeight featureWeight: filtered)
                if(featureWeight.getWeight()<=0)
                    filtered.add(featureWeight);
        }
        return filtered;
    }

    public HashMap<String, Double> selectFeautresPercentage(double percent, ArrayList<FeatureWeight> a){
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        // THE LINE BELOW IS VERY IMPORTANT. IT SELECT THE PERCENTAGE OF WEIGHTS WE CONSIDER FOR CORRECTION
        double partition = a.size() * ((double)percent/100);
        int indexStart = (int)partition;
        int indexEnd = (int) (a.size() - partition);
        int count = 0;
        for (int i = 0; i < a.size(); i++) {
            if (i < indexStart ||  i > indexEnd) {
            //if(i>indexEnd){
                //if (i >= indexStart &&  i <= indexEnd) {
                count++;
                hashMap.put(a.get(i).getFeature(), a.get(i).getWeight());
            }
        }
        //System.out.println("Percentage Filtering\t" + count);
        return hashMap;
    }

    public HashMap<String, Double> selectFeautresPercentage(int numberOfFeatures, ArrayList<FeatureWeight> a){
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        /*int indexStart = numberOfFeatures/2;
        int indexEnd = a.size() - (numberOfFeatures/2);
        for (int i = 0; i < a.size(); i++) {
            if (i < indexStart ||  i >= indexEnd) {
                hashMap.put(a.get(i).getFeature(), a.get(i).getWeight());
            }
        }*/
        //System.out.println("Percentage Filtering\t" + count);
        int indexStart = numberOfFeatures;
        int indexEnd = a.size() - (numberOfFeatures);
        for (int i = 0; i < a.size(); i++) {
            if (i < indexStart) {
            //if (i >= indexEnd) {
                hashMap.put(a.get(i).getFeature(), a.get(i).getWeight());
            }
        }
        return hashMap;
    }

    /*public HashMap<String,Double> selectFeautres(int numberOfFeatures, ArrayList<FeatureWeight> a, State state) {
        HashMap<String, Double> hashMap = new HashMap<>();
        TObjectFloatHashMap<String> usedFeatures = state.getWeightFeedbacks();
        for(String key: usedFeatures.keySet()){
            for(int i=0; i< a.size(); i++){
                FeatureWeight f = a.get(i);
                if(f.getFeature().equals(key)) {
                    hashMap.put(f.getFeature(), f.getWeight());
                    a.remove(i);   //OPTIMIZE with break
                }
            }
        }
        int indexStart = numberOfFeatures/2;
        int indexEnd = a.size() - (numberOfFeatures/2);
        for (int i = 0; i < a.size(); i++) {
            if (i < indexStart ||  i >= indexEnd) {
                hashMap.put(a.get(i).getFeature(), a.get(i).getWeight());
                usedFeatures.put(a.get(i).getFeature(), (float)a.get(i).getWeight());
            }
        }
        return hashMap;
    }*/


    public HashMap<String, Double> selectFeautresTopk(int sign, int k, ArrayList<FeatureWeight> a) {
        //remember a is sorted in ascending order
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        if(sign < 0){
            //sign<0 means the features with least weights
            for(int i = 0; i<k; i++){
                hashMap.put(a.get(i).getFeature(), a.get(i).getWeight());
            }
        }
        else if(sign>0){
            //sign>0 means top-k features with most weights
            for(int i = a.size()-k; i<a.size(); i++){
                hashMap.put(a.get(i).getFeature(), a.get(i).getWeight());
            }
        }
        else if(sign==0) {
            //System.out.println("size" + a.size());
            int limit = k/2;
            if((k/2) >= a.size())
                limit = a.size();
            //sign=0 means top-k from both sides
            for(int i = 0; i< limit; i++){
                hashMap.put(a.get(i).getFeature(), a.get(i).getWeight());
            }
            if(limit!= a.size() && (k%2!=0))
                limit++;
            for(int i = a.size()-limit; i<a.size(); i++){
                hashMap.put(a.get(i).getFeature(), a.get(i).getWeight());
            }
        }

        return hashMap;
    }

    public HashMap<String, Double> selectTopFeature(ArrayList<FeatureWeight> a){
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        /*int index;
        if(numberOfWeights%2==0)
            index = 0;
        else*/
        int index = a.size()-1;
        //int index = returnUncertainWeightIndex(a.size());
        FeatureWeight featureWeight = a.get(index);
        hashMap.put(featureWeight.getFeature(), featureWeight.getWeight());
        return hashMap;
    }

    public HashMap<String, Double> selectUncertainFeature(ArrayList<FeatureWeight> a){
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        /*int index;
        if(numberOfWeights%2==0)
            index = 0;
        else*/
        int index = returnUncertainWeightIndex(a.size());
        FeatureWeight featureWeight = a.get(index);
        hashMap.put(featureWeight.getFeature(), featureWeight.getWeight());
        return hashMap;
    }

    public HashMap<String, Double> selectCommonFeature(ArrayList<FeatureWeight> a){
        String str="";
        Pattern p = Pattern.compile("(.*)(p\\[\\d*\\]=)(\\w+)(.*)");

        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        HashMap<String, Integer> featureCount = new HashMap<>();
        for(FeatureWeight f: a){
            //System.out.println(f.feature);
            Matcher m = p.matcher(f.feature);
            if(m.find()==true && m.group(3).length()>=1){
                System.out.println("Found " + m.group(2) + m.group(3));
                String val = m.group(2) + m.group(3);
                if(val==null) continue;
                if(featureCount.containsKey(val)){
                    featureCount.put(val, featureCount.get(val)+1);
                }else{
                    featureCount.put(val, 1);
                }
            }
        }
        int max=0;
        for(String key: featureCount.keySet()){
            if(featureCount.get(key)>max){
                str = key;
            }
        }

        System.out.println("Selected New Feature " + str);
        System.out.println("Count " + featureCount.get(str));
        //hashMap.put(featureWeight.getFeature(), featureWeight.getWeight());
        hashMap.put(str, 0d);
        return hashMap;
    }

    public HashMap<String, Double> selectAllFeatures(ArrayList<FeatureWeight> a) {
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        for(FeatureWeight f: a){
            hashMap.put(f.getFeature(), f.getWeight());
            //hashMap.put(f.getFeature(), (double) optimalMap.get(f.getFeature()));
        }
        /*for(String key: optimalMap.keySet()){
            if(!hashMap.containsKey(key)){
                hashMap.put(key, 0d);
            }
        }*/
        return hashMap;
    }

    public HashMap<String, Double> selectClusterFeatures(ArrayList<FeatureWeight> a) {
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        double averageWeight = 0;
        for(FeatureWeight f: a){
            hashMap.put(f.getFeature(), f.getWeight());
            averageWeight+=f.getWeight();

        }
        for(String key: optimalMap.keySet()){
            if(!hashMap.containsKey(key)){
                hashMap.put(key, 0d);
            }
        }
        //return optimalMap;
        return hashMap;
    }

    public HashMap<String, Double> selectContextWordFeatures(ArrayList<FeatureWeight> a) {
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        double averageWeight = 0;
        for(FeatureWeight f: a){
            hashMap.put(f.getFeature(), f.getWeight());
            averageWeight+=f.getWeight();
        }
        for(String key: optimalMap.keySet()){
            if(!hashMap.containsKey(key)){
                hashMap.put(key, 0d);
            }
        }
        //return optimalMap;
        return hashMap;
    }

    public HashMap<String, Double> selectTopTwoFeature(ArrayList<FeatureWeight> a){
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        /*int index;
        if(numberOfWeights%2==0)
            index = 0;
        else*/
        int index = a.size()-1;
        FeatureWeight featureWeight = a.get(index);
        hashMap.put(featureWeight.getFeature(), featureWeight.getWeight());
        featureWeight = a.get(index-1);
        hashMap.put(featureWeight.getFeature(), featureWeight.getWeight());
        return hashMap;
    }
    /*public selectTopFeature(int k, ArrayList<FeatureWeight> a){

        return
    }*/
    public HashMap<String, Double> constructContextFeatures(State state){
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        Set<String> contextTokens = state.getExploredTokens();
        for(String str: contextTokens){
            hashMap.put("w[L]=" + str, 0.1);
            hashMap.put("w[R]=" + str, 0.1);
            //hashMap.put("w[L]=" + str.toLowerCase(), 0.1);
            //hashMap.put("w[R]=" + str.toLowerCase(), 0.1);
        }
        return hashMap;
    }

    public void addnerfeatures(HashMap<String, Double> selectedFeaturesMap){
        List<String> features = new ArrayList<>();
        features.add("O");
        features.add("ORGANIZATION");
        features.add("NUMBER");
        features.add("-$-");
        features.add("LOCATION");
        features.add("TIME");
        features.add("MISC");
        features.add("PERSON");
        features.add("ORDINAL");
        features.add("DURATION");
        features.add("PERCENT");
        features.add("MONEY");

        for(String s:features){
            for(int i = -3; i<4; i++){
                String feature = "ner[" + String.valueOf(i) + "]=" + s;
                selectedFeaturesMap.put(feature, 0.5);
            }
        }
        //return selectedFeaturesMap;
    }
    public HashMap<String, Double> constructContextFeaturesWithoutTokens(State state){
        HashMap<String, Double> hashMap = new HashMap<String, Double>();
        Set<String> contextTokens = state.getExploredTokens();
        for(String str: contextTokens){
            hashMap.put(str, 0.1);
        }
        return hashMap;
    }

    public static Set<String> getNewContextWordFeatures(Set<String> currentFeatures, Set<String> usedFeatures){
        Set<String> newFeatures = new HashSet<>();
        for(String s: currentFeatures){
            if(!usedFeatures.contains(s)){
                newFeatures.add(s);
            }
        }

        return newFeatures;
    }

    static int returnUncertainWeightIndex(int arrayListSize){
        if(arrayListSize==0)
            return 0;
        else return arrayListSize/2;

    }

    public static Set<String> getFeatureSet(TObjectFloatHashMap<String> listFeaturesMap) {
        Set<String> features = new HashSet<>();

        for (String s : listFeaturesMap.keySet()) {
            Pattern p = Pattern.compile("(.*w)(\\[L\\]=|\\[R\\]=)(\\w+)(.*)");
            Matcher m = p.matcher(s);
            if (m.find() == true && m.group(3).length() >= 1) {
                //System.out.println("Found " + m.group(1) + m.group(2) + m.group(3));
                String val = m.group(3);// + m.group(3);
                if (val == null) continue;
                features.add(val);
            }
        }
        return features;
    }

    public static Set<String> getFeatureSet(Set<String> listFeaturesMap) {
        Set<String> features = new HashSet<>();

        for (String s : listFeaturesMap) {
            Pattern p = Pattern.compile("(.*w)(\\[L\\]=|\\[R\\]=)(\\w+)(.*)");
            Matcher m = p.matcher(s);
            if (m.find() == true && m.group(3).length() >= 1) {
                //System.out.println("Found " + m.group(1) + m.group(2) + m.group(3));
                String val = m.group(3);// + m.group(3);
                if (val == null) continue;
                features.add(val);
            }
        }
        return features;
    }


    public static Set<String> getRawFeatureSet(TObjectFloatHashMap<String> listFeaturesMap) {
        Set<String> features = new HashSet<>();
        for (String s : listFeaturesMap.keySet()) {
            features.add(s);
        }
        return features;
    }
    /*public static Set<String> getRawFeatureSet(TObjectFloatHashMap<String> listFeaturesMap) {
        Set<String> features = new HashSet<>();
        for (String s : listFeaturesMap.keySet()) {

            Pattern p = Pattern.compile("(.*w)(\\[L\\]=|\\[R\\]=)(\\w+)(.*)");
            Matcher m = p.matcher(s);
            if (m.find() == true && m.group(3).length() >= 1) {
                features.add(s);
            }
        }
        return features;
    }*/
}
