package edu.umass.cs.ciir.searchie.starter.Test;

import java.io.*;
import java.util.Scanner;

/**
 * Created by sarwar on 4/7/17.
 */
public class TestProcessBuilder {

    public static void main(String[] args) throws Exception {
        try {
            //ProcessBuilder builder = new ProcessBuilder("java", "-cp", "/home/sarwar/IdeaProjects/searchie/target/starter-0.1-SNAPSHOT.jar", "edu.umass.cs.ciir.searchie.starter.Test.ProcessBuilderTest");
            ProcessBuilder builder = new ProcessBuilder("./distance_mod", "/mnt/nfs/work1/jfoley/code/word2vec/make-aquaint/vector_data", "swingley");
            //System.out.println(builder.command());
            builder.directory(new File("/home/smsarwar/scripts/bin"));
            Process process = builder.start();
            //process.waitFor();
            OutputStream stdin = process.getOutputStream(); // <- Eh?
            InputStream stdout = process.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));
            //Thread.sleep(3000);
            //writer.write("swingley");
            //writer.flush();
            //writer.close();

            Scanner scanner = new Scanner(stdout);
            //while (scanner.hasNextLine()) {
            //for(int i=0;i<10;i++)
            //    System.out.println(scanner.nextLine());
            //}
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
