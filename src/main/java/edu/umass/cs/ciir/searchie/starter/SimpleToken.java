package edu.umass.cs.ciir.searchie.starter;

import ciir.jfoley.chai.io.LinesIterable;
import edu.umass.cs.ciir.searchie.starter.Test.TestCoreNLP;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Token;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class SimpleToken {
  public String truthLabel;
  public String lemma;
  public String token;
  public HashSet<String> features;

  public SimpleToken(String truthLabel, String lemma, HashSet<String> features) {
    this.truthLabel = truthLabel;
    this.lemma = lemma;
    this.features = features;
  }

  public SimpleToken(String truthLabel, String lemma, String token) {
    this.truthLabel = truthLabel;
    this.token = token;
    this.lemma = lemma;
  }

  /*public SimpleToken(SimpleToken token){
    this.truthLabel = token.truth
  }*/

  public SimpleToken(SimpleToken another) {
    this.truthLabel = another.getLabel(); // you can access
    this.lemma = another.getLemma();
    this.token = another.token;
    this.features = new HashSet<String>();
    for(String str: another.getFeatures()){
      this.features.add(str);
    }
  }
  /** If you've already got CRFSuite files, load it into this simple representation. */
  public static List<List<SimpleToken>> loadCRFSuiteInputFormat(File where) throws IOException {
    List<SimpleToken> tokens = new ArrayList<>();
    try (LinesIterable lines = LinesIterable.fromFile(where)) {
      for (String line : lines) {
        String what = line.trim();
        if (what.isEmpty()) {
          tokens.add(null); // sentence split marker
          continue;
        }
        String[] data = line.split("\t");
        String label = data[0];
        //if(label==null)
        //System.out.println(label);
        String token = data[1]; // first feature is token name; also keep as feature!
        HashSet<String> features = new HashSet<>(data.length);
        for (int i = 1; i < data.length; i++) {
          features.add(data[i].intern());
        }
        tokens.add(new SimpleToken(label, token, features));
      }
    }
    List<List<SimpleToken>> dataBySentence = new ArrayList<>();
    List<SimpleToken> cur = new ArrayList<>();
    for (SimpleToken token : tokens) {
      if(token == null){// && !cur.isEmpty()) {
        if(!cur.isEmpty()) {
          dataBySentence.add(cur);
          cur = new ArrayList<>();
        }
      } else {
        cur.add(token);
      }
    }
    if(!cur.isEmpty()) {
      dataBySentence.add(cur);
    }
    return dataBySentence;
  }

  public String getLabel() {
    return truthLabel;
  }
  public String getLemma () {return lemma;}
  public Set<String> getFeatures() {
    return features;
  }
  public void setFeatures(HashSet<String> features) {
    this.features = features;
  }

  /*public static SimpleToken convertTokenToSimpleToken(Token t){
     SimpleToken s = new SimpleToken(t.getLabel(), t.getToken());
     return s;
  }*/

  public static SimpleToken convertTokenToSimpleToken(Token t){
    SimpleToken s = new SimpleToken(t.getLabel(), t.getLemma(), t.getToken());
    String featureString = t.getFeatures();
    HashSet<String> features = new HashSet<>();
    String data[] = featureString.split(" ");
    for (int i = 1; i < data.length; i++) {
      //System.out.println(data[i]);
      features.add(data[i].intern());
    }
    s.setFeatures(features);
    return s;
  }

  public static SimpleToken convertTokenToSimpleTokenLemmatized(Token t, TestCoreNLP test){
    SimpleToken s = new SimpleToken(t.getLabel(), t.getLemma(), t.getToken());
    return s;
  }
}

/*
class ComplexToken extends SimpleToken{
  ComplexToken(String truthLabel, String lemma, HashSet<String> features, ){

  }
  int sentenceId;

}*/