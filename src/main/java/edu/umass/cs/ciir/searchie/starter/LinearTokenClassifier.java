package edu.umass.cs.ciir.searchie.starter;

import ciir.jfoley.chai.collections.TopKHeap;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import gnu.trove.map.hash.TObjectFloatHashMap;

import java.util.ArrayList;
import java.util.Set;

public class LinearTokenClassifier {
  public final TObjectFloatHashMap<String> featureWeights;
  int flag;
  ArrayList<SearchResult> resultsSeen;

  public int getFlag() {
    return flag;
  }

  public void setFlag(int flag) {
    this.flag = flag;
  }

  public LinearTokenClassifier(TObjectFloatHashMap<String> featureWeights) {
    this.featureWeights = featureWeights;
    flag = 0;
  }

  public void pushResultsSeen(ArrayList<SearchResult> resultsSeen){
    this.resultsSeen = resultsSeen;
  }

  public double score(Set<String> features) {
    double pred = 0;
    int count = 0;
    for (String feature : features) {
        if(featureWeights.containsKey(feature))
          count++;
        pred += featureWeights.get(feature);
    }
    //System.out.println("I found weight for " + count + " features");
    return pred;
  }

  public double getIntercept() {
    return 0;
  }

  public long getSize() {
    return featureWeights.size();
  }

  public LinearTokenClassifier deriveSampled(int k) {
    // limit to however many features...
    TopKHeap<ComparableFeature> features = new TopKHeap<>(Math.min(k, featureWeights.size()));
    featureWeights.forEachEntry((fname, fval) -> {
      features.offer(new ComparableFeature(fval, fname));
      return true;
    });

    TObjectFloatHashMap<String> heaviestFeatures = new TObjectFloatHashMap<>();
    for (ComparableFeature feature : features) {
      heaviestFeatures.put(feature.name, feature.weight);
    }
    return new LinearTokenClassifier(heaviestFeatures);
  }

  public boolean isSelectedToken(String token){
    boolean flag = false;
    for (SearchResult searchResult: resultsSeen){
      if(token.equals(searchResult.getMaxScoredToken())){
        flag = true;
        return flag;
      }
    }
    return flag;
  }

  public void addSearchResult(SearchResult searchResult){
    resultsSeen.add(searchResult);
  }

  /*public void addToken(String token){
    selectedTokens.add(token);
  }

  public void clearSelectedTokens(){
    selectedTokens.clear();
  }*/
}