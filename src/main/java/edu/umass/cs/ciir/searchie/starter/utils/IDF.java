package edu.umass.cs.ciir.searchie.starter.utils;

import ciir.jfoley.chai.collections.Pair;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sarwar on 5/8/17.
 */
public class IDF {
    Map<String, Double> idfMap;
    int sentenceCount;
    int topk;
    public PriorityQueue<Pair<String, Double>> pq;

    public static void main(String args[]){
        IDF idf = new IDF();
        idf.pq.add(Pair.of("hello", 10d));
    }

    public int getTopk() {
        return topk;
    }

    public void setTopk(int topk) {
        this.topk = topk;
    }

    public int getIDFMapSize(){
        return idfMap.size();
    }

    public IDF(){
        //<TokenCount> tokenCounts = new ArrayList<>();
        idfMap = new HashMap<>();
        sentenceCount = 0;
        pq = new PriorityQueue<>((o1, o2) -> o1.getValue().compareTo(o2.getValue()));
    }

    public boolean containsToken(String token){
        if(idfMap.containsKey(token))
            return true;
        else return  false;
    }

    public void addSentence(List<SimpleToken> tokenList){
        Set<String> temp = new HashSet<>();
        for(SimpleToken t: tokenList){
            if(!Stopwords.isStopword(t.lemma))
                temp.add(t.lemma);
        }
        for(String s: temp){
            if(idfMap.containsKey(s)){
                idfMap.put(s, idfMap.get(s)+1);
            }else{
                idfMap.put(s, 1d);
            }
        }
        sentenceCount++;
    }

    public double getIDF(String term){
        if(containsToken(term)){
            return Math.log(sentenceCount/idfMap.get(term));
        }
        else
            return 0d;
    }

    public Set<String> getTopkList(List<SimpleToken> baseTokens, int topk){
        List<Pair<String, Double>> l = new ArrayList<>();
        for(SimpleToken s: baseTokens){
            if(idfMap.containsKey(s.lemma)) {
                l.add(Pair.of(s.lemma, Math.log(sentenceCount / idfMap.get(s.lemma))));
            }
        }
        l.sort((Comparator<? super Pair<String, Double>>) Pair.cmpRight().reversed());
        Set<String> refinedTokens = new HashSet<>();
        for(int i=0; i<topk && i<l.size(); i++){
            refinedTokens.add(l.get(i).getKey());
        }
        return refinedTokens;
    }

    public Set<String> getTopkList(Set<String> baseTokens, int topk){
        List<Pair<String, Double>> l = new ArrayList<>();
        for(String s: baseTokens){
            if(idfMap.containsKey(s)) {
                l.add(Pair.of(s, Math.log(sentenceCount / idfMap.get(s))));
            }
        }
        l.sort((Comparator<? super Pair<String, Double>>) Pair.cmpRight().reversed());
        Set<String> refinedTokens = new HashSet<>();
        for(int i=0; i<topk && i<l.size(); i++){
            refinedTokens.add(l.get(i).getKey());
        }
        return refinedTokens;
    }

    public void formWordCloud(Set<String> wordFeatures, Set<String> relevantFeatures){
        if(relevantFeatures.size()==0)
            return;
        //Extract tokens from current word features
        Set<String> tokens = new HashSet<>();
        for(String s: relevantFeatures){
            Pattern p = Pattern.compile("(.*w)(\\[L\\]=|\\[R\\]=)(\\w+)(.*)");
            Matcher m = p.matcher(s);
            if (m.find() == true && m.group(3).length() >= 1) {
                //System.out.println("Found " + m.group(1) + m.group(2) + m.group(3));
                String val = m.group(3);// + m.group(3);
                if (val == null) continue;
                    tokens.add(val);
            }
        }
        List<String> extrapolatedTokens = DataSelector.extrapolateTokens(tokens, this); //construct exploredtokens from tokens found so far

        for(int i=0; i<extrapolatedTokens.size() && i<10; i++){
            String s = extrapolatedTokens.get(i);
            wordFeatures.add("w[L]=" + s);
            wordFeatures.add("w[R]=" + s);
        }

        for(String s: extrapolatedTokens){
            //System.out.println(s);
            pq.add(Pair.of(s, idfMap.get(s)));
        }
        int index=0;
        while(!pq.isEmpty() && index<10){
            String s = pq.peek().getKey();
            wordFeatures.add("w[L]=" + s);
            wordFeatures.add("w[R]=" + s);
            index++;
            pq.poll();
        }
        pq.clear();

    }

    @Override
    public String toString() {
        return "map size " + idfMap.size() +" number of sentences " + sentenceCount;
    }
}
