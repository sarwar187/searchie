package edu.umass.cs.ciir.searchie.starter.experiments_wsdm;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.Island;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EntityGraph;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 8/9/17.
 */
public class LTRTrainingNonOptimized {
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", "/home/sarwar/crfsuite/");
        String inputFileName = argp.get("file", "train_list.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        PrintResults pr = new PrintResults();

        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int size = fullPositives.size();
        int totalSize = fullConllTrain.size();


        //System.out.println("train size " + fullConllTrain.size());
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        List<List<SimpleToken>> positives = new ArrayList<>();
        mergedList.addAll(fullPositives);

        for(int i=0;i<totalSize-size;i++){
            mergedList.add(fullNegatives.get(i));
        }
        positives.add(mergedList.get(0));
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);

        //typespan code
        FeatureExtractor f = new FeatureExtractor(mergedList.get(0));
        String targetEntityType = f.getTargetEntityType();
        Set<List<String>> foundSpansCaseSensitive = DataSelector.getUniqueSpansCaseSensitive(positives, etype);
        List<Double> targetEntityEmbedding = EmbeddingSearch.getEntityEmbedding(foundSpansCaseSensitive, sm);
        //typespan code ends

        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);

        double prior[] = new double[mergedList.size()];
        double vectorLength[] = new double[totalSize];
        double points[][] = new double[totalSize][embeddingSize];

        //String expansionApproach = "LUCENE";
        String expansionApproach = "EMBEDDING";
        //String expansionApproach = "FULL";
        //String expansionApproach = "NONE";
        //String expansionApproach = "ALL";
        //String expansionParameter = "TOPK";
        String expansionParameter = "PRF";
        //String expansionParameter = "ISLAND";
        List<Integer> pseudoAndFullPositives = new ArrayList<>();

        int row = 0;
        int countPRF = 0;
        int topk = 3;
        LuceneInMemory lucene = new LuceneInMemory();

        List<Island> islands;
        Set<Integer> sentences = null;

        if(expansionParameter.equals("ISLAND")) {
            islands = EntityGraph.createIslands(mergedList, availableSpans, etype, inputFileName);
            for (Island island : islands) {
                if (island.containsSentence(0)) {
                    sentences = island.getSentences();
                    break;
                }
            }
        }
        //System.out.println(sentences.size() + " sentences found in target island");
        FeatureExtractor featureExtractor = null;
        Set<List<String>> typeSpans = null;
        for(List<SimpleToken> tokenList: mergedList) {
            featureExtractor = new FeatureExtractor(mergedList.get(row), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityList();
            for(List<String> strList: typeSpans)
                sm.updateContextExpansionMap(strList, row);

            lucene.addTokenList(row, tokenList);
            if(expansionParameter.equals("PRF")) {
                if (expansionApproach.equals("ALL")) {
                    if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, availableSpans) == 1) {
                        prior[row] = 10;
                        countPRF++;
                    }
                    pseudoAndFullPositives.add(row);
                } else {
                    if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, foundSpans) == 1) {
                        prior[row] = 10;
                        countPRF++;
                    }
                }
            }
            if(expansionParameter.equals("ISLAND")){
                if(sentences.contains(row)) {
                    prior[row] = 10;
                    countPRF++;
                }
            }
            //List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList), idf);
            List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList));
            int col = 0;
            for(double d: doubleList){
                points[row][col++] = d;
                vectorLength[row]+= (d*d);
            }
            if(vectorLength[row]-0d < 0.00000001) vectorLength[row]=1;
            row++;
        }
        //System.out.println("row " + row);
        sm.constructEntityEmbedding(points);

        for(int i=0;i<vectorLength.length;i++){
            vectorLength[i] = Math.sqrt(vectorLength[i]);
            if(vectorLength[i] - 0d < 0.00000001) vectorLength[row]=1;
            //System.out.print(vectorLength[i] + ",");
        }
        //BE CAREFUL ABOUT THE LINE BELOW
        //countPRF=3;

        //context expansion begins here
        int bootstrapSentenceId = 0;
        int [] expandedSentences;
        double[] expandedSentencesWeight;
        if(expansionParameter.equals("TOPK")) {
            expandedSentences = new int[topk];
            expandedSentencesWeight = new double[topk];
        }
        else if(expansionParameter.equals("PRF") || expansionParameter.equals("ALL") || expansionParameter.equals("ISLAND")) {
            expandedSentences = new int[countPRF];
            expandedSentencesWeight = new double[countPRF];
        }
        else {
            expandedSentences = new int[fullPositives.size()];
            expandedSentencesWeight = new double[fullPositives.size()];
        }
        //Map<Integer, Double> hashMap = lucene.search(mergedList.get(bootstrapSentenceId), StaticVariables.sizeOfInitialRetrieval);
        List<Integer> l = new ArrayList<>();
        if(expansionApproach.equals("LUCENE")) {
            l = lucene.search(mergedList.get(bootstrapSentenceId));
            row = 0;
            for (Integer s : l) {
                expandedSentences[row++] = s;
                if(expansionParameter.equals("TOPK")) {
                    if (row == topk)
                        break;
                }
                else if(expansionParameter.equals("PRF")) {
                    if (row == countPRF)
                        break;
                }
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }else if(expansionApproach.equals("EMBEDDING")) {
            row = 0;
            for (SearchResult s : EmbeddingSearch.search(bootstrapSentenceId, points, totalSize, embeddingSize, prior, vectorLength)) {
                expandedSentences[row] = s.getSentenceId();
                expandedSentencesWeight[row] = countPRF - row;
                row++;
                //System.out.println(s.getScore());
                //pr.printSimpleToken(mergedList.get(s.getSentenceId()), "found");
                if(expansionParameter.equals("TOPK")) {
                    if (row == topk)
                        break;
                }
                else if(expansionParameter.equals("PRF") || expansionParameter.equals("ISLAND")) {
                    if (row == countPRF)
                        break;
                }
                /*if (row == countPRF)
                    break;*/
            }
            //int [] expandedSentences = {bootstrapSentenceId};
            //EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, expandedSentencesWeight, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
            //context expansion done. the vector is pasted at location 0 of points array
        }else if(expansionApproach.equals("FULL")) {
            for(int i=0; i<fullPositives.size(); i++){
                expandedSentences[i] = i;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }else if(expansionApproach.equals("ALL")){
            row =0;
            for(Integer i: pseudoAndFullPositives){
                expandedSentences[row] = i;
                row++;
                if(expansionParameter.equals("PRF")) {
                    if (row == countPRF)
                        break;
                }
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }

        EmbeddingSearch.setPrior(prior, 0d);
        List<SearchResult> results = EmbeddingSearch.search(bootstrapSentenceId, points, totalSize, embeddingSize, prior, vectorLength);

        //type scoring begins

        featureExtractor = null;
        typeSpans = null;
        Set<List<String>> fullTypeSpans = new HashSet<>();
        featureExtractor = new FeatureExtractor(mergedList.get(0), targetEntityType);
        typeSpans = featureExtractor.getTargetEntityList();
        fullTypeSpans.addAll((typeSpans));
        int spanSize = fullTypeSpans.size();
        List<Double> entityContextEmbedding = new ArrayList<>();

        for(int i=0;i<200;i++){
            entityContextEmbedding.add(points[0][i]);
        }

        for (SearchResult s : results) {
            featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityList();
            double score = 0;
            double contextExpansionScore = 0;
            for (List<String> stringList : typeSpans) {
                if(!fullTypeSpans.contains(stringList)) {
                    List<Double> entityEmbedding = sm.getSentenceEmbedding(stringList);
                    double val = EmbeddingSearch.cosineSimilarity(targetEntityEmbedding, entityEmbedding);
                    if (val > score)
                        score = val;
                    val = EmbeddingSearch.cosineSimilarity(entityContextEmbedding, sm.getEntityEmbedding(stringList));
                    if (val > contextExpansionScore)
                        contextExpansionScore = val;
                }
            }
            //System.out.println("score " + score);
            fullTypeSpans.addAll((typeSpans));
            if (fullTypeSpans.size() - spanSize == 0) {
                s.setScore(0d);
                s.setScoreEntity(score);
                s.setScoreEntityContext(contextExpansionScore);
            } else {
                //pr.printSpans(typeSpans, "type spans");
                spanSize = fullTypeSpans.size();
                s.setScoreEntity(score);
                s.setScoreEntityContext(contextExpansionScore);
                //System.out.println(s.getScore() + "\t" + s.getScoreEntity() + "\t" + s.getScoreEntityContext());
                //s.setScore(s.getScore() + score + contextExpansionScore);
            }

        }
        BufferedWriter br= new BufferedWriter(new FileWriter(new File(featureDirectory + featureFileName)));
        NumberFormat formatter = new DecimalFormat("#0.000");
        for (SearchResult s : results) {
            //positives.add(mergedList.get(s.getSentenceId()));
            //int successful = DataSelector.addNewSpansWithGroundTruth(constantSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            //foundSpans.addAll(constantSpans);
            //constantSpans.clear();
            //constantSpans = DataSelector.getUniqueSpans(positives, etype);
            String featureString = inputFileName + "," + s.getSentenceId() + ",";
            FeatureExtractor fe = new FeatureExtractor(mergedList.get(s.getSentenceId()));
            featureString += fe.posNERFeatures();
            int containsFoundEntity = DataSelector.containsFoundEntity(availableSpans, mergedList.get(s.getSentenceId()), etype, foundSpans);
            featureString+= String.valueOf(containsFoundEntity) + ",";
            //List<SimpleToken> t = mergedList.get(i).get();
            if(successful==1) {
                //if(hashMap.containsKey(i)) {
                //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 1);
                featureString += (formatter.format(s.getScore()) + "," + mergedList.get(s.getSentenceId()).size() + "," + formatter.format(s.getScoreEntity()) + "," + formatter.format(s.getScoreEntityContext()) + "," + 1);
                //System.out.println(featureString);
                br.write(featureString);
                br.newLine();
                //}
            }else{
                //if(hashMap.containsKey(i)) {
                //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 0);
                featureString += (formatter.format(s.getScore()) + "," + mergedList.get(s.getSentenceId()).size() + "," + formatter.format(s.getScoreEntity()) + "," + formatter.format(s.getScoreEntityContext()) + "," + 0);
                //System.out.println(featureString);
                br.write(featureString);
                br.newLine();
                //}
            }
            //PrintResults pr = new PrintResults();
            //pr.printSimpleToken(mergedList.get(i), "input");

        }
        System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + (1.0 * foundSpans.size()/availableSpans.size()));
        br.close();
    }

    /*public static void setPrior(double prior[], double val){
        for(int i=0;i<prior.length;i++)
            prior[i] = val;
    }*/

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            /*if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
            }*/
            if(!Stopwords.isStemmedStopword(token.lemma)){
                strList.add(token.lemma);
            }
        }
        return strList;
        //return s.trim();
    }

}
