package edu.umass.cs.ciir.searchie.starter.utils;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.TrainingData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.print.DocFlavor;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by sarwar on 1/3/17.
 */
public class PrintResults {
    String directory;
    String fileName;
    JSONObject jsonObject;
    public PrintResults(String directory, String fileName){
        this.directory = directory;
        this.fileName = fileName;
        jsonObject = new JSONObject();
    }

    public PrintResults(){

    }
    void printAP(double a[][], int methods, int loop){

    }

    void printuAP(){

    }

    void printf1(){

    }

    public void addJSON(String tag, String value){
        jsonObject.put(tag, value);
    }

    public void writeJSON(){
        try (FileWriter file = new FileWriter(directory + fileName)) {
            file.write(jsonObject.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printTwoD(int labelType, double eval[][], int numExperiments, int loop){
        DecimalFormat df = new DecimalFormat("#0.000");
        if(labelType==0){
            for(int j = 0; j < StaticVariables.fblabel.length; j++) {
                System.out.print(StaticVariables.fblabel[j] + "\t");
                for (int i = 0; i < loop; i++) {
                    System.out.print(df.format(eval[i][j] / numExperiments) + "\t");
                }
                System.out.println();
            }
        }else if(labelType==1) {
            for(int j=0; j < StaticVariables.label.length; j++) {
                JSONArray list = new JSONArray();
                System.out.print(StaticVariables.label[j] + "\t");
                for (int i = 0; i < loop; i++) {
                    if(j>7) System.out.print((int) (eval[i][j] / numExperiments) + "\t");
                    else System.out.print(df.format(eval[i][j] / numExperiments) + "\t");
                    list.add(df.format(eval[i][j] / numExperiments));
                }
                jsonObject.put(StaticVariables.label[j], list);
                System.out.println();
                //Have to define result directory
                //PrintResults.writeJSON(obj, resultDirectory + featureFileName);
            }
        }else
            System.out.println("nothing");

    }

    public void printSimpleTokenList(List<List<SimpleToken>> tokens, String content){
        if(content.length()>0)
            System.out.println(content);
        for(List<SimpleToken> simpleTokenList: tokens){
            for(SimpleToken simpleToken: simpleTokenList){
                //System.out.print(simpleToken.lemma + "=" + simpleToken.truthLabel + ",");
                System.out.print(simpleToken.lemma + "-" + simpleToken.truthLabel + " , " );
            }
            System.out.println();
        }
    }

    public String printSimpleToken(List<SimpleToken> tokens, String header){
        String output = "";
        //if(!header.equals("none"))
            //System.out.println(header);
        for(SimpleToken simpleToken: tokens){
            //System.out.print(simpleToken.lemma + "=" + simpleToken.truthLabel + ",");
            output+=simpleToken.lemma + " ";
        }
        output = output.trim();
        //System.out.println();
        return output;
    }

    public String printSimpleTokenSimply(List<SimpleToken> tokens, String header){
        //List<String> restrictedTokenList = Arrays.asList("null");
        //int a[] = new int[restrictedTokenList.size()];
        //if(!header.equals("none"))
        //    System.err.println(header);
        String output = "";
        for(SimpleToken simpleToken: tokens){
            //if(restrictedTokenList.contains(simpleToken.lemma)){
            //    a[restrictedTokenList.indexOf(simpleToken.lemma)]++;
            // }
            output+=simpleToken.token + " ";
            //System.out.print(simpleToken.lemma + " ");
        }
        output = output.trim();
        int count = 0;
        //for(int i=0;i<a.length;i++){
        //    if(a[i]>0)
        //        count++;
        //}
        //if(count < 1)
        output = output.replaceAll("null.*-- ", "");
        output = output.replaceAll("null.*; ", "");
        output = output.replaceAll(" -RRB- ", ")");
        output = output.replaceAll(" -LRB- ", "(");
        //System.err.print(output + "\t");
        //System.out.println();
        return output;
    }

    public void printSearchResultList(List<SearchResult> searchResultList){
        for(SearchResult searchResult: searchResultList){
            System.out.print(searchResult.getMaxScoredToken() + "\t");
        }
        System.out.println();
    }

    public void printTokenSet(Set<String> tokenSet, String header){
        System.out.println(header);
        for(String token: tokenSet){
            System.out.print(token + ",");
        }
        System.out.println();
    }

    public void printTokenList(List<String> tokenSet){
        for(String token: tokenSet){
            System.out.print(token + "\t");
        }
        System.out.println();
    }

    public void printSpans(Set<List<String>> span, String header) {
        System.out.println(header);
        for (List<String> tokenList : span) {
            for (String token : tokenList)
                System.out.println(token);
            System.out.println();

        }
    }

    public String SimpleTokenListToString(List<SimpleToken> tokenList) {
        String indexString = "";
        for(SimpleToken token: tokenList) {
            indexString+=token.lemma;
            indexString+=" ";
        }
        return indexString;
    }


    public String printQueryModel(HashMap<String, Double> queryModel){
        double sum = 0;
        String output = "";
        for(String key: queryModel.keySet()){
            //System.err.println(key + "\t" + queryModel.get(key));
            output+=key + "\t" + queryModel.get(key)+ "\n";
            sum+=queryModel.get(key);
        }
        output+="total weight " + sum;
        return output;
        //System.err.println("total weight " + sum);
    }

    public List<SimpleToken> removeEntityFromSimpleTokenList(List<SimpleToken> tokenList, Set<List<String>> foundEntities) {
        List<SimpleToken> tempTokenList = new ArrayList<>();
        for (SimpleToken token : tokenList) {
            tempTokenList.add(token);
        }
        Set<String> temp = new HashSet<>();

        for (List<String> stringList : foundEntities) {
            for (String s : stringList) {
                temp.add(s);
            }
        }

        for (String s : temp) {
            List<Integer> indexes = new ArrayList<>();
            for (int i = 0; i < tempTokenList.size(); i++) {
                if (tempTokenList.get(i).lemma.toLowerCase().equals(s.toLowerCase())) {
                    indexes.add(i);
                }
            }
            Collections.sort(indexes, Collections.reverseOrder());
            for (int i : indexes)
                tempTokenList.remove(i);
        }
        return tempTokenList;
    }

    public void printEvaluate(List<Double> list){
        String str = "";
        for(Double l: list)
            str+= l + "\t";
        str = str.trim();
        System.out.println(str);
    }

    public void printTrainingData(TrainingData queryTrainingData, TrainingData trainingData) {
        String output = "";
        output+= trainingData.getQueryId() + "\t";
        output+= String.valueOf(trainingData.getSentenceId()) + "\t";
        //pasting the query entity and context at first
        //query entity
        for (String str : queryTrainingData.getEntity()) {
            output += str + " ";
        }
        output = output.trim();
        output += "\t";
        //query context
        for (String str : queryTrainingData.getContext()) {
            output += str + " ";
        }
        output = output.trim();
        output += "\t";
        //candidate entity
        for (String str : trainingData.getEntity()) {
            output += str + " ";
        }
        output = output.trim();
        output += "\t";
        //candidate context
        for (String str : trainingData.getContext()) {
            output += str + " ";
        }
        output = output.trim();
        output += "\t";
        //entity similarity value
        output+=trainingData.getTruthValue();
        //outputs
        System.out.println(output);
    }



}
