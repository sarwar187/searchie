package edu.umass.cs.ciir.searchie.starter.learning_to_rank;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by sarwar on 6/24/17.
 */
public class FeatureInfo {
    public static int containsFoundEntity = 1;  //item specific
    public static int POSTagFeatures = 1;  //item specific
    public static int NERFeatures = 1;
    public static int sentenceLength= 1;
    public static int embeddingScore = 1;
    public static int luceneScore = 0;
    public static String getFeatureString() {

        String result = "@relation searchie\r\n";
        result += "@attribute queryId string\r\n";
        result += "@attribute instanceId integer\r\n";


        if(POSTagFeatures == 1){
            PartOfSpeech partOfSpeech[] = PartOfSpeech.values();
            for(int i=0; i<partOfSpeech.length; i++){
                result += "@attribute ";
                result += partOfSpeech[i].toString();
                result += " integer\r\n";
            }
        }

        if(NERFeatures == 1){
            NER ner[] = NER.values();
            for(int i=0; i<ner.length; i++){
                result += "@attribute ";
                result += ner[i].toString();
                result += " integer\r\n";
            }
        }
        if(containsFoundEntity == 1){
            result += "@attribute containsFoundEntity integer\r\n";
        }

        if(embeddingScore == 1){
            result += "@attribute embeddingScore real\r\n";
        }

        if(luceneScore == 1){
            result += "@attribute luceneScore real\r\n";
        }
        if(sentenceLength == 1){
            result += "@attribute sentenceLength integer\r\n";
        }

        //result += "@attribute clickBuyRatio real\r\n";
        result += "@attribute class {1, 0}\r\n";
        result += "@DATA";
        return result;

    }

    public static void main(String args[]) throws IOException {
        BufferedWriter br=null;
        //br = new BufferedWriter(new FileWriter(new File("/mnt/nfs/work1/smsarwar/searchie/ltr/train/train_header.crfsuite")));
        br = new BufferedWriter(new FileWriter(new File("/home/sarwar/crfsuite/ltr/train_header.crfsuite")));
        br.write(FeatureInfo.getFeatureString());
        br.newLine();
        br.close();
    }
}
