package edu.umass.cs.ciir.searchie.starter.experiments_wsdm;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by sarwar on 8/11/17.
 */
public class EntityCount {
    public static void main (String args[]) throws FileNotFoundException {
        Map<String, Integer> hashMap = new HashMap<>();
        try (Scanner sc = new Scanner(new File("/home/sarwar/Desktop/entity_count.txt"))) {
            while(sc.hasNext()){
                String val = sc.next();
                val = val.trim();
                if(!hashMap.containsKey(val))
                    hashMap.put(val,1);
                else
                    hashMap.put(val, hashMap.get(val)+1);
            }
        }
        for(String key: hashMap.keySet()){
            System.out.println(key + "\t" + hashMap.get(key));
        }

    }
}

