package edu.umass.cs.ciir.searchie.starter.utils;

/**
 * Created by sarwar on 4/11/17.
 */
public class FeatureFilters {

    /*String str contextFeatureFilter(String feature){

    }*/
//    public static int featureFilterNgram(String feature) {
//        if (feature.contains("ng")) {
//            System.out.println("found ngram");
//            return 1;
//        } else
//            return 0;
//    }
//
//
//    public static int featureFilterPOS(String feature) {
//        if (feature.contains("p[") && feature.length() <=10) {
//            System.out.println("found POS");
//            return 1;
//        } else
//            return 0;
//    }
//
//    public static int featureFilterCluster(String feature) {
//        if (feature.contains("c[") && feature.length() <=10) {
//            String string = feature;
//            String[] parts = string.split("=");
//            if (parts[0].charAt(2) == '0') {
//                System.out.println("found cluster");
//                return 1;
//            }
//        }
//        return 0;
//    }

    public static int featureFilterContextWords(String feature) {
        int count = 0;
        for(int i = 0; i< feature.length(); i++){
            if(feature.charAt(i) == '[')
                count++;
        }
        //if ((feature.contains("w[") && count ==1)) {
        if (((feature.contains("w[L]")||feature.contains("w[R]") && count==1))) {
            //System.out.println("found ngram");
            return 1;
        } else
            return 0;
        //}
    }

    public static int featureFilterNER(String feature) {
        int count = 0;
        for(int i = 0; i< feature.length(); i++){
            if(feature.charAt(i) == '[')
                count++;
        }
        //if ((feature.contains("w[") && count ==1)) {
        if (((feature.contains("ner[") && count==1))) {
            //System.out.println("found ngram");
            return 1;
        } else
            return 0;
        //}
    }

    public static int featureFilterNgram(String feature) {
        int count = 0;
        for(int i = 0; i< feature.length(); i++){
            if(feature.charAt(i) == '[')
                count++;
        }
        //if ((feature.contains("w[") && count ==1)) {
        if ((feature.contains("w[") && count==1)) {
            //System.out.println("found ngram");
            return 1;
        } else
            return 0;
        //}
    }


    public static int featureFilterPOS(String feature) {
        int count = 0;
        for(int i = 0; i< feature.length(); i++){
            if(feature.charAt(i) == '[')
                count++;
        }

        if (feature.contains("p[") && count==1) {
            //if (feature.contains("p[") && feature.length() <=10) {
            //System.out.println("found POS");
            return 1;
        } else
            return 0;
    }

    public static int featureFilterCluster(String feature) {
        int count = 0;
        for(int i = 0; i< feature.length(); i++){
            if(feature.charAt(i) == '[')
                count++;
        }

        if (feature.contains("c[") && count==1) {
            //if (feature.contains("c[") && feature.length() <=10) {
            String string = feature;
            String[] parts = string.split("=");
            if (parts[0].charAt(2) == '0') {
                //System.out.println("found cluster");
                return 1;
            }
        }
        return 0;
    }

    public static int featureFilterNERZERO(String feature) {
        int count = 0;
        for(int i = 0; i< feature.length(); i++){
            if(feature.charAt(i) == '[')
                count++;
        }
        //if ((feature.contains("w[") && count ==1)) {
        if (((feature.contains("ner[0]") && count==1))) {
            //System.out.println("found ngram");
            return 1;
        } else
            return 0;
        //}
    }

    public static int featureFilterPOSZERO(String feature) {
        int count = 0;
        for(int i = 0; i< feature.length(); i++){
            if(feature.charAt(i) == '[')
                count++;
        }

        if (feature.contains("p[0]") && count==1) {
            //if (feature.contains("p[") && feature.length() <=10) {
            //System.out.println("found POS");
            return 1;
        } else
            return 0;
    }
}
