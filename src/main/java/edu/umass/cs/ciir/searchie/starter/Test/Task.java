package edu.umass.cs.ciir.searchie.starter.Test;

import java.util.List;

public class Task {
    private String doc;
    private String qid;
    private double score;
    //List<Sentences>;
    private final long id;
    private String summary;
    private String description;
    private Status status;
    private int priority;
    private List<String> keywords;


    public Task(long id, String summary, String description, Status status, int priority, String doc) {
        this.id = id;
        this.summary = summary;
        this.description = description;
        this.status = status;
        this.priority = priority;
        this.doc = doc;
    }


    public enum Status {
        CREATED, ASSIGNED, CANCELED, COMPLETED
    }
    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public long getId() {
        return id;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Task [id=" + id + ", summary=" + summary + ", description=" + description + ", status=" + status
                + ", priority=" + priority + "]";
    }

}