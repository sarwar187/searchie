package edu.umass.cs.ciir.searchie.starter.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class DataStructure {
    public static List<String> spansToStringList(Set<List<String>> stringSet){
        ArrayList<String> result = new ArrayList<>();
        for(List<String > stringList: stringSet){
            String output = "";
            for(String s: stringList){
                output+=s.toLowerCase() + " ";
            }
            output = output.trim();
            result.add(output);
        }
        return result;
    }
}
