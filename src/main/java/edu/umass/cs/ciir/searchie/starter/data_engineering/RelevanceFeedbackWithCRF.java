package edu.umass.cs.ciir.searchie.starter.data_engineering;

/**
 * Created by sarwar on 6/8/17.
 */
public class RelevanceFeedbackWithCRF {
    public interface SentenceScorer{
        double scoreSentence(double scoreArray[]);
    }

    public static final class ScoreSumPositive implements SentenceScorer {
        @Override
        public double scoreSentence(double scoreArray[]) {
            double score = 0;
            for(int i=0; i<scoreArray.length; i++){
                if(scoreArray[i]>0)
                    score+=scoreArray[i];
            }
            return score;
        }
    }

    public static final class ScoreMax implements SentenceScorer {
        @Override
        public double scoreSentence(double scoreArray[]) {
            double maxScore = Double.NEGATIVE_INFINITY; //maxScore will contain the highest scoring token in a sentence, eventually
            for(int i=0; i<scoreArray.length; i++){
                if(scoreArray[i]> maxScore)
                    maxScore = scoreArray[i];
            }
            return maxScore;
        }
    }
    public static final class ScoreSumAll implements SentenceScorer {
        @Override
        public double scoreSentence(double scoreArray[]) {
            double score = 0;
            for(int i=0; i<scoreArray.length; i++){
                score+=scoreArray[i];
            }
            return score;
        }
    }
}
