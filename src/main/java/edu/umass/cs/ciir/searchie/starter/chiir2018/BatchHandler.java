package edu.umass.cs.ciir.searchie.starter.chiir2018;

import edu.stanford.nlp.trees.TreeFilters;
import edu.stanford.nlp.util.EditDistance;
import edu.stanford.nlp.util.StringUtils;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Query;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.QueryModel;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.lemurproject.galago.core.util.WordLists;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.SocketImpl;
import java.util.*;

public class BatchHandler {
    public static HashMap<String, List<String>> loadBatch(int bootstrapSentenceId) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("/home/smsarwar/batch_" + bootstrapSentenceId + ".txt"));
        HashMap<String, List<String>> hashMap = new HashMap<>();
        while (sc.hasNextLine()){
            String str = sc.nextLine();
            //System.out.println(str);
            StringTokenizer st = new StringTokenizer(str, "\t");
            String file_id = st.nextToken();
            ArrayList<String> context_words = new ArrayList<>();
            while(st.hasMoreTokens()){
                context_words.add(st.nextToken());
            }
            hashMap.put(file_id, context_words);
        }
        return hashMap;
    }

    public static HashMap<String, Integer> loadBootstrapSentence() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("/home/smsarwar/output.txt"));
        HashMap<String, Integer> hashMap = new HashMap<>();
        while (sc.hasNextLine()){
            String str = sc.nextLine();
            //System.out.println(str);
            StringTokenizer st = new StringTokenizer(str, "\t");
            hashMap.put(st.nextToken(), Integer.parseInt(st.nextToken()));
        }
        return hashMap;
    }


    //adds
    public static List<List<SimpleToken>> getPossibleQueries(int bootstrapSentenceId, List<List<SimpleToken>> mergedList){
        List<List<SimpleToken>> possibleQueries = new ArrayList<>();
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);
        //int[] weightArray = {0,0,0,0,0,0,0,0,3,3,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,3,0};
        int[][] weightArray = getDefaultWeightArray(queryTokenList);

//        int[][] weightArray = {
//                {1,1,1,1,1,1,1,1,1,1,1,5,3,1,1,1,1,1,1,1,1,1,1,1,1,1,5,1,1,1,1,1,1,1,3,1,1,1},
//                {1,1,1,1,1,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
//                {1,1,1,1,1,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,1,1},
//                {1,1,1,1,1,1,1,1,1,1,1,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,3,1,1,1},
//                {1,1,1,1,1,1,1,1,1,1,1,4,4,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,4,1,1,1},
//                {1,1,1,1,1,1,1,1,1,1,1,3,2,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,2,1,1,1},
//        };

//        int[][] weightArray = {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1},
//                {0,0,0,0,0,0,0,0,2,2,0,5,0,0,0,2,0,0,0,0,0,0,0,0,0,2,0},
//                {0,0,0,0,0,0,0,0,2,2,0,5,0,0,0,2,0,0,0,0,0,0,0,0,0,2,0},
//                {1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1}};
//                {1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
//                {1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
//                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
//                {1,1,1,1,1,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,5,1},
//                {1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1},
//                {1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1},
//                {1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1},
//                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},};
        //int[] weightArray = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

        for(int i=0; i < weightArray.length;i++){
            List<SimpleToken> possibleQuery = new ArrayList<>();
            for(int j=0; j < weightArray[i].length;j++){
                for(int k=0; k < weightArray[i][j]; k++){
                    SimpleToken st = new SimpleToken(queryTokenList.get(j));
                    possibleQuery.add(st);
                }
            }
            possibleQueries.add(possibleQuery);
        }
        /*int[] weightArray = new int[queryTokenList.size()];
        Scanner sc1 = new Scanner(System.in);
        int weightIndex = 0;
        for(SimpleToken t: queryTokenList){
            System.out.println(t.token);
            weightArray[weightIndex++] = sc1.nextInt();
        }
        List<SimpleToken> modifiedQuery = new ArrayList<>();*/
        //System.out.println("possible queries size " + possibleQueries.size());
        return possibleQueries;

    }

    //performs expansion using BM25 lucence after getting the contextual tokens from users
    public static List<List<SimpleToken>> getContextuallyExpandedQueries(String inputFileName, int bootstrapSentenceId, List<List<SimpleToken>> mergedList, HashMap<String, List<String>> hashMap){
        List<List<SimpleToken>> possibleQueries = new ArrayList<>();
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);
        List<SimpleToken> possibleQuery = new ArrayList<>();
        //At first push the original tokens into the sentence
        for(SimpleToken simpleToken: queryTokenList){
            //if((!simpleToken.truthLabel.equals("LIST"))) {
                SimpleToken st = new SimpleToken(simpleToken);
                possibleQuery.add(st);
            //}
        }
        List<String> contextWords = hashMap.get(inputFileName);

        //Finally the context words using the hashmap
        for(String str: contextWords){
            for(SimpleToken simpleToken: mergedList.get(bootstrapSentenceId)){
                if(simpleToken.token.equals(str)){ // && (!simpleToken.truthLabel.equals("LIST"))){
                    possibleQuery.add(simpleToken);
                }
            }
        }

        possibleQueries.add(possibleQuery);
        /*int[] weightArray = new int[queryTokenList.size()];
        Scanner sc1 = new Scanner(System.in);
        int weightIndex = 0;
        for(SimpleToken t: queryTokenList){
            System.out.println(t.token);
            weightArray[weightIndex++] = sc1.nextInt();
        }
        List<SimpleToken> modifiedQuery = new ArrayList<>();*/
        //System.out.println("possible queries size " + possibleQueries.size());
        return possibleQueries;
    }

    public static double[] getContextualQueryModel(String inputFileName, int bootstrapSentenceId, List<List<SimpleToken>> mergedList, HashMap<String, List<String>> hashMap, double alpha, double beta, double gamma){
        double contextQueryModel[]  = new double[mergedList.get(bootstrapSentenceId).size()];
        double entityQueryModel[]  = new double[mergedList.get(bootstrapSentenceId).size()];
        double uniformQueryModel[]  = new double[mergedList.get(bootstrapSentenceId).size()];
        double finalQueryModel[]  = new double[mergedList.get(bootstrapSentenceId).size()];



        //double alpha = 0.4, beta = 0.3, gamma = 0.3;

        List<List<SimpleToken>> possibleQueries = new ArrayList<>();
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);
        List<SimpleToken> possibleQuery = new ArrayList<>();

        //At first push the original tokens into the sentence
        int index = 0;
        int flag = 0;
        double numEntityToken = 0d;
        double numContextToken = 0d;
        //There can be multiple query entities, we pick the first one

        for(SimpleToken simpleToken: queryTokenList) {
            if(Stopwords.isStopword(simpleToken.lemma)) {
                uniformQueryModel[index] = 1.0 / mergedList.get(bootstrapSentenceId).size();
                if (simpleToken.truthLabel.equals("LIST")) {
                    flag = 1;
                }
                if (!simpleToken.truthLabel.equals("LIST") && flag == 1) {
                    flag = 0;
                }
                if (flag == 1) {
                    entityQueryModel[index] = 1.0;
                    numEntityToken++;
                }
                index++;
            }
        }




        List<String> contextWords = hashMap.get(inputFileName);
        //Finally the context words using the hashmap
        for(String str: contextWords){
            index=0;
            for(SimpleToken simpleToken: mergedList.get(bootstrapSentenceId)){
                if(simpleToken.token.equals(str)){ // && (!simpleToken.truthLabel.equals("LIST"))){
                    possibleQuery.add(simpleToken);
                    contextQueryModel[index++] = 1.0;
                    //numContextToken++;
                }

            }
        }
        numContextToken = Arrays.stream(contextQueryModel).sum();
        for(int i=0; i<contextQueryModel.length; i++){
            if(contextQueryModel[i]==1){
                contextQueryModel[i]/=numContextToken;
            }
            if(entityQueryModel[i]==1){
                entityQueryModel[i]/=numEntityToken;
            }
        }

        for(int i=0; i<finalQueryModel.length; i++) {
            finalQueryModel[i] = alpha * entityQueryModel[i]  + beta * contextQueryModel[i] + gamma * uniformQueryModel[i];
        }
        //possibleQueries.add(possibleQuery);
        /*int[] weightArray = new int[queryTokenList.size()];
        Scanner sc1 = new Scanner(System.in);
        int weightIndex = 0;
        for(SimpleToken t: queryTokenList){
            System.out.println(t.token);
            weightArray[weightIndex++] = sc1.nextInt();
        }
        List<SimpleToken> modifiedQuery = new ArrayList<>();*/
        //System.out.println("possible queries size " + possibleQueries.size());

        System.out.println(Arrays.stream(entityQueryModel).sum());
        System.out.println(Arrays.stream(contextQueryModel).sum());
        System.out.println(Arrays.stream(uniformQueryModel).sum());
        System.out.println(Arrays.stream(finalQueryModel).sum());

        return finalQueryModel;
    }

    public static HashMap<String, Double> getContextualQueryModelMap(String inputFileName, int bootstrapSentenceId, List<List<SimpleToken>> mergedList, HashMap<String, List<String>> hashMap, double alpha, double beta, double gamma){

        HashMap<String, Double> contextQueryModel = new HashMap<>();
        HashMap<String, Double> entityQueryModel = new HashMap<>();
        HashMap<String, Double> uniformQueryModel = new HashMap<>();
        HashMap<String, Double> finalQueryModel = new HashMap<>();
        //double alpha = 0.4, beta = 0.3, gamma = 0.3;
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);

        //At first push the original tokens into the sentence
        int index = 0;
        int flag = 0;
        //There can be multiple query entities, we pick the first one
//        for(SimpleToken simpleToken: queryTokenList) {
//            if(!Stopwords.isStopword(simpleToken.lemma)) {
//                uniformQueryModel.put(simpleToken.lemma, 1.0);
//                if (simpleToken.truthLabel.equals("LIST")) {
//                    flag = 1;
//                }
//                if (!simpleToken.truthLabel.equals("LIST") && flag == 1) {
//                    flag = 0;
//                }
//                if (flag == 1) {
//                    entityQueryModel.put(simpleToken.lemma, 1.0);
//                }
//                index++;
//            }
//        }

        for(SimpleToken simpleToken: queryTokenList) {
            if(!Stopwords.isStopword(simpleToken.lemma)) {
                uniformQueryModel.put(simpleToken.lemma, 1.0);
                if (simpleToken.truthLabel.equals("LIST")) {
                    entityQueryModel.put(simpleToken.lemma, 1.0);
                }
                index++;
            }
        }

        List<String> contextWords = hashMap.get(inputFileName);
        //Finally the context words using the hashmap
        for(String str: contextWords){
            index=0;
            for(SimpleToken simpleToken: mergedList.get(bootstrapSentenceId)){
                if(simpleToken.token.equals(str)){ // && (!simpleToken.truthLabel.equals("LIST"))){
                    contextQueryModel.put(simpleToken.lemma, 1.0);
                }
            }
        }

        PrintResults pr = new PrintResults();
        //pr.printQueryModel(uniformQueryModel);
        //pr.printQueryModel(entityQueryModel);
        //pr.printQueryModel(contextQueryModel);

        QueryModel.normalizeQueryModel(uniformQueryModel, uniformQueryModel.size());
        QueryModel.normalizeQueryModel(entityQueryModel, entityQueryModel.size());
        QueryModel.normalizeQueryModel(contextQueryModel, contextQueryModel.size());
        QueryModel.mergeQueryModel(finalQueryModel, uniformQueryModel, alpha);
        QueryModel.mergeQueryModel(finalQueryModel, entityQueryModel, beta);
        //QueryModel.mergeQueryModel(finalQueryModel, contextQueryModel, gamma);
        //possibleQueries.add(possibleQuery);
        /*int[] weightArray = new int[queryTokenList.size()];
        Scanner sc1 = new Scanner(System.in);
        int weightIndex = 0;
        for(SimpleToken t: queryTokenList){
            System.out.println(t.token);
            weightArray[weightIndex++] = sc1.nextInt();
        }
        List<SimpleToken> modifiedQuery = new ArrayList<>();*/
        //System.out.println("possible queries size " + possibleQueries.size());

        return finalQueryModel;
    }

    public static HashMap<String, Double> getQueryModelMap(String list_query, int bootstrapSentenceId, List<List<SimpleToken>> mergedList, HashMap<String, List<String>> hashMap, double alpha, double beta){

        HashMap<String, Double> entityQueryModel = new HashMap<>();
        HashMap<String, Double> uniformQueryModel = new HashMap<>();
        HashMap<String, Double> listQueryModel = new HashMap<>();
        HashMap<String, Double> finalQueryModel = new HashMap<>();
        //double alpha = 0.4, beta = 0.3, gamma = 0.3;
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);


        //String[] list_query_tokenized = list_query.split(" ");
        //for(String str: list_query_tokenized) {
        //   listQueryModel.put(str, (1.0/list_query_tokenized.length));
        //}


        //At first push the original tokens into the sentence
        int index = 0;
        int flag = 0;

        for(SimpleToken simpleToken: queryTokenList) {
            if(!Stopwords.isStopword(simpleToken.lemma)) {
                uniformQueryModel.put(simpleToken.lemma, 1.0);
                if (simpleToken.truthLabel.equals("LIST")) {
                    entityQueryModel.put(simpleToken.lemma, 1.0);
                }
                index++;
            }
        }

        PrintResults pr = new PrintResults();
        //pr.printQueryModel(uniformQueryModel);
        //pr.printQueryModel(entityQueryModel);
        //pr.printQueryModel(contextQueryModel);

        QueryModel.normalizeQueryModel(uniformQueryModel, uniformQueryModel.size());
        QueryModel.normalizeQueryModel(entityQueryModel, entityQueryModel.size());
        //QueryModel.normalizeQueryModel(listQueryModel, listQueryModel.size());
        //QueryModel.mergeQueryModel(finalQueryModel, uniformQueryModel, 1.0);
        QueryModel.mergeQueryModel(finalQueryModel, uniformQueryModel, alpha);
        QueryModel.mergeQueryModel(finalQueryModel, entityQueryModel, beta);
        //QueryModel.mergeQueryModel(finalQueryModel, listQueryModel, (1-alpha-beta));
        return finalQueryModel;
    }


    public static HashMap<String, Double> getEntityQueryModelMap(String list_query, int bootstrapSentenceId, List<List<SimpleToken>> mergedList, HashMap<String, List<String>> hashMap, double alpha, double beta){

        HashMap<String, Double> entityQueryModel = new HashMap<>();
        HashMap<String, Double> uniformQueryModel = new HashMap<>();
        HashMap<String, Double> listQueryModel = new HashMap<>();
        HashMap<String, Double> finalQueryModel = new HashMap<>();
        //double alpha = 0.4, beta = 0.3, gamma = 0.3;
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);


        String[] list_query_tokenized = list_query.split(" ");
        for(String str: list_query_tokenized) {
            listQueryModel.put(str, (1.0/list_query_tokenized.length));
        }


        //At first push the original tokens into the sentence
        int index = 0;
        int flag = 0;

        for(SimpleToken simpleToken: queryTokenList) {
            if(!Stopwords.isStopword(simpleToken.lemma)) {
                uniformQueryModel.put(simpleToken.lemma, 1.0);
                if (simpleToken.truthLabel.equals("LIST")) {
                    entityQueryModel.put(simpleToken.lemma, 1.0);
                }
                index++;
            }
        }

        PrintResults pr = new PrintResults();
        //pr.printQueryModel(uniformQueryModel);
        //pr.printQueryModel(entityQueryModel);
        //pr.printQueryModel(contextQueryModel);

        QueryModel.normalizeQueryModel(uniformQueryModel, uniformQueryModel.size());
        QueryModel.normalizeQueryModel(entityQueryModel, entityQueryModel.size());
        //QueryModel.normalizeQueryModel(listQueryModel, listQueryModel.size());
        QueryModel.mergeQueryModel(finalQueryModel, uniformQueryModel, alpha);
        QueryModel.mergeQueryModel(finalQueryModel, entityQueryModel, beta);
        //QueryModel.mergeQueryModel(finalQueryModel, listQueryModel, (1-alpha-beta));
        return entityQueryModel;
    }

    //This is the unweighted version
    public static List<List<SimpleToken>> getContextuallyExpandedUnweightedQueries(String inputFileName, int bootstrapSentenceId, List<List<SimpleToken>> mergedList, HashMap<String, List<String>> hashMap){
        List<List<SimpleToken>> possibleQueries = new ArrayList<>();
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);
        List<SimpleToken> possibleQuery = new ArrayList<>();

        for(SimpleToken simpleToken: queryTokenList){
            //if((!simpleToken.truthLabel.equals("LIST"))){
                SimpleToken st = new SimpleToken(simpleToken);
                possibleQuery.add(st);
            //}
        }
        List<String> contextWords = hashMap.get(inputFileName);
        //constructing the unweighted version
        Set<String> contextWordsSet = new HashSet<String>(contextWords);

        for(String str: contextWordsSet){
            for(SimpleToken simpleToken: mergedList.get(bootstrapSentenceId)){
                if(simpleToken.token.equals(str)){ // && (!simpleToken.truthLabel.equals("LIST"))){
                    possibleQuery.add(simpleToken);
                }
            }
        }
        possibleQueries.add(possibleQuery);
        /*int[] weightArray = new int[queryTokenList.size()];
        Scanner sc1 = new Scanner(System.in);
        int weightIndex = 0;
        for(SimpleToken t: queryTokenList){
            System.out.println(t.token);
            weightArray[weightIndex++] = sc1.nextInt();
        }
        List<SimpleToken> modifiedQuery = new ArrayList<>();*/
        //System.out.println("possible queries size " + possibleQueries.size());
        return possibleQueries;
    }

    //public static List<List<SimpleToken>> getEntitySimilarityExpandedQueries(String inputFileName, int bootstrapSentenceId, List<List<SimpleToken>> mergedList, HashMap<String, List<String>> hashMap){
    public static List<List<SimpleToken>> getEntitySimilarityExpandedQueries(int bootstrapSentenceId, List<List<SimpleToken>> mergedList, Set<List<String>> foundSpans, SentenceEmbedding sm){

        List<List<SimpleToken>> possibleQueries = new ArrayList<>();
        List<String> contextWordsList = sm.getContextWordsWithEntitySimilarity(mergedList.get(bootstrapSentenceId), foundSpans, 10);
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);
        List<SimpleToken> possibleQuery = new ArrayList<>();
        for(SimpleToken simpleToken: queryTokenList){
            //if((!simpleToken.truthLabel.equals("LIST"))){
                SimpleToken st = new SimpleToken(simpleToken);
                possibleQuery.add(st);
            //}
        }

        for(String str: contextWordsList){
            for(SimpleToken simpleToken: mergedList.get(bootstrapSentenceId)){
                if(simpleToken.token.equals(str)) // && (!simpleToken.truthLabel.equals("LIST"))){
                    possibleQuery.add(simpleToken);
                //}
            }
        }
        possibleQueries.add(possibleQuery);
        /*int[] weightArray = new int[queryTokenList.size()];
        Scanner sc1 = new Scanner(System.in);
        int weightIndex = 0;
        for(SimpleToken t: queryTokenList){
            System.out.println(t.token);
            weightArray[weightIndex++] = sc1.nextInt();
        }
        List<SimpleToken> modifiedQuery = new ArrayList<>();*/
        //System.out.println("possible queries size " + possibleQueries.size());
        return possibleQueries;
    }

    class WordCount{
        String word;

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        int count;

        WordCount(String word, int count){
            this.word = word;
            this.count = count;
        }
    }



    public List<List<SimpleToken>> getContextuallyExpandedMultipleQueries(String inputFileName, int bootstrapSentenceId, List<List<SimpleToken>> mergedList, HashMap<String, List<String>> hashMap, int k){
        List<List<SimpleToken>> possibleQueries = new ArrayList<>();
        List<SimpleToken> queryTokenList = mergedList.get(bootstrapSentenceId);
        List<String> contextWords = hashMap.get(inputFileName);
        List<String> topkWords = topkWords(contextWords, k);
        for(int i=0;i<topkWords.size();i++){
            List<SimpleToken> possibleQuery = new ArrayList<>();
            //System.out.println(str);
            //At first push the original tokens into the sentence
            for (SimpleToken simpleToken : queryTokenList) {
                //if((!simpleToken.truthLabel.equals("LIST"))) {
                SimpleToken st = new SimpleToken(simpleToken);
                possibleQuery.add(st);
                //}
            }

            //Finally the context words using the hashmap
            for(int j = 0;j<i;j++) {
                String str = topkWords.get(j);
                SimpleToken simpleTokenToPush = null;
                int numberOfPushes = Collections.frequency(contextWords, str);
                for (SimpleToken simpleToken : mergedList.get(bootstrapSentenceId)) {
                    if (simpleToken.token.equals(str)) { // && (!simpleToken.truthLabel.equals("LIST"))){
                        simpleTokenToPush = simpleToken;
                        break;
                    }
                }
                if(simpleTokenToPush==null) {
                    double scoreBest=0;
                    EditDistance ed = new EditDistance();
                    for (SimpleToken simpleToken : mergedList.get(bootstrapSentenceId)) {
                        double currentScore = ed.score(str, simpleToken.token);
                        if(currentScore>scoreBest){
                            scoreBest = currentScore;
                            simpleTokenToPush = simpleToken;
                        }
                    }
                }
                if(simpleTokenToPush==null)
                    continue;
                for (int m = 0; m < numberOfPushes; m++) {
                    possibleQuery.add(simpleTokenToPush);
                }
            }
            possibleQueries.add(possibleQuery);
        }

        /*for(List<SimpleToken> possi: possibleQueries){
            if(possi.isEmpty())

        }*/
        /*int[] weightArray = new int[queryTokenList.size()];
        Scanner sc1 = new Scanner(System.in);
        int weightIndex = 0;
        for(SimpleToken t: queryTokenList){
            System.out.println(t.token);
            weightArray[weightIndex++] = sc1.nextInt();
        }
        List<SimpleToken> modifiedQuery = new ArrayList<>();*/
        //System.out.println("possible queries size " + possibleQueries.size());
        return possibleQueries;
    }

    //This will return at least k words
    List<String> topkWords(List<String> wordList, int k){
        //new PrintResults().printTokenList(wordList);
        Set<String> unique = new HashSet<>(wordList);
        ArrayList<WordCount> l = new ArrayList<WordCount>();

        for (String key : unique) {
            l.add(new WordCount(key, Collections.frequency(wordList, key)));
        }
        Collections.sort(l, (o1,o2) -> Integer.compare(o2.getCount(), o1.getCount()));

        ArrayList<String> topkWords = new ArrayList<>();
//        ArrayList<String> listToPush = new ArrayList<>();

        for(WordCount wc: l){
            topkWords.add(wc.getWord());
        }
        //If we do not have enough words. We push some by copying from the last element.
//        if(topkWords.size()<k){
//            String elementToPush = topkWords.get(topkWords.size()-1);
//            for(int i=0; i<k-topkWords.size(); i++){
//                listToPush.add(elementToPush);
//            }
//            topkWords.addAll(listToPush);
//        }

        return topkWords;
    }

    public static int[][] getDefaultWeightArray(List<SimpleToken> queryTokenList){
        int [][] weightArray = new int[1][queryTokenList.size()];
        for(int i=0;i<queryTokenList.size();i++){
            weightArray[0][i] = 1;
        }
        return weightArray;
    }


}
