package edu.umass.cs.ciir.searchie.starter.sentence_analysis;

import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import org.lemurproject.galago.utility.Parameters;

import java.io.*;
import java.util.*;


/**
 * Created by sarwar on 6/5/17.
   THIS PROGRAM BRINGS TOP 1000 SENTENCES (StaticVariable.sizeOfInitialRetrieval) BASED ON WORD EMBEDDING AND CALCULATES THE RECALL and Mean Average Precision
   PRESENT IN ALLRFWUCOMBEXPERIMENT
 */



public class EmbeddingFromTextFile{
    static String prefix = StaticVariables.prefix;
    //This file is for loading word embedding from text files
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);

        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        List<List<SimpleToken>> pseudoPositives = new ArrayList<>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        /*if (fullPositives.size() <= StaticVariables.minPositivestoTrain)
            return;
        if (fullNegatives.size() >= StaticVariables.maxNumSentence)
            return;*/
        int size = fullPositives.size();
        //int totalSize = fullConllTrain.size();
        int totalSize = fullConllTrain.size();
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        List<List<SimpleToken>> positives = new ArrayList<>();

        mergedList.addAll(fullPositives);

        for(int i=0;i<totalSize-size;i++){
            mergedList.add(fullNegatives.get(i));
        }
        positives.add(mergedList.get(0));
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        //System.out.println("merged list size " + mergedList.size());
        double vectorLength[] = new double[totalSize];
        double points[][] = new double[totalSize][embeddingSize];
        //BufferedWriter br = new BufferedWriter(new FileWriter(new File(featureDirectory + "//embedding//test//" + inputFileName)));
        SentenceEmbedding sm = new SentenceEmbedding();
        //only load words that appear in the idf word vector. Then get embedding for sentences.
        sm.loadWordEmbedding(idf);
        int row = 0;
        for(List<SimpleToken> tokenList: mergedList){
            //System.out.println("Processed row " + row);
            List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList), idf);
            //writeEmbeddingToFile(doubleList, br);
            int col = 0;
            for(double d: doubleList){
                points[row][col++] = d;
                vectorLength[row]+= (d*d);
            }
            row++;
        }
        //br.close();
        //System.out.println("wrote sentence embedding");

        for(int i=0;i<vectorLength.length;i++){
            vectorLength[i] = Math.sqrt(vectorLength[i]);
        }
        //System.out.println("sentence embedding done ");
        PriorityQueue<SearchResult> pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        //pq = new PriorityQueue<>((o1, o2) -> o1.getValue().compareTo(o2.getValue()));
        int bootstrapSentenceId = 0;
        for(int i=0;i<totalSize;i++){
            double score = 0;
            for(int j=0; j<embeddingSize; j++){
                score += (points[bootstrapSentenceId][j] * points[i][j]);
            }
            score/=vectorLength[bootstrapSentenceId];
            score/=vectorLength[i];
            if(pq.size() < StaticVariables.sizeOfInitialRetrieval) {
                pq.add(new SearchResult(i, score));
            }else{
                SearchResult s = pq.peek();
                if(s.getScore() < score) {
                    pq.poll();
                    pq.add(new SearchResult(i, score));
                }
            }
            //System.out.println(i + "\t" + score);
        }

        List<SearchResult> results = new ArrayList<>(pq.size());
        results.addAll(pq);
        //Collections.sort(results, (o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        //initially we have only the bootstrapped sentence in positives. So we catch the spans from them
        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        Set<List<String>> foundSpans = new HashSet<>();
        //System.out.println(mergedList.size() + "\t" + fullConllTrain.size());
        double rankingPoint = 1, averagePrecision = 0, foundSoFar = 0;
        for(SearchResult s: results) {
            positives.add(mergedList.get(s.getSentenceId()));
            if(DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype)==1){
                foundSoFar++;
                averagePrecision+=(foundSoFar/rankingPoint);
            }
            rankingPoint++;
        }

        /*List<List<SimpleToken>> bootstrappedSentence = new ArrayList<>();
        bootstrappedSentence.add(mergedList.get(0));

        try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
            CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
            learner.setModel(argp.get("model", "lbfgs"));
            Parameters info = Parameters.create();
            TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(bootstrappedSentence, etype, info);
            LinearTokenClassifier rf;
            SearchResult searchResult;
            Map<String, TokenCount> retrievedTokenMap = new HashMap<>();
            ArrayList<SearchResult> resultsSeen = new ArrayList<>();
            int unsuccessful = 0;
            for (int i = 0; i < 30; i++) {
                rf = new LinearTokenClassifier(originalMap);
                //searchResult = Metrics.addRelevanceFeedback(rf, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                searchResult = Metrics.addRelevanceFeedback(rf, positives, bootstrappedSentence, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                int successful = 0;
                successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, resultsSeen.get(i).getSimpleTokens(), etype);
                if (successful != 1) {
                    unsuccessful++;
                }
                if (unsuccessful > 2) {
                    StaticVariables.isUnique = StaticVariables.isUnique ^ true;
                    unsuccessful = 0;
                }
                originalMap = learner.learnFeatureWeights(positives, etype, info);
            }
            PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
            printResults.printSimpleTokenList(bootstrappedSentence, "found sentences");
        }*/
        double MAP = (averagePrecision/availableSpans.size());
        System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + inputFileName + "\t" + MAP);
    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
                strList.add(token.lemma);
            }
        }
        return strList;
        //return s.trim();
    }


    public static void writeEmbeddingToFile(List<Double> l, BufferedWriter br){
        String str = "";
        for(Double d: l){
            str+= d;
            str+=" ";
        }
        try {
            br.write(str);
            br.write("\n");
        } catch (IOException e) {
            //System.out.println("error in printing embeddings");
            e.printStackTrace();
        }
    }


}
