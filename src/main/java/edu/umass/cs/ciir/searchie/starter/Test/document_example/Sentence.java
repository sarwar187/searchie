package edu.umass.cs.ciir.searchie.starter.Test.document_example;

import java.util.List;

public class Sentence {
    public int getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(int sentenceId) {
        this.sentenceId = sentenceId;
    }

    private int sentenceId;

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

    public List<Token> tokens;
}
