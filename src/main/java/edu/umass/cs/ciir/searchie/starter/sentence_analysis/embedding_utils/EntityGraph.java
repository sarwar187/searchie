package edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.Island;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;

import java.util.*;

/**
 * Created by sarwar on 5/26/17.
 */
public class EntityGraph {
    public static List<Island> createIslands(List<List<SimpleToken>> fullConllTrain, Set<List<String>> availableSpans, String etype, String inputFileName){
        List<Island> islands = new ArrayList<>();
        int occurrence[] = new int[100];
        Map<List<String>, Integer> entityMap = new HashMap<>();
        int index = 0;
        for(List<String> strList: availableSpans){
            entityMap.put(strList, index++);
        }
        int graph[][] = new int[availableSpans.size()][availableSpans.size()];
        //PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        //printResults.printSpans(availableSpans, "available spans");

        for(int i=0; i<fullConllTrain.size(); i++){
            Set<List<String>> str = DataSelector.annotateUser(availableSpans, fullConllTrain.get(i), etype);
            if(str.size()>1) {
                int a[] = new int[str.size()];
                index = 0;
                for(List<String> strList: str){
                    a[index++] = entityMap.get(strList);
                }
                for(int m=0; m<a.length; m++){
                    for(int n=m+1; n<a.length; n++){
                        graph[a[m]][a[n]] = 1;
                        graph[a[n]][a[m]] = 1;
                    }
                }
                //printResults.printSimpleToken(fullConllTrain.get(i), "sentence");
                //printResults.printSpans(str, "found spans");

            }
            for(int j=0; j<str.size(); j++)
                occurrence[j]++;
        }

        /*System.out.println("printing graph");
        for(int i=0;i<availableSpans.size();i++){
            for(int j=0;j<availableSpans.size();j++){
                System.out.print(graph[i][j]);
            }
            System.out.println();
        }*/
        //printResults.printTwoD();

        int nodes[] = new int[availableSpans.size()];
        int visited[] = new int[availableSpans.size()];
        Arrays.fill(visited, -1);
        for(int i=0;i<availableSpans.size();i++){
            nodes[i] = i;
        }
        int components = 0;
        try{
            for(int i=0;i<availableSpans.size();i++){
                if(visited[i] == -1) {
                    Island island = new Island(components);
                    components++;
                    dfs(i, nodes, visited, graph, i, island);
                    islands.add(island);
                    //System.out.println();
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        Set<Integer> s = new HashSet<>();
        for(int i=0;i<visited.length;i++){
            s.add(visited[i]);
        }


        int lengthCount[] = new int[visited.length];
        for(int i=0;i<visited.length;i++) {
            lengthCount[visited[i]]++;
        }
        int maximumSize = lengthCount[0];
        for(int i=0;i<lengthCount.length;i++) {
            if(lengthCount[i] > maximumSize){
                maximumSize = lengthCount[i];
            }
        }

        //System.out.print(s.size() + "\t" + availableSpans.size() + "\t" + maximumSize + "\t" + inputFileName + "\t" + islands.size() + "\t");
        System.out.print(islands.size() + "\t");


        for(int i=0; i<fullConllTrain.size(); i++) {
            Set<List<String>> strList = DataSelector.annotateUser(availableSpans, fullConllTrain.get(i), etype);
            if (strList.size() >= 1) {
                for(List<String> str: strList){
                    int id = entityMap.get(str);
                    for(Island island: islands) {
                        if(island.containsEntity(id)){
                            island.pushSentences(i);
                        }
                    }
                }
            }
        }
        /*PrintResults p = new PrintResults();
        for(Island island: islands){
            System.out.println("id " + island.getId());
            island.printIsland();
            System.out.println();
            for(Integer i: island.getSentences()){
                //System.out.println(fullConllTrain.get(i));
                p.printSimpleToken(fullConllTrain.get(i), "assigned to island " + island.getId());
            }
        }*/
        return islands;
    }

    static void dfs(int node, int[] nodes, int[] visited, int [][] graph, int color, Island island){
        visited[node] = color;
        island.pushEntities(node);
        //System.out.print(node + " ");
        for(int j=0; j < nodes.length; j++){
            if(graph[node][j] == 1){
                if(visited[j] == -1)
                    dfs(j, nodes, visited, graph, color, island);
            }
        }
    }

    public static int discoveredIslands(List<Integer> l, List<Island> islands){
        Set<Integer> discovered = new HashSet<>();
        for(int i: l){
            for(Island island: islands){
                if(island.containsSentence(i)) {
                    discovered.add(island.getId());
                    break;
                }
            }
        }
        return  discovered.size();
    }

//    public static Map<String, Integer> createEntityMap(List<List<SimpleToken>> fullConllTrain){
//        Map<String, Integer> entityMap = new HashMap<>();
//
//        for (List<SimpleToken> sent : fullConllTrain) {
//
//        }
//
//    }
}
