package edu.umass.cs.ciir.searchie.starter.Test.document_example;

import java.util.List;

public class DocumentContainer {
    public String doc;
    public String qid;
    public double score;
    public List<Sentence> sentences;
}
