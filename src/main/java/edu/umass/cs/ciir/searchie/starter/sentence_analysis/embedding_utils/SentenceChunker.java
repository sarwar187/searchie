package edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;

import java.util.ArrayList;
import java.util.List;

public class SentenceChunker {

    public static double[][] getChunkedRepresentation(List<SimpleToken> tokenList, int windowSize, SentenceEmbedding sm, int embeddingSize){
        //PrintResults pr = new PrintResults();
        //System.out.println("printing chunks");

        List<SimpleToken> filteredTokenList = new ArrayList<>();
        //At first clearing the tokenlist by removing stopwords
        for(SimpleToken token: tokenList) {
            if(!Stopwords.isStopword(token.lemma)){
                filteredTokenList.add(token);
                //System.out.println(token.lemma);
            }
        }
        double points[][] = null;
        int numberOfWindows = 1;
        if(filteredTokenList.size()-windowSize+1 <= 0) {
            ArrayList<String> temp = new ArrayList<>();
            points = new double[numberOfWindows][embeddingSize];
            for(int i=0; i< filteredTokenList.size(); i++){
                temp.add(filteredTokenList.get(i).lemma);
            }
            List<Double> chunkEmbedding = sm.getSentenceEmbedding(temp);
            int k = 0;
            for (Double d : chunkEmbedding) {
                points[numberOfWindows-1][k] = d.doubleValue();
                k++;
            }
        }else {
            points = new double[filteredTokenList.size() - windowSize + 1][embeddingSize];
            numberOfWindows = filteredTokenList.size() - windowSize + 1;
            for (int i = 0; i < filteredTokenList.size() - windowSize + 1; i++) {
                ArrayList<String> temp = new ArrayList<>();
                for (int j = i; j < i + windowSize; j++) {
                    temp.add(filteredTokenList.get(j).lemma);
                }
                List<Double> chunkEmbedding = sm.getSentenceEmbedding(temp);
                int k = 0;
                for (Double d : chunkEmbedding) {
                    points[i][k] = d.doubleValue();
                    k++;
                }
                //pr.printTokenList(temp);
            }
        }
        for(int i=0; i <numberOfWindows; i++){
            double length = EmbeddingSearch.getVectorLength(points, i, embeddingSize);
            for(int j=0; j<embeddingSize; j++){
                points[i][j]/=length;
            }
        }

        return points;
    }
}
