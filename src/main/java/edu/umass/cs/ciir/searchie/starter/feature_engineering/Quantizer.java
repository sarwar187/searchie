package edu.umass.cs.ciir.searchie.starter.feature_engineering;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Created by sarwar on 12/10/16.
 */
public class Quantizer {
    public Quantizer(){

    }

    public void equalBucket(ArrayList<QFeatureWeight> optimalWeightList, int maxLabel){
        if(optimalWeightList.size() < maxLabel){
            int tempLabel = maxLabel;
            for(int i = optimalWeightList.size()-1; i >= 0; i--){
                optimalWeightList.get(i).quantized = tempLabel;
                tempLabel--;
            }
            return;
        }

        int limit = (int) (optimalWeightList.size()/(double) maxLabel);
        int label = 1;
        for(int i=limit;i<optimalWeightList.size();i+=limit){
            for(int j=i-limit;j<i && j<optimalWeightList.size(); j++){
                optimalWeightList.get(j).setQuantized(label);
            }
            if(label<maxLabel)  //label_local is becoming 6, thats why applied this trick
                label++;
        }

        for(int i=0;i<optimalWeightList.size();i++) {
            if (optimalWeightList.get(i).getQuantized() == -1)
                optimalWeightList.get(i).setQuantized(maxLabel);
        }

    }

    public void decraseFidelity(ArrayList<QFeatureWeight> optimalWeightList){
        for(int i=0;i<optimalWeightList.size();i++) {
            Double toBeTruncated = new Double(optimalWeightList.get(i).getWeight());
            Double truncatedDouble = BigDecimal.valueOf(toBeTruncated).setScale(1, RoundingMode.HALF_UP).doubleValue()*10;
            optimalWeightList.get(i).quantized = truncatedDouble.intValue();
        }
    }

    public int getLabel(ArrayList<QFeatureWeight> l, String feature){
        int label=0;
        for(QFeatureWeight f: l){
            if(f.getFeature().equals(feature)) {
                label = f.quantized;
                //System.out.println("printed from getLabel function feature weight " + f.getWeight() + "\t" + label);
                break;
            }
        }
        return label;
    }


}
