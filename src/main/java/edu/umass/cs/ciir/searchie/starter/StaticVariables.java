package edu.umass.cs.ciir.searchie.starter;

import java.util.ArrayList;

/**
 * Created by sarwar on 10/11/16.
 */
public class StaticVariables {
    ///home/sarwar/crfsuite-0.12/bin/crfsuite
    //public static String prefix = "//home//sarwar//crfsuite//";
    public static String prefix = "//mnt//scratch//smsarwar//crfsuite//";

    public static String aquaint_prefix = "//home//sarwar//dumps//";
    public static String outputDirectory = "//home//sarwar//dumps//results//";
    public static String dump_prefix = prefix + "//final//";
    public static String loc_dump_prefix = "//home//sarwar//dumps//";
    public static int positive = 1;
    public static int negative = -1;
    public static int zero = 0;
    public static int maxLabel = 10;
    public static int factor = 1;
    //MUST CHECK
    public static int errorActivated = 0; //If we want error model activated this should be set to 1
    public static int labelError = 2; // error window
    public static boolean isUnique = true;//false; //If we are showing a unique token each time
    public static int top = 1; //look at RFExperiment. indicates how many sentences we are going to take as feedback
    public static String entityType = "LIST";
    public static String label[] = {"uP", "P", "uR", "R", "uF1", "F1", "uAP", "AP", "uTP", "TP", "uFP", "FP", "uTN", "TN", "uFN", "FN"};
    public static String fblabel[] = {"P"};
    public static int minPositivestoTrain = 5;  //We need a minimum number of positives to split train and test from crfsuite files. if there are 2 positives I can not even build the initial model.
    public static String maxExperiments = "1"; //The default number of maximum run. More than 20 would take a lot of time
    public static int rankedListSize = 20; //the number of elements in the ranked list of what user have seen so far
    //MUST CHECK
    public static int numRelevanceFeedbacks = 30; //Number of relevance feedbacks on weights and tokens
    public static int maxNumSentence = 20000;  //Disregard the queries with negatives greater than maxNumSentence
    public static int numberOfPRFSentences = 5;
    public static int meanOriginalWeight = 0;
    public static int sizeOfInitialRetrieval = 1000;
    //Query Expansion Begins
    public static String expansionApproachLucene = "LUCENE";
    public static String expansionApproachEmbedding = "EMBEDDING";
    public static String expansionApproachFull = "FULL";
    public static String expansionApproachFullContextToken = "FULLTOKEN";
    public static String expansionParameterNone = "NONE";
    public static String expansionApproachNone = "NONE";
    public static String expansionApproachAll = "ALL";
    public static String expansionApproachIsland = "ISLAND";
    public static String expansionParametertopk = "TOPK";
    public static String expansionParameterPRF = "PRF";
    public static String luceneScore = "LUCENE_SCORE";
    public static String entityTokenScore = "ETOKEN_SCORE";
    public static String entityContextScore = "ECONTEXT_SCORE";
    public static String embeddingscore = "EMBEDDING_SCORE";
    public static String crfScore = "CRF_SCORE";
    public static int batchID = 0;
    public static int expansionRM3 = 1;
    public static int topk =  0;
    public static double threshold = 0.0;
    public static double sentenceExpansionWeight = 0.5;
    public static double queryEntityExpansionWeight = 0.3;
    public static double otherEntityExpansionWeight = 0.2;
    public static int topkEntities = 1;
    public static int printFlag = 1;
    public static int embedding_size = 200;
    public static int windowSize = 10;
    public static int weightingScheme = 0;
    public static int RMweighted = 1; //if weight of expansion sentences are equal it should be zero
}


//ASK the following questions
//Am I doing feature selection? YES
//Am I doing Weight Quantization? Depends on the Configuration
//Am I activating error model? Depends on the Configuration