package edu.umass.cs.ciir.searchie.starter.feature_engineering;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import java.util.ArrayList;
import java.util.List;

public class SearchResult{
    int sentenceId;

    public String getSentenceIdString() {
        return sentenceIdString;
    }

    public void setSentenceIdString(String sentenceIdString) {
        this.sentenceIdString = sentenceIdString;
    }

    String sentenceIdString;
    public List<SimpleToken> getSimpleTokens() {
        return simpleTokens;
    }

    List<SimpleToken> simpleTokens;
    protected Double score;
    protected Double originalScore;
    protected Double originalLuceneScore;

    public Double getCRFScore() {
        return CRFScore;
    }

    public void setCRFScore(Double CRFScore) {
        this.CRFScore = CRFScore;
    }

    protected Double CRFScore;

    public Double getOriginalScore() {
        return originalScore;
    }

    public void setOriginalScore(Double originalScore) {
        this.originalScore = originalScore;
    }

    public Double getOriginalLuceneScore() {
        return originalLuceneScore;
    }

    public void setOriginalLuceneScore(Double originalLuceneScore) {
        this.originalLuceneScore = originalLuceneScore;
    }

    public Double getOriginalScoreEntity() {
        return originalScoreEntity;
    }

    public void setOriginalScoreEntity(Double originalScoreEntity) {
        this.originalScoreEntity = originalScoreEntity;
    }

    public Double getOriginalScoreEntityContext() {
        return originalScoreEntityContext;
    }

    public void setOriginalScoreEntityContext(Double originalScoreEntityContext) {
        this.originalScoreEntityContext = originalScoreEntityContext;
    }

    protected Double originalScoreEntity;
    protected Double originalScoreEntityContext;

    public Double getLuceneScore() {
        return luceneScore;
    }

    public void setLuceneScore(Double luceneScore) {
        this.luceneScore = luceneScore;
    }

    protected Double luceneScore;
    public Double getScoreEntity() {
        return scoreEntity;
    }

    public void setScoreEntity(Double scoreEntity) {
        this.scoreEntity = scoreEntity;
    }

    protected Double scoreEntity;

    public Double getScoreEntityContext() {
        return scoreEntityContext;
    }

    public void setScoreEntityContext(Double scoreEntityContext) {
        this.scoreEntityContext = scoreEntityContext;
    }

    protected Double scoreEntityContext;
    boolean isTrue;

    public String getMaxScoredToken() {
        return maxScoredToken;
    }

    public void setMaxScoredToken(String maxScoredToken) {
        this.maxScoredToken = maxScoredToken;
    }

    String maxScoredToken;

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }

    public SearchResult(int sentenceId, Double score) {
        this.sentenceId = sentenceId;
        this.score = score;
    }

    public SearchResult(String sentenceIdString, Double score) {
        this.sentenceIdString = sentenceIdString;
        this.score = score;
    }

    public SearchResult(int sentenceId, Double score, List<SimpleToken> simpleTokens) {
        this.sentenceId = sentenceId;
        this.score = score;
        this.simpleTokens = new ArrayList<>();
        for(SimpleToken token: simpleTokens){
            this.simpleTokens.add(token);
        }
    }

    public List<String> getTypeSpan() {
        return typeSpan;
    }

    public void setTypeSpan(List<String> typeSpan) {
        this.typeSpan = typeSpan;
    }

    List<String> typeSpan = new ArrayList<>();

    public SearchResult(List<String> typeSpan, Double score) {
        this.typeSpan = typeSpan;
        //this.sentenceId = sentenceId;
        this.score = score;
    }

    public int getSentenceId() {
        return sentenceId;
    }

    public Double getScore() {
        return score;
    }

    public SearchResult setScore(Double score) {
        this.score = score;
        return this;
    }

}