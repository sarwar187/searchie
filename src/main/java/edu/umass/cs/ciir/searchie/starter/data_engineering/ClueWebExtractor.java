package edu.umass.cs.ciir.searchie.starter.data_engineering;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class ClueWebExtractor {
    public static void main(String args[]) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        String annotation_file = argp.get("anns", "/home/smsarwar/PycharmProjects/clueweb_annotation/anns500.tsv");
        String warc_file = argp.get("warc", "/home/smsarwar/PycharmProjects/clueweb_annotation/tmp.warc");
        Scanner sc = new Scanner(new File(annotation_file));
        Set<List<String>> entitySet = new HashSet<>();
        while(sc.hasNextLine()){
            String lineToParse = sc.nextLine();
            String[] parsedValues = lineToParse.split("\t");
            String CWFileName = parsedValues[0];
            String annotatedEntity = parsedValues[2];
            String[] annotatedEntitySplitted = annotatedEntity.split(" ");
            List<String> annotatedEntitySplittedList = new ArrayList<>();
            for(String s: annotatedEntitySplitted){
                annotatedEntitySplittedList.add(s);
            }
            entitySet.add(annotatedEntitySplittedList);
        }

        PrintResults pr = new PrintResults();
        pr.printSpans(entitySet, "printing entiites");

        sc.close();
        //Done with pooling entities

        sc = new Scanner(new File(warc_file));
        while(sc.hasNextLine()){
            String warcLine = sc.nextLine();
            String[] warcLineSpaceTokenized = warcLine.split(" ");
            List<SimpleToken> simpleTokenList = new ArrayList<>();
            for(String s: warcLineSpaceTokenized){
                simpleTokenList.add(new SimpleToken("True", s,s));
            }
            for(List<String> entityTokenList: entitySet) {
                if(DataSelector.containsFoundEntity(simpleTokenList, entityTokenList)==1){
                    System.out.println(warcLine);
                    System.out.println("found entity");
                    pr.printTokenList(entityTokenList);
                    //break;
                }
            }

        }
    }
}

