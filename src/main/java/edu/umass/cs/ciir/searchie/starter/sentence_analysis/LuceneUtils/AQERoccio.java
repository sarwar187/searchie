package edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.store.Directory;
import java.util.AbstractMap.SimpleEntry;

/**
 * Created by sarwar on 6/6/17.
 */
public class AQERoccio {

    private final float alpha;
    private final float beta;
    private final float gama;
    private final int docsLimit;
    private final int termsLimit;
    private Analyzer analyzer;
    private final String field;

    public AQERoccio(Analyzer analyzer, final String field,
                           float alpha, float beta, float gama,
                           int docsLimit, int termsLimit) {
        this.analyzer = analyzer;
        this.field = field;
        this.alpha = alpha;
        this.beta = beta;
        this.gama = gama;
        this.docsLimit = docsLimit;
        this.termsLimit = termsLimit;
    }


    /*public Query expand(final Query original, final List<Document> relevantDocs)
            throws ParseException, CorruptIndexException,
            LockObtainFailedException, IOException {
        String field = "content";
        Directory index = createIndex(relevantDocs);
        // newQVector = alpha * oldQVector + beta * Sum{i=1..docs}( DocsVector )
        List<Entry<String, Float>> newQVector = getTermScoreList(index);
        for (String term : Arrays.asList(original.getQuery().toString(field).split("\\s+"))) {
            float score = alpha * Utils.getScore(index, term);
            boolean found = false;
            for (Entry<String, Float> entry : newQVector) {
                if (entry.getKey().equalsIgnoreCase(term)) {
                    entry.setValue(entry.getValue() + score);
                    found = true;
                    break;
                }
            }
            if (!found) {
                newQVector.add(new SimpleEntry<String, Float>(term, score));
            }
        }
        Collections.sort(newQVector, new ScoreComparator<String>());
        Collections.reverse(newQVector);
        StringBuilder rocchioTerms = new StringBuilder();
        for (int limit = 0; limit < termsLimit && limit < newQVector.size(); limit++) {
            rocchioTerms.append(' ').append(newQVector.get(limit).getKey());
        }
        Query rocchioQuery = new Query(original.getQid(), Utils.normalizeQuery(rocchioTerms.toString(), field, analyzer));
        return rocchioQuery;
    }*/
}
