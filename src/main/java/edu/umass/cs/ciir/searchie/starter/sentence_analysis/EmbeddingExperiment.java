package edu.umass.cs.ciir.searchie.starter.sentence_analysis;

import ca.pjer.ekmeans.EKmeans;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding.sentenceEmbedding;

/**
 * Created by sarwar on 5/18/17.
 */
public class EmbeddingExperiment {
    static String prefix = StaticVariables.prefix;

    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 400;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);


        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");

        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);

        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        IDF idf = new IDF();
        List<List<SimpleToken>> pseudoPositives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        Random random = new Random(System.currentTimeMillis());
        int size = fullPositives.size();
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        mergedList.addAll(fullPositives);
        /*mergedList.add(fullPositives.get(0));
        mergedList.add(fullPositives.get(1));
        mergedList.add(fullPositives.get(3));
        mergedList.add(fullPositives.get(4));
        mergedList.add(fullPositives.get(5));
        */
        for(int i=0;i<size;i++){
            mergedList.add(fullNegatives.get(i));
        }
        System.out.println("merged list size " + mergedList.size());

        double points[][] = new double[size*2][embeddingSize];
        int row = 0;
        for(List<SimpleToken> tokenList: mergedList){
            ArrayList<String> strList = simpleTokenToString(tokenList, idf);
            List<Double> doubleList  = sentenceEmbedding(strList.get(0), strList.get(1));
            int col = 0;
            for(double d: doubleList){
                points[row][col] = d;
                col++;
            }
            row++;
        }
        System.out.println("sentence embedding done ");

        int[] centroidIndex = new Random().ints(0, (size)-1).distinct().limit(2).toArray();
        int k = 2;
        double centroids[][] = new double[k][embeddingSize];
        centroidIndex[0] = random.nextInt(size);
        centroidIndex[1] = random.nextInt(size) + size;

        centroids[0] = Arrays.copyOf(points[centroidIndex[0]], embeddingSize);
        centroids[1] = Arrays.copyOf(points[centroidIndex[1]], embeddingSize);

//        int n = 1000; // the number of data to cluster
//        int k = 10; // the number of cluster
//        double[][] points = new double[n][2];
//        // lets create random points between 0 and 100
//        for (int i = 0; i < n; i++) {
//            points[i][0] = Math.abs(random.nextInt() % 100);
//            points[i][1] = Math.abs(random.nextInt() % 100);
//        }
//        double[][] centroids = new double[k][2];
//        // lets create random centroids between 0 and 100 (in the same space as our points)
//        for (int i = 0; i < k; i++) {
//            centroids[i][0] = Math.abs(random.nextInt() % 100);
//            centroids[i][1] = Math.abs(random.nextInt() % 100);
//        }
        EKmeans eKmeans = new EKmeans(centroids, points);
        eKmeans.setIteration(100);
        //eKmeans.setDistanceFunction(EKmeans.MANHATTAN_DISTANCE_FUNCTION);
        eKmeans.setDistanceFunction(EKmeans.EUCLIDEAN_DISTANCE_FUNCTION);
        //eKmeans.setListener(this);
        eKmeans.run();
        int[] assignments = eKmeans.getAssignments();
        // here we just print the assignement to the console.
        int labelZero = 0;
        int labelOne = 0;
        for (int i = 0; i < size/2; i++) {
            //System.out.println(MessageFormat.format("point {0} is assigned to cluster {1}", i, assignments[i]));
            if(assignments[i]==0)
                labelZero++;
            else
                labelOne++;
        }
        System.out.println("fullpositives zero:" + labelZero + " one:" + labelOne);
        labelZero = 0;
        labelOne = 0;
        for (int i = size/2; i < size; i++) {
            //System.out.println(MessageFormat.format("point {0} is assigned to cluster {1}", i, assignments[i]));
            if(assignments[i]==0)
                labelZero++;
            else
                labelOne++;

        }
        System.out.println("fullnegatives zero:" + labelZero + " one:" + labelOne);
    }

    public static String simpleTokenToString(List<SimpleToken> tokenList){
        String s = "";
        for(SimpleToken token: tokenList){
            if(token.truthLabel.equals("LIST")){
                System.out.println(token.lemma);
                continue;
            }
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            if(token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if(token.lemma.length()>2) {
                s += token.lemma;
                s += " ";
            }
        }
        return s.trim();
    }

    public static ArrayList<String> simpleTokenToString(List<SimpleToken> tokenList, IDF idf){
        ArrayList<String> strList = new ArrayList<>();
        String tokenString = "";
        String idfString = "";
        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            if(token.truthLabel.equals("LIST")){
                System.out.println(token.lemma);
                continue;
            }
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
                tokenString += token.lemma;
                tokenString += " ";
                idfString += idf.getIDF(token.lemma);
                idfString += " ";
            }

        }
        strList.add(tokenString);
        strList.add(idfString);
        return strList;
        //return s.trim();
    }


}