package edu.umass.cs.ciir.searchie.starter.sigir2018;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.DocumentContainer;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.QueryFileParser;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Sentence;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Token;
import edu.umass.cs.ciir.searchie.starter.chiir2018.BatchHandler;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.Evaluation;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

public class ETS {
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args) throws IOException {
        StaticVariables.threshold = 0.1;
        //System.out.println("working");
        //StaticVariables.expansionRM3 = 0;
        double[] expandedSentencesWeight;
        int[] expandedSentences;
        int embeddingSize = 200;
        int[] occurrence = new int[100];
        PrintResults pr = new PrintResults();
        LuceneInMemory lucene = new LuceneInMemory();

        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String inputDirectory = argp.get("dir", "/home/smsarwar/");
        String inputFileName = argp.get("file", "trecqalist_94.4_1000.jsonl.gz");
        String hostName = InetAddress.getLocalHost().getHostName();
        //System.out.println(inputFileName);
        HashMap<String, Integer> bootstrapHashMap = BatchHandler.loadBootstrapSentence();
        int bootstrapSentenceId = bootstrapHashMap.get(inputFileName);
        //int bootstrapSentenceId = 0;

        HashMap<String, List<String>> hashMap = BatchHandler.loadBatch(0);

        //System.out.println(hashMap.size());
        //if(!hashMap.containsKey(inputFileName)) {
        //System.out.println("working");
        //    return;
        //}

        double previousScore = 0;
        Map<Integer, String> senteceToDocumentMapping = new HashMap<>();

        //Data loading
        List<List<SimpleToken>> fullConllTrain = new ArrayList<>();
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        IDF idf = new IDF();
        int row = 0;


        GZIPInputStream gzip = new GZIPInputStream(new FileInputStream(inputDirectory + inputFileName));
        BufferedReader br = new BufferedReader(new InputStreamReader(gzip));

        while(true){
            //while (sc.hasNextLine()) {
            //String jsonString = sc.nextLine();
            String jsonString = br.readLine();
            if(jsonString == null)
                break;
            //Gson gson = new Gson();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            //JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/Desktop/volume.json"), JsonElement.class);
            JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
            DocumentContainer documentContainer = gson.fromJson(jsonElement.toString(), DocumentContainer.class);
            //System.out.println(documentContainer.doc + "\t" + documentContainer.qid + "\t" + documentContainer.score);
            if (documentContainer.score == previousScore)
                continue;
            previousScore = documentContainer.score;
            int i = 0;
            for (Sentence sentence : documentContainer.sentences) {
                String output = "";
                List<SimpleToken> sList = new ArrayList<>();
                for (Token token : sentence.tokens) {
                    sList.add(SimpleToken.convertTokenToSimpleToken(token));
                    //if(!token.getLemma().equals(token.getToken()))
                    //System.out.println(token.getLemma() + "\t" + token.getToken());
                }
                fullConllTrain.add(sList);
                senteceToDocumentMapping.put(row++, documentContainer.doc);
            }
            //for (Task task : fromJson) {
            //    System.out.println(task);
            //}
        }
        //Data loading ends
        //System.out.println("size " + fullConllTrain.size());
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }

        ArrayList<List<SimpleToken>> mergedList = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> positives = new ArrayList<List<SimpleToken>>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);

        String output="";
        positives.add(mergedList.get(bootstrapSentenceId));
        SentenceEmbedding sm = new SentenceEmbedding();
        if(hostName.equals("brooloo"))
            sm.setEnvironment(hostName);
        sm.loadWordEmbedding(idf);
        FeatureExtractor f = new FeatureExtractor((List)mergedList.get(bootstrapSentenceId));
        String targetEntityType = f.getTargetEntityType();
        Evaluation eval = new Evaluation();
        //String expansionApproach = StaticVariables.expansionApproachFull;
        //String expansionParameter = StaticVariables.expansionParameterPRF;
        //String expansionApproach = StaticVariables.expansionApproachNone;
        //String expansionParameter = StaticVariables.expansionParameterNone;
        String expansionApproach = StaticVariables.expansionApproachEmbedding;
        String expansionParameter = StaticVariables.expansionParametertopk;
        //String expansionParameter = StaticVariables.expansionParameterPRF;

        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        //pr.printSpans(availableSpans, "available spans main function");
        int topk = StaticVariables.topk;
        //Preparing for RM3
        //double alpha=1, beta=0, gamma=0;
        //0.000	0.273	0.273	0.000	0.200	0.100	0.100	0.068	0.818	9.000	11.000
        //double alpha=0, beta=1, gamma=0;
        //0.091	0.273	0.364	0.200	0.300	0.150	0.150	0.195	0.909	10.000	11.000
        //double alpha=0, beta=0, gamma=1;
        //0.182	0.182	0.455	0.200	0.200	0.250	0.250	0.196	0.909	10.000	11.000
        //uniform, entity, and context
        //pr.printSimpleToken(mergedList.get(0), "query");
        //double[] queryModel = BatchHandler.getContextualQueryModel(inputFileName, bootstrapSentenceId, mergedList, hashMap, alpha, beta, gamma);
        EmbeddingSearch embeddingSearch = new EmbeddingSearch(mergedList, fullPositives, embeddingSize, etype, inputFileName, lucene, sm);

        //double alpha=0.0, beta=1.0, gamma=0.0;
        //for()
        double configuration [][] = {{0, 0, 1}};

        for(int i = 0; i<configuration.length; i++) {
            Set<List<String>> foundSpans = new HashSet<>();
            //pr.printSimpleTokenSimply(mergedList.get(bootstrapSentenceId), "query");
            DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(bootstrapSentenceId), etype);
            //System.out.println(foundSpans.size());
            //pr.printSimpleTokenSimply(mergedList.get(bootstrapSentenceId), "query");
            double alpha = configuration[i][0];
            double beta = configuration[i][1];
            double gamma = configuration[i][2];
            //System.out.println(alpha + "\t" + beta + "\t" + gamma);
            //HashMap<String, Double> queryModelMap = BatchHandler.getContextualQueryModelMap(inputFileName, bootstrapSentenceId, mergedList, hashMap, alpha, beta, gamma);
            //pr.printQueryModel(queryModelMap);
            //System.out.println(Arrays.stream(queryModel).sum());
            //embeddingSearch.setQueryModel(queryModel);
            //embeddingSearch.setQueryModelMap(queryModelMap);
            //This function is not doing any query expansion now
            List<SearchResult> expandedSentencesQuery = embeddingSearch.queryExpansion(bootstrapSentenceId, expansionApproach, expansionParameter, availableSpans, targetEntityType, topk);

            //Query Expansion Ends
            //Seaching with New Query Representation
            //System.out.println(sm.entityEmbeddingContextExpansion.size());
            //List<SearchResult> results = embeddingSearch.search(bootstrapSentenceId);

            //List<List<SimpleToken>> possibleQueries = BatchHandler.getPossibleQueries(bootstrapSentenceId, mergedList);
            //List<List<SimpleToken>> possibleQueries = BatchHandler.getContextuallyExpandedQueries(inputFileName, bootstrapSentenceId, mergedList, hashMap);
            int topk_list[] = {5};
            //automated process
            //List<List<SimpleToken>> possibleQueries = BatchHandler.getEntitySimilarityExpandedQueries(bootstrapSentenceId, mergedList, foundSpans, sm);
            //List<List<SimpleToken>> possibleQueries = BatchHandler.getContextuallyExpandedQueries(inputFileName, bootstrapSentenceId, mergedList, hashMap);
            //alpha for entity query model, beta for context query model, gamma for uniform query model
            List<SearchResult> results = embeddingSearch.search(bootstrapSentenceId);
            embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, "none");
            embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, "none");
            //results.sort((o1, o2) -> o2.getLuceneScore().compareTo(o1.getLuceneScore()));
            //results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
            //results.sort((o1, o2) -> o2.getScoreEntityContext().compareTo(o1.getScoreEntityContext()));
            //results.sort((o1, o2) -> o2.getScoreEntityContext().compareTo(o1.getScoreEntityContext()));
            results.sort((o1, o2) -> o2.getScoreEntity().compareTo(o1.getScoreEntity()));

            eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);

            if (eval.getNumberOfTargetEntities() != 1) {
                output += eval.getEvaluationScoresString() + "\t"; // + eval.getFoundSpansString();
                output += "\t" + inputFileName + "\t" + targetEntityType + "\t";
                output += QueryFileParser.fileToQueryString(inputFileName, "/home/smsarwar/trecqalist.json.gz");
                System.out.println(output);
            }
            embeddingSearch.reloadQueryRepresentation(bootstrapSentenceId);
        }


    }
}

