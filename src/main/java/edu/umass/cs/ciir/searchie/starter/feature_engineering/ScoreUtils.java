package edu.umass.cs.ciir.searchie.starter.feature_engineering;

import ciir.jfoley.chai.collections.Pair;
import edu.umass.cs.ciir.searchie.starter.LinearTokenClassifier;
import edu.umass.cs.ciir.searchie.starter.ScoresForUniqueLemma;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import gnu.trove.map.hash.TObjectFloatHashMap;

import java.util.List;
import java.util.Map;

/**
 * Created by sarwar on 4/10/17.
 */
public class ScoreUtils {
    /*public interface Score {


        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, double percent, Oracle oracle);
        public WeightInfo getWeightInfo();
    }*/

    public static SearchResult score(List<SimpleToken> tokens, LinearTokenClassifier model, List<Pair<Boolean, Double>> rankedPred, Map<String, ScoresForUniqueLemma> bestScoreByLemma, String etype, int i){
        String maxScoredToken = "";
        boolean truthSentence = false;
        double maxScore = Double.NEGATIVE_INFINITY;
        //In this way we find highest scoring sentence and it is the one with the HIGHEST SCORING TOKEN
        double nonZero=0;
        for (SimpleToken token : tokens) {
            boolean truth = token.truthLabel.equals(etype);
            double score = model.score(token.getFeatures());
            rankedPred.add(Pair.of(truth, score));
            if (score > maxScore) {
                maxScore = score;
                truthSentence = truth;
                maxScoredToken = token.lemma;
            }
            if(score>0)
                nonZero++;
            bestScoreByLemma.computeIfAbsent(token.lemma, ScoresForUniqueLemma::new).score(score, truth);
        }
        SearchResult searchResult = new SearchResult(i, maxScore, tokens);
        searchResult.setTrue(truthSentence);
        searchResult.setMaxScoredToken(maxScoredToken);
        return searchResult;
    }

}
