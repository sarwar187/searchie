package edu.umass.cs.ciir.searchie.starter.feature_engineering;

public class WeightInfo {
    public int numberOfWeightsChanged;

    public WeightInfo() {
        numberOfWeightsChanged = 0;
    }

    public int getNumberOfWeightsChanged() {
        return numberOfWeightsChanged;
    }

    public void setNumberOfWeightsChanged(int numberOfWeightsChanged) {
        this.numberOfWeightsChanged = numberOfWeightsChanged;
    }
}
