package edu.umass.cs.ciir.searchie.starter.Test.document_example;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.utils.DataStructure;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.lemurproject.galago.utility.Parameters;

import javax.xml.crypto.Data;
import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class ShowSentences {
    public static void main(String args[]) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String inputDirectory = argp.get("dir", "/home/smsarwar/");
        String inputFileName = argp.get("file", "trecqalist_114.4_1000.jsonl");
        Scanner sc = new Scanner(new File (inputDirectory + inputFileName));
        double previousScore = 0;
        Map<Integer, String> senteceToDocumentMapping = new HashMap<>();

        //Data loading
        List<List<SimpleToken>> fullConllTrain = new ArrayList<>();
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        int row = 0;

        while (sc.hasNextLine()) {
            String jsonString = sc.nextLine();
            Gson gson = new Gson();
            //JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/Desktop/volume.json"), JsonElement.class);
            JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
            DocumentContainer documentContainer = gson.fromJson(jsonElement.toString(), DocumentContainer.class);
            //System.out.println(documentContainer.doc + "\t" + documentContainer.qid + "\t" + documentContainer.score);
            if (documentContainer.score == previousScore)
                continue;
            previousScore = documentContainer.score;
            int i = 0;
            for (Sentence sentence : documentContainer.sentences) {
                String output = "'";
                List<SimpleToken> sList = new ArrayList<>();
                for (Token token : sentence.tokens) {
                    sList.add(SimpleToken.convertTokenToSimpleToken(token));
                    if(!token.getLemma().equals(token.getToken()))
                        System.out.println(token.getLemma() + "\t" + token.getToken());
                }
                fullConllTrain.add(sList);
                senteceToDocumentMapping.put(row++, documentContainer.doc);
            }
            //for (Task task : fromJson) {
            //    System.out.println(task);
            //}
        }
        //Data loading ends
        PrintResults pr = new PrintResults();
        System.out.println("mapping size "  + senteceToDocumentMapping.size());
        System.out.println("fullconlll train size " + fullConllTrain.size());
        for(int k=0;k<20;k++) {
            pr.printSimpleTokenSimply(fullConllTrain.get(k), "none");
            System.out.println();
        }
        //print top-5 sentences


        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }

        int bootstrapSentenceId = 1;
        ArrayList<List<SimpleToken>> mergedList = new ArrayList<>();
        ArrayList<List<SimpleToken>> positives = new ArrayList<>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);
        positives.add(mergedList.get(bootstrapSentenceId));
        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        HashMap<String, List<Integer>> entitySentenceMapping = new HashMap<>();
        row=0;
        for(List<SimpleToken> sent:mergedList){
            Set<List<String>> tempSpans = DataSelector.annotateUser(availableSpans, sent, etype);
            List<String> stringList = DataStructure.spansToStringList(tempSpans);
            for(String str: stringList) {//{
                entitySentenceMapping.computeIfAbsent(str, k -> new ArrayList<>()).add(row);

            }
            row++;
            //Collections.sortby(Person::name)
            //pr.printSimpleToken(sent, "none");
        }
        List<String> foundSpansString = DataStructure.spansToStringList(foundSpans);

        int flag = 0;
        String targetEntity = "";
        for(String key: entitySentenceMapping.keySet()){
            if((!foundSpansString.contains(key)) && entitySentenceMapping.get(key).size() > 5) {
                flag = 1;
                targetEntity = key;
                break;
            }
        }

        System.out.println("--------Entity Retrieval----------------------");
        Gson gson = new Gson();
        GZIPInputStream gzip = new GZIPInputStream(new FileInputStream("/home/smsarwar/trecqalist.json.gz"));
        BufferedReader br = new BufferedReader(new InputStreamReader(gzip));
        //br.readLine();
        //JsonElement jsonElement = gson.fromJson(new FileReader("/home/smsarwar/trecqalist.json"), JsonElement.class);
        JsonElement jsonElement = gson.fromJson(br, JsonElement.class);
        //System.out.println(jsonElement.toString());
        QueryContainer queryContainer = gson.fromJson(jsonElement.toString(), QueryContainer.class);
        for(Query q: queryContainer.queries){
            if(inputFileName.contains(q.qid)) {
                System.out.print("Original List Query: ");
                System.out.println(q.getText());
            }
        }

        pr.printSimpleTokenSimply(positives.get(0), "Query as a Sentence:");
        System.out.println("-----------------------------------------------------");
        int index_temp=0;
        if(fullConllTrain.contains(positives.get(0))){
            index_temp = fullConllTrain.indexOf(positives.get(0));
        }
        String queryDocumentID = senteceToDocumentMapping.get(index_temp);
        //index_temp = 1000;
        for(int i = index_temp-8; i<index_temp+8 ; i++){
            if((i>0 && i<fullConllTrain.size())){
                if(senteceToDocumentMapping.get(i).equals(queryDocumentID))
                    pr.printSimpleTokenSimply(fullConllTrain.get(i), "none");
            }
        }

        String instructions = "Now assume that you are interested all the movies of Jesse Ventura (Example: \"Predator\" in this passage) + "
                + "Which keywords from the given paragraph best describes your information need? Double click on those words " +
                "to put them into the text box (type none in the text box if no word makes any sense considering your information need. Assume that " +
                "these words will be sent to a keyword-based search engine that will be used to retrieve the entities you are looking for. So, help it with keywords!! " +
                "Finally, click the submit button to submit your queries";

        pr.printSpans(foundSpans, "Query Entity:");
        String separator = "\t";
        int id = 0;
        if(flag==1) {
            System.out.println("Target Entity: " + targetEntity);
            for (Integer index: entitySentenceMapping.get(targetEntity)) {

                if(fullPositives.contains(mergedList.get(index))){
                    //pr.printSimpleTokenSimply(mergedList.get(index), "sentence id " + index);
                    System.out.print(inputFileName+id + separator + inputFileName + separator);
                    pr.printSimpleTokenSimply(mergedList.get(index), "none");
                    System.out.print(separator + "0" + separator +instructions);
                    System.out.println();
                    //System.out.println("found in full positives");
                    id++;
                }
            }
        }
        else{
            System.out.println("no target entity found");
        }
        //System.out.println("full positives before " + fullPositives.size());
        //System.out.println("full negatives before " + fullNegatives.size());

        /*fullPositives.clear();
        fullNegatives.clear();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }*/

        //System.out.println("full positives after " + fullPositives.size());
        //System.out.println("full negatives after " + fullNegatives.size());

        /*String output="";
        positives.add(mergedList.get(bootstrapSentenceId));
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        FeatureExtractor f = new FeatureExtractor((List)mergedList.get(bootstrapSentenceId));
        String targetEntityType = f.getTargetEntityType();
        //System.out.println(targetEntityType);
        //FeatureExtractor queryFeatureExtractor = new FeatureExtractor((List)mergedList.get(0));
        //String targetEntityType = queryFeatureExtractor.getTargetEntityType();
        //Set<List<String>> foundSpansCaseSensitive = DataSelector.getUniqueSpansCaseSensitive(positives, etype);
        //List<Double> targetEntityEmbedding = EmbeddingSearch.getEntityEmbedding(foundSpansCaseSensitive, sm);
        Evaluation eval = new Evaluation();
        String expansionApproach = StaticVariables.expansionApproachAll;
        String expansionParameter = StaticVariables.expansionParameterPRF;
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        int topk = 0;
        EmbeddingSearch embeddingSearch = new EmbeddingSearch(mergedList, fullPositives, embeddingSize, etype, inputFileName, lucene, sm);
        embeddingSearch.queryExpansion(bootstrapSentenceId,expansionApproach,expansionParameter,availableSpans, targetEntityType, topk);
        //Query Expansion Ends
        //Seaching with New Query Representation
        //System.out.println(sm.entityEmbeddingContextExpansion.size());
        List<SearchResult> results = embeddingSearch.search(bootstrapSentenceId);
        //eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
        //if(eval.getNullEntities() == 1) return;
        //output+=eval.getEvaluationScoresString();
        //output+="\t";
        embeddingSearch.reRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, StaticVariables.embeddingscore);
        //embeddingSearch.modReRankingFilterSearchResults(bootstrapSentenceId, results, mergedList, targetEntityType, availableSpans, StaticVariables.entityContextScore);
        eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType, bootstrapSentenceId);
        //output+=eval.getEvaluationScoresString();
        output+=eval.getStrRecall();
        System.out.println(output);*/
    }

}