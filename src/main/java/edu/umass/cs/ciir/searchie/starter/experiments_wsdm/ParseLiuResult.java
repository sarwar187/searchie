package edu.umass.cs.ciir.searchie.starter.experiments_wsdm;

import ciir.jfoley.chai.collections.Pair;

import java.io.*;
import java.util.*;

/**
 * Created by sarwar on 8/6/17.
 */
public class ParseLiuResult {
    public static void main(String args[]) throws IOException {
        Map<String, List<Pair<Integer,Double>>> hashMap = new HashMap<>();
        String method = "QL";
        //String method = "DL";
        //String method = "WC";
        //String method = "WCIDF";
        Scanner sc = null;
        if(method.equals("QL"))
            sc = new Scanner(new File("/home/sarwar/gdrive/UMass Amherst Reserach/Work with James/searchIE/wsdm 2018 submission/forSheikh_WSDM18_Liu/QL_WordCount_score_qrel_files/test.tsv.tokenized.QL.score"));
        if(method.equals("DL"))
            sc = new Scanner(new File("/home/sarwar/gdrive/UMass Amherst Reserach/Work with James/searchIE/wsdm 2018 submission/forSheikh_WSDM18_Liu/CNN_score_qrel_files/Pred-TEST-Step-21800.score"));
        if(method.equals("WC"))
            sc = new Scanner(new File("/home/sarwar/gdrive/UMass Amherst Reserach/Work with James/searchIE/wsdm 2018 submission/forSheikh_WSDM18_Liu/QL_WordCount_score_qrel_files/test.tsv.tokenized.WordCount.score"));
        if(method.equals("WCIDF"))
            sc = new Scanner(new File("/home/sarwar/gdrive/UMass Amherst Reserach/Work with James/searchIE/wsdm 2018 submission/forSheikh_WSDM18_Liu/QL_WordCount_score_qrel_files/test.tsv.tokenized.WCIDFWeighted.score"));

        double[] fold1 = new double[] {132.4,112.6,112.5,201.7,212.7,102.6,171.6,140.4,187.4,104.4,97.6,163.5,97.5,74.6,189.1,192.3,106.6,180.5,76.7,215.7,121.3,190.6,142.6,142.7};
        List<Double> listFold1 = new ArrayList<>();
        for(int i=0;i<fold1.length;i++)
            listFold1.add(new Double(fold1[i]));
        double[] fold2 = new double[] {69.7,210.7,92.3,92.4,131.7,207.7,116.6,144.6,144.7,114.4,114.3,113.5,138.3,181.8,90.1,181.6,164.2,186.7,162.6,79.3,96.3,185.5,94.4,214.7};
        List<Double> listFold2 = new ArrayList<>();
        for(int i=0;i<fold2.length;i++)
            listFold2.add(new Double(fold2[i]));
        double[] fold3 = new double[] {89.3,136.7,115.7,161.3,80.6,179.7,179.5,174.6,213.4,175.6,82.4,182.3,82.3,78.7,139.3,184.7,108.4,108.5,197.4,170.6,125.1,72.6,170.5,99.1};
        List<Double> listFold3 = new ArrayList<>();
        for(int i=0;i<fold3.length;i++)
            listFold3.add(new Double(fold3[i]));
        double[] fold4 = new double[] {101.7,153.4,150.7,156.4,84.7,149.7,110.5,110.6,206.7,134.3,134.2,154.6,147.7,205.6,68.6,68.7,141.7,141.6,183.4,81.2,105.6,158.6,75.7,75.5};
        List<Double> listFold4 = new ArrayList<>();
        for(int i=0;i<fold4.length;i++)
            listFold4.add(new Double(fold4[i]));
        double[] fold5 = new double[] {124.6,98.5,77.6,77.7,70.7,93.7,151.2,151.3,204.5,127.6,143.7,143.6,157.6,211.7,91.3,85.6,85.1,209.7,119.4,87.4,172.7,111.4,145.6,202.4};
        List<Double> listFold5 = new ArrayList<>();
        for(int i=0;i<fold5.length;i++)
            listFold5.add(new Double(fold5[i]));

        //System.out.println(listFold1.size());

        while(sc.hasNext()){
            String str = sc.nextLine();
            //System.out.println(str);
            String[] input = str.split("\t");
            //System.out.println(input.length);
//            StringTokenizer st = new StringTokenizer(str);
//            String[] input = new String[6];
//            int i=0;
//            while(st.hasMoreTokens()){
//                String temp = st.nextToken();
//                input[i++] = temp;
//                System.out.println(temp);
//            }

            String fileName = extractFileName(input[0]);
            Double score = Double.parseDouble(input[4]);
            Integer docNo = getDocNo(input[2]);
            if(!hashMap.containsKey(fileName)) {
                List<Pair<Integer, Double>> rankedPred = new ArrayList<>();
                rankedPred.add(Pair.of(docNo, score));
                hashMap.put(fileName, rankedPred);
            }else{
                hashMap.get(fileName).add(Pair.of(docNo, score));
            }
        }
        for(String s: hashMap.keySet()){
            List<Pair<Integer,Double>> l = hashMap.get(s);
            l.sort(((o1, o2) -> o2.right.compareTo(o1.right)));
        }
        for(String s: hashMap.keySet()){
            String fold = fromFileNameToFold(s, listFold1, listFold2, listFold3, listFold4, listFold5);
            BufferedWriter br=null;
            if(method.equals("DL"))
                br = new BufferedWriter(new FileWriter(new File("/home/sarwar/crfsuite/sydney/deep_learning/fold" + fold + "/test/" + s)));
            if(method.equals("QL"))
                br = new BufferedWriter(new FileWriter(new File("/home/sarwar/crfsuite/sydney/QL/fold" + fold + "/test/" + s)));
            if(method.equals("WC"))
                br = new BufferedWriter(new FileWriter(new File("/home/sarwar/crfsuite/sydney/WC/fold" + fold + "/test/" + s)));
            if(method.equals("WCIDF"))
                br = new BufferedWriter(new FileWriter(new File("/home/sarwar/crfsuite/sydney/WCIDF/fold" + fold + "/test/" + s)));

            List<Pair<Integer,Double>> l = hashMap.get(s);
            for(Pair p: l){
                br.write(String.valueOf(p.left));
                br.newLine();
            }
            br.close();
        }
    }

    public static String extractFileName(String str){
        //StringTokenizer st = new StringTokenizer(str, "_");
        String[] parts = str.split("_");
        String output = parts[0]+ "_" + parts[1] + "_1000.test.result";
        return  output;
    }
    public static String fromFileNameToFold(String str, List<Double> listFold1, List<Double> listFold2, List<Double> listFold3, List<Double> listFold4, List<Double> listFold5){
        String[] parts = str.split("_");
        double fold = Double.parseDouble(parts[1]);
        if(listFold1.contains(fold))
            return "1";
        if(listFold2.contains(fold))
            return "2";
        if(listFold3.contains(fold))
            return "3";
        if(listFold4.contains(fold))
            return "4";
        if(listFold5.contains(fold))
            return "5";
        return "1";
    }
    public static Integer getDocNo(String str){
        //StringTokenizer st = new StringTokenizer(str, "_");
        //System.out.println(str);
        String[] parts = str.split("_");
        return  Integer.parseInt(parts[3]);
    }
}
