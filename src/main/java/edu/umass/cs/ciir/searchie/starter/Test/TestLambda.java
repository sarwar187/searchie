package edu.umass.cs.ciir.searchie.starter.Test;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sarwar on 5/12/17.
 */
@FunctionalInterface
interface Converter<F, T> {
    T convert(F from);
}


public class TestLambda {
//    public static void main(String args[]) {
//        Converter<String, Integer> converter = (from) -> Integer.valueOf(from);
//        Integer converted = converter.convert("123");
//        System.out.println(converted);    // 123
//    }
    public static void main(String args[]) {
//        String check = "null 1999-05-22 12:41:32 usa Ventura Eager for Soap Appearance ST. PAUL , Minn. -LRB- AP -RRB- -- Jesse Ventura confesses he has a soft spot for soap operas , so he 's tickled at the prospect of appearing on `` The Young and The Restless . ''";
//        System.out.println(check.matches("null.*--.*"));
//        String output = check.replaceAll("null.*-- ", "");
//        System.out.println(output);
//        //System.out.println(check.matches("null.*--.*"));
//                List<Double> list = new ArrayList<>();
//                list.add(2d);
//                list.add(3d);
//                list.sort((a,b) -> Double.compare(b,a));
//                for(Double d: list){
//                    System.out.println(d);
//                }
//            }

        ArrayList <Pair <String,Integer> > l =
                new ArrayList <Pair <String,Integer> > ();

        /*  Create pair of name of student  with their
            corresponding score and insert into the
            Arraylist */
        l.add(new Pair <String,Integer> ("Student A", 90));
        l.add(new Pair <String,Integer> ("Student B", 54));
        l.add(new Pair <String,Integer> ("Student C", 99));
        l.add(new Pair<String,Integer>("Student D", 88));
        l.add(new Pair <String,Integer> ("Student E", 89));

        Collections.sort(l, (o1,o2) -> o1.getValue().compareTo(o2.getValue()));
        for(Pair<String, Integer> item: l) {
            System.out.println(item.getKey());
        }
        return;
    }
}
