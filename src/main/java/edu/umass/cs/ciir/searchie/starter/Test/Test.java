package edu.umass.cs.ciir.searchie.starter.Test;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sarwar on 2/6/17.
 */

class Employee {
    private String name;
    private Integer age;
    private Double salary;
    public Employee(String name, Integer age, Double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    public String toString(){
        DecimalFormat dformat = new DecimalFormat(".##");
        return "Employee Name:"+this.name
                +"  Age:"+this.age
                +"  Salary:"+dformat.format(this.salary);
    }
//getters and setters for name, age and salary go here
//standard equals() and hashcode() code go here
}
public class Test {

//    public static void main(String[] args) {
////        Map<Integer, List<Employee>> employeeDOJMap = new HashMap<>();
////        System.out.println("\nJava 7 way of adding a new key(2017) in a multi-value map\n");
////        List empList2017 = employeeDOJMap.get(2017);
////        if (empList2017 == null) {
////            empList2017 = new ArrayList<>();
////        }
////        empList2017.add(new Employee("Tom Newman", 45, 12000.00));
////        employeeDOJMap.put(2017, empList2017);
////        employeeDOJMap.forEach((year, empList) -> System.out.println(year + "-->" + empList));
////        System.out.println("\nUsing Map.computeIfAbsent() to add a new key(2018) in a multi-value map\n");
////        employeeDOJMap.computeIfAbsent(2018, empList -> new ArrayList<>())
////                .add(new Employee("Dick Newman", 35, 10000.00));
////        employeeDOJMap.computeIfAbsent(2018, empList -> new ArrayList<>())
////                .add(new Employee("Dick Newman", 36, 10000.00));
////
////        employeeDOJMap.forEach((year, empList) -> System.out.println(year + "-->" + empList));
////        ArrayList<Integer> a = new ArrayList<>();
////        ArrayList<Integer> b = new ArrayList<>();
////        a.add(1);
////        b.add(2);
////        a.addAll(b);
////        System.out.println(a.get(0) + "\t" + a.get(1));
////        b.clear();
////
////        System.out.println(a.get(0) + "\t" + a.get(1));
////        System.out.println(b.get(0) + "\t" + a.get(1));
//
//        Pattern p = Pattern.compile("(c\\[\\d*\\]=)(\\d*)");
//        Matcher m = p.matcher("c[0]=121");
//
//        if (m.find() == true) {
//            System.out.println(m.group(1));
//        }
//
//        PriorityQueue<Integer> pq = new PriorityQueue<>((o1, o2) -> o1 - o2);
//
//        int a[] = new int[3];
//        a[0] = 1;
//        a[1] = 5;
//        a[2] = 3;
//        for (int i = 0; i < a.length; i++) {
//            if (pq.size() < 1) {
//                pq.add(a[i]);
//            } else {
//                int temp = pq.peek();
////                if (temp etScore() < searchResult.getScore()) {
////                    pq.poll();
////                    pq.add(searchResult);
////                }
//            }
//        }
//    }
    public static void main(String args[]){

//        NumberFormat formatter = new DecimalFormat("#0000");
//        System.out.println(formatter.format(0));
//
//        ArrayList<SearchResult> a = new ArrayList<>();
//
//        a.add(new SearchResult(1, 1d));
//        a.add(new SearchResult(2, Math.sqrt(-1)));
//        a.add(new SearchResult(3, 3d));
//        a.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
//
//        for(SearchResult s: a){
//            System.out.println(s.getScore() + 0);
//            formatter = new DecimalFormat("#0000");
//            System.out.println(formatter.format(s.getScore()));
//        }
        int count =0;
        for(double d1=0.0; d1<=1.0; d1+=0.1) {
            for (double d2 = 0.0; d2 <= 1.0; d2 += 0.1) {
                for (double d3 = 0.0; d3 <= 1.0; d3 += 0.1) {
                    if (d1 + d2 + d3 == 1) {
                        count++;
                    }
                }
            }
        }
        System.out.println(count);
//        String a = "Hello";
//        String b = "World";
//        String c = "hello";
//        String d = "World";
//        ArrayList<String> strList = new ArrayList<>();
//        strList.add(a);
//        strList.add(b);
//        System.out.println(strList.get(0) == a);
//        System.out.println(strList.get(1) == a);
//        System.out.println(strList.get(0) == b);
//        System.out.println(strList.get(1) == b);
//        //A list only contains references to an object
//        Set<List<String>> source = new HashSet<>();
//        List<String> l = new ArrayList<>();
//        l.add(a);
//        l.add(b);
//        source.add(l);
//
//        List<String> n = new ArrayList<>();
//        n.add(c);
//        n.add(d);
//        if(source.contains(n))
//            System.out.println("true");



    }
}