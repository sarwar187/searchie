package edu.umass.cs.ciir.searchie.starter.feature_engineering;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import gnu.trove.map.hash.TObjectFloatHashMap;

import java.util.*;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.sym.error;

/**
 * Created by sarwar on 1/31/17.
 */
public class State {
    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }


    public HashSet<String> getRelevantFeatures() {
        return relevantFeatures;
    }

    public void setRelevantFeatures(HashSet<String> relevantFeatures) {
        this.relevantFeatures = relevantFeatures;
    }

    public State() {
        trainingSamples = new ArrayList<>();
        //this is to track on how many weights we have got feedbacks
        usedFeatures = new HashSet<>();
        relevantFeatures = new HashSet<>();
        numberofWeightErrors = 0;
        isOptimal = 0;
        returnPrevious = false;
        alpha = 1f;
    }
    //this is to specify how many weights need to contain error
    public int getNumberofWeightErrors() {
        return numberofWeightErrors;
    }

    public void setNumberofWeightErrors(int numberofWeightErrors) {
        this.numberofWeightErrors = numberofWeightErrors;
    }

    public List<List<SimpleToken>> getTrainingSamples() {
        return trainingSamples;
    }

    public void setTrainingSamples(List<List<SimpleToken>> trainingSamples) {
        this.trainingSamples = trainingSamples;
    }

    public HashSet<String> getUsedFeatures() {
        return usedFeatures;
    }

    public void setIsOptimal(int val){
        this.isOptimal = val;
    }

    public int getIsOptimal(){
        return this.isOptimal;
    }

    public int getSizeOfUsedFeatures(){
        return usedFeatures.size();
    }
    /*public void setWeightFeedbacks(TObjectFloatHashMap<String> weightFeedbacks) {
        this.weightFeedbacks = weightFeedbacks;
    }*/

    public void setReturnPrevious(boolean returnPrevious){
        this.returnPrevious = returnPrevious;
    }

    public boolean getReturnPrevious(){
        return this.returnPrevious;
    }

    public void setAvailableTokens(Set<String> availableTokens){
        this.availableTokens = availableTokens;
    }
    public void updateUsedFeatures(String str){
        usedFeatures.add(str);
    }
    public void updateRelevantFeatures(String str){
        relevantFeatures.add(str);
    }

    public List<List<SimpleToken>> trainingSamples;
    public HashSet<String> usedFeatures;
    public HashSet<String> relevantFeatures;

    public Set<String> getExploredTokens() {
        return exploredTokens;
    }

    public void setExploredTokens(Set<String> exploredTokens) {
        this.exploredTokens = exploredTokens;
    }

    public Set<String> exploredTokens;
    int numberofWeightErrors;
    int isOptimal;
    boolean returnPrevious;
    float alpha;
    Set<String> availableTokens;

    public State(List<List<SimpleToken>> initialSamples) {
        trainingSamples = new ArrayList<>();
        usedFeatures = new HashSet<>();
        for (List<SimpleToken> s : initialSamples)
            trainingSamples.add(s);
    }

    public boolean queryUsedFeatures(String key){
        if(usedFeatures.contains(key))
            return true;
        else
            return false;
    }

    public boolean queryRelevantFeatures(String key){
        if(relevantFeatures.contains(key))
            return true;
        else
            return false;
    }

    /*void updateSample(List<List<SimpleToken>> newSamples) {
        for (List<SimpleToken> s : newSamples)
            trainingSamples.add(s);
    }*/

    /*void updateWeights(HashMap<String, Double> newWeights) {
        for (String key : newWeights.keySet()) {
            if (!weightFeedbacks.containsKey(key))
                weightFeedbacks.put(key, newWeights.get(key));
        }
    }*/



    /*void pushWeights(String key, Double value){
        if(weightFeedbacks.containsKey(key)){

        }
    }

    int queryWeights(String feature) {
        if (weightFeedbacks.containsKey(feature))
            return 1;
        else
            return 0;
    }

    //List<List<SimpleToken>> sample(List<List<SimpleToken>> fullPositives, int numTraining){
    public void sample(List<List<SimpleToken>> fullPositives, int numTraining) {
        List<List<SimpleToken>> sample = new ArrayList<>();
        Random r = new Random();
        int index;
        for (int i = 0; i < numTraining; i++) {
            index = r.nextInt(fullPositives.size());
            sample.add(fullPositives.get(index));
            fullPositives.remove(index);
        }
        //this.trainingSamples = sample;
        this.updateSample(sample);
    }*/
}

