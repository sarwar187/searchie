package edu.umass.cs.ciir.searchie.starter.learning_to_rank;

import ciir.jfoley.chai.collections.Pair;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.lemurproject.galago.utility.Parameters;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 6/26/17.
 */
public class WekaTest {
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws Exception {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        List<List<SimpleToken>> pseudoPositives = new ArrayList<>();

        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        //lucene.close();
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        List<List<SimpleToken>> positives = new ArrayList<>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);
        positives.add(mergedList.get(0));
        System.out.println("input file" + "\t" +inputFileName);
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        //ConverterUtils.DataSource sourceTest = new ConverterUtils.DataSource("/home/sarwar/crfsuite/ltr/test/" + inputFileName +".arff");
        ConverterUtils.DataSource sourceTest = new ConverterUtils.DataSource("/mnt/nfs/work1/smsarwar/searchie/ltr/test/" + inputFileName +".arff");
        //DataSource sourceTest = new DataSource("D://own_training//test_featureFile.arff");
        //System.out.println("working");
        Instances initial = sourceTest.getDataSet();
        //System.out.println(initial.size());
        //ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/home/sarwar/crfsuite/ltr/train.model"));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/mnt/nfs/work1/smsarwar/searchie/ltr/train.model"));

        //System.out.println("hello");
        String[] options = new String[2];
        options[0] = "-R";                                    // "range"
        options[1] = "1,2";                                     // first attribute
        //options[2] = "2";                                     // first attribute
        //options[3] = "4";
        //options[2] = "9";                                     // first attribute
        //options[3] = "3";                                     // first attribute
        //options[4] = "4";                                     // first attribute

        Remove remove = new Remove();                         // new instance of filter
        remove.setOptions(options);                           // set options
        remove.setInputFormat(initial);                          // inform filter about dataset **AFTER** setting options
        Instances test = Filter.useFilter(initial, remove);   // apply filter
        Logistic fc = (Logistic) ois.readObject();
        //Logistic fc = (Logistic) ois.readObject();

        //fc.setFilter(rm);
        //fc.setClassifier(cls);
        ois.close();
        test.setClassIndex(test.numAttributes() - 1);

        ArrayList<Pair<Integer, Double>> sortedList = new ArrayList<>();

        for (int i = 0; i < test.numInstances(); i++) {
            int sentenceID = (int)initial.instance(i).value(1);
            double pred = fc.classifyInstance(test.instance(i));
            // Get the true class label from the instance's own classIndex.
            String trueClassLabel =
                    test.instance(i).toString(test.classIndex());

            // Make the prediction here.
            double predictionIndex = fc.classifyInstance(test.instance(i));

            // Get the predicted class label from the predictionIndex.
            String predictedClassLabel = test.classAttribute().value((int) predictionIndex);

            // Get the prediction probability distribution.
            double[] predictionDistribution = fc.distributionForInstance(test.instance(i));

            // Print out the true label, predicted label, and the distribution.
            //System.out.printf("%5d: true=%-10s, predicted=%-10s, distribution=",
            //        i, trueClassLabel, predictedClassLabel);

            //sortedList.add(Pair.of(sentenceID, predictionDistribution[0]));
            sortedList.add(Pair.of(Integer.parseInt(trueClassLabel), predictionDistribution[0]));

            // Loop over all the prediction labels in the distribution.
            /*for (int predictionDistributionIndex = 0;
                 predictionDistributionIndex < predictionDistribution.length;
                 predictionDistributionIndex++)
            {
                // Get this distribution index's class label.
                String predictionDistributionIndexAsClassLabel =
                        test.classAttribute().value(
                                predictionDistributionIndex);

                // Get the probability.
                double predictionProbability =
                        predictionDistribution[predictionDistributionIndex];

                System.out.printf("[%10s : %6.3f]",
                        predictionDistributionIndexAsClassLabel,
                        predictionProbability );
            }*/

            //o.printf("\n");

        }
        Collections.sort(sortedList, (o1, o2) -> Double.compare(o2.right.doubleValue(), o1.right.doubleValue()));
        PrintResults printResults = new PrintResults();
        int count=0;
        for(Pair p: sortedList){
            if(count==10)
                break;
            System.out.println(p.getKey() + "\t" + p.getValue());
            //printResults.printSimpleToken(mergedList.get((int) p.getKey()), "sentence");
            count++;
        }
    }


}
