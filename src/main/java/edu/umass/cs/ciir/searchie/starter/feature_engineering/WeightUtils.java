package edu.umass.cs.ciir.searchie.starter.feature_engineering;

import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import gnu.trove.map.hash.TObjectFloatHashMap;

import java.util.*;

/**
 * Created by sarwar on 12/10/16.
 */

public class WeightUtils {
    //static int [] errorPct = {20,10,5,2};
    //static int index = 0;
    public interface FixWeight {
        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, double percent, Oracle oracle);
        public WeightInfo getWeightInfo();
    }

    public interface FixWeightInteractive {
        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, int numberOfWeights, Oracle oracle, State state);
    }

    public static final class FSBinInteractive implements FixWeightInteractive {
        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, int numberOfWeights, Oracle oracle, State state){
            int maxLabel = StaticVariables.maxLabel;
            FeatureSelector featureSelector = new FeatureSelector(originalMap, optimalMap);
            TObjectFloatHashMap<String> modifiedWeights = new TObjectFloatHashMap<>();
            for (String key : originalMap.keySet()) {
                modifiedWeights.put(key, originalMap.get(key));
            }
            if(numberOfWeights==0)
                return modifiedWeights;
            ArrayList<FeatureWeight> a = featureSelector.filterByPattern();  //WEIGHTS ARE SORTED
            HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresTopk(StaticVariables.zero, numberOfWeights, a);
            Translator translator = new Translator(FeatureSelector.mapToList(originalMap));
            //erro will contain the number of weight error that we are allowing.
            int error = state.getNumberofWeightErrors();
            int countError = 0;
            for (String key : optimalMap.keySet()) {
                if (selectedFeaturesMap.containsKey(key)) {
                    int label_local = (int) oracle.getLabel(key);
                    if(StaticVariables.errorActivated == 1 && countError <= error) {
                        label_local = oracle.getErrorLabel(key);
                        countError++;
                    }
                    float translatedWeight = translator.translateLabel(label_local, maxLabel, StaticVariables.factor);
                    float weight = selectedFeaturesMap.get(key).floatValue();
                    float alpha = 0.5f;
                    weight = (alpha * translatedWeight) + ((1-alpha) * weight);
                    if(state.getIsOptimal()==0) {
                        modifiedWeights.put(key, weight);
                    }
                    else {
                        modifiedWeights.put(key, optimalMap.get(key));
                    }
                }
            }
            return modifiedWeights;
        }
    }

    public static final class FSBPreviousWeightRemoved implements FixWeightInteractive {
        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, int weightFlag, Oracle oracle, State state){
            int maxLabel = StaticVariables.maxLabel;
            TObjectFloatHashMap<String> modifiedWeightsMap = new TObjectFloatHashMap<>();
            //put the unused features in modifiedweightsMap
            for (String key : originalMap.keySet()) {
                if(!state.queryUsedFeatures(key))
                    modifiedWeightsMap.put(key, originalMap.get(key));
            }
            if(weightFlag==0)
                return modifiedWeightsMap;
            //at first we remove all the weights from modified weights that has been used
            //pick new weights from original Map by selecting specific types of features and then top-k of them
            //no business of modified map here
            FeatureSelector featureSelector = new FeatureSelector(modifiedWeightsMap, optimalMap);
            featureSelector.setState(state);
            ArrayList<FeatureWeight> a = featureSelector.filterByPattern();  //WEIGHTS ARE SORTED
            //System.out.println("a size " + a.size());
            //HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresTopk(StaticVariables.zero, numberOfWeights, a);
            //System.out.println("Feature Editing Begins");
            HashMap<String, Double> selectedFeaturesMap;
            if (a.size()>= 1 && state.getReturnPrevious()==false) { //If I want the previous weights, I do not need to change selectedFeaturesMap, BY default false
                //selectedFeaturesMap = featureSelector.selectTopTwoFeature(a);
                //selectedFeaturesMap = featureSelector.selectTopFeature(a);
                //selectedFeaturesMap = featureSelector.selectUncertainFeature(a);
                //selectedFeaturesMap = featureSelector.selectCommonFeature(a);
                //selectedFeaturesMap = featureSelector.selectClusterFeatures(a);
                //selectedFeaturesMap = featureSelector.selectContextWordFeatures(a);
                //selectedFeaturesMap = featureSelector.constructContextFeatures(state); //I am not using a to create context features. Those features come from context word of positive tokens
                selectedFeaturesMap = featureSelector.constructContextFeaturesWithoutTokens(state); //I am not using a to create context features. Those features come from context word of positive tokens
                //featureSelector.addnerfeatures(selectedFeaturesMap);
                //selectedFeaturesMap = featureSelector.selectAllFeatures(a);
            } else {
                selectedFeaturesMap = new HashMap<>();
            }


            Iterator it = state.getUsedFeatures().iterator();
            while (it.hasNext()) {
                selectedFeaturesMap.put((String) it.next(), 0d);
            }

            //BulkFeatures Module
            /*HashMap<String, Integer> bulkFeatures = new HashMap<>();
            for(String key: selectedFeaturesMap.keySet()){
                for(String modKey: modifiedWeightsMap.keySet()){
                    if(modKey.contains(key)){
                        bulkFeatures.put(modKey, 0);
                    }
                }
            }*/
            //BulkFeatures Module ends
            //System.out.println("Feature Editing Begins");

            //System.out.println(selectedFeaturesMap.size());
            //If optimal map is perfectly loaded then feature selection is not in action
            //System.out.println("weights being changed " + (numberOfWeights - state.getSizeOfUsedFeatures()));
            Translator translator = new Translator(FeatureSelector.mapToList(originalMap));
            //error will contain the number of weight error that we are allowing.
            int error = state.getNumberofWeightErrors();
            if(error==-1) error = selectedFeaturesMap.size();
            int countError = 0;
            int countFeatures = 0;

            //System.out.println("entered");

            for (String key : selectedFeaturesMap.keySet()) {
                //if(foundInAvailable(state.availableTokens, key)==1)
                //    continue;
                //if(key.contains("ner["))
                //    System.out.println(key);
                countFeatures++;
                int label_local = (int) oracle.getLabel(key); //oracle label is constant.
                //System.out.println("feature " + key + " label_local " + label_local) ;
                //if error is activated and the error variable is negative 1 the proceed with label error
                if (StaticVariables.errorActivated == 1 && error == -1) {
                    label_local = oracle.getErrorLabel(key);
                    countError++;
                }
                //BULK FEATURES CODE
                /*for (String modKey : modifiedWeightsMap.keySet()) {
                    if (modKey.contains(key)) {
                        bulkFeatures.put(modKey, label_local);
                    }
                }*/
                //BULK FEATURES CODE ENDS
                //label_local means the label from user
                float translatedWeight = translator.translateLabel(label_local, maxLabel, StaticVariables.factor);
                float weight = 0;
                float alpha = state.getAlpha(); //
                if (originalMap.containsKey(key)){
                    weight = originalMap.get(key); //selectedFeaturesMap.get(key).floatValue();
                    //if(label_local==2)
                    weight = (alpha * translatedWeight) + ((1 - alpha) * weight);
                }else{
                    //weight = optimalMap.get(key);
                    weight = translatedWeight;
                }
                //label_local will return -1 if the feature is not found in the optimal map
                //analogous to the case where the user doesn't know about the feature
                //IS IT A RATIONAL ASSUMPTION?

                if (state.getIsOptimal() == 0 && label_local>=0) {
                    //if(key.contains("ner["))
                    //    System.out.println(key + "=" + weight);

                    modifiedWeightsMap.put(key, weight);
                    if(label_local==10)
                        state.updateRelevantFeatures(key);
                    state.updateUsedFeatures(key);

                    //System.out.println("key\t" + key + "\tpredicted\t" + weight + "\toptimal\t" + optimalMap.get(key));
                } else {
                    //modifiedWeightsMap.put(key, optimalMap.get(key));
                    if(optimalMap.containsKey(key)) {
                        modifiedWeightsMap.put(key, optimalMap.get(key));
                        state.updateUsedFeatures(key);
                    }
                }
            }
            //System.out.println("exited");

            //System.out.println(countFeatures + "\tFEATURES CORRECTED");


            //System.out.println(bulkFeatures.size() + "\tBULK FEATURES CORRECTED");
            //Translate Bulk Features
            /*for(String key: bulkFeatures.keySet()){
                int label_local = bulkFeatures.get(key);
                float translatedWeight = translator.translateLabel(label_local, maxLabel, StaticVariables.factor);
                float weight = originalMap.get(key); //selectedFeaturesMap.get(key).floatValue();
                float alpha = state.getAlpha(); //
                weight = (alpha * translatedWeight) + ((1 - alpha) * weight);
                if (state.getIsOptimal() == 0) {
                    modifiedWeightsMap.put(key, weight);
                    //System.out.println("key\t" + key + "\tpredicted\t" + weight + "\toptimal\t" + optimalMap.get(key) + "\tlabel_local\t" + label_local);
                } else {
                    modifiedWeightsMap.put(key, optimalMap.get(key));
                }
            }*/
            //Bulk Features Translated

            /*for (String key : optimalMap.keySet()) {
                if (selectedFeaturesMap.containsKey(key)) {
                    int label_local = (int) oracle.getLabel(key);
                    if(StaticVariables.errorActivated == 1 && countError <= error) {
                        label_local = oracle.getErrorLabel(key);
                        countError++;
                    }
                    float translatedWeight = translator.translateLabel(label_local, maxLabel, StaticVariables.factor);
                    float weight = originalMap.get(key); //selectedFeaturesMap.get(key).floatValue();
                    float alpha = 0.5f; //
                    weight = (alpha * translatedWeight) + ((1-alpha) * weight);
                    if(state.getIsOptimal()==0) {
                        modifiedWeightsMap.put(key, weight);
                        state.updateUsedFeatures(key);
                    }
                    else {
                        modifiedWeightsMap.put(key, optimalMap.get(key));
                        state.updateUsedFeatures(key);
                    }
                }
            }*/
            return modifiedWeightsMap;
        }





        int foundInAvailable(Set<String> availableTokens, String key){
            int flag =0;
            for(String str: availableTokens){
                if(key.toLowerCase().contains(str.toLowerCase().trim())){
                    flag =1;
                    //System.out.println(key);
                    break;
                }
            }
            return flag;
        }
    }

    public static final class FSBPreviousWeightRemovedExplored implements FixWeightInteractive {
        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, int weightFlag, Oracle oracle, State state){
            int maxLabel = StaticVariables.maxLabel;
            TObjectFloatHashMap<String> modifiedWeightsMap = new TObjectFloatHashMap<>();
            //put the unused features in modifiedweightsMap
            for (String key : originalMap.keySet()) {
                if(!state.queryUsedFeatures(key))
                    modifiedWeightsMap.put(key, originalMap.get(key));
            }
            if(weightFlag==0)
                return modifiedWeightsMap;
            //at first we remove all the weights from modified weights that has been used
            //pick new weights from original Map by selecting specific types of features and then top-k of them
            //no business of modified map here
            FeatureSelector featureSelector = new FeatureSelector(modifiedWeightsMap, optimalMap);
            featureSelector.setState(state);
            ArrayList<FeatureWeight> a = featureSelector.filterByPattern();  //WEIGHTS ARE SORTED
            System.out.println("size after filtering " + a.size());
            //HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresTopk(StaticVariables.zero, numberOfWeights, a);
            //System.out.println("Feature Editing Begins");
            HashMap<String, Double> selectedFeaturesMap;
            HashMap<String, Double> featuresMap;

            if (a.size()>= 1 && state.getReturnPrevious()==false) { //If I want the previous weights, I do not need to change selectedFeaturesMap
                //selectedFeaturesMap = featureSelector.selectTopTwoFeature(a);
                //selectedFeaturesMap = featureSelector.selectTopFeature(a);
                //selectedFeaturesMap = featureSelector.selectUncertainFeature(a);
                //selectedFeaturesMap = featureSelector.selectCommonFeature(a);
                featuresMap = featureSelector.selectAllFeatures(a);
                selectedFeaturesMap = featureSelector.selectAllFeatures(a);

                //selectedFeaturesMap = featureSelector.selectClusterFeatures(a);
                //selectedFeaturesMap = featureSelector.selectContextWordFeatures(a);
                //selectedFeaturesMap = featureSelector.constructContextFeatures(state);
                int count=0;
                for(String s: featuresMap.keySet()){
                    if(selectedFeaturesMap.containsKey(s)){
                        count++;
                    }

                }
                System.out.println("percentage of features matched " + count/(double)featuresMap.size());

            } else {
                selectedFeaturesMap = new HashMap<>();
            }


            Iterator it = state.getUsedFeatures().iterator();
            while (it.hasNext()) {
                selectedFeaturesMap.put((String) it.next(), 0d);
            }

            //BulkFeatures Module
            /*HashMap<String, Integer> bulkFeatures = new HashMap<>();
            for(String key: selectedFeaturesMap.keySet()){
                for(String modKey: modifiedWeightsMap.keySet()){
                    if(modKey.contains(key)){
                        bulkFeatures.put(modKey, 0);
                    }
                }
            }*/
            //BulkFeatures Module ends
            System.out.println("Feature Editing Begins");

            System.out.println(selectedFeaturesMap.size());
            //If optimal map is perfectly loaded then feature selection is not in action
            //System.out.println("weights being changed " + (numberOfWeights - state.getSizeOfUsedFeatures()));
            Translator translator = new Translator(FeatureSelector.mapToList(originalMap));
            //error will contain the number of weight error that we are allowing.
            int error = state.getNumberofWeightErrors();
            if(error==-1) error = selectedFeaturesMap.size();
            int countError = 0;
            int countFeatures = 0;

            for (String key : selectedFeaturesMap.keySet()) {
                if(foundInAvailable(state.availableTokens, key)==1)
                    continue;
                if(optimalMap.containsKey(key) == false)
                    continue;
                countFeatures++;
                //System.out.println(key);
                int label_local = (int) oracle.getLabel(key); //oracle label is constant.
                if (StaticVariables.errorActivated == 1 && error == -1) {
                    label_local = oracle.getErrorLabel(key);
                    countError++;
                }
                //BULK FEATURES CODE
                /*for (String modKey : modifiedWeightsMap.keySet()) {
                    if (modKey.contains(key)) {
                        bulkFeatures.put(modKey, label_local);
                    }
                }*/
                //BULK FEATURES CODE ENDS
                float translatedWeight = translator.translateLabel(label_local, maxLabel, StaticVariables.factor);
                float weight=0;
                float alpha = state.getAlpha(); //
                if (originalMap.containsKey(key)){
                    weight = originalMap.get(key); //selectedFeaturesMap.get(key).floatValue();
                    weight = (alpha * translatedWeight) + ((1 - alpha) * weight);
                }else{
                    weight = optimalMap.get(key);
                }
                if (state.getIsOptimal() == 0) {
                    modifiedWeightsMap.put(key, weight);
                    //System.out.println("key\t" + key + "\tpredicted\t" + weight + "\toptimal\t" + optimalMap.get(key));
                } else {
                    modifiedWeightsMap.put(key, optimalMap.get(key));
                }
                state.updateUsedFeatures(key);
            }
            System.out.println("feature done");
            return modifiedWeightsMap;
        }

        int foundInAvailable(Set<String> availableTokens, String key){
            int flag =0;
            for(String str: availableTokens){
                if(key.toLowerCase().contains(str.toLowerCase().trim())){
                    flag =1;
                    //System.out.println(key);
                    break;
                }
            }
            return flag;
        }
    }



    public static final class FSEqualBinQuantization implements FixWeight {
        WeightInfo weightInfo;
        int error;
        public FSEqualBinQuantization(int error) {
            //weightInfo = new WeightInfo();
            this.error = error;
        }
        public WeightInfo getWeightInfo() {
            return weightInfo;
        }
        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, double percent, Oracle oracle) {
            int maxLabel = StaticVariables.maxLabel;
            //this will be eventually returned. copy weights into it modify it and return.. :)
            FeatureSelector featureSelector = new FeatureSelector(originalMap, optimalMap);
            TObjectFloatHashMap<String> modifiedWeights = new TObjectFloatHashMap<>();
            for (String key : originalMap.keySet()) {
                modifiedWeights.put(key, originalMap.get(key));
            }
            //optimalMap contains a specific type of feature like cluster feature
            //originalMap contains all the features. But we are interested in the cluster features only. So, we select only the cluster features and keep them in a
            //we wish to develop a filterwithoutPattern() and it would return all the features in the originalMap
            //we have sorted cluster weights in a
            ArrayList<FeatureWeight> a = featureSelector.filterByPattern();  //WEIGHTS ARE SORTED
            //HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresPercentage(percent, a);
            HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresPercentage((int)percent, a);

            Translator translator = new Translator(new FeatureSelector().mapToList(originalMap));
            weightInfo = new WeightInfo();
            Random r = new Random();
            for (String key : optimalMap.keySet()) {
                if (selectedFeaturesMap.containsKey(key)) {
                    int label_local = (int) oracle.getLabel(key);
                    label_local = implementError(label_local, r, error);
                    float translatedLabel = translator.translateLabel(label_local, maxLabel, StaticVariables.factor);
                    float weight = selectedFeaturesMap.get(key).floatValue();
                    float alpha = 0.8f;
                    weight = weight + (alpha * (translatedLabel - weight));
                    modifiedWeights.put(key, weight);
                    weightInfo.setNumberOfWeightsChanged(weightInfo.getNumberOfWeightsChanged() + 1);
                }
            }
            //PearsonsCorrelation p = new PearsonsCorrelation();
            //System.out.print(p.correlation(a1,a2) + "\n");
            return modifiedWeights;
        }

        int implementError(int label_local, Random r, int labelError) {
            int val = label_local;
            int test = r.nextInt(2);
            if(test==0)
                val = label_local + labelError;
            else
                val = label_local - labelError;
            /*int max = labelError, min = max * -1;
            int error = r.nextInt((max - min) + 1) + min;
            val += error;*/
            if (val < 0) val+= (2*labelError);
            if (val > 10) val-= (2*labelError);
            return val;
        }

    }

    public static final class OptimalTruncated implements FixWeight {
        WeightInfo weightInfo;

        public OptimalTruncated() {
            weightInfo = new WeightInfo();
        }

        public WeightInfo getWeightInfo() {
            return weightInfo;
        }

        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, double percent, Oracle oracle) {
            Translator translator = new Translator();
            TObjectFloatHashMap<String> modifiedWeights = new TObjectFloatHashMap<>();
            for (String key : originalMap.keySet()) {
                modifiedWeights.put(key, originalMap.get(key));
            }
            for (String key : optimalMap.keySet()) {
                if (originalMap.containsKey(key)) {
                    modifiedWeights.put(key, translator.truncateFeature(optimalMap.get(key)));
                }
            }
            return modifiedWeights;
        }
    }

    public static final class FSOptimalCopy implements FixWeight {
        //I dont intend to modify the weight map. so creating a copy of it
        WeightInfo weightInfo;

        public FSOptimalCopy() {
            weightInfo = new WeightInfo();
        }

        public WeightInfo getWeightInfo() {
            return weightInfo;
        }

        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, double percent, Oracle oracle) {
            FeatureSelector featureSelector = new FeatureSelector(originalMap, optimalMap);
            TObjectFloatHashMap<String> modifiedWeights = new TObjectFloatHashMap<>();
            for (String key : originalMap.keySet()) {
                modifiedWeights.put(key, originalMap.get(key));
            }
            ArrayList<FeatureWeight> a = featureSelector.filterByPattern();
            //HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresPercentage(percent, a);
            HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresPercentage((int)percent, a);

            //correcting all the weights found in the hashMap with optimal weights
            HashSet<String> hashSet = new HashSet<>();
            weightInfo = new WeightInfo();
            for (String key : optimalMap.keySet()) {
                if (selectedFeaturesMap.containsKey(key)) {
                    modifiedWeights.put(key, optimalMap.get(key));
                    weightInfo.setNumberOfWeightsChanged(weightInfo.getNumberOfWeightsChanged() + 1);
                }
            }
            return modifiedWeights;
        }
    }

    public static final class FSOptimalTruncated implements FixWeight {
        WeightInfo weightInfo;

        public FSOptimalTruncated() {
            weightInfo = new WeightInfo();
        }

        public WeightInfo getWeightInfo() {
            return weightInfo;
        }

        //I dont intend to modify the weight map. so creating a copy of it
        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, double percent, Oracle oracle) {
            FeatureSelector featureSelector = new FeatureSelector(originalMap, optimalMap);
            TObjectFloatHashMap<String> modifiedWeights = new TObjectFloatHashMap<>();
            for (String key : originalMap.keySet()) {
                modifiedWeights.put(key, originalMap.get(key));
            }
            ArrayList<FeatureWeight> a = featureSelector.filterByPattern();
            //HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresPercentage(percent, a);
            HashMap<String, Double> selectedFeaturesMap = featureSelector.selectFeautresPercentage((int)percent, a);

            //correcting all the weights found in the hashMap with optimal weights
            Translator translator = new Translator();
            weightInfo = new WeightInfo();
            for (String key : optimalMap.keySet()) {
                if (selectedFeaturesMap.containsKey(key)) {
                    modifiedWeights.put(key, translator.truncateFeature(optimalMap.get(key)));
                    weightInfo.setNumberOfWeightsChanged(weightInfo.getNumberOfWeightsChanged() + 1);
                }
            }
            return modifiedWeights;
        }
    }

    public static final class OriginalTruncated implements FixWeight {
        WeightInfo weightInfo;

        public OriginalTruncated() {
            weightInfo = new WeightInfo();
        }

        public WeightInfo getWeightInfo() {
            return weightInfo;
        }

        public TObjectFloatHashMap fixWeight(TObjectFloatHashMap<String> optimalMap, TObjectFloatHashMap<String> originalMap, double percent, Oracle oracle) {
            Translator translator = new Translator();
            TObjectFloatHashMap<String> modifiedWeights = new TObjectFloatHashMap<>();
            weightInfo = new WeightInfo();
            for (String key : originalMap.keySet()) { //changed
                modifiedWeights.put(key, translator.truncateFeature(originalMap.get(key)));
                modifiedWeights.put(key, originalMap.get(key));
                weightInfo.setNumberOfWeightsChanged(weightInfo.getNumberOfWeightsChanged() + 1);
            }
            return modifiedWeights;
        }

    }
}

