package edu.umass.cs.ciir.searchie.starter.experiments_wsdm;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.Island;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EntityGraph;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.Evaluation;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.*;


//Span Based AddPRF
//

public class OldReRankingWithScoring {
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args) throws IOException {
        double[] expandedSentencesWeight;
        int[] expandedSentences;
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", "/home/sarwar/crfsuite/");
        String inputFileName = argp.get("file", "train_list.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int[] occurrence = new int[100];
        PrintResults pr = new PrintResults();
        ArrayList<List<SimpleToken>> fullPositives = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> fullNegatives = new ArrayList<List<SimpleToken>>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (!token.getLabel().equals(etype)) continue;
                isPositive = true;
                break;
            }
            if (isPositive) {
                fullPositives.add(sent);
                continue;
            }
            fullNegatives.add(sent);
        }
        int size = fullPositives.size();
        int totalSize = fullConllTrain.size();
        ArrayList<List<SimpleToken>> mergedList = new ArrayList<List<SimpleToken>>();
        ArrayList<List<SimpleToken>> positives = new ArrayList<List<SimpleToken>>();
        mergedList.addAll(fullPositives);
        for (int i = 0; i < totalSize - size; ++i) {
            mergedList.add((List<SimpleToken>)fullNegatives.get(i));
        }
        positives.add((List<SimpleToken>)mergedList.get(0));
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        FeatureExtractor f = new FeatureExtractor((List)mergedList.get(0));
        String targetEntityType = f.getTargetEntityType();
        Set<List<String>> foundSpansCaseSensitive = DataSelector.getUniqueSpansCaseSensitive(positives, etype);
        List<Double> targetEntityEmbedding = EmbeddingSearch.getEntityEmbedding(foundSpansCaseSensitive, sm);
        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        double[] prior = new double[mergedList.size()];
        double[] vectorLength = new double[totalSize];
        double[][] points = new double[totalSize][embeddingSize];
        String expansionApproach = "EMBEDDING";
        String expansionParameter = "PRF";
        ArrayList<Integer> pseudoAndFullPositives = new ArrayList<Integer>();
        int row = 0;
        int countPRF = 0;
        int topk = 3;
        LuceneInMemory lucene = new LuceneInMemory();
        Set<Integer> sentences = null;
        if (expansionParameter.equals("ISLAND")) {
            List<Island> islands = EntityGraph.createIslands(mergedList, availableSpans, etype, inputFileName);
            for (Island island : islands) {
                if (!island.containsSentence(0)) continue;
                sentences = island.getSentences();
                break;
            }
        }
        FeatureExtractor featureExtractor = null;
        Set<List<String>> typeSpans = null;
        for (List<SimpleToken> tokenList : mergedList) {
            featureExtractor = new FeatureExtractor(mergedList.get(row), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityList();
            for (List<String> strList : foundSpans) {
                if (DataSelector.containsFoundEntity(tokenList, strList) != 1) continue;
                sm.updateContextExpansionMap(strList, row);
            }
            for (List<String> strList : typeSpans) {
                sm.updateContextExpansionMap(strList, row);
            }
            lucene.addTokenList(row, tokenList);
            if (expansionParameter.equals("PRF")) {
                if (expansionApproach.equals("ALL")) {
                    if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, availableSpans) == 1) {
                        prior[row] = 10.0;
                        ++countPRF;
                    }
                    pseudoAndFullPositives.add(row);
                } else if (DataSelector.containsFoundEntity(availableSpans, tokenList, etype, foundSpans) == 1) {
                    prior[row] = 10.0;
                    ++countPRF;
                }
            }
            if (expansionParameter.equals("ISLAND") && sentences.contains(row)) {
                prior[row] = 10.0;
                ++countPRF;
            }
            List<Double> doubleList = sm.getSentenceEmbedding(EmbeddingSearch.simpleTokenToList(tokenList));
            int col = 0;
            Iterator iterator = doubleList.iterator();
            while (iterator.hasNext()) {
                double d = (Double)iterator.next();
                points[row][col++] = d;
                double[] arrd = vectorLength;
                int n = row;
                arrd[n] = arrd[n] + d * d;
            }
            if (vectorLength[row] - 0.0 < 1.0E-8) {
                vectorLength[row] = 1.0;
            }
            ++row;
        }
        sm.constructEntityEmbedding(points);
        for (int i = 0; i < vectorLength.length; ++i) {
            vectorLength[i] = Math.sqrt(vectorLength[i]);
            if (vectorLength[i] - 0.0 >= 1.0E-8) continue;
            vectorLength[row] = 1.0;
        }
        int bootstrapSentenceId = 0;
        if (expansionParameter.equals("TOPK")) {
            expandedSentences = new int[topk];
            expandedSentencesWeight = new double[topk];
        } else if (expansionParameter.equals("PRF") || expansionParameter.equals("ALL") || expansionParameter.equals("ISLAND")) {
            expandedSentences = new int[countPRF];
            expandedSentencesWeight = new double[countPRF];
        } else {
            expandedSentences = new int[fullPositives.size()];
            expandedSentencesWeight = new double[fullPositives.size()];
        }
        List<Integer> l = new ArrayList();
        if (expansionApproach.equals("LUCENE")) {
            l = lucene.search(mergedList.get(bootstrapSentenceId));
            row = 0;
            for (Integer s : l) {
                expandedSentences[row++] = s;
                if (!(expansionParameter.equals("TOPK") ? row == topk : expansionParameter.equals("PRF") && row == countPRF)) continue;
                break;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        } else if (expansionApproach.equals("EMBEDDING")) {
            row = 0;
            for (SearchResult s : EmbeddingSearch.search(bootstrapSentenceId, points, totalSize, embeddingSize, prior, vectorLength)) {
                expandedSentences[row] = s.getSentenceId();
                expandedSentencesWeight[row] = countPRF - row;
                ++row;
                if (!(expansionParameter.equals("TOPK") ? row == topk : (expansionParameter.equals("PRF") || expansionParameter.equals("ISLAND")) && row == countPRF)) continue;
                break;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, expandedSentencesWeight, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        } else if (expansionApproach.equals("FULL")) {
            int i = 0;
            while (i < fullPositives.size()) {
                expandedSentences[i] = i++;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        } else if (expansionApproach.equals("ALL")) {
            row = 0;
            for (Integer i : pseudoAndFullPositives) {
                expandedSentences[row] = i;
                if (!expansionParameter.equals("PRF") || ++row != countPRF) continue;
                break;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }
        EmbeddingSearch.setPrior(prior, 0.0);
        List<SearchResult> results = EmbeddingSearch.search(bootstrapSentenceId, points, totalSize, embeddingSize, prior, vectorLength);
        HashSet<List<String>> fullTypeSpans = new HashSet<List<String>>();
        featureExtractor = new FeatureExtractor(mergedList.get(0), targetEntityType);
        typeSpans = featureExtractor.getTargetEntityList();
        fullTypeSpans.addAll(typeSpans);
        int spanSize = fullTypeSpans.size();
        ArrayList<Double> targetEntityContextEmbedding = new ArrayList<Double>();
        ArrayList targetEntityTokenEmbedding = new ArrayList();
        for (int i = 0; i < 200; ++i) {
            targetEntityContextEmbedding.add(points[0][i]);
        }
        List<List<Double>> targetEntityTokenEmbeddingList = EmbeddingSearch.targetEntityTokenEmbedingScore(sm, foundSpans);
        List<List<Double>> targetEntityContextEmbeddingList = EmbeddingSearch.targetEntityContextEmbedingScore(sm, foundSpans);
        Map<Integer, Double> luceneScore = lucene.normalizedSearchScore(mergedList.get(0), StaticVariables.sizeOfInitialRetrieval);
        for (SearchResult s : results) {
            featureExtractor = new FeatureExtractor(mergedList.get(s.getSentenceId()), targetEntityType);
            typeSpans = featureExtractor.getTargetEntityList();
            List<Double> sentenceScore = EmbeddingSearch.computeScore(typeSpans, fullTypeSpans, targetEntityTokenEmbeddingList, targetEntityContextEmbeddingList, sm);
            fullTypeSpans.addAll(typeSpans);
            if (fullTypeSpans.size() - spanSize == 0) {
                s.setScore(0.0);
                continue;
            }
            spanSize = fullTypeSpans.size();
            double lscore = 0.0;
            if (luceneScore.containsKey(s.getSentenceId())) {
                lscore = luceneScore.get(s.getSentenceId());
            }
            s.setScore(sentenceScore.get(1));
            //s.setScore(s.getScore() + sentenceScore.get(0));
        }
        results.sort((o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        int limit = 0;
        int relevantSentences = 0;
        Evaluation eval = new Evaluation();
        eval.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType,  bootstrapSentenceId);
        pr.printEvaluate(eval.getEvaluationScores());

//      Evaluation.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType);
        //pr.printEvaluate(Evaluation.evaluateAll(results, mergedList, availableSpans, etype, targetEntityType));
//      ArrayList<Boolean> list = new ArrayList<Boolean>();
//        for (SearchResult s : results) {
//            positives.add(mergedList.get(s.getSentenceId()));
//            l.add(s.getSentenceId());
//            if (DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype) == 1) {
//                list.add(true);
//                ++relevantSentences;
//            } else {
//                list.add(false);
//            }
//            if (limit < 20) {
//                if (list.get(list.size() - 1).booleanValue()) {
//                    pr.printSimpleToken(mergedList.get(s.getSentenceId()), "relevant");
//                } else {
//                    pr.printSimpleToken(mergedList.get(s.getSentenceId()), "non-relevant");
//                }
//            }
//            ++limit;
//        }
//        System.out.println("" + availableSpans.size() + "\t" + foundSpans.size() + "\t" + Metrics.computeAP(list, relevantSentences) + "\t" + 1.0 * (double)foundSpans.size() / (double)availableSpans.size() + "\t" + targetEntityType + "\t" + inputFileName + "\t" + mergedList.size());
    }

    public static void setPrior(double[] prior, double val) {
        for (int i = 0; i < prior.length; ++i) {
            prior[i] = val;
        }
    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList) {
        ArrayList<String> strList = new ArrayList<String>();
        for (SimpleToken token : tokenList) {
            if (Stopwords.isStemmedStopword(token.lemma)) continue;
            strList.add(token.lemma);
        }
        return strList;
    }

    public static void writeEmbeddingToFile(List<Double> l, BufferedWriter br) {
        String str = "";
        for (Double d : l) {
            str = str + d;
            str = str + " ";
        }
        try {
            br.write(str);
            br.write("\n");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
