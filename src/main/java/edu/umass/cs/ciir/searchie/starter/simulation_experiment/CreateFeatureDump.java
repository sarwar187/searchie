package edu.umass.cs.ciir.searchie.starter.simulation_experiment;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class CreateFeatureDump {
    static String prefix = StaticVariables.prefix;
    public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";

    public static void main(String[] args) throws IOException {
        // galago's Parameters class gives us argument parsing:
        // defaults: java ... BasicExperiment --class=PER --trainingStart=3
        // other: java ... BasicExperiment --class=LOC --trainingStart=20
        Parameters argp = Parameters.parseArgs(args);
        // parameters looks in args for a key or uses the default value:
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "new_dump.out");
        File featureFile = new File(featureDirectory + featureFileName);
        //String etype = argp.get("class", StaticVariables.entityType);
        //File trainFile = new File(argp.get("train", StaticVariables.aquaint_prefix + "train.crfsuite"));
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");

        try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
            OptimalModelDump learner = new OptimalModelDump(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary), featureFile);
            learner.setModel(argp.get("model", "lbfgs"));
            Parameters info = Parameters.create();
            TObjectFloatHashMap<String> weights = learner.learnFeatureWeights(fullConllTrain, etype, info);
        }
    }
}
