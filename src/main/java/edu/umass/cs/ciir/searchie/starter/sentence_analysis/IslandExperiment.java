package edu.umass.cs.ciir.searchie.starter.sentence_analysis;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class IslandExperiment {
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));

        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);



        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");

        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);

        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        for (List<SimpleToken> sent : fullConllTrain) {
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }

        if (fullPositives.size() <= StaticVariables.minPositivestoTrain)
            return;
        if (fullNegatives.size() >= StaticVariables.maxNumSentence)
            return;

        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        Map<List<String>, Integer> entityMap = new HashMap<>();
        int index = 0;
        for(List<String> strList: availableSpans){
            entityMap.put(strList, index++);
        }
        int graph[][] = new int[availableSpans.size()][availableSpans.size()];
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
        //printResults.printSpans(availableSpans, "available spans");

        for(int i=0; i<fullConllTrain.size(); i++){
            Set<List<String>> str = DataSelector.annotateUser(availableSpans, fullConllTrain.get(i), etype);
            if(str.size()>1) {
                int a[] = new int[str.size()];
                index = 0;
                for(List<String> strList: str){
                    a[index++] = entityMap.get(strList);
                }
                for(int m=0; m<a.length; m++){
                    for(int n=m+1; n<a.length; n++){
                        graph[a[m]][a[n]] = 1;
                        graph[a[n]][a[m]] = 1;
                    }
                }
                //printResults.printSimpleToken(fullConllTrain.get(i), "sentence");
                //printResults.printSpans(str, "found spans");

            }
            for(int j=0; j<str.size(); j++)
                occurrence[j]++;
        }

        /*System.out.println("printing graph");
        for(int i=0;i<availableSpans.size();i++){
            for(int j=0;j<availableSpans.size();j++){
                System.out.print(graph[i][j]);
            }
            System.out.println();
        }*/
        //printResults.printTwoD();

        int nodes[] = new int[availableSpans.size()];
        int visited[] = new int[availableSpans.size()];
        Arrays.fill(visited, -1);
        for(int i=0;i<availableSpans.size();i++){
            nodes[i] = i;
        }
        int components = 0;
        try{
            for(int i=0;i<availableSpans.size();i++){
                if(visited[i] == -1) {
                    components++;
                    dfs(i, nodes, visited, graph, i);
                    //System.out.println();
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
            return;
        }

//        System.out.println("printing occurrence");
//        for(int i=0;i<20;i++)
//            System.out.print(occurrence[i] + "\t");
//        System.out.print(fullConllTrain.size() + "\t" + inputFileName);
//        System.out.println();

        /*System.out.println("printing visited");
        for(int i=0;i<availableSpans.size();i++)
            System.out.print(visited[i] + "\t");
        System.out.print(fullConllTrain.size() + "\t" + inputFileName);
        System.out.println();
        */
        Set<Integer> s = new HashSet<>();
        for(int i=0;i<visited.length;i++){
            s.add(visited[i]);
        }


        int lengthCount[] = new int[visited.length];
        for(int i=0;i<visited.length;i++) {
            lengthCount[visited[i]]++;
        }
        int maximumSize = lengthCount[0];
        for(int i=0;i<lengthCount.length;i++) {
            if(lengthCount[i] > maximumSize){
                maximumSize = lengthCount[i];
            }
        }
        //System.out.println(s.size() + "\t" + availableSpans.size() + "\t" + maximumSize + "\t" + inputFileName);
        System.out.println(maximumSize + "\t" + inputFileName);
        //System.out.println("components " + s.size());

        //System.out.println("entities " + availableSpans.size());
        /*System.out.println("entity map");
        for(List<String> tokenList: entityMap.keySet()){
            printResults.printTokenList(tokenList);
            System.out.println(entityMap.get(tokenList));
        }*/

    }

    static void dfs(int node, int[] nodes, int[] visited, int [][] graph, int color){
        visited[node] = color;
        //System.out.print(node + " ");
        for(int j=0; j < nodes.length; j++){
            if(graph[node][j] == 1){
                if(visited[j] == -1)
                    dfs(j, nodes, visited, graph, color);
            }
        }
    }
}

