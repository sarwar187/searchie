package edu.umass.cs.ciir.searchie.starter.interaction_experiment_new;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.*;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by sarwar on 7/13/17.
 */
public class FeatureReweightingLatest {
    static String prefix = StaticVariables.prefix;

    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "1"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int loop = StaticVariables.numRelevanceFeedbacks; // loop is the number of relevance feedback we take
        if (fullPositives.size() <= StaticVariables.minPositivestoTrain)
            return;
        if (fullNegatives.size() >= StaticVariables.maxNumSentence)
            return;
        Oracle oracle = new Oracle(defaultCRFSuiteBinary);
        oracle.createOracle(featureDirectory, featureFileName, etype);
        TObjectFloatHashMap<String> optimalMap = oracle.getOptimalMap();
        PrintResults printResults = new PrintResults(resultDirectory, inputFileName);

        //System.out.println(fullPositives.size() + "\t" + fullNegatives.size() + "\t" + inputFileName + "\t");
        int numExperiments = fullPositives.size() / numPositives;  // number of rounds we calculate AP, uAP and F1
        double testAP = 0d, testuAP = 0d;
        numExperiments = (numExperiments > maximumNumberOfExperiments) ? maximumNumberOfExperiments : numExperiments;
        ArrayList<Integer> configuration = new ArrayList<>();
        // pair of optimal and error (1,0) means that we are considering optimal weights and errorActivated
        // we need to send two parameters to state object. One thorugh isOptimal parameter and the other through
        // setError parameter
        configuration.add(1); //worse

        for (int numConfig = 0; numConfig < configuration.size(); numConfig++) {
            double evalfb[][] = new double[loop][StaticVariables.fblabel.length];
            double AP = 0d;
            double fullAP = 0d;  //added
            SearchResult searchResult;
            for (int index = 0; index < numExperiments * numPositives; index += numPositives) { //iterating over 300 positives, taking three in each round
                //System.out.println("round " + index);
                State state = new State(); //CREATING STATE
                state.setIsOptimal(0);
                StaticVariables.errorActivated = 0;
                Set<String> exploredTokens = null;
                boolean resultsFound = true;
                List<List<SimpleToken>> positives = new ArrayList<>(); //positives is the training data
                List<List<SimpleToken>> fullConllTest = new ArrayList<>(); //fullConllTest is the testing data
                ArrayList<SearchResult> resultsSeen = new ArrayList<>();
                DataSelector.splitTrainTest(fullPositives, fullNegatives, positives, fullConllTest, index, numPositives);
                Set<String> availableTokens = DataSelector.getUniqueTokens(fullPositives, etype);
                Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
                /*for(List<String> l: availableSpans){
                    System.out.print("Span-> ");
                    for(String s: l){
                        System.out.print(s + " ");
                    }
                    System.out.println();
                }*/
                //System.out.println("available tokens size " + availableTokens.size());
                Set<String> foundTokens = DataSelector.getUniqueTokens(positives, etype);
                //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
                //WRITE SOON
                //alternative of previous statement
                //Set<String> foundTokens = DataSelector.addNewTokens(.getUniqueTokens(positives, etype);

                //Set<String> exploredTokens = new HashSet<>();
                //DataSelector.extrapolateTokens(foundTokens, exploredTokens); //construct exploredtokens from tokens found so far
                state.setAvailableTokens(setMinus(availableTokens, foundTokens));

                //if (fullConllTrain.size() != (positives.size() + fullConllTest.size())) // Just checking if the training and test data sum up to the full training data
                //    System.out.println("error happened");

                try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
                    CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
                    learner.setModel(argp.get("model", "lbfgs"));
                    Parameters info = Parameters.create();
                    learner.learnFeatureWeights(fullPositives, etype, info);
                    Set<String> groundTruthFeatures = FeatureSelector.getFeatureSet(learner.getListFeatureMap());
                    LinearTokenClassifier rf;
                    //PRF.addPRF(fullConllTrain, positives, etype);
                    Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
                    //Set<Integer> alreadyAdded = new HashSet<>();
                    //PRF.addPRFWithSpan(fullConllTrain, foundSpans , positives, etype, alreadyAdded);
                    //printResults.printSimpleTokenFeatures(positives, "Token Features");
                    TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(positives, etype, info);
                    Map<String, TokenCount> retrievedTokenMap = new HashMap<>();
                    //PRF.addRankedPRFWithSpan(fullConllTrain, foundSpans , positives, etype, new LinearTokenClassifier(originalMap));
                    //printResults.printSimpleTokenList(positives, "printing positives...");
                    //starting the loop with i=0. It means we are not taking any weight relevance feedback in the beginning
                    int unsuccessful = 0;
                    int i=0;

                    for ( ; i < loop; i++) {
                        //EDIT THE MODEL
                        //Set<String> baseTokens = groundTruthFeatures;
                        //Set<String> baseTokens = FeatureSelector.getFeatureSet(learner.getListFeatureMap()); //THIS returns the context word tokens only
                        Set<String> baseTokens = FeatureSelector.getRawFeatureSet(learner.getListFeatureMap());
                        DataSelector.removeGroundTruthFeaturesFromBase(baseTokens, availableTokens); //remove grountruth tokens because users can not judge them
                        //Set<String> extrapolatedTokens = DataSelector.extrapolateTokens(baseTokens); //construct exploredtokens from tokens found so far
                        //baseTokens.addAll(extrapolatedTokens);
                        //DataSelector.removeGroundTruthFromBase(baseTokens, availableTokens);
                        //printResults.printTokenSet(baseTokens, "printing base tokens");
                        Set<String> newFeatures = FeatureSelector.getNewContextWordFeatures(baseTokens, state.getUsedFeatures());
                        //FORMFEATURECLOUD can be used interchangeably
                        idf.formWordCloud(newFeatures, state.getRelevantFeatures());
                        printResults.printTokenSet(baseTokens, "base features");
                        printResults.printTokenSet(newFeatures, "new features");
                        printResults.printTokenSet(state.getRelevantFeatures(), "relevant features");
                        exploredTokens = newFeatures;
                        //exploredTokens = baseTokens;
                        //printResults.printTokenSet(newFeatures, "new extrapolated features");
                        //System.out.println(newFeatures.size());
                        //exploredTokens = idf.getTopkList(baseTokens, 30);
                        //printResults.printTokenSet(exploredTokens, "printing explored tokens");
                        int numberOfRelevantFeatures = state.getRelevantFeatures().size();
                        state.setExploredTokens(exploredTokens);
                        //state.setExploredTokens(exploredTokens); //Setting context word tokens for feature exploration
                        WeightUtils.FSBPreviousWeightRemoved fixWeightInteractive = new WeightUtils.FSBPreviousWeightRemoved();
                        if (StaticVariables.errorActivated == 1)
                            state.setNumberofWeightErrors(-1);
                        TObjectFloatHashMap<String> modifiedWeights = fixWeightInteractive.fixWeight(optimalMap, originalMap, 1, oracle, state);

                        //RELEVANCE FEEDBACK + SENTENCE RETRIEVAL
                        rf = new LinearTokenClassifier(modifiedWeights);
                        searchResult = Metrics.addRelevanceFeedback(rf, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                        //USER ANNOTATION
                        DataSelector.addNewTokensWithGroundTruth(foundTokens, availableTokens, resultsSeen.get(i).getSimpleTokens(), etype);

                        int successful = 0;
                        for(List<SimpleToken> t: fullPositives){
                            if(t.equals(resultsSeen.get(i).getSimpleTokens())){
                                successful=1;
                            }
                        }
                        //int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, resultsSeen.get(i).getSimpleTokens(), etype);
                        //This always creates a new tokenlist and appends it with positive. So, nothing is changed in fullConllTrain
                        //PRF.addPRFWithSpan(fullConllTrain, foundSpans , positives, etype, alreadyAdded);
                        //printResults.printSimpleToken(resultsSeen.get(i).getSimpleTokens(), "Results Seen");
                        //System.out.println("max scored token " + resultsSeen.get(i).getMaxScoredToken());
                        if (successful == 1) {
                            retrievedTokenMap.get(resultsSeen.get(i).getMaxScoredToken()).incrementSuccessfulByOne();
                            printResults.printSimpleToken(resultsSeen.get(i).getSimpleTokens(), "Successful Results Seen");
                            DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, resultsSeen.get(i).getSimpleTokens(), etype);
                            //System.out.println("found spans size " + foundSpans.size());
                        } else
                            unsuccessful++;

                        if (unsuccessful > 2) {
                            StaticVariables.isUnique = StaticVariables.isUnique ^ true;
                            unsuccessful = 0;
                        }

                        //System.out.println("after");
                        //printResults.printSimpleToken(resultsSeen.get(i).getSimpleTokens(), "printing results seen");
                        if(foundSpans.size()==availableSpans.size()) {
                            //System.out.println("number of interactions" + i);
                            break;
                        }
                        if (searchResult == null) {
                            System.out.println(i + "\t" + state.getUsedFeatures().size() + "\t" + inputFileName + "\t" + fullConllTest.size());
                            break;
                        }
                        //REBUILDING THE MODEL
                        if(state.getRelevantFeatures().size() == numberOfRelevantFeatures)
                            positives.remove(positives.size()-1);
                        originalMap = learner.learnFeatureWeights(positives, etype, info);
                    }
                    //System.out.println("ground truth feature size " + groundTruthFeatures.size());
                    //System.out.println("recovered " + foundInGroundTruth(exploredTokens, groundTruthFeatures));
                    //System.out.println("percentage of groundtruth " + 1.0 * groundTruthFeatures.size());
                    System.out.println(i + "\t" +  availableSpans.size() + "\t" + foundSpans.size() + "\t" + state.getRelevantFeatures().size() + "\t" + inputFileName);
                    //System.out.println(testuAP + "\t" + testAP + "\t" + availableSpans.size() + "\t" + foundSpans.size() + "\t" + state.getUsedFeatures().size() + "\t" + inputFileName);
                    //System.out.println(testuAP + "\t" + testAP + "\t" + availableTokens.size() + "\t" + foundTokens.size() + "\t" + state.getUsedFeatures().size() + "\t" + inputFileName);
                }
            }
        }
    }

    public static Set<String> setMinus(Set<String> availableTokens, Set<String> foundTokens) {
        Set<String> tempSet = new HashSet<>();
        tempSet.addAll(availableTokens);
        tempSet.removeAll(foundTokens);
        return tempSet;
    }


    public static int compareMaps(TObjectFloatHashMap<String> a, TObjectFloatHashMap<String> b) {
        int flag = 0;
        for (String key : a.keySet()) {
            if (b.containsKey(key)) {
                if (a.get(key) != b.get(key)) {
                    System.out.println("a values " + a.get(key));
                    System.out.println("b values " + b.get(key));

                    flag = 1;
                    //return flag;
                }
            }
        }
        return flag;
    }

    public static int foundInGroundTruth(Set<String> extrapolated, Set<String> groundTruthFeatures) {
        int hit = 0;
        System.out.println("extrapolated tokens");
        if (extrapolated == null)
            return 0;
        for (String s : extrapolated) {
            if (groundTruthFeatures.contains(s)) {
                //System.out.println(s);
                hit++;
            }
        }
        return hit;
    }

}
