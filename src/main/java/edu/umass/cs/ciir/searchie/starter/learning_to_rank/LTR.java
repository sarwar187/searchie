package edu.umass.cs.ciir.searchie.starter.learning_to_rank;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.EmbeddingFromTextFile;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Created by sarwar on 6/14/17.
 */
public class LTR {
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        List<List<SimpleToken>> pseudoPositives = new ArrayList<>();

        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        //lucene.close();
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        List<List<SimpleToken>> positives = new ArrayList<>();
        mergedList.addAll(fullPositives);
        mergedList.addAll(fullNegatives);
        positives.add(mergedList.get(0));
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);

        double vectorLength[] = new double[mergedList.size()];
        //double scoreList[] = new double[mergedList.size()];
        double points[][] = new double[mergedList.size()][embeddingSize];
        SentenceEmbedding sm = new SentenceEmbedding();
        sm.loadWordEmbedding(idf);
        int row = 0;
        LuceneInMemory lucene = new LuceneInMemory();
        int index = 0;
        for(List<SimpleToken> tokenList: mergedList) {
            lucene.addTokenList(index, tokenList);
            index++;
            //System.out.println("Processed row " + row);
            List<Double> doubleList = sm.getSentenceEmbedding(EmbeddingFromTextFile.simpleTokenToList(tokenList), idf);
            //writeEmbeddingToFile(doubleList, br);
            int col = 0;
            for (double d : doubleList) {
                points[row][col++] = d;
                vectorLength[row] += (d * d);
            }
            row++;
        }

        for(int i=0;i<vectorLength.length;i++){
            vectorLength[i] = Math.sqrt(vectorLength[i]);
        }
        //System.out.println("sentence embedding done ");
        //pq = new PriorityQueue<>((o1, o2) -> o1.getValue().compareTo(o2.getValue()));
        //BufferedWriter br = new BufferedWriter(new FileWriter(new File("/mnt/nfs/work1/smsarwar/searchie/ltr/features/" + inputFileName)));
        Random r = new Random();
        BufferedWriter br=null;
        if(r.nextInt(3)==0) {
            br = new BufferedWriter(new FileWriter(new File("/mnt/nfs/work1/smsarwar/searchie/ltr/test/" + inputFileName + ".arff")));
            br.write(FeatureInfo.getFeatureString());
            br.newLine();
            System.out.println(inputFileName);
        }
        else
            br = new BufferedWriter(new FileWriter(new File("/mnt/nfs/work1/smsarwar/searchie/ltr/train/" + inputFileName)));
        //BufferedWriter br = new BufferedWriter(new FileWriter(new File("/home/sarwar/crfsuite/ltr/" + inputFileName)));

        //br.write("test");
        //br.newLine();
        Map<Integer, Double> hashMap = lucene.search(mergedList.get(0), mergedList.size()); //integer values in list l is indexed on fullconlltrain
        PriorityQueue<SearchResult> pq = new PriorityQueue<>((o1, o2) -> o1.getScore().compareTo(o2.getScore()));
        int bootstrapSentenceId = 0;
        NumberFormat formatter = new DecimalFormat("#0.000");
        for(int i=0;i<mergedList.size();i++){
            double score = 0;
            for(int j=0; j<embeddingSize; j++){
                score += (points[bootstrapSentenceId][j] * points[i][j]);
            }
            score/=vectorLength[bootstrapSentenceId];
            score/=vectorLength[i];
            if(pq.size() < StaticVariables.sizeOfInitialRetrieval) {
                pq.add(new SearchResult(i, score));
            }else{
                SearchResult s = pq.peek();
                if(s.getScore() < score) {
                    pq.poll();
                    pq.add(new SearchResult(i, score));
                }
            }
            //System.out.println(i + "\t" + score);
        }
        List<SearchResult> results = new ArrayList<>(pq.size());
        results.addAll(pq);
        //br.write(FeatureInfo.getFeatureString());
        //br.newLine();
        for (SearchResult s : results) {
            positives.add(mergedList.get(s.getSentenceId()));
            //DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            foundSpans.clear();
            foundSpans = DataSelector.getUniqueSpans(positives, etype);
            String featureString = inputFileName + "," + s.getSentenceId() + ",";
            FeatureExtractor fe = new FeatureExtractor(mergedList.get(s.getSentenceId()));
            featureString += fe.posNERFeatures();
            int containsFoundEntity = DataSelector.containsFoundEntity(availableSpans, mergedList.get(s.getSentenceId()), etype, foundSpans);
            featureString+= String.valueOf(containsFoundEntity) + ",";
            //List<SimpleToken> t = mergedList.get(i).get();
            if(successful==1) {
                //if(hashMap.containsKey(i)) {
                //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 1);
                featureString += (formatter.format(s.getScore()) + "," + mergedList.get(s.getSentenceId()).size() + "," + 1);
                //System.out.println(featureString);
                br.write(featureString);
                br.newLine();
                //}
            }else{
                //if(hashMap.containsKey(i)) {
                //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 0);
                featureString += (formatter.format(s.getScore()) + "," + mergedList.get(s.getSentenceId()).size() + "," + 0);
                //System.out.println(featureString);
                br.write(featureString);
                br.newLine();
                //}
            }
            //PrintResults pr = new PrintResults();
            //pr.printSimpleToken(mergedList.get(i), "input");

        }
        //System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + inputFileName);

        /*for(int i=0; i<1000; i++){
            double score = 0;
            for(int j=0; j<embeddingSize; j++){
                score += (points[bootstrapSentenceId][j] * points[i][j]);
            }
            score/=vectorLength[bootstrapSentenceId];
            score/=vectorLength[i];
            //scoreList[index] = score;
            int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(i), etype);
            foundSpans.clear();
            foundSpans = DataSelector.getUniqueSpans(positives, etype);
            String featureString = "";
            FeatureExtractor fe = new FeatureExtractor(mergedList.get(i));
            featureString += fe.posNERFeatures();
            int containsFoundEntity = DataSelector.containsFoundEntity(availableSpans, mergedList.get(i), etype, foundSpans);
            featureString+= String.valueOf(containsFoundEntity) + ",";
            //List<SimpleToken> t = mergedList.get(i).get();
            if(successful==1) {
                //if(hashMap.containsKey(i)) {
                    //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 1);
                    featureString += (formatter.format(score) + "," + mergedList.get(i).size() + "," + 1);
                    //System.out.println(featureString);
                    br.write(featureString);
                    br.newLine();
                //}
            }else{
                //if(hashMap.containsKey(i)) {
                    //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 0);
                    featureString += (formatter.format(score) + "," + mergedList.get(i).size() + "," + 0);
                    //System.out.println(featureString);
                    br.write(featureString);
                    br.newLine();
                //}
            }
            //PrintResults pr = new PrintResults();
            //pr.printSimpleToken(mergedList.get(i), "input");

        }*/
        br.close();
    }
}
