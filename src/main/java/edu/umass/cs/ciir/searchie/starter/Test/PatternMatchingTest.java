package edu.umass.cs.ciir.searchie.starter.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sarwar on 4/8/17.
 */
public class PatternMatchingTest {
    public static void main(String args[]) {
        String feature = "w[R] = sarwar";
        Pattern p = Pattern.compile("(.*w)(\\[L\\]=|\\[R\\]=)(\\w+)(.*)");
        Matcher m = p.matcher(feature);
        if (m.find() == true && m.group(3).length() >= 1) {
            System.out.println("Found " + m.group(3));
        }
    }
}
