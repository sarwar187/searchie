/*
 * Decompiled with CFR 0_122.
 *
 * Could not load the following classes:
 *  ciir.jfoley.chai.io.TemporaryDirectory
 *  gnu.trove.map.hash.TObjectFloatHashMap
 */
package edu.umass.cs.ciir.searchie.starter.data_engineering;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import gnu.trove.map.hash.TObjectFloatHashMap;
import javafx.util.Pair;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataSelector {
    public final TemporaryDirectory tmpdir = null;

    public static void splitTrainTest(List<List<SimpleToken>> fullPositives, List<List<SimpleToken>> fullNegatives, List<List<SimpleToken>> train, List<List<SimpleToken>> test, int index, int numPositives) {
        int i;
        for (i = 0; i < numPositives; ++i) {
            train.add(fullPositives.get(index + i));
        }
        test.addAll(fullNegatives);
        for (i = 0; i < fullPositives.size(); ++i) {
            if (i >= index && i < index + numPositives) continue;
            test.add(fullPositives.get(i));
        }
    }

    public static void splitTrainTestWithNegatives(List<List<SimpleToken>> fullPositives, List<List<SimpleToken>> fullNegatives, List<List<SimpleToken>> train, List<List<SimpleToken>> test, int index, int numPositives) {
        int i;
        for (int i2 = 0; i2 < numPositives; ++i2) {
            train.add(fullPositives.get(index + i2));
        }
        ArrayList<List<SimpleToken>> remainings = new ArrayList<List<SimpleToken>>();
        for (i = 0; i < StaticVariables.numRelevanceFeedbacks; ++i) {
            List<SimpleToken> removed = fullNegatives.remove(i);
            remainings.add(removed);
            train.add(removed);
        }
        test.addAll(fullNegatives);
        for (i = 0; i < fullPositives.size(); ++i) {
            if (i >= index && i < index + numPositives) continue;
            test.add(fullPositives.get(i));
        }
        fullNegatives.addAll(remainings);
    }

    public static int compareMaps(TObjectFloatHashMap<String> originalMap, TObjectFloatHashMap<String> modifiedMap) {
        int count = 0;
        for (String key : originalMap.keySet()) {
            if (!modifiedMap.containsKey((Object)key) || modifiedMap.get((Object)key) == originalMap.get((Object)key)) continue;
            ++count;
        }
        return count;
    }

    public static Set<String> getUniqueTokens(List<List<SimpleToken>> fullPositives, String eType) {
        HashSet<String> uniqueTokens = new HashSet<String>();
        for (List<SimpleToken> tokenList : fullPositives) {
            for (SimpleToken s : tokenList) {
                if (!s.truthLabel.equals(eType)) continue;
                uniqueTokens.add(s.lemma);
            }
        }
        return uniqueTokens;
    }

    public static Set<List<String>> getUniqueSpans(List<List<SimpleToken>> fullPositives, String eType) {
        HashSet<List<String>> tokenSpans = new HashSet<List<String>>();

        for (List<SimpleToken> tokenList : fullPositives) {
            //take one sentence at a time
            boolean listToken = false;
            ArrayList<String> temp = new ArrayList<String>();
            for (SimpleToken s : tokenList) {
                if (s.truthLabel.equals(eType)) {
                    temp.add(s.lemma.toLowerCase().trim());
                    listToken = true;
                    continue;
                }
                if (!listToken) continue;
                listToken = false;
                tokenSpans.add(temp);
                temp = new ArrayList();
            }
            if (!listToken) continue;
            tokenSpans.add(temp);
        }
        return tokenSpans;
    }

    public static Set<List<String>> getUniqueSpansCaseSensitive(List<List<SimpleToken>> fullPositives, String eType) {
        HashSet<List<String>> tokenSpans = new HashSet<List<String>>();
        for (List<SimpleToken> tokenList : fullPositives) {
            boolean listToken = false;
            ArrayList<String> temp = new ArrayList<String>();
            for (SimpleToken s : tokenList) {
                if (s.truthLabel.equals(eType)) {
                    temp.add(s.lemma.trim());
                    listToken = true;
                    continue;
                }
                if (!listToken) continue;
                listToken = false;
                tokenSpans.add(temp);
                temp = new ArrayList();
            }
            if (!listToken) continue;
            tokenSpans.add(temp);
        }
        return tokenSpans;
    }

    public static Set<String> addNewTokens(Set<String> currentTokens, List<SimpleToken> resultSeen, String eType) {
        HashSet<String> addedTokens = new HashSet<String>();
        for (SimpleToken s : resultSeen) {
            if (!s.truthLabel.equals(eType)) continue;
            currentTokens.add(s.lemma);
            for (String feature : s.getFeatures()) {
                String val;
                Pattern p = Pattern.compile("(.*w)(\\[L\\]=|\\[R\\]=)(\\w+)(.*)");
                Matcher m = p.matcher(feature);
                if (!m.find() || m.group(3).length() < 1 || (val = m.group(3)) == null) continue;
                addedTokens.add(s.lemma);
            }
        }
        return addedTokens;
    }

    public static int addNewSpansWithGroundTruth(Set<List<String>> currentSpans, Set<List<String>> groundTruths, List<SimpleToken> resultSeen, String eType) {
        int foundNew = DataSelector.annotateUser(groundTruths, resultSeen, eType, currentSpans);
        return foundNew;
    }

    public static void addNewSpansWithGroundTruth(List<SimpleToken> query, List<SimpleToken> candidate, Set<List<String>> currentSpans) {
    }

    public static int annotateUser(Set<List<String>> groundTruths, List<SimpleToken> resultSeen, String eType, Set<List<String>> currentSpans) {
        int flag = 0; //nothing new
        int count = 0;
        for (List<String> tokenList : groundTruths) {
            int length = tokenList.size();
            for (int i = 0; i < resultSeen.size() - length + 1; ++i) {
                //generate candidate list
                int j;
                ArrayList<String> temp = new ArrayList<String>();
                for (j = i; j < i + length; ++j) {
                    temp.add(resultSeen.get((int)j).lemma.toLowerCase().trim());
                }
                if (DataSelector.compareStringList(temp, tokenList) != 1) continue;
                if (flag == 0) {
                    boolean subFlag = true;
                    for (List<String> t : currentSpans) {
                        if (DataSelector.compareStringList(t, temp) != 1) continue;
                        subFlag = false;
                        break;
                    }
                    if (subFlag) {
                        flag = 1;
                    }
                }
                for (j = i; j < i + length; ++j) {
                    resultSeen.get((int)j).truthLabel = eType;
                }
                currentSpans.add(temp);
            }
        }
        return flag;
    }

    //this function returns the count how many new entities are there in a sentence except the query entity set
    public static int trainingDataLabeling(Set<List<String>> groundTruths, List<SimpleToken> resultSeen) {
        int count = 0;
        for (List<String> tokenList : groundTruths) {
            int length = tokenList.size();
            for (int i = 0; i < resultSeen.size() - length + 1; ++i) {
                //generate candidate list
                int j;
                ArrayList<String> temp = new ArrayList<String>();
                for (j = i; j < i + length; ++j) {
                    temp.add(resultSeen.get((int)j).lemma.toLowerCase().trim());
                }
                if (DataSelector.compareStringList(temp, tokenList) != 1)
                    continue;
                else
                    count++;
            }
        }
        return Math.min(count, 5);
    }


    public static int containsFoundEntity(Set<List<String>> groundTruths, List<SimpleToken> resultSeen, String eType, Set<List<String>> currentSpans) {

        int flag = 0;

        for (List<String> tokenList : currentSpans) {
            int length = tokenList.size();
            for (int i = 0; i < resultSeen.size() - length + 1; ++i) {
                ArrayList<String> temp = new ArrayList<String>();
                for (int j = i; j < i + length; ++j) {
                    temp.add(resultSeen.get((int)j).lemma.toLowerCase().trim());
                }
                if (DataSelector.compareStringList(temp, tokenList) != 1) continue;
                flag = 1;
            }
        }
        return flag;
    }

    public static int containsQueryEntity(List<SimpleToken> resultSeen, Set<List<String>> currentSpans) {
        int flag = 0;
        for (List<String> tokenList : currentSpans) {
            int length = tokenList.size();
            for (int i = 0; i < resultSeen.size() - length + 1; ++i) {
                ArrayList<String> temp = new ArrayList<String>();
                for (int j = i; j < i + length; ++j) {
                    temp.add(resultSeen.get((int)j).lemma.toLowerCase().trim());
                }
                if (DataSelector.compareStringList(temp, tokenList) != 1) continue;
                flag = 1;
            }
        }
        return flag;
    }

    public static int containsFoundEntity(List<SimpleToken> resultSeen, List<String> currentSpans) {
        int flag = 0;
        List<String> tokenList = currentSpans;
        int length = tokenList.size();
        for (int i = 0; i < resultSeen.size() - length + 1; ++i) {
            ArrayList<String> temp = new ArrayList<String>();
            for (int j = i; j < i + length; ++j) {
                temp.add(resultSeen.get((int)j).lemma.toLowerCase().trim());
            }
            if (DataSelector.compareStringList(temp, tokenList) != 1) continue;
            flag = 1;
        }
        return flag;
    }

    //This function would return representation of entity for an expansion sentence
    public static int listEntityChunkEmbedding(List<SimpleToken> resultSeen, List<String> currentSpans, int winddowSize, SentenceEmbedding sm, double entityPoints[][], int expansionSentenceId) {
        //PrintResults pr = new PrintResults();
        int flag = 0;
        List<String> tokenList = currentSpans;
        int length = tokenList.size();

        List<SimpleToken> filteredTokenList = new ArrayList<>();
        //At first clearing the tokenlist by removing stopwords
        for(SimpleToken token: resultSeen) {
            if(!Stopwords.isStopword(token.lemma)){
                filteredTokenList.add(token);
                //System.out.println(token.lemma);
            }
        }

        for (int i = 0; i < filteredTokenList.size() - length + 1; ++i) {
            ArrayList<String> temp = new ArrayList<String>();
            for (int j = i; j < i + length; ++j) {
                temp.add(filteredTokenList.get((int)j).lemma.toLowerCase().trim());
            }
            if (DataSelector.compareStringList(temp, tokenList) != 1) continue;
            int beginIndex = i;
            int endIndex = i + currentSpans.size();

            if((beginIndex - (winddowSize/2) < 0) || (endIndex + (winddowSize/2) > filteredTokenList.size())){
                winddowSize*= 2;
            }

            beginIndex = Math.max(0, beginIndex - winddowSize/2);


            endIndex = Math.min(filteredTokenList.size(), endIndex + winddowSize/2);



            ArrayList<String> entityChunk = new ArrayList<>();
            for(int index = beginIndex; index < endIndex; index++){
                entityChunk.add(filteredTokenList.get(index).lemma);
            }
            List<Double> entityChunkEmbedding = sm.getSentenceEmbedding(entityChunk);
            for(int index=0; index<StaticVariables.embedding_size; index++){
                entityPoints[expansionSentenceId][index] = entityChunkEmbedding.get(index).doubleValue();
            }
            flag = 1;
            //pr.printTokenList(entityChunk);
        }
        return flag;
    }

    public static Pair<Integer, Integer> listEntityPosition(List<SimpleToken> resultSeen, List<String> currentSpans) {
        int flag = 0;
        List<String> tokenList = currentSpans;
        Pair<Integer, Integer> pair = null;
        int length = tokenList.size();
        for (int i = 0; i < resultSeen.size() - length + 1; ++i) {
            ArrayList<String> temp = new ArrayList<String>();
            for (int j = i; j < i + length; ++j) {
                temp.add(resultSeen.get((int)j).lemma.toLowerCase().trim());
            }
            if (DataSelector.compareStringList(temp, tokenList) != 1) continue;
            Integer beginIndex = i;
            Integer endIndex = i + currentSpans.size();
            pair = new Pair<>(beginIndex, endIndex);
            flag = 1;
            break;
        }
        if(flag==0)
            return new Pair<>(0,0);
        else
            return pair;
    }


    public static Set<List<String>> annotateUser(Set<List<String>> groundTruths, List<SimpleToken> resultSeen, String eType) {
        HashSet<List<String>> currentSpans = new HashSet<List<String>>();
        for (List<String> tokenList : groundTruths) {
            int length = tokenList.size();
            for (int i = 0; i < resultSeen.size() - length + 1; ++i) {
                int j;
                ArrayList<String> temp = new ArrayList<String>();
                for (j = i; j < i + length; ++j) {
                    temp.add(resultSeen.get((int)j).lemma.toLowerCase().trim());
                }
                if (DataSelector.compareStringList(temp, tokenList) != 1) continue;
                boolean rediscovering = false;
                for (List<String> t : currentSpans) {
                    if (DataSelector.compareStringList(t, temp) != 1) continue;
                    rediscovering = true;
                    break;
                }
                if (!rediscovering) {
                    currentSpans.add(temp);
                }

                for (j = i; j < i + length; ++j) {
                    resultSeen.get((int)j).truthLabel = eType;
                }
            }
        }
        return currentSpans;
    }

    public static int compareStringList(List<String> a, List<String> b) {
        int flag = 1;
        if (a.size() != b.size()) {
            return 0;
        }
        for (int i = 0; i < a.size(); ++i) {
            if (a.get(i).toLowerCase().equals(b.get(i).toLowerCase())) continue;
            flag = 0;
            break;
        }
        return flag;
    }

    public static Set<String> addNewTokensWithGroundTruth(Set<String> foundTokens, Set<String> availableTokens, List<SimpleToken> resultSeen, String eType) {
        HashSet<String> addedTokens = new HashSet<String>();
        for (SimpleToken s : resultSeen) {
            if (!availableTokens.contains(s.lemma)) continue;
            if (!foundTokens.contains(s.lemma)) {
                addedTokens.add(s.lemma);
            }
            foundTokens.add(s.lemma);
        }
        return addedTokens;
    }

    public static void modifyDataWithAllTokensLastResult(List<SimpleToken> train, Set<String> available, String etype) {
        for (SimpleToken token : train) {
            if (!available.contains(token.lemma)) continue;
            token.truthLabel = etype;
        }
    }

    public static void modifyData(List<List<SimpleToken>> train, Set<String> found, String etype) {
        for (List<SimpleToken> list : train) {
            for (SimpleToken token : list) {
                if (!found.contains(token.lemma)) continue;
                token.truthLabel = etype;
            }
        }
    }

    public static void modifyDataWithAllTokens(List<List<SimpleToken>> train, Set<String> available, String etype) {
        for (List<SimpleToken> list : train) {
            for (SimpleToken token : list) {
                if (!available.contains(token.lemma)) continue;
                token.truthLabel = etype;
            }
        }
    }

    public static Set<String> extrapolateTokens(Set<String> source) {
        HashSet<String> extrapolatedTokens = new HashSet<String>();
        try {
            for (String s : source) {
                ProcessBuilder builder = new ProcessBuilder("./distance_mod", "/mnt/nfs/work1/jfoley/code/word2vec/make-aquaint/vector_data", s);
                builder.directory(new File("/home/smsarwar/scripts/bin"));
                Process process = builder.start();
                OutputStream stdin = process.getOutputStream();
                InputStream stdout = process.getInputStream();
                Scanner scanner = new Scanner(stdout);
                ArrayList l = new ArrayList();
                for (int i = 0; i < 10; ++i) {
                    if (!scanner.hasNextLine()) continue;
                    String val = scanner.nextLine();
                    StringTokenizer st = new StringTokenizer(val, ",");
                    String temp = st.nextToken();
                    extrapolatedTokens.add(temp);
                }
            }
        }
        catch (Exception e) {
            System.out.println(e.toString() + "Exception in finding word embeddings ");
        }
        return extrapolatedTokens;
    }

    public static List<String> extrapolateTokens(Set<String> source, IDF idf) {
        ArrayList<String> extrapolatedTokens = new ArrayList<String>();
        try {
            for (String s : source) {
                ProcessBuilder builder = new ProcessBuilder("./distance_mod", "/home/sarwar/word2vec/vector_data", s);
                builder.directory(new File("/home/sarwar/scripts/bin"));
                Process process = builder.start();
                OutputStream stdin = process.getOutputStream();
                InputStream stdout = process.getInputStream();
                Scanner scanner = new Scanner(stdout);
                for (int i = 0; i < 100; ++i) {
                    if (!scanner.hasNextLine()) continue;
                    String val = scanner.nextLine();
                    StringTokenizer st = new StringTokenizer(val, ",");
                    String temp = st.nextToken();
                    System.out.println("temp " + temp);
                    if (!idf.containsToken(temp)) continue;
                    extrapolatedTokens.add(temp);
                }
            }
        }
        catch (Exception e) {
            System.out.println(e.toString() + "Exception in finding word embeddings ");
        }
        return extrapolatedTokens;
    }

    public static void removeGroundTruthFromBase(Set<String> baseTokens, Set<String> groundTruths) {
        for (String s : groundTruths) {
            if (!baseTokens.contains(s)) continue;
            baseTokens.remove(s);
        }
    }

    public static void removeGroundTruthFeaturesFromBase(Set<String> baseTokens, Set<String> groundTruths) {
        for (String s : groundTruths) {
            String temp1 = "w[L]=" + s;
            String temp2 = "w[R]=" + s;
            if (baseTokens.contains(temp1)) {
                baseTokens.remove(temp1);
            }
            if (!baseTokens.contains(temp2)) continue;
            baseTokens.remove(temp2);
        }
    }
}
