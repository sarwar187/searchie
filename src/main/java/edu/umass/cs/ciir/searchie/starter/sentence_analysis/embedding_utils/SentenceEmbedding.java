package edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils;

import com.google.common.primitives.Doubles;
import edu.stanford.nlp.neural.Embedding;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.lemurproject.galago.core.tools.Search;
import org.lemurproject.galago.utility.Parameters;
import weka.clusterers.EM;

import java.io.*;
import java.util.*;

/**
 * Created by sarwar on 5/23/17.
 */


public class SentenceEmbedding {
    Map<String, Integer> wordEmbeddingIndex;
    List<List<Double>> embeddingValues;
    static String prefix = StaticVariables.prefix;
    //context expansion map takes the following structure:
    //(entity string, sentence ID's) -> (John Foley, 2)
    public Map<String, List<Integer>> contextExpansionMap;
    //entityEmbeddingContextExpansion is entity string to embedding mapping. These are all named entities found by the named entity tagger. However, we also have the mapping
    //for query entity. We update this map after we are done with the contextExpansionMap. But everything is converted to lowercase before getting inserted into the entityEmbeddingContextExpansionMap
    public Map<String, List<Double>> entityEmbeddingContextExpansion;
    public Map<String, Double> entityScoreMap;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Set<String> getMissingTokenEmbeddings(){
        return missingTokenEmbeddings;
    }

    public int getNumberOfMissingTokens(){
        return missingTokenEmbeddings.size();
    }

    public String environment;
    private Set<String> missingTokenEmbeddings;

    public SentenceEmbedding(){
        wordEmbeddingIndex = new HashMap<>();
        embeddingValues = new ArrayList<>();
        contextExpansionMap = new HashMap<>();
        entityEmbeddingContextExpansion = new HashMap<>();
        environment = "server";
        entityScoreMap = new HashMap<>();
        missingTokenEmbeddings = new HashSet<String>();
    }

    public static void main(String args[]) throws IOException {

        SentenceEmbedding sm = new SentenceEmbedding();
        //sm.loadWordEmbedding(idf);

    }

    //This function returns entity representation with context based approach
    public List<Double> getEntityEmbedding(List<String> typeSpans){
        String typeSpanString = "";
        for(String s: typeSpans){
            typeSpanString = typeSpanString + s.toLowerCase() + " ";
        }
        typeSpanString = typeSpanString.trim();
        return entityEmbeddingContextExpansion.get(typeSpanString);
    }

    public List<Double> getEntityEmbeddingWithSentences(Set<List<String>> typeSpans){
        double[] d = new double[StaticVariables.embedding_size];
        ArrayList<Double> entityEmbedding = new ArrayList<>(); // note, use a generic list
        for(List<String> typeSpan: typeSpans) {
            String typeSpanString = "";
            for (String s : typeSpan) {
                typeSpanString = typeSpanString + s.toLowerCase() + " ";
            }
            typeSpanString = typeSpanString.trim();
            List<Double> embedding = entityEmbeddingContextExpansion.get(typeSpanString);
            for(int i=0; i<StaticVariables.embedding_size; i++){
                d[i]+=embedding.get(i);
            }
        }
        for(int i=0; i<StaticVariables.embedding_size; i++) {
            d[i]/=(double)typeSpans.size();
        }
        for(int i=0;i<StaticVariables.embedding_size; i++)
            entityEmbedding.add(d[i]);
        return entityEmbedding;
    }


    public double getEntityScore(List<String> typeSpans){
        String typeSpanString = "";
        for(String s: typeSpans){
            typeSpanString = typeSpanString + s.toLowerCase() + " ";
        }
        typeSpanString = typeSpanString.trim();
        return entityScoreMap.get(typeSpanString);
    }


    public List<Double> getEntityEmbeddingInContext(List<String> typeSpans, int contextSentenceId){
        String typeSpanString = "";
        for(String s: typeSpans){
            typeSpanString = typeSpanString + s.toLowerCase() + " ";
        }
        typeSpanString = typeSpanString.trim();
        return entityEmbeddingContextExpansion.get(typeSpanString);
    }


    //This function takes an entity and a sentence in which it appears. Then it appends the contextExpansionMap.
    public void updateContextExpansionMap(List<String> typeSpans, int sentenceID){
        String typeSpanString = "";
        for(String s: typeSpans){
            typeSpanString = typeSpanString + s + " ";
        }
        typeSpanString = typeSpanString.trim();

        if(contextExpansionMap.containsKey(typeSpanString)){
            contextExpansionMap.get(typeSpanString).add(sentenceID);
        }else{
            List<Integer> expandedSentences = new ArrayList<>();
            expandedSentences.add(sentenceID);
            contextExpansionMap.put(typeSpanString, expandedSentences);
        }

    }

    //After the contextExpansionMap is created this function computes average of sentence embeddings to obtain entity embedding
    public void constructEntityEmbedding(double points[][]){
        for(String s: contextExpansionMap.keySet()){
            /*List<String> strList = new ArrayList<>();
            StringTokenizer st = new StringTokenizer(s, " ");
            while(st.hasMoreTokens()){
                strList.add(st.nextToken());
            }
            List<Double> entityEmbedding = getSentenceEmbedding(strList);*/
            List<Integer> sentenceIds = contextExpansionMap.get(s);
            double[] d = new double[StaticVariables.embedding_size];
            for(Integer ids: sentenceIds){
                //System.out.println(ids);
                for(int i=0; i<StaticVariables.embedding_size; i++){
                    d[i]+=points[ids][i];
                }
            }
            for(int i=0; i<StaticVariables.embedding_size; i++) {
                d[i]/=(double)sentenceIds.size();
            }
            ArrayList<Double> entityEmbedding = new ArrayList<>(); // note, use a generic list
            for(int i=0;i<StaticVariables.embedding_size; i++)
                entityEmbedding.add(d[i]);
            entityEmbeddingContextExpansion.put(s.toLowerCase(), entityEmbedding);
        }
        contextExpansionMap.clear();
    }

    public void constructEntityEmbedding(double points[][], List<SearchResult> searchResults, int bootStrapsentenceID, int embeddingSize, List<List<SimpleToken>> mergedList){
//      HashMap<Integer, Integer> searchResultHashMap = new HashMap<>();
//        for(SearchResult sr: searchResults){
//            searchResultHashMap.put(sr.getSentenceId(), sr.getSentenceId());
//        }

        List<Double> queryVector = new ArrayList<>();
        for(int i=0;i < embeddingSize; i++){
            queryVector.add(points[bootStrapsentenceID][i]);
        }


        for(String s: contextExpansionMap.keySet()){
            //LuceneInMemory lm = new LuceneInMemory();
            /*List<String> strList = new ArrayList<>();
            StringTokenizer st = new StringTokenizer(s, " ");
            while(st.hasMoreTokens()){
                strList.add(st.nextToken());
            }
            List<Double> entityEmbedding = getSentenceEmbedding(strList);*/

            List<Integer> sentenceIds = contextExpansionMap.get(s);
            //Collections.reverse(sentenceIds);
            Double[] scores = new Double[sentenceIds.size()];
            double[] d = new double[embeddingSize];
            int count = 0;
            int index = 0;

            for(Integer ids: sentenceIds){

                //lm.addTokenList(ids, mergedList.get(ids));
                //System.out.println(ids);
                //if(count<=StaticVariables.topk) {
                List<Double> candidateVector = new ArrayList<>();
                for (int i = 0; i < embeddingSize; i++) {
                    candidateVector.add(points[ids][i]);
                }
                double sim = EmbeddingSearch.cosineSimilarity(queryVector, candidateVector);
                scores[index++] = sim;
                //if(sim <= StaticVariables.threshold){
                for (int i = 0; i < embeddingSize; i++) {
                        d[i] += points[ids][i];
                }
                count++;
                //}
            }
            if(count==0) count++;
            for(int i=0; i<embeddingSize; i++) {
                d[i]/=count;
            }

            //median based thresholding
//            d = new double[embeddingSize];
//            count = 0;
//
//            Arrays.sort(scores, Collections.reverseOrder());
//            double median;
//            if(sentenceIds.size()<=3)
//                median = scores[sentenceIds.size()-1];
//            else
//                median = scores[sentenceIds.size()/2];
//
//            for(int j=0; j<scores.length; j++){
//                //lm.addTokenList(ids, mergedList.get(ids));
//                //System.out.println(ids);
//                //if(count<=StaticVariables.topk) {
//                if(scores[j]> median){
//                    for (int i = 0; i < embeddingSize; i++) {
//                        d[i] += points[sentenceIds.get(j)][i];
//                    }
//                    count++;
//                }
//            }
//
//            if(count==0) count++;
//            for(int i=0; i<embeddingSize; i++) {
//                d[i]/=count;
//            }

            ArrayList<Double> entityEmbedding = new ArrayList<>(); // note, use a generic list
            for(int i=0;i<embeddingSize; i++)
                entityEmbedding.add(d[i]);
            entityEmbeddingContextExpansion.put(s.toLowerCase(), entityEmbedding);
        }
        contextExpansionMap.clear();
    }

    //This is for embedding with ranked PRF sentences for each entity
    public void constructEntityEmbedding(double points[][], int bootStrappedSentenceID, int embeddingSize, double[] vectorLength, int[] expandedSentencesQueryEntity, List<Double> queryEmbedding){

        for(String s: contextExpansionMap.keySet()){
            List<Integer> sentenceIds = contextExpansionMap.get(s);
            //expandedSentences contain candidate sentence ids
            int[] expandedSentences = new int[sentenceIds.size()];
            for(int i = 0; i < sentenceIds.size(); i++) expandedSentences[i] = sentenceIds.get(i);

            //Now rank those candidate sentences
            List<SearchResult> searchResultList = EmbeddingSearch.rankPRFSentences(queryEmbedding, expandedSentences, points, embeddingSize, vectorLength);
            //List<SearchResult> searchResultList = EmbeddingSearch.rankPRFSentences(bootStrappedSentenceID, expandedSentences, points, embeddingSize, vectorLength);

            double[] d = new double[StaticVariables.embedding_size];
            int count = 0;
            double maxAll = 0;
            int flag = 0;
            //double weight = searchResultList.size();
            double weight = expandedSentences.length;

            double sumWeights = 0;
            //System.out.println(s + "\t");
            //Working on the evidence
            for(SearchResult searchResult: searchResultList){
            //for(Integer ids: expandedSentences){
                //System.out.println(ids);
                int ids = searchResult.getSentenceId();
                double max = 0;
                double evidence = 0;
                //checking the impact of the evidence
                for(Integer expandedSentenceId: expandedSentencesQueryEntity){
                    double temp = EmbeddingSearch.cosineSimilarity(ids, expandedSentenceId, points);
                    if(temp>max)
                        max = temp;
                    evidence+=temp;
                }

                evidence/=expandedSentences.length;
                //if(max>maxAll)
                //    maxAll = max;
                //maxAll+=evidence;
                maxAll+=max;
                if(count<=StaticVariables.topk) {
                    for (int i = 0; i < StaticVariables.embedding_size; i++) {
                        d[i] += points[ids][i];
                        //d[i] += Math.pow(2, (weight - count)) * points[ids][i];
                        //d[i] += (weight - count) * points[ids][i];
                    }
                    sumWeights+= Math.pow(2, (weight - count));
                    count++;
                }

            }
            if(count==0) count++;
            //maxAll/=count;
            for(int i=0; i<StaticVariables.embedding_size; i++) {
                d[i]/=count;
                //d[i]/= ((weight * (weight + 1))/2);
                //d[i]/= (sumWeights);
            }
            ArrayList<Double> entityEmbedding = new ArrayList<>(); // note, use a generic list
            for(int i=0;i<StaticVariables.embedding_size; i++)
                entityEmbedding.add(d[i]);
            entityEmbeddingContextExpansion.put(s.toLowerCase(), entityEmbedding);
            entityScoreMap.put(s.toLowerCase(), maxAll/count);
        }

        int count =0;
        /*for(String str: contextExpansionMap.keySet()){
            System.out.println(str + "\t" + contextExpansionMap.get(str).size());
            count++;
            if(count==100)
                break;
        }*/
        //System.out.println(contextExpansionMap.size());
        contextExpansionMap.clear();
    }


//    public void constructEntityEmbedding(List<List<SimpleToken>> mergedList, SentenceEmbedding sm){
//        for(String s: contextExpansionMap.keySet()){
//            /*List<String> strList = new ArrayList<>();
//            StringTokenizer st = new StringTokenizer(s, " ");
//            while(st.hasMoreTokens()){
//                strList.add(st.nextToken());
//            }
//            List<Double> entityEmbedding = getSentenceEmbedding(strList);*/
//            ArrayList<String> stringList = new ArrayList<>();
//            List<Integer> sentenceIds = contextExpansionMap.get(s);
//            for(Integer ids: sentenceIds){
//                List<SimpleToken> tokenList = mergedList.get(ids);
//                stringList.addAll(simpleTokenToList(tokenList));
//            }
//            List<Double> entityEmbedding = sm.getSentenceEmbedding(stringList);
//            entityEmbeddingContextExpansion.put(s.toLowerCase(), entityEmbedding);
//        }
//        contextExpansionMap.clear();
//    }


    public double computeEntitySimilarity(List<String> typeSpan1, List<String> typeSpan2){
        String typeSpanString1 = "";
        for(String s: typeSpan1){
            typeSpanString1 = typeSpanString1 + s + " ";
        }
        typeSpanString1 = typeSpanString1.trim();
        String typeSpanString2 = "";
        for(String s: typeSpan2){
            typeSpanString2 = typeSpanString2 + s + " ";
        }
        typeSpanString2 = typeSpanString2.trim();
        return EmbeddingSearch.cosineSimilarity(entityEmbeddingContextExpansion.get(typeSpanString1), entityEmbeddingContextExpansion.get(typeSpanString2));
    }

    /*public Set<Integer> setIntersection(Set<Integer> s1, Set<Integer> s2){
        Set<Integer> intersection = new HashSet<>(s1); // use the copy constructor
        intersection.retainAll(s2);
        return intersection;
    }*/


    //This is for idf weighted averaging. Usually we do not need idf weighted averaging
    public List<Double> getSentenceEmbedding(List<String> tokens, IDF idf){
        List<Double> idfValues = new ArrayList<>();
        for(String s: tokens){
            idfValues.add(idf.getIDF(s));
        }
        //System.out.println("word embedding index size "+ wordEmbeddingIndex.size());
        double sEmbedding[] = new double[StaticVariables.embedding_size];

        //List<Double> sEmbedding = new ArrayList<>();
        double idfSum = 0d;
        for(int i=0; i<tokens.size(); i++){
            if(wordEmbeddingIndex.containsKey(tokens.get(i))){
                double temp = idfValues.get(i);
                idfSum+=temp;
                //get token of sentence and its IDF value in variable temp
                //get index of token from wordEmbeddingIndex to get embeddingValues
                int index = wordEmbeddingIndex.get(tokens.get(i));
                int j=0;
                for(double d: embeddingValues.get(index)){
                    sEmbedding[j]+=(temp*d);
                    j++;
                }
            }
        }
        //System.out.println("idf sum" + "\t" + idfSum);


        List<Double> returnList = new ArrayList<>();
        if(idfSum - 0d < 0.000000001) {
            for(int i=0;i<StaticVariables.embedding_size;i++){
                returnList.add(0d);
            }
            return returnList;
        }

        for(int i=0;i<StaticVariables.embedding_size;i++){
            sEmbedding[i]/=idfSum;
            returnList.add(sEmbedding[i]);
        }
        return returnList;
        //return  Arrays.asList(sEmbedding);
    }

    //This is for averaging. The normal averaging of words to obtain sentence embedding
    public List<Double> getSentenceEmbedding(List<String> tokens){
        //System.out.println("word embedding index size "+ wordEmbeddingIndex.size());
        double sEmbedding[] = new double[StaticVariables.embedding_size];
        double idfSum = 0d;
        //List<Double> sEmbedding = new ArrayList<>();
        for(int i=0; i<tokens.size(); i++){
            if(wordEmbeddingIndex.containsKey(tokens.get(i))){
                idfSum+=1;
                int index = wordEmbeddingIndex.get(tokens.get(i));
                int j=0;
                for(double d: embeddingValues.get(index)){
                    sEmbedding[j]+=(d);
                    j++;
                }
            }else
                missingTokenEmbeddings.add(tokens.get(i));
        }
        //System.out.println("idf sum" + "\t" + idfSum);


        List<Double> returnList = new ArrayList<>();

        if(idfSum - 0d < 0.000000001) {
            for(int i=0;i<StaticVariables.embedding_size;i++){
                returnList.add(0d);
            }
            return returnList;
        }

        for(int i=0;i<StaticVariables.embedding_size;i++){
            sEmbedding[i]/=idfSum;
            returnList.add(sEmbedding[i]);
        }
        return returnList;
        //return  Arrays.asList(sEmbedding);
    }


    public List<Double> getSentenceEmbedding(List<String> tokens, List<Double> idfValues){
        //System.out.println("word embedding index size "+ wordEmbeddingIndex.size());
        double sEmbedding[] = new double[StaticVariables.embedding_size];

        //List<Double> sEmbedding = new ArrayList<>();
        double idfSum = 0d;
        for(int i=0; i<tokens.size(); i++){
            if(wordEmbeddingIndex.containsKey(tokens.get(i))){
                double temp = idfValues.get(i);
                idfSum+=temp;
                //get token of sentence and its IDF value in variable temp
                //get index of token from wordEmbeddingIndex to get embeddingValues
                int index = wordEmbeddingIndex.get(tokens.get(i));
                int j=0;
                for(double d: embeddingValues.get(index)){
                    sEmbedding[j]+=(temp*d);
                    j++;
                }
            }
        }
        //System.out.println("idf sum" + "\t" + idfSum);
        List<Double> returnList = new ArrayList<>();
        if(idfSum - 0d < 0.000000001) {
            for(int i=0;i<StaticVariables.embedding_size;i++){
                returnList.add(0d);
            }
            return returnList;
        }

        for(int i=0;i<StaticVariables.embedding_size;i++){
            sEmbedding[i]/=idfSum;
            returnList.add(sEmbedding[i]);
        }
        //if(returnList.size()==199){System.out.println("caught");}
        return returnList;
        //return  Arrays.asList(sEmbedding);
    }

    //This function loads word embedding givens the terms in a training file
    public void loadWordEmbedding(IDF idf) throws FileNotFoundException {
        String fileName;
        if(environment.equals("server"))
            fileName = "/mnt/nfs/work1/smsarwar/searchie/vector_data_text/vector_data.txt";
            //fileName = "/mnt/nfs/work1/smsarwar/searchie/vector_data_text/google_news.txt";
        else
            fileName = "/home/smsarwar/word2vec/vector_data.txt";

        try (Scanner sc = new Scanner(new File(fileName))) {
            //try (Scanner sc = new Scanner(new File("/home/smsarwar/word2vec/vector_data.txt"))) {
            int i = 0;
            String str = sc.nextLine();
            //System.out.println(str);
            while(sc.hasNextLine()){
                str = sc.nextLine();
                //System.out.println("str " + str);
                StringTokenizer st = new StringTokenizer(str, " ");
                String token = st.nextToken();
                if(idf.containsToken(token)) {
                    //System.out.println("token " + token);
                    //System.out.println(st.countTokens());
                    wordEmbeddingIndex.put(token, i);
                    List<Double> l = new ArrayList<>();
                    while (st.hasMoreTokens()) {
                        l.add(Double.parseDouble(st.nextToken()));
                    }
                    embeddingValues.add(l);
                    //System.out.println(l.size());
                    i++;
                    //if (i == 10)
                    //    break;
                    //System.out.println("i " + i);
                }
            }
            //System.out.println("i " + i);
            //System.out.println("list size " + wordEmbeddingIndex.size());
        }catch(Exception e){
            throw e;
            //System.out.println(e);
        }
    }
        //public static List<Double>


    class WordScore{
        String word;
        Double score;
        WordScore(String word){
            this.word = word;
            this.score = 0d;
        }
    }
    public List<String> getContextWordsWithEntitySimilarity(List<SimpleToken> userQuery, Set<List<String>> foundSpans, int topk){
        List<String> nonEntityString = new ArrayList<>();
         for(SimpleToken st: userQuery){
             if(!st.truthLabel.equals("LIST")){
                 nonEntityString.add(st.lemma);
            }
        }
        List<List<Double>> entityEmbedding = new ArrayList<>();
        List<List<Double>> contextWordEmbedding = new ArrayList<>();
        for(List<String> entityString: foundSpans) {
            entityEmbedding.add(getSentenceEmbedding(entityString));
        }
        for(String contextWord: nonEntityString){
            List<String> contextWordList = new ArrayList<>();
            contextWordList.add(contextWord);
            contextWordEmbedding.add(getSentenceEmbedding(contextWordList));
        }

        List<WordScore> wordScoreList = new ArrayList<>();

        for(String contextWord: nonEntityString){
            wordScoreList.add(new WordScore(contextWord));
        }

        for(List<Double> e: entityEmbedding){
            for(List<Double> w: contextWordEmbedding){
                Double sim = EmbeddingSearch.cosineSimilarity(e,w);
                for(WordScore ws: wordScoreList){
                    if(ws.word.equals(w)){
                        ws.score+=sim;
                    }
                }
            }
        }
        Collections.sort(wordScoreList, (o1,o2) -> Double.compare(o2.score, o1.score));
        List<String> result = new ArrayList<>();
        for(int i=0; i<topk; i++){
            if(i<wordScoreList.size())
                result.add(wordScoreList.get(i).word);
        }
        return result;
    }

    //This was created to use a processbuilder approach to use the C file provided by Google to use the word embedding
    public static List<Double> sentenceEmbedding(String source, String sourceIdf){
        //method: take from source, extraploate and put into destination. destination is the currently explored tokens
        List<Double> l = new ArrayList<>();
        //String another = "another";
        try {
            if(source.length()>3999 || sourceIdf.length()>=3999 || source.length()==0 || sourceIdf.length()==0){
                for(int i=0;i<StaticVariables.embedding_size;i++)
                    l.add(0d);
                return l;
            }

            //ProcessBuilder builder = new ProcessBuilder("./distance", "/mnt/nfs/work1/jfoley/code/word2vec/make-aquaint/vector_data", source.trim(), sourceIdf.trim());
            ProcessBuilder builder = new ProcessBuilder("./distance", "/home/smsarwar/word2vec/vector_data", source, sourceIdf); //sarwar hoilo local smsarwar international
            //System.out.println(builder.command());
            //builder.directory(new File("/home/sarwar/word2vec/bin"));
            builder.directory(new File("/home/smsarwar/scripts/bin"));
            Process process = builder.start();
            //process.waitFor();
            OutputStream stdin = process.getOutputStream(); // <- Eh?
            InputStream stdout = process.getInputStream();

            //BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
            //BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));
            //Thread.sleep(3000);
            //writer.write("swingley");
            //writer.flush();
            //writer.close();
            StringTokenizer st;
            Scanner scanner = new Scanner(stdout);
            //System.out.println("source " + source);
            //System.out.println("source length " + source.length());
            if (scanner.hasNextLine()) {
                //System.out.println("loop entered");
                String val = scanner.nextLine();
                //System.out.println(val);
                int flag = 0;
                st = new StringTokenizer(val, "\t");
                while(st.hasMoreTokens()){
                    String temp = st.nextToken();
                    if(Doubles.tryParse(temp)!=null) {
                        l.add(Double.parseDouble(temp));
                    }else{
                        //System.out.println("error parsing " + temp);
                        flag = 1;
                        l.add(0d);
                    }
                }
                    /*if(flag==1) {
                        System.out.println("error parsing");
                        System.out.println(source);
                        System.out.println();
                        System.out.println(sourceIdf);
                    }*/
                //System.out.println("size of l " + l.size());
                //String temp = st.nextToken();
                //System.out.println("temp " + temp);
                //destination.add(temp);
                //extrapolatedTokens.add(temp);
            }
            //System.out.println("embedding ends");

        }catch(Exception e){
            System.out.println(e.toString() + "Exception in finding word embeddings ");
        }
        return l;
    }

    @Override
    public String toString() {
        return wordEmbeddingIndex.size() + "\t" + embeddingValues.size();
        //return super.toString();
    }

    public static List<Double> addEmbeddings(List<Double> op1, List<Double> op2){
        List<Double> output = new ArrayList<>();
        for(int i=0; i< op1.size(); i++){
            output.add(op1.get(i) + op2.get(i));
        }
        return output;
    }

    public static boolean checkZeroVector(List<Double> vector){
        boolean flag = true;
        for(Double d: vector){
            if(d > 0d){
                flag = false;
                break;
            }
        }
        return flag;
    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList) {
        ArrayList<String> strList = new ArrayList<String>();
        for (SimpleToken token : tokenList) {
            if (Stopwords.isStemmedStopword(token.lemma)) continue;
            strList.add(token.lemma);
        }
        return strList;
    }
}
