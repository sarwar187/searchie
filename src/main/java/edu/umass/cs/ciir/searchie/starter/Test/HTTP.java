package edu.umass.cs.ciir.searchie.starter.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by sarwar on 6/30/17.
 */
public class HTTP {
    public static void main(String args[]) throws IOException {
        //String str = "1,2;3,4;5,6";
        String str1 = "trecqalist_75.5_1000.crfsuite,6409,1,0,0,0,0,0,0,3,5,0,3,0,0,0,0,2,1,12,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,20,7,0,0,4,0,0,3,0,0,0,0,1,0.777,35,1";
        String str2 = "trecqalist_75.5_1000.crfsuite,3771,4,1,0,1,0,0,0,3,6,0,4,1,0,0,0,8,2,4,0,0,1,2,1,0,0,0,0,2,0,2,1,0,0,0,2,2,1,46,3,0,0,1,0,0,0,0,0,0,0,0,0.777,50,0";
        String str = str1 + ";" + str2;
        /*for(int i=0; i<1000;i++){
            str+="test";
        }*/
        System.out.println(str.length());
        try (java.util.Scanner s = new java.util.Scanner(new java.net.URL("http://127.0.0.1:5000/user/"+ str).openStream())) {
            System.out.println(s.useDelimiter("\\A").next());
        }
        //System.out.println(executePost("http://127.0.0.1:5000/user/a", "1"));
    }


    public static String executePost(String targetURL, String urlParameters) {
        HttpURLConnection connection = null;

        try {
            //Create connection
            URL url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length",
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
