package edu.umass.cs.ciir.searchie.starter.Test.volume_example;
import edu.umass.cs.ciir.searchie.starter.Test.volume_example.Volume;

import java.util.List;

public class VolumeContainer {
    public String vol;
    public List<Volume> volumes;
}
