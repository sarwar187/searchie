package edu.umass.cs.ciir.searchie.starter.learning_to_rank;

import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import org.lemurproject.galago.utility.Parameters;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.supervised.instance.Resample;
import weka.filters.unsupervised.attribute.Remove;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by sarwar on 6/26/17.
 */
public class WekaTrain {
    public static void main(String args[]) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        try{
            //Scanner sc = new Scanner(new File("/mnt/nfs/work1/smsarwar/searchie/ltr/train/train.csv")

            ConverterUtils.DataSource sourceTrain;
            sourceTrain = new ConverterUtils.DataSource("/home/sarwar/crfsuite/ltr/train.arff");
            //sourceTrain = new ConverterUtils.DataSource("/mnt/nfs/work1/smsarwar/searchie/ltr/train/train.arff");
            Instances train = sourceTrain.getDataSet();
            String[] options = new String[2];
            options[0] = "-R";                                    // "range"
            options[1] = "1,2";                                     // first attribute
            //options[2] = "2";                                     // first attribute
            //options[3] = "4";
            //options[2] = "9";                                     // first attribute
            //options[3] = "3";                                     // first attribute
            //options[4] = "4";                                     // first attribute

            Remove remove = new Remove();                         // new instance of filter
            remove.setOptions(options);                           // set options
            remove.setInputFormat(train);                          // inform filter about dataset **AFTER** setting options
            Instances newData = Filter.useFilter(train, remove);   // apply filter
            System.out.println("number of attributes " + newData.numAttributes());
            System.out.println(newData.firstInstance());

            if (newData.classIndex() == -1) {
                newData.setClassIndex(newData.numAttributes() - 1);
            }

            /*Resample sampler = new Resample();
            String Fliteroptions="-B 1.0";
            sampler.setOptions(weka.core.Utils.splitOptions(Fliteroptions));
            sampler.setRandomSeed((int)System.currentTimeMillis());
            sampler.setInputFormat(newData);
            newData = Resample.useFilter(newData, sampler);*/
            //Normalize normalize = new Normalize();
            //normalize.toSource(Fliteroptions, newData);
            //Remove remove = new Remove();                         // new instance of filter
            //remove.setOptions(options);                           // set options
            //remove.setInputFormat(train);                          // inform filter about dataset **AFTER** setting options
            //Instances newData = Filter.useFilter(train, remove);   // apply filter

            //rm.setAttributeIndices("2");
            //rm.setAttributeIndices("3");
            //rm.setAttributeIndices("4");
            //rm.setAttributeIndices("5");
            //rm.setAttributeIndices("6");



            //rm.setAttributeIndices("6");
            //rm.setAttributeIndices("5");


            //Remove rm = new Remove();
            //rm.setInputFormat(train);
            //rm.setAttributeIndices("1");
            //FilteredClassifier fc = new FilteredClassifier();
            //cls.setOptions(args);
            //J48 cls = new J48();
            //LibSVM cls = new LibSVM();
            //SMO cls = new SMO();
            Logistic cls = new Logistic();
            cls.setRidge(1e-8);
            cls.buildClassifier(newData);
            //BayesianLogisticRegression cls = new BayesianLogisticRegression();
            //cls.setThreshold(0.52);
            //AdaBoostM1 cls = new AdaBoostM1();
            //NaiveBayes cls = new NaiveBayes();
            //weka.classifiers.meta.Bagging cls = new Bagging();
            //weka.classifiers.functions.IsotonicRegression cls = new IsotonicRegression();
            //j48.setUnpruned(true);        // using an unpruned J48
            // meta-classifier

            //BayesNet cls = new BayesNet();
            //RandomForest cls = new RandomForest();
            //cls.setNumTrees(100);
            //cls.setMaxDepth(3);
            //cls.setNumFeatures(3);

            //fc.setClassifier(cls);
            //fc.setFilter(rm);

            // train and make predictions
            //System.out.println(fc.globalInfo());
            //System.out.println(fc.getFilter());
            //fc.buildClassifier(train);
            //Evaluation eval = new Evaluation(newData);
            //Random rand = new Random(1);  // using seed = 1
            //int folds = 2;
            //eval.crossValidateModel(cls, newData, folds, rand);
            //System.out.println(eval.toSummaryString());
            //System.out.println("precision on buy " + eval.precision(newData.classAttribute().indexOfValue("buy")));

            //System.out.println("recall on buy " + eval.recall(newData.classAttribute().indexOfValue("buy")));
            //System.out.println(eval.confusionMatrix().toString());
            //System.out.println("Precision " + eval.precision(newData.classIndex()-1));
            //System.out.println("Recall " + eval.recall(newData.classIndex()-1));
            //Classfier cls = new weka.classifiers.bayes.NaiveBayes();
            //FilteredClassifier fc = new FilteredClassifier();
            //fc.setFilter(rm);
            //fc.setClassifier(cls);

            //train and make predictions
            //fc.buildClassifier(train);

            // serialize model

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/home/sarwar/crfsuite/ltr/train.model"));
            //ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/mnt/nfs/work1/smsarwar/searchie/ltr/train.model"));
            oos.writeObject(cls);
            oos.flush();
            oos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
