package edu.umass.cs.ciir.searchie.starter.sentence_analysis;

import ciir.jfoley.chai.io.TemporaryDirectory;
import edu.umass.cs.ciir.searchie.starter.*;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.Metrics;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import gnu.trove.map.hash.TObjectFloatHashMap;
import org.lemurproject.galago.utility.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by sarwar on 5/30/17.
 */
public class SearchWithCRF{
    static String prefix = StaticVariables.prefix;

    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");

        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);

        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();
        List<List<SimpleToken>> pseudoPositives = new ArrayList<>();
        IDF idf = new IDF();
        LuceneInMemory lucene = new LuceneInMemory();
        int index = 0;
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            lucene.addTokenList(index, sent);
            index++;
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        //lucene.close();

        int size = fullPositives.size();
        //int totalSize = fullConllTrain.size();
        int totalSize = fullConllTrain.size();
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        List<List<SimpleToken>> positives = new ArrayList<>();

        mergedList.addAll(fullPositives);

        for(int i=0;i<totalSize-size;i++){
            mergedList.add(fullNegatives.get(i));
        }
        positives.add(mergedList.get(0));
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);

        //Set<List<String>> foundSpans = new HashSet<>();
        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        List<Integer> l = lucene.search(mergedList.get(0));

        /*double rankingPoint = 1, averagePrecision = 0, foundSoFar = 0;
        for(Integer i: l){
            positives.add(fullConllTrain.get(i));
            if(DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, fullConllTrain.get(i), etype)==1){
                foundSoFar++;
                averagePrecision+=(foundSoFar/rankingPoint);
            }
            rankingPoint++;
        }*/

        for(Integer i: l) {
            positives.add(fullConllTrain.get(i));
            //DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, fullConllTrain.get(i), etype);
        }

        List<List<SimpleToken>> bootstrappedSentence = new ArrayList<>();
        bootstrappedSentence.add(mergedList.get(0));

        try (TemporaryDirectory tmpdir = new TemporaryDirectory()) {
            CRFSuiteLearner learner = new CRFSuiteLearner(tmpdir, argp.get("crfsuite", defaultCRFSuiteBinary));
            learner.setModel(argp.get("model", "lbfgs"));
            Parameters info = Parameters.create();
            TObjectFloatHashMap<String> originalMap = learner.learnFeatureWeights(bootstrappedSentence, etype, info);
            LinearTokenClassifier rf;
            SearchResult searchResult;
            Map<String, TokenCount> retrievedTokenMap = new HashMap<>();
            ArrayList<SearchResult> resultsSeen = new ArrayList<>();
            int unsuccessful = 0;
            for (int i = 0; i < StaticVariables.numRelevanceFeedbacks; i++) {
                rf = new LinearTokenClassifier(originalMap);
                //searchResult = Metrics.addRelevanceFeedback(rf, fullConllTest, positives, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                //searchResult = Metrics.addRelevanceFeedback(rf, positives, bootstrappedSentence, etype, resultsSeen, StaticVariables.isUnique, retrievedTokenMap);
                int successful = 0;
                successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, resultsSeen.get(i).getSimpleTokens(), etype);
                if (successful != 1) {
                    unsuccessful++;
                }
                if (unsuccessful > 2) {
                    StaticVariables.isUnique = StaticVariables.isUnique ^ true;
                    unsuccessful = 0;
                }
                originalMap = learner.learnFeatureWeights(bootstrappedSentence, etype, info);
            }
            PrintResults printResults = new PrintResults(resultDirectory, inputFileName);
            //printResults.printSimpleTokenList(bootstrappedSentence, "found sentences");
        }
        //System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + inputFileName + "\t" + (averagePrecision/availableSpans.size()));
        System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + inputFileName);

    }


}