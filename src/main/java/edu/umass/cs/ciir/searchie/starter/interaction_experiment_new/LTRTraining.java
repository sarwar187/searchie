package edu.umass.cs.ciir.searchie.starter.interaction_experiment_new;

import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureInfo;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.Stopwords;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by sarwar on 7/5/17.
 */
public class LTRTraining {
    static String prefix = StaticVariables.prefix;
    //public static String defaultCRFSuiteBinary = prefix + "bin//crfsuite";
    public static void main(String[] args) throws IOException {
        Parameters argp = Parameters.parseArgs(args);
        //int numTraining = argp.get("trainingStart", 3);
        String etype = argp.get("class", "LIST");
        String defaultCRFSuiteBinary = argp.get("crfsuite", prefix + "bin//crfsuite");
        String inputDirectory = argp.get("dir", StaticVariables.aquaint_prefix);
        String inputFileName = argp.get("file", "train.crfsuite");
        File trainFile = new File(inputDirectory + inputFileName);
        String featureDirectory = argp.get("fdir", StaticVariables.loc_dump_prefix);
        String featureFileName = argp.get("ffile", "dump_list.out");
        String resultDirectory = argp.get("rdir", "result_directory");
        int numPositives = Integer.parseInt(argp.get("p", "3"));
        int maximumNumberOfExperiments = Integer.parseInt(argp.get("maxexp", StaticVariables.maxExperiments));
        int embeddingSize = 200;
        List<List<SimpleToken>> fullConllTrain = SimpleToken.loadCRFSuiteInputFormat(trainFile);
        int occurrence[] = new int[100];
        //System.out.println("Training data loaded: " + fullConllTrain.size() + " sentences.");
        //Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        List<List<SimpleToken>> fullPositives = new ArrayList<>();
        List<List<SimpleToken>> fullNegatives = new ArrayList<>();

        IDF idf = new IDF();
        for (List<SimpleToken> sent : fullConllTrain) {
            idf.addSentence(sent);
            boolean isPositive = false;
            for (SimpleToken token : sent) {
                if (token.getLabel().equals(etype)) {
                    isPositive = true;
                    break;
                }
            }
            if (isPositive) {
                fullPositives.add(sent);
            } else {
                fullNegatives.add(sent);
            }
        }
        int size = fullPositives.size();
        int totalSize = fullConllTrain.size();
        //merged list construction begins
        List<List<SimpleToken>> mergedList = new ArrayList<>();
        List<List<SimpleToken>> positives = new ArrayList<>();
        mergedList.addAll(fullPositives);
        for(int i=0;i<totalSize-size;i++){
            mergedList.add(fullNegatives.get(i));
        }
        //mergedlist construction ends
        positives.add(mergedList.get(0));
        Set<List<String>> foundSpans = DataSelector.getUniqueSpans(positives, etype);
        Set<List<String>> constantSpans = DataSelector.getUniqueSpans(positives, etype);
        Set<List<String>> availableSpans = DataSelector.getUniqueSpans(fullPositives, etype);
        //Now loading the sentence embedding two dimensional matrix
        //prior would increase weight for the query entity
        double prior[] = new double[mergedList.size()];
        double vectorLength[] = new double[totalSize];
        //points array would hold embedding vectors for all the sentences
        double points[][] = new double[totalSize][embeddingSize];
        //BufferedWriter br = new BufferedWriter(new FileWriter(new File(featureDirectory + "//embedding//test//" + inputFileName)));
        SentenceEmbedding sm = new SentenceEmbedding();
        //only load words that appear in the idf word vector. Then get embedding for sentences.
        sm.loadWordEmbedding(idf);
        int row = 0;
        int countPRF = 0;
        //LuceneInMemory lucene = new LuceneInMemory();
        //pr.printSimpleToken(mergedList.get(0), "query");
        for(List<SimpleToken> tokenList: mergedList){
            //lucene.addTokenList(row, tokenList);
            if(DataSelector.containsFoundEntity(availableSpans, tokenList, etype, foundSpans)==1) {
                prior[row] = 10;
                countPRF++;
                //countPRF is the number of sentences having found entity
            }
            //List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList), idf);
            //this version of embedding is without idf
            List<Double> doubleList  = sm.getSentenceEmbedding(simpleTokenToList(tokenList));
            int col = 0;
            for(double d: doubleList){
                points[row][col++] = d;
                vectorLength[row]+= (d*d);
            }
            if(vectorLength[row]-0d < 0.00000001) vectorLength[row]=1;
            row++;
        }

        for(int i=0;i<vectorLength.length;i++){
            vectorLength[i] = Math.sqrt(vectorLength[i]);
            if(vectorLength[i] - 0d < 0.00000001) vectorLength[row]=1;
        }

        //CONTEXT EXPANSION begins here
        //String expansionApproach = "LUCENE";
        String expansionApproach = "EMBEDDING";
        //String expansionApproach = "FULL";
        //String expansionApproach = "NONE";
        int bootstrapSentenceId = 0;
        int [] expandedSentences = new int[countPRF];
        //int [] expandedSentences = new int[fullPositives.size()];
        double[] expandedSentencesWeight = new double[countPRF];
        //List<Integer> l = lucene.search(mergedList.get(bootstrapSentenceId));
        //Map<Integer, Double> hashMap = lucene.search(mergedList.get(bootstrapSentenceId), StaticVariables.sizeOfInitialRetrieval);
        List<Integer> l = new ArrayList<>();
        if(expansionApproach.equals("LUCENE")) {
            row = 0;
            for (Integer s : l) {
                expandedSentences[row++] = s;
                if (row == countPRF)
                    break;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }else if(expansionApproach.equals("EMBEDDING")) {
            row = 0;
            for (SearchResult s : EmbeddingSearch.search(bootstrapSentenceId, points, totalSize, embeddingSize, prior, vectorLength)) {
                expandedSentences[row] = s.getSentenceId();
                expandedSentencesWeight[row] = countPRF - row;
                row++;
                //System.out.println(s.getScore());
                //pr.printSimpleToken(mergedList.get(s.getSentenceId()), "found");
                if (row == countPRF)
                    break;
            }
            //int [] expandedSentences = {bootstrapSentenceId};
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            //EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, expandedSentencesWeight, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
            //context expansion done. the vector is pasted at location 0 of points array
        }else if(expansionApproach.equals("FULL")) {
            for(int i=0; i<fullPositives.size(); i++){
                expandedSentences[i] = i;
            }
            EmbeddingSearch.getAggregatedRepresentationCopiedtoZero(points, expandedSentences, embeddingSize);
            vectorLength[bootstrapSentenceId] = EmbeddingSearch.getVectorLength(points, bootstrapSentenceId, embeddingSize);
        }

        //CONTEXT Expansion Completed
        double rankingPoint = 1, averagePrecision = 0, foundSoFar = 0;
        setPrior(prior, 0d);
        List<SearchResult> results = EmbeddingSearch.search(bootstrapSentenceId, points, totalSize, embeddingSize, prior, vectorLength);
        BufferedWriter br= new BufferedWriter(new FileWriter(new File(featureDirectory + featureFileName)));
        if(featureDirectory.contains("test")) {
            br.write(FeatureInfo.getFeatureString());
            br.newLine();
        }
        //br.write(FeatureInfo.getFeatureString());
        //br.newLine();
        NumberFormat formatter = new DecimalFormat("#0.000");

        for (SearchResult s : results) {
            //positives.add(mergedList.get(s.getSentenceId()));
            //int successful = DataSelector.addNewSpansWithGroundTruth(constantSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            int successful = DataSelector.addNewSpansWithGroundTruth(foundSpans, availableSpans, mergedList.get(s.getSentenceId()), etype);
            //foundSpans.addAll(constantSpans);
            //constantSpans.clear();
            //constantSpans = DataSelector.getUniqueSpans(positives, etype);
            String featureString = inputFileName + "," + s.getSentenceId() + ",";
            FeatureExtractor fe = new FeatureExtractor(mergedList.get(s.getSentenceId()));
            featureString += fe.posNERFeatures();
            int containsFoundEntity = DataSelector.containsFoundEntity(availableSpans, mergedList.get(s.getSentenceId()), etype, foundSpans);
            featureString+= String.valueOf(containsFoundEntity) + ",";
            //List<SimpleToken> t = mergedList.get(i).get();
            if(successful==1) {
                //if(hashMap.containsKey(i)) {
                //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 1);
                featureString += (formatter.format(s.getScore()) + "," + mergedList.get(s.getSentenceId()).size() + "," + 1);
                //System.out.println(featureString);
                br.write(featureString);
                br.newLine();
                //}
            }else{
                //if(hashMap.containsKey(i)) {
                //featureString += (formatter.format(score) + "," + formatter.format(hashMap.get(i).doubleValue()) + "," + mergedList.get(i).size() + "," + 0);
                featureString += (formatter.format(s.getScore()) + "," + mergedList.get(s.getSentenceId()).size() + "," + 0);
                //System.out.println(featureString);
                br.write(featureString);
                br.newLine();
                //}
            }
            //PrintResults pr = new PrintResults();
            //pr.printSimpleToken(mergedList.get(i), "input");

        }
        System.out.println(availableSpans.size() + "\t" + foundSpans.size() + "\t" + (1.0 * foundSpans.size()/availableSpans.size()));
        br.close();
    }

    public static void setPrior(double prior[], double val){
        for(int i=0;i<prior.length;i++)
            prior[i] = val;
    }

    public static ArrayList<String> simpleTokenToList(List<SimpleToken> tokenList){
        ArrayList<String> strList = new ArrayList<>();

        //Set<String> filtered = idf.getTopkList(tokenList, tokenList.size());
        for(SimpleToken token: tokenList){
            //String str = token.lemma.replaceAll("[^0-9a-zA-Z]+","");
            /*if (token.lemma.equals("-lrb-") || token.lemma.equals("-rrb-") || token.lemma.equals("&ql;"))
                continue;
            if (token.lemma.length() > 2) {
            }*/
            if(!Stopwords.isStemmedStopword(token.lemma)){
                strList.add(token.lemma);
            }
        }
        return strList;
        //return s.trim();
    }
}
