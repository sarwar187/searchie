package edu.umass.cs.ciir.searchie.starter.interaction_experiment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.StaticVariables;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.DocumentContainer;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.QueryFileParser;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Sentence;
import edu.umass.cs.ciir.searchie.starter.Test.document_example.Token;
import edu.umass.cs.ciir.searchie.starter.chiir2018.BatchHandler;
import edu.umass.cs.ciir.searchie.starter.data_engineering.DataSelector;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.SearchResult;
import edu.umass.cs.ciir.searchie.starter.learning_to_rank.FeatureExtractor;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.LuceneUtils.LuceneInMemory;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.EmbeddingSearch;
import edu.umass.cs.ciir.searchie.starter.sentence_analysis.embedding_utils.SentenceEmbedding;
import edu.umass.cs.ciir.searchie.starter.utils.Evaluation;
import edu.umass.cs.ciir.searchie.starter.utils.IDF;
import edu.umass.cs.ciir.searchie.starter.utils.PrintResults;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

public class RFUDExperiment {
    private static final Logger logger = Logger.getLogger(TwoOneUExperiment.class.getName());
    static String prefix = StaticVariables.prefix;

    public static void main(String[] args) throws IOException {
        logger.setLevel(Level.FINE);
        StaticVariables.threshold = 0.1;
        StaticVariables.expansionRM3 = 1;
        int embeddingSize = StaticVariables.embedding_size;
        LuceneInMemory lucene = new LuceneInMemory();
        PrintResults pr = new PrintResults();

        //Parameter handling
        Parameters argp = Parameters.parseArgs(args);
        String etype = argp.get("class", "LIST");
        String inputDirectory = argp.get("dir", "/home/smsarwar/");
        String inputFileName = argp.get("file", "trecqalist_99.1_1000.jsonl.gz");
        String hostName = InetAddress.getLocalHost().getHostName();
        HashMap<String, Integer> bootstrapHashMap = BatchHandler.loadBootstrapSentence();
        int bootstrapSentenceId = bootstrapHashMap.get(inputFileName);
        System.out.println(inputFileName);
        return;
    }
}

