package edu.umass.cs.ciir.searchie.starter.feature_engineering;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Created by sarwar on 12/10/16.
 */
public class Translator {
    public ArrayList<FeatureWeight> a;
    public Translator(){

    }

    public Translator(ArrayList<FeatureWeight> a){
        this.a = a;
    }

    public float translateLabel(int label_local, int maxLabel){
         return (float) a.get(((a.size()/maxLabel)*label_local)-1).getWeight();
    }

    public float translateLabel(int label_local, int maxLabel, int factor){
        //When we are using error with label_local, we are already keeping it into a range of 10. So, no IndexOutOfBounds exception.
        if(label_local==0) label_local++;
        if(label_local==-1) return 0; //can be negative 1
        float multiplier = (float) label_local;
        multiplier = multiplier/(float) maxLabel;
        multiplier = (float) a.size() * multiplier;
        int index = (int) multiplier;
        if (a.size() == 0)
            return 0;
        else if(a.size() == 1)
            return (float) a.get(0).getWeight();
        else
            return (float) ((a.get(index-1).getWeight()));
    }

    public float truncateFeature(double featureWeight){
        Double toBeTruncated = new Double(featureWeight);
        Double truncatedDouble = BigDecimal.valueOf(toBeTruncated).setScale(1, RoundingMode.HALF_UP).doubleValue();
        //optimalWeightList.get(i).quantized = truncatedDouble.intValue();
        //modifiedWeights.put(key, optimalWeights.get(key));
        return truncatedDouble.floatValue();
    }

    public float truncateFeatureRange(double featureWeight, int round){
        Double toBeTruncated = new Double(featureWeight);
        Double truncatedDouble = BigDecimal.valueOf(toBeTruncated).setScale(1, RoundingMode.HALF_UP).doubleValue();
        for(int i=0; i<round ;i++) {
            Double furtherTruncated = new Double(truncatedDouble / 2);
            truncatedDouble = BigDecimal.valueOf(furtherTruncated).setScale(1, RoundingMode.HALF_UP).doubleValue();
        }
        //converting weights to [-5 to 5]
        //optimalWeightList.get(i).quantized = truncatedDouble.intValue();
        //modifiedWeights.put(key, optimalWeights.get(key));
        return truncatedDouble.floatValue();
    }
}
