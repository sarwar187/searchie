package edu.umass.cs.ciir.searchie.starter.learning_to_rank;

import ciir.jfoley.chai.collections.Pair;
import edu.umass.cs.ciir.searchie.starter.SimpleToken;
import edu.umass.cs.ciir.searchie.starter.feature_engineering.FeatureSelector;
import edu.umass.cs.ciir.searchie.starter.utils.FeatureFilters;

import java.util.*;

/**
 * Created by sarwar on 6/14/17.
 */
public class FeatureExtractor {

    //numberOfNoun
    String posTagFeatures[];
    String nerFeatures[];
    boolean truthArray[];
    Map<String, String> tokenToEntityType;
    List<SimpleToken> tokenList;

    public String[] getNerFeatures() {
        return nerFeatures;
    }

    public void setNerFeatures(String[] nerFeatures) {
        this.nerFeatures = nerFeatures;
    }

    public int getIntersectionWithQueryEntity(String targetEntityType){
        int count=0;
        for(int i=0;i<nerFeatures.length;i++){
            if(nerFeatures[i].equals(targetEntityType))
                count++;
        }
        return count;
    }

    public FeatureExtractor(List<SimpleToken> tokenList){
        this.tokenList = tokenList;
        tokenToEntityType = new HashMap<>();
        posTagFeatures = new String[tokenList.size()];
        nerFeatures = new String[tokenList.size()];
        truthArray = new boolean[tokenList.size()];

        int index = 0;
        for(SimpleToken token: tokenList){
            Set<String> tokenFeatures = token.getFeatures();
            for(String tokenFeature: tokenFeatures){
                if(FeatureFilters.featureFilterPOSZERO(tokenFeature)==1){
                    Pair p = parseFeature(tokenFeature);
                    posTagFeatures[index] = (String) p.right;
                }else if(FeatureFilters.featureFilterNERZERO(tokenFeature)==1){
                    Pair p = parseFeature(tokenFeature);
                    nerFeatures[index] = (String) p.right;
                    if(!tokenToEntityType.containsKey(p.left)){
                        String t = (String) p.left;
                        tokenToEntityType.put(t.toLowerCase(), (String) p.right);
                    }
                }
            }

            if(token.truthLabel.equals("LIST"))
                truthArray[index] = true;
            else
                truthArray[index] = false;

            if(posTagFeatures[index]==null) posTagFeatures[index] = PartOfSpeech.DETERMINER.toString();
            if(nerFeatures[index]==null) nerFeatures[index] = "O";
            index++;
        }
    }

    public FeatureExtractor(List<SimpleToken> tokenList, String targetEntityType){
        this.tokenList = tokenList;
        tokenToEntityType = new HashMap<>();
        posTagFeatures = new String[tokenList.size()];
        nerFeatures = new String[tokenList.size()];
        truthArray = new boolean[tokenList.size()];

        int index = 0;
        for(SimpleToken token: tokenList){
            Set<String> tokenFeatures = token.getFeatures();
            for(String tokenFeature: tokenFeatures){
                if(FeatureFilters.featureFilterPOSZERO(tokenFeature)==1){
                    Pair p = parseFeature(tokenFeature);
                    posTagFeatures[index] = (String) p.right;
                }else if(FeatureFilters.featureFilterNERZERO(tokenFeature)==1){
                    Pair p = parseFeature(tokenFeature);
                    nerFeatures[index] = (String) p.right;
                    if(!tokenToEntityType.containsKey(p.left)){
                        String t = (String) p.left;
                        tokenToEntityType.put(t.toLowerCase(), (String) p.right);
                    }
                }
            }

            if(nerFeatures[index].equals(targetEntityType))
                truthArray[index] = true;
            else
                truthArray[index] = false;

            if(posTagFeatures[index]==null) posTagFeatures[index] = PartOfSpeech.DETERMINER.toString();
            if(nerFeatures[index]==null) nerFeatures[index] = "O";
            index++;
        }
    }

    //This function creates (entity, entity type, context) triplets
//    public CreateTokenEntityPairs(List<SimpleToken> tokenList, String targetEntityType){
//        this.tokenList = tokenList;
//        tokenToEntityType = new HashMap<>();
//        posTagFeatures = new String[tokenList.size()];
//        nerFeatures = new String[tokenList.size()];
//        truthArray = new boolean[tokenList.size()];
//
//        int index = 0;
//        for(SimpleToken token: tokenList){
//            Set<String> tokenFeatures = token.getFeatures();
//            for(String tokenFeature: tokenFeatures){
//                if(FeatureFilters.featureFilterPOSZERO(tokenFeature)==1){
//                    Pair p = parseFeature(tokenFeature);
//                    posTagFeatures[index] = (String) p.right;
//                }else if(FeatureFilters.featureFilterNERZERO(tokenFeature)==1){
//                    Pair p = parseFeature(tokenFeature);
//                    nerFeatures[index] = (String) p.right;
//                    if(!tokenToEntityType.containsKey(p.left)){
//                        String t = (String) p.left;
//                        tokenToEntityType.put(t.toLowerCase(), (String) p.right);
//                    }
//                }
//            }
//
//            if(nerFeatures[index].equals(targetEntityType))
//                truthArray[index] = true;
//            else
//                truthArray[index] = false;
//
//            if(posTagFeatures[index]==null) posTagFeatures[index] = PartOfSpeech.DETERMINER.toString();
//            if(nerFeatures[index]==null) nerFeatures[index] = "O";
//            index++;
//        }
//    }



    String getPosTagFeatures(){
        PartOfSpeech partOfSpeech[] = PartOfSpeech.values();
        int count[] = new int[partOfSpeech.length];
        for(String posTag: posTagFeatures){
            for(int i=0; i<partOfSpeech.length; i++){
                if(posTag.toString().equals(partOfSpeech[i].toString())){
                    count[i]++;
                }
            }
        }
        String posTagFeaturesString = "";
        for(int i: count){
            posTagFeaturesString += String.valueOf(i) + ",";
        }
        return posTagFeaturesString;
    }

    String getNERFeatures(){
        NER ner[] = NER.values();
        int count[] = new int[ner.length];
        for(String nerTag: nerFeatures){
            for(int i=0; i<ner.length; i++){
                if(nerTag.toString().equals(ner[i].toString())){
                    count[i]++;
                }
            }
        }
        String nerFeaturesString = "";
        for(int i: count){
            nerFeaturesString += String.valueOf(i) + ",";
        }
        return nerFeaturesString;
    }

    public List<Double> getTokenWeightList(String targetEntityType){
        List<Double> tokenWeightList = new ArrayList<>();
        for(int i=0; i < nerFeatures.length; i++){
            if(nerFeatures[i].equals(targetEntityType)){
                tokenWeightList.add(2d);
            }else{
                tokenWeightList.add(1d);
            }
        }
        return tokenWeightList;
    }

    public String getTargetEntityType(){
        Map<String, Integer> hashMap = new HashMap<>();
        for(int i=0; i<truthArray.length; i++){
            if(truthArray[i]){
                String key = nerFeatures[i];
                //System.out.println(key);
                if(hashMap.containsKey(key)) {
                    hashMap.put(key, hashMap.get(key) + 1);
                }else{
                    hashMap.put(key, 1);
                }
            }
        }
        return Collections.max(hashMap.entrySet(), (entry1, entry2) -> entry1.getValue() - entry2.getValue()).getKey();
    }

    public Set<List<String>> getTargetEntityList(){
        Set<List<String>> foundSpans = new HashSet<>();
        List<String> l = new ArrayList<>();
        for(int i=0; i<truthArray.length; i++){
            if(truthArray[i]==true){
                if((i==0) || (truthArray[i-1]==false)) {
                    l = new ArrayList<>();
                    l.add(tokenList.get(i).lemma.trim());
                }else{
                    l.add(tokenList.get(i).lemma.trim());
                }
            }else{
                if(l.size()>0)
                    foundSpans.add(l);
            }
        }
        return foundSpans;
    }

    public Set<List<String>> getTargetEntityListToLowerCase(){
        Set<List<String>> foundSpans = new HashSet<>();
        List<String> l = new ArrayList<>();
        for(int i=0; i<truthArray.length; i++){
            if(truthArray[i]==true){
                if((i==0) || (truthArray[i-1]==false)) {
                    l = new ArrayList<>();
                    l.add(tokenList.get(i).lemma.toLowerCase().trim());
                }else{
                    l.add(tokenList.get(i).lemma.toLowerCase().trim());
                }
            }else{
                if(l.size()>0)
                    foundSpans.add(l);
            }
        }
        return foundSpans;
    }
    //From a list of tokens form several (entity, context) pairs
    public List<TrainingData> getTargetEntityListToLowerCaseContextPairs(){
        List<TrainingData> trainingDataList = new ArrayList<>();
        List<Boolean> list=new ArrayList<Boolean>(Arrays.asList(new Boolean[truthArray.length]));
        //This list is for picking up tokens that does not belong to the entity
        Collections.fill(list, Boolean.FALSE);
        TrainingData trainingData = new TrainingData();
        for(int i=0; i<truthArray.length; i++){
            if(truthArray[i]==true){
                //trutharray checks if we have got a person, org or misc
                if((i==0) || (truthArray[i-1]==false)) { //If a new list entity has begun
                    //instantiating a training data that contains entity, context and truth value
                    //if a single token in an entity is true then the entity is true
                    trainingData.entity.add(tokenList.get(i).lemma.trim());
                    if(tokenList.get(i).truthLabel.equals("LIST"))
                        trainingData.truthValue = "1";
                    list.set(i, Boolean.TRUE);
                }else{ //continuing the existing entity
                    trainingData.entity.add(tokenList.get(i).lemma.trim());
                    if(tokenList.get(i).truthLabel.equals("LIST"))
                        trainingData.truthValue = "1";
                    list.set(i, Boolean.TRUE);
                }
            }else{
                if (trainingData.entity.size() > 0) {
                    for (int index=0; index < list.size(); index++) {
                        if (list.get(index) == Boolean.FALSE)
                            trainingData.context.add(tokenList.get(index).lemma.trim());
                    }
                    trainingDataList.add(trainingData);
                    trainingData = new TrainingData();
                    Collections.fill(list, Boolean.FALSE);
                }
            }
        }

        if(trainingDataList.size()==0){
            trainingData = new TrainingData();
            int flag = 0;
            for (int index=0; index < list.size(); index++) {
                trainingData.context.add(tokenList.get(index).lemma.trim());
                trainingData.entity.add(tokenList.get(index).lemma.trim());
                if(tokenList.get(index).truthLabel.equals("LIST"))
                    flag = 1;
            }

            if(flag==1)
                trainingData.truthValue = "1";
            else
                trainingData.truthValue = "0";

            trainingDataList.add(trainingData);
        }

        return trainingDataList;
    }



    public String posNERFeatures(){
        return getPosTagFeatures() + getNERFeatures();
    }

    public static Pair<String,String> parseFeature(String token){
        String parts[] = token.split("=");
        if(parts.length!=2)
            throw new IllegalArgumentException("String has got an extra equal");
        else
            return Pair.of(parts[0],parts[1]);
    }

    public static void main(String args[]){
        System.out.println(PartOfSpeech.get("JJ"));
        System.out.println(PartOfSpeech.DETERMINER.toString());
    }
}



