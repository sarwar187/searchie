DIR=/home/sarwar/crfsuite
EXPERIMENT=ErrorExperiment
cd ../..
mvn clean install
cd target/
#COMMAND="java -Xms1024m -Xmx4096m -cp starter-0.1-SNAPSHOT.jar interaction_experiment_old.RFWExperiment --dir ${DIR} --file train_list.crfsuite --fdir ${DIR}features/ --ffile dump_list.out --crfsuite ${DIR}bin/crfsuite --class LIST > ${DIR}/results/res.out"
#echo ${COMMAND}

java -Xms1024m -Xmx4096m -cp starter-0.1-SNAPSHOT.jar edu.umass.cs.ciir.searchie.starter.error_experiment.${EXPERIMENT} --dir=${DIR}/ --file=train.crfsuite --fdir=${DIR}/features/ --ffile=dump_per --crfsuite=${DIR}/bin/crfsuite --class=PER > ${DIR}/results/conll/PER.out

java -Xms1024m -Xmx4096m -cp starter-0.1-SNAPSHOT.jar edu.umass.cs.ciir.searchie.starter.error_experiment.${EXPERIMENT} --dir=${DIR}/ --file=train.crfsuite --fdir=${DIR}/features/ --ffile=dump_loc --crfsuite=${DIR}/bin/crfsuite --class=LOC > ${DIR}/results/conll/LOC.out

java -Xms1024m -Xmx4096m -cp starter-0.1-SNAPSHOT.jar edu.umass.cs.ciir.searchie.starter.error_experiment.${EXPERIMENT} --dir=${DIR}/ --file=train.crfsuite --fdir=${DIR}/features/ --ffile=dump_org --crfsuite=${DIR}/bin/crfsuite --class=ORG > ${DIR}/results/conll/ORG.out


