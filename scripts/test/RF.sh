DIR=/home/sarwar/crfsuite
cd ..
mvn clean install
cd target/
#COMMAND="java -Xms1024m -Xmx4096m -cp starter-0.1-SNAPSHOT.jar interaction_experiment_old.RFWExperiment --dir ${DIR} --file train_list.crfsuite --fdir ${DIR}features/ --ffile dump_list.out --crfsuite ${DIR}bin/crfsuite --class LIST > ${DIR}/results/res.out"
#echo ${COMMAND}

#java -Xms1024m -Xmx4096m -cp starter-0.1-SNAPSHOT.jar edu.umass.cs.ciir.searchie.starter.interaction_experiment.RFBaselineExperiment --dir=${DIR}/ --file=train_list.crfsuite --fdir=${DIR}/features/ --ffile=dump_list --crfsuite=${DIR}/bin/crfsuite --class=LIST > ${DIR}/results/res_baseline.out

#java -Xms1024m -Xmx4096m -cp starter-0.1-SNAPSHOT.jar edu.umass.cs.ciir.searchie.starter.interaction_experiment.RFWUExperiment --dir=${DIR}/ --file=train_list.crfsuite --fdir=${DIR}/features/ --ffile=dump_list --crfsuite=${DIR}/bin/crfsuite --class=LIST > ${DIR}/results/res_rfwu.out

java -Xms1024m -Xmx4096m -cp starter-0.1-SNAPSHOT.jar edu.umass.cs.ciir.searchie.starter.interaction_experiment.AllRFWUCombExperiment --dir=${DIR}/ --file=train_list.crfsuite --fdir=${DIR}/features/ --ffile=dump_list --crfsuite=${DIR}/bin/crfsuite --class=LIST > ${DIR}/results/res_rfwu_optimal_0.8.out
